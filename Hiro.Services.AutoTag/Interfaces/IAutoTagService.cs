﻿namespace Hiro.Services.Interfaces
{
    using Hiro.Domain;

    public interface IAutoTagService
    {
        string GetAutoTags(Order order);

        string GetAutoTags(Product product);
    }
}
