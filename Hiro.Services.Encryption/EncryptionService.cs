﻿namespace Hiro.Services
{
    using Hiro.Domain.Enums;
    using Hiro.Services.Interfaces;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class EncryptionService : IEncryptionService
    {
        private readonly IConfiguration configuration;

        public EncryptionService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public virtual string Decrypt(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            var password = this.GetSha1();
            var result = string.Empty;
            var algorithmSim = Rijndael.Create();
            var sdata = Convert.FromBase64String(text);
            var sMemory = new MemoryStream(sdata, 0, sdata.Length);
            var salt = Encoding.Unicode.GetBytes(password);
            var secretKey = new PasswordDeriveBytes(password, salt, "SHA1", 2);
            algorithmSim.Mode = CipherMode.CBC;
            const CryptoEnums.KeySize keySize = CryptoEnums.KeySize.Bits_256;
            var keyBytes = secretKey.GetBytes((int)keySize / 8);
            var vi = secretKey.GetBytes(16);
            var encryptor = algorithmSim.CreateDecryptor(keyBytes, vi);
            var cStream = new CryptoStream(sMemory, encryptor, CryptoStreamMode.Read);

            try
            {
                var sr = new StreamReader(cStream);
                result = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
            }
            finally
            {
                cStream.Close();
                sMemory.Close();
            }

            return result;
        }

        public virtual string Encrypt(string text)
        {
            var password = this.GetSha1();
            var result = string.Empty;
            var algorithmSim = Rijndael.Create();
            var sdata = Encoding.ASCII.GetBytes(text);
            var sMemory = new MemoryStream();
            var salt = Encoding.Unicode.GetBytes(password);
            var secretKey = new PasswordDeriveBytes(password, salt, "SHA1", 2);
            const CryptoEnums.KeySize keySize = CryptoEnums.KeySize.Bits_256;
            var keyBytes = secretKey.GetBytes((int)keySize / 8);
            algorithmSim.Mode = CipherMode.CBC;
            var vi = secretKey.GetBytes(16);
            var encryptor = algorithmSim.CreateEncryptor(keyBytes, vi);
            var cStream = new CryptoStream(sMemory, encryptor, CryptoStreamMode.Write);

            try
            {
                cStream.Write(sdata, 0, sdata.Length);
                cStream.FlushFinalBlock();
                var cryptoByte = sMemory.ToArray();

                result = Convert.ToBase64String(cryptoByte, 0, cryptoByte.GetLength(0));
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
            }
            finally
            {
                cStream.Close();
                sMemory.Close();
            }

            return result;
        }

        private string GetSha1()
        {
            var stringBuilder = new StringBuilder();
            var cypherKey = this.configuration["CypherKey"];
            var stream = SHA1.Create().ComputeHash(new ASCIIEncoding().GetBytes(cypherKey));

            foreach (var t in stream)
            {
                stringBuilder.AppendFormat("{0:x2}", t);
            }

            return stringBuilder.ToString();
        }
    }
}
