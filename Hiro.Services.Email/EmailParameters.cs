﻿namespace Hiro.Services.Email
{
    using System.Collections.Generic;
    using Hiro.Domain;

    public class EmailParameters
    {
        public Country LocalizationCountry { get; set; }

        public string To { get; set; }

        public Dictionary<string, string> Substitutions { get; set; } = new Dictionary<string, string>();
    }
}