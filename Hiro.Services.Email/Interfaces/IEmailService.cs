﻿namespace Hiro.Services.Email.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IEmailService
    {
        Task SendSingleEmailAsync(string emailName, EmailParameters emailParameters);

        Task SendMultipleEmailsAsync(string emailName, List<EmailParameters> emailParametersList);

        void AddSentEmail(string emailName, string to, string from = null, string reference = null, Guid? orderId = null, Guid? couponId = null, Guid? cartId = null);
    }
}