﻿namespace Hiro.Services.Email
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Threading.Tasks;
    using Hiro.DataAccess;
    using Hiro.Domain;
    using Hiro.Domain.Extensions;
    using Hiro.Services.Email.Interfaces;
    using Hiro.Services.Interfaces;
    using Hiro.Shared.Extensions;
    using Microsoft.AspNetCore.Mvc;

    public class EmailService : IEmailService
    {
        private readonly HiroDbContext dbContext;
        private readonly IEncryptionService encryptionService;

        public EmailService(
            HiroDbContext dbContext,
            IEncryptionService encryptionService)
        {
            this.dbContext = dbContext;
            this.encryptionService = encryptionService;
        }

        public Task SendSingleEmailAsync(string emailName, EmailParameters emailParameters)
        {
            var email = this.dbContext.Emails.First(x => x.Name == emailName);

            var content = email.ContentSection.GetLocalizedContent(emailParameters.LocalizationCountry);

            var templateSubject = content.ParseJson().Subject as string;
            var templateBody = new EmailsController().RenderViewAsync(email.ViewName, emailParameters.LocalizationCountry.LanguageCode, content).Result;

            var emailSubject = this.PerformSubstitutions(templateSubject, emailParameters.Substitutions);
            var emailBody = this.PerformSubstitutions(templateBody, emailParameters.Substitutions);

            using (var message = new MailMessage())
            {
                var cmsSettings = this.dbContext.CmsSettings.First();

                var from = email.GetLocalizedFrom(emailParameters.LocalizationCountry).IfNullOrEmptyConvertTo(cmsSettings.SmtpEmail);
                var displayName = email.GetLocalizedDisplayName(emailParameters.LocalizationCountry).IfNullOrEmptyConvertTo(cmsSettings.SmtpDisplayName);
                var replyTo = email.GetLocalizedReplyTo(emailParameters.LocalizationCountry);

                message.To.Add(emailParameters.To);
                message.From = new MailAddress(from, displayName);
                message.Subject = emailSubject;
                message.Body = emailBody;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;

                if (!string.IsNullOrEmpty(replyTo))
                {
                    message.ReplyToList.Add(replyTo);
                }

                //if (attachment != null)
                //{
                //    message.Attachments.Add(new Attachment(attachment, MediaTypeNames.Application.Octet));
                //}

                using var smtpClient = new SmtpClient
                {
                    Host = cmsSettings.SmtpHost ?? string.Empty,
                    Port = cmsSettings.SmtpPort,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(cmsSettings.SmtpEmail, this.encryptionService.Decrypt(cmsSettings.SmtpPassword))
                };

                smtpClient.SendAsync(message, null);

                return Task.CompletedTask;
            }
        }

        public Task SendMultipleEmailsAsync(string emailName, List<EmailParameters> emailParametersList)
        {
            foreach (var emailParameters in emailParametersList)
            {
                this.SendSingleEmailAsync(emailName, emailParameters);
            }

            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public virtual void AddSentEmail(string emailName, string to, string @from = null, string reference = null, Guid? orderId = null, Guid? couponId = null, Guid? cartId = null)
        {
            this.dbContext.SentEmails.Add(new SentEmail
            {
                To = to,
                From = from,
                Reference = reference,
                Email = this.dbContext.Emails.FirstOrDefault(x => x.Name.Equals(emailName)),
                Order = orderId == null ? null : this.dbContext.Orders.Find(orderId),
                Coupon = couponId == null ? null : this.dbContext.Coupons.Find(couponId),
                Cart = cartId == null ? null : this.dbContext.Carts.Find(cartId)
            });

            this.dbContext.SaveChanges();
        }

        private string PerformSubstitutions(string text, Dictionary<string, string> substitutions)
        {
            if (substitutions == null)
            {
                return text;
            }

            foreach (var substitution in substitutions)
            {
                text = text.Replace(substitution.Key, substitution.Value);
            }

            return text;
        }

        protected class EmailsController : Controller
        {
        }
    }
}
