﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiro.Braintree.Services.Options
{
    public class BraintreeOptions
    {
        public string Environment { get; set; }
        public string MerchantId { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
        public string AccessToken { get; set; }
        public string ClientId { get; set; }
    }
}
