﻿using System;
using System.Collections.Generic;
using Braintree;
using Hiro.Domain;
using Hiro.Shared.Dtos;

namespace Hiro.Braintree.Services.Interfaces
{
    public interface IBraintreeService
    {
        BraintreeGateway GetGateway();
        Customer AddOrUpdateCustomer(User user, Dictionary<string, string> customFields = null);
        Customer FindCustomer(string userId);
        Transaction FindOrderTransaction(Order order);
        string GetMerchantAccountId(Country localizationCountry);
        OperationResult VoidOrRefundOrderTransaction(Order order);
        string GenerateClientToken(Country localizationCountry, Guid userId);
    }
}