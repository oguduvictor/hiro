﻿using System;
using System.Collections.Generic;
using Braintree;
using Hiro.Braintree.Services.Interfaces;
using Hiro.Braintree.Services.Options;
using Hiro.Domain;
using Hiro.Shared.Dtos;
using JetBrains.Annotations;
using Microsoft.Extensions.Options;

namespace Hiro.Braintree.Services
{
    public class BraintreeService : IBraintreeService
    {
        private readonly BraintreeGateway braintreeGateway;

        public BraintreeService(IOptionsMonitor<BraintreeOptions> optionsAccessor)
        {
            var braintreeOptions = optionsAccessor.CurrentValue;

            this.braintreeGateway = new BraintreeGateway(
                braintreeOptions.Environment,
                braintreeOptions.MerchantId,
                braintreeOptions.PublicKey,
                braintreeOptions.PrivateKey
            );
        }

        public BraintreeGateway GetGateway()
        {
            return this.braintreeGateway;
        }

        public string GenerateClientToken(Country localizationCountry, Guid userId)
        {
            try
            {
                return this.braintreeGateway.ClientToken.Generate(new ClientTokenRequest
                {
                    CustomerId = userId.ToString(),
                    MerchantAccountId = this.GetMerchantAccountId(localizationCountry)
                });
            }
            catch
            {
                return this.braintreeGateway.ClientToken.Generate(new ClientTokenRequest
                {
                    MerchantAccountId = this.GetMerchantAccountId(localizationCountry)
                });
            }
        }

        /// <summary>
        /// Return the MerchantAccountId for the current country using the We.config settings.
        /// </summary>
        public string GetMerchantAccountId(Country localizationCountry)
        {
            var merchantAccountId = localizationCountry.PaymentAccountId;

            if (string.IsNullOrEmpty(merchantAccountId))
            {
                throw new NullReferenceException("Null or empty 'PaymentAccountId' found in Country settings");
            }
            return merchantAccountId;
        }

        /// <summary>
        /// Find a Braintree Customer by Id.
        /// If no Customer is found, return null.
        /// </summary>
        [CanBeNull]
        public Customer FindCustomer(string userId)
        {
            try
            {
                return this.braintreeGateway.Customer.Find(userId);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Add or Update a Braintree Customer based on the properties of a User.
        /// Id, Email, First and Last Name are automatically included,
        /// to add more fields use the optional parameter "customFields".
        /// </summary>
        [NotNull]
        public Customer AddOrUpdateCustomer(User user, Dictionary<string, string> customFields = null)
        {
            var customerRequest = new CustomerRequest
            {
                Id = user.Id.ToString(),
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CustomFields = customFields
            };

            Customer customer;

            try
            {
                var updateCustomerResult = this.braintreeGateway.Customer.Update(user.Id.ToString(), customerRequest);
                customer = updateCustomerResult.Target;
            }
            catch
            {
                var createCustomerResult = this.braintreeGateway.Customer.Create(customerRequest);
                customer = createCustomerResult.Target;
            }

            return customer;
        }

        /// <summary>
        /// Find the Braintree Transaction associated to the Order.
        /// If the Transaction is not found, return null.
        /// </summary>
        [CanBeNull]
        public Transaction FindOrderTransaction(Order order)
        {
            try
            {
                return this.braintreeGateway.Transaction.Find(order.TransactionId);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Void or Refund and Order Transaction according to its Status.
        /// If the Transaction has already been settled, a Refund will be procesed.
        /// If the Transaction is settling, a Void operation will be processed.
        /// </summary>
        [CanBeNull]
        public OperationResult VoidOrRefundOrderTransaction(Order order)
        {
            var operationResult = new OperationResult();

            var transaction = this.FindOrderTransaction(order);

            if (transaction == null)
            {
                operationResult.Errors.Add(new OperationError("Transaction not found"));

                return operationResult;
            }

            Result<Transaction> result;

            if (transaction.Status == TransactionStatus.SETTLED || transaction.Status == TransactionStatus.SETTLING)
            {
                result = this.braintreeGateway.Transaction.Refund(order.TransactionId);
            }
            else
            {
                result = this.braintreeGateway.Transaction.Void(order.TransactionId);
            }

            if (!result.IsSuccess())
            {
                operationResult.Errors.Add(new OperationError(result.Message));
            }

            return operationResult;
        }
    }
}
