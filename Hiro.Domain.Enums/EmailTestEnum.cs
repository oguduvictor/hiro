﻿#pragma warning disable 1591

namespace Hiro.Domain.Enums
{
    using System.ComponentModel;

    public enum EmailTestEnum
    {
        [Description("All")]
        All = 0,
        [Description("Registration")]
        Registration = 1,
        [Description("Password Recovery")]
        PasswordRecovery = 2,
        [Description("Contact Form")]
        ContactForm = 3
    }
}
