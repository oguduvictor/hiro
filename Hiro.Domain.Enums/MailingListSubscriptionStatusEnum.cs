﻿namespace Hiro.Domain.Enums
{
    using System.ComponentModel;

#pragma warning disable 1591
    public enum MailingListSubscriptionStatusEnum
    {
        [Description("Subscribed")]
        Subscribed = 0,

        [Description("Unsubscribed")]
        Unsubscribed = 5,

        [Description("Invalid")]
        Invalid = 10
    }
}
