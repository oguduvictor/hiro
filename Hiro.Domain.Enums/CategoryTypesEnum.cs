﻿#pragma warning disable 1591

namespace Hiro.Domain.Enums
{
    using System.ComponentModel;

    public enum CategoryTypesEnum
    {
        [Description("Products")]
        Products = 0,
        [Description("Product Image Kits")]
        ProductImageKits = 1,
        [Description("Product Stock Units")]
        ProductStockUnits = 2
    }
}