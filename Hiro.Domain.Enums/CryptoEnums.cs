﻿#pragma warning disable 1591
namespace Hiro.Domain.Enums
{
    public class CryptoEnums
    {
        public enum SymProviders
        {
            Rijndael, DES, TripleDES, RC2
        }

        public enum HashProvide
        {
            MD5, SHA1
        }

        public enum KeySize : int
        {
            Bits_64 = 64,
            Bits_128 = 128,
            Bits_192 = 192,
            Bits_256 = 256
        }

        public enum Action { Encrypt, Decrypt }
    }
}