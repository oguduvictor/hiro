﻿namespace Hiro.Services
{
    using System;
    using System.Linq;
    using Hiro.DataAccess;
    using Hiro.DataAccess.Extensions;
    using Hiro.Domain;
    using Hiro.Services.Interfaces;
    using JetBrains.Annotations;

    public class WishListService : IWishListService
    {
        private readonly HiroDbContext dbContext;

        public WishListService(HiroDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <inheritdoc />
        public virtual WishList CreateWishList([NotNull] User user)
        {
            if (user == null)
            {
                throw new NullReferenceException();
            }

            var wishList = new WishList
            {
                User = user
            };

            this.dbContext.WishLists.Add(wishList);

            this.dbContext.SaveChanges();

            return wishList;
        }

        /// <inheritdoc />
        public virtual CartItem AddToWishList([NotNull] WishList wishList, [NotNull] CartItem cartItem)
        {
            var product = this.dbContext.Products.Find(cartItem.Product?.Id);

            cartItem.Product = product ?? throw new NullReferenceException();

            foreach (var cartItemVariant in cartItem.CartItemVariants.ToList())
            {
                cartItemVariant.CartItem = cartItem;
                cartItemVariant.Variant = this.dbContext.Variants.Find(cartItemVariant.Variant?.Id);
                cartItemVariant.VariantOptionValue = this.dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id);
            }

            cartItem.CartShippingBox = null;
            cartItem.WishList = wishList;

            this.dbContext.CartItems.Add(cartItem);

            this.dbContext.SaveChanges();

            return cartItem;
        }

        /// <inheritdoc />
        public virtual void RemoveFromWishList(Guid cartItemId)
        {
            this.dbContext.CartItems.Delete(cartItemId, true);
        }
    }
}