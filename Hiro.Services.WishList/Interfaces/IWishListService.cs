﻿namespace Hiro.Services.Interfaces
{
    using System;
    using Hiro.Domain;
    using JetBrains.Annotations;

    public interface IWishListService
    {
        WishList CreateWishList([NotNull] User user);

        CartItem AddToWishList([NotNull] WishList wishList, [NotNull] CartItem cartItem);

        void RemoveFromWishList(Guid cartItemId);
    }
}
