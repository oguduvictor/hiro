﻿namespace Hiro.Services.Schemas
{
    using Hiro.Shared.Attributes;
    using JetBrains.Annotations;

    [Schema(Title = "Use this ContentSection customize the messages returned by the Cart.GetErrors() method")]
    public class Hiro_CartErrors
    {
        [CanBeNull]
        [SchemaProperty(
            Title = "CartItem With Disabled Product",
            Description = "Use the tag [[Product]] to insert the name of the CartItem Product")]
        public string CartItemWithDisabledProduct { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "CartItem With Out Of Stock Product",
            Description = "Use the tag [[Product]] to insert the name of the CartItem Product")]
        public string CartItemWithOutOfStockProduct { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "CartItem With Invalid ShippingBox",
            Description = "Use the tag [[Product]] to insert the name of the CartItem Product")]
        public string CartItemWithInvalidShippingBox { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "Invalid Coupon",
            Description = "")]
        public string InvalidCoupon { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "Invalid User Credit",
            Description = "")]
        public string InvalidUserCredit { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "Invalid Tax Override",
            Description = "")]
        public string InvalidTaxOverride { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "Localized Shipping Address Country Not Matching Current Country",
            Description = "")]
        public string LocalizedShippingAddressCountryNotMatchingCurrentCountry { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "Non Localized Shipping Address Country Not Matching Default Country",
            Description = "")]
        public string NonLocalizedShippingAddressCountryNotMatchingDefaultCountry { get; set; }

        [CanBeNull]
        [SchemaProperty(
            Title = "Shipping Address Required",
            Description = "")]
        public string ShippingAddressRequired { get; set; }
    }
}
