﻿namespace Hiro.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.DataAccess;
    using Hiro.DataAccess.Extensions;
    using Hiro.Domain;
    using Hiro.Domain.Enums;
    using Hiro.Domain.Extensions;
    using Hiro.Services.Interfaces;
    using Hiro.Services.Schemas;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Constants;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Extensions;
    using JetBrains.Annotations;

    /// <inheritdoc />
    public class CartService : ICartService
    {
        private readonly HiroDbContext dbContext;
        private readonly ILogService logService;
        private readonly IProductService productService;
        private readonly IContentSectionService contentSectionService;
        private readonly IAddOrUpdateAddressValidator addOrUpdateAddressValidator;
        private readonly IApplyCouponToCartValidator applyCouponToCartValidator;
        private readonly IAutoTagService autoTagService;

        public CartService(
            HiroDbContext dbContext,
            ILogService logService,
            IProductService productService,
            IContentSectionService contentSectionService,
            IAddOrUpdateAddressValidator addOrUpdateAddressValidator,
            IApplyCouponToCartValidator applyCouponToCartValidator,
            IAutoTagService autoTagService)
        {
            this.dbContext = dbContext;
            this.logService = logService;
            this.productService = productService;
            this.contentSectionService = contentSectionService;
            this.addOrUpdateAddressValidator = addOrUpdateAddressValidator;
            this.applyCouponToCartValidator = applyCouponToCartValidator;
            this.autoTagService = autoTagService;
        }

        /// <inheritdoc />
        public virtual bool AddUserCredit(Country localizationCountry, User user, double amount, string reference = null)
        {
            var userLocalizedKit = user.UserLocalizedKits.GetByCountry(localizationCountry);

            if (userLocalizedKit == null)
            {
                userLocalizedKit = new UserLocalizedKit
                {
                    Country = localizationCountry,
                    User = user
                };

                this.dbContext.UserLocalizedKits.Add(userLocalizedKit);
            }

            var userCredit = new UserCredit
            {
                UserLocalizedKit = userLocalizedKit,
                Amount = amount,
                Reference = reference
            };

            this.dbContext.UserCredits.Add(userCredit);

            this.dbContext.SaveChanges();

            return true;
        }

        /// <inheritdoc />
        public virtual OperationResult<Address> AddOrUpdateAddress(Cart cart, Address address, bool setUser = false, bool setAsCartShippingAddress = false, bool setAsCartBillingAddress = false)
        {
            if (cart == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cart)}' cannot be null.");
            }

            var operationResult = new OperationResult<Address>();

            var errors = this.addOrUpdateAddressValidator.Validate(address);

            if (errors.Any())
            {
                operationResult.Errors.AddRange(errors);

                return operationResult;
            }
            else
            {
                var dbAddress = this.dbContext.Addresses.Find(address.Id);

                if (dbAddress == null)
                {
                    address.User = setUser ? cart.User : null;
                    address.Country = this.dbContext.Countries.Find(address.Country?.Id);

                    this.dbContext.Addresses.Add(address);

                    operationResult.Data = address;
                }
                else
                {
                    dbAddress.User = setUser ? cart.User : null;
                    dbAddress.Country = this.dbContext.Countries.Find(address.Country?.Id);
                    dbAddress.AddressLine1 = address.AddressLine1;
                    dbAddress.AddressLine2 = address.AddressLine2;
                    dbAddress.City = address.City;
                    dbAddress.FirstName = address.FirstName;
                    dbAddress.LastName = address.LastName;
                    dbAddress.PhoneNumber = address.PhoneNumber;
                    dbAddress.Postcode = address.Postcode;
                    dbAddress.StateCountyProvince = address.StateCountyProvince;

                    operationResult.Data = dbAddress;
                }

                this.dbContext.SaveChanges();

                if (setAsCartShippingAddress || setAsCartBillingAddress)
                {
                    if (setAsCartShippingAddress)
                    {
                        var setCartShippingAddressOperationResult = this.SetCartShippingAddress(cart, address.Id);

                        operationResult.Errors.AddRange(setCartShippingAddressOperationResult.Errors);
                    }

                    if (setAsCartBillingAddress)
                    {
                        var setCartBillingAddressOperationResult = this.SetCartBillingAddress(cart, address.Id);

                        operationResult.Errors.AddRange(setCartBillingAddressOperationResult.Errors);
                    }
                }
            }

            return operationResult;
        }

        /// <inheritdoc />
        public virtual OperationResult SetCartShippingAddress(Cart cart, Guid? addressId)
        {
            if (cart == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cart)}' cannot be null.");
            }

            if (addressId == null)
            {
                if (cart.ShippingAddress != null)
                {
                    cart.ShippingAddress = null;

                    this.dbContext.SaveChanges();
                }
            }
            else
            {
                cart.ShippingAddress = this.dbContext.Addresses.Find(addressId);

                this.dbContext.SaveChanges();
            }

            return new OperationResult();
        }

        /// <inheritdoc />
        public virtual OperationResult SetCartBillingAddress(Cart cart, Guid? addressId)
        {
            if (cart == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cart)}' cannot be null.");
            }

            if (addressId == null)
            {
                if (cart.BillingAddress != null)
                {
                    cart.BillingAddress = null;

                    this.dbContext.SaveChanges();
                }
            }
            else
            {
                cart.BillingAddress = this.dbContext.Addresses.Find(addressId);

                this.dbContext.SaveChanges();
            }

            return new OperationResult();
        }

        /// <inheritdoc />
        public virtual CartItem GetCartItemFromProduct([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, Product product, List<VariantOption> selectedVariantOptions = null)
        {
            var shippingBox = this.productService.GetShippingBoxesForShippingCountry(localizationCountry, shippingCountry, product).FirstOrDefault();

            if (shippingBox == null)
            {
                this.logService.LogAsync(
                    LogTypesEnum.Content,
                    $"No ShippingBox found for the product {product.GetLocalizedTitle(localizationCountry)} for Country Url '{localizationCountry}'.");
            }

            var cartItem = new CartItem
            {
                Product = product,
                CartShippingBox = new CartShippingBox
                {
                    ShippingBox = shippingBox ?? product.ShippingBoxes.OrderBySortOrder().FirstOrDefault()
                }
            };

            foreach (var productVariant in product.ProductVariants)
            {
                var cartItemVariant = new CartItemVariant
                {
                    CartItem = cartItem,
                    Variant = productVariant.Variant,
                    BooleanValue = productVariant.DefaultBooleanValue,
                    DoubleValue = productVariant.DefaultDoubleValue,
                    IntegerValue = productVariant.DefaultIntegerValue,
                    StringValue = productVariant.DefaultStringValue,
                    JsonValue = productVariant.DefaultJsonValue,
                };

                var variantOptionValue = selectedVariantOptions?.FirstOrDefault(x => x.Variant?.Id == productVariant.Variant?.Id);

                if (variantOptionValue == null || variantOptionValue.IsDisabled || variantOptionValue.GetLocalizedIsDisabled(localizationCountry))
                {
                    variantOptionValue = productVariant.VariantOptions
                        .OrderBySortOrder()
                        .FirstOrDefault(x => !x.IsDisabled && !x.GetLocalizedIsDisabled(localizationCountry));
                }

                cartItemVariant.VariantOptionValue = variantOptionValue;

                cartItem.CartItemVariants.Add(cartItemVariant);
            }

            return cartItem;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual Cart CreateCart(User user)
        {
            if (user == null)
            {
                throw new NullReferenceException();
            }

            if (user.Cart != null)
            {
                return user.Cart;
            }

            var cart = new Cart
            {
                User = user
            };

            this.dbContext.Carts.Add(cart);

            this.dbContext.SaveChanges();

            return cart;
        }

        /// <inheritdoc />
        public virtual void DeleteCart(Guid? cartId = null)
        {
            this.dbContext.Carts.Delete(cartId, true);
        }

        /// <inheritdoc />
        public virtual CartShippingBox GetCartShippingBox([NotNull] Cart cart, Guid shippingBoxId)
        {
            return cart.CartShippingBoxes.FirstOrDefault(x => x.ShippingBox?.Id == shippingBoxId);
        }

        /// <inheritdoc />
        public virtual CartShippingBox CreateCartShippingBox([NotNull] Cart cart, Guid shippingBoxId)
        {
            if (cart == null)
            {
                throw new NullReferenceException();
            }

            // -------------------------------------------------------------------------------
            // Check if a CartShippingBox with the same ShippingBox.Id already exists
            // -------------------------------------------------------------------------------

            var existingCartShippingBox = this.GetCartShippingBox(cart, shippingBoxId);

            if (existingCartShippingBox != null)
            {
                return existingCartShippingBox;
            }

            // -------------------------------------------------------------------------------
            // Create the CartShippingBox
            // -------------------------------------------------------------------------------

            var shippingBox = this.dbContext.ShippingBoxes.Find(shippingBoxId);

            var cartShippingBox = new CartShippingBox
            {
                Cart = cart,
                ShippingBox = shippingBox ?? throw new NullReferenceException("ShippingBox was null")
            };

            this.dbContext.CartShippingBoxes.Add(cartShippingBox);

            this.dbContext.SaveChanges();

            return cartShippingBox;
        }

        /// <inheritdoc />
        public virtual void SetShippingBox(Country localizationCountry, Cart cart, Guid cartItemId, string shippingBoxName)
        {
            var cartItem = this.dbContext.CartItems.Find(cartItemId);

            if (cartItem == null)
            {
                return;
            }

            var shippingBox = this.dbContext.ShippingBoxes.FirstOrDefault(x => x.Name == shippingBoxName);

            if (shippingBox == null)
            {
                return;
            }

            // leave this here, it must be declared before cartItem is modified

            var oldCartShippingBox = cartItem.CartShippingBox;

            // ------------------------------------------------------------
            // Move CartItem
            // ------------------------------------------------------------

            var cartShippingBox = this.GetCartShippingBox(cart, shippingBox.Id) ?? this.CreateCartShippingBox(cart, shippingBox.Id);

            cartItem.CartShippingBox = cartShippingBox;

            this.dbContext.SaveChanges();

            // ------------------------------------------------------------
            // Delete old CartShippingBox if empty
            // ------------------------------------------------------------

            if (!oldCartShippingBox?.CartItems.Any() ?? false)
            {
                this.dbContext.CartShippingBoxes.Delete(oldCartShippingBox.Id, true);
            }
        }

        /// <inheritdoc />
        public virtual OperationResult<double> ApplyUserCredit(Country localizationCountry, Cart cart, double amount, string reference = null)
        {
            var operationResult = new OperationResult<double>();

            // ----------------------------------------------------------------------
            // Invalid amount
            // ----------------------------------------------------------------------

            if (amount.ApproximatelyEqualsTo(0))
            {
                var errorMessage = this.contentSectionService.GetOrSetLocalizedContent(
                    localizationCountry,
                    ContentSectionNames.Cms_UserCredit_InvalidAmountError,
                    ContentSectionNames.Cms_UserCredit_InvalidAmountError_FallbackValue);

                operationResult.Errors.Add(new OperationError(errorMessage));
            }

            // ----------------------------------------------------------------------
            // Insufficient Funds
            // ----------------------------------------------------------------------

            var creditsTotal = cart.User.GetLocalizedUserCredits(localizationCountry).Sum(x => x.Amount);

            if (amount > creditsTotal)
            {
                var errorMessage = this.contentSectionService.GetOrSetLocalizedContent(
                    localizationCountry,
                    ContentSectionNames.Cms_UserCredit_InsufficientFundsError,
                    ContentSectionNames.Cms_UserCredit_InsufficientFundsError_FallbackValue);

                operationResult.Errors.Add(new OperationError(errorMessage));
            }

            // ----------------------------------------------------------------------
            // Excessive amount
            // ----------------------------------------------------------------------

            if (amount > this.GetTotalAfterTax(localizationCountry, cart))
            {
                var errorMessage = this.contentSectionService.GetOrSetLocalizedContent(
                    localizationCountry,
                    ContentSectionNames.Cms_UserCredit_ExcessiveAmountError,
                    ContentSectionNames.Cms_UserCredit_ExcessiveAmountError_FallbackValue);

                operationResult.Errors.Add(new OperationError(errorMessage));
            }

            // ----------------------------------------------------------------------
            // Return errors
            // ----------------------------------------------------------------------

            if (operationResult.Errors.Any())
            {
                return operationResult;
            }

            // ----------------------------------------------------------------------
            // Apply credit
            // ----------------------------------------------------------------------

            if (cart.UserCredit != null)
            {
                this.RemoveCredit(localizationCountry, cart);
            }

            var userLocalizedKit = cart.User?.UserLocalizedKits.GetByCountry(localizationCountry);

            if (userLocalizedKit == null)
            {
                userLocalizedKit = new UserLocalizedKit
                {
                    User = cart.User,
                    Country = localizationCountry
                };

                this.dbContext.UserLocalizedKits.Add(userLocalizedKit);
            }

            var userCredit = new UserCredit
            {
                Amount = -amount,
                Reference = reference,
                UserLocalizedKit = userLocalizedKit
            };

            this.dbContext.UserCredits.Add(userCredit);

            cart.UserCredit = userCredit;

            this.dbContext.SaveChanges();

            operationResult.Data = creditsTotal + userCredit.Amount;

            return operationResult;
        }

        /// <inheritdoc />
        public virtual OperationResult<double> RemoveCredit(Country localizationCountry, [NotNull] Cart cart)
        {
            var operationResult = new OperationResult<double>();

            if (cart.UserCredit == null)
            {
                return operationResult;
            }

            this.dbContext.UserCredits.Delete(cart.UserCredit.Id, true);

            var creditsTotal = cart.User.GetLocalizedUserCredits(localizationCountry).Sum(x => x.Amount);

            operationResult.Data = creditsTotal;

            return operationResult;
        }

        /// <inheritdoc />
        public virtual OperationResult ApplyCoupon(Country localizationCountry, Cart cart, string couponCode)
        {
            var dbCoupon = this.dbContext.Coupons.FirstOrDefault(x => x.Code == couponCode);

            var errors = this.applyCouponToCartValidator.Validate(localizationCountry, dbCoupon);

            if (errors.Any())
            {
                return new OperationResult(errors);
            }

            if (dbCoupon == null)
            {
                throw new NullReferenceException();
            }

            dbCoupon.ValidityTimes -= 1;

            cart.Coupon = dbCoupon;

            this.dbContext.SaveChanges();

            return new OperationResult();
        }

        /// <inheritdoc />
        public virtual void RemoveCoupon(Country localizationCountry, [NotNull] Cart cart)
        {
            var coupon = cart.Coupon;

            if (coupon == null)
            {
                return;
            }

            coupon.ValidityTimes += 1;

            cart.Coupon = null;

            this.dbContext.SaveChanges();
        }

        /// <inheritdoc />
        [NotNull]
        public virtual Order CreateOrder(Country localizationCountry, [NotNull] Cart cart, string transactionId = null)
        {
            if (cart == null)
            {
                throw new NullReferenceException("Impossible to create the order: the Cart was null.");
            }

            var order = new Order
            {
                OrderNumber = (this.dbContext.Orders.Max(x => x.OrderNumber) ?? 0) + 1,
                CountryLanguageCode = localizationCountry.LanguageCode,
                CountryName = localizationCountry.Name,
                CurrencySymbol = localizationCountry.GetCurrencySymbol(),
                ISO4217CurrencySymbol = localizationCountry.GetIso4217CurrencySymbol(),
                TransactionId = transactionId,
                TotalBeforeTax = this.GetTotalBeforeTax(localizationCountry, cart),
                TotalAfterTax = this.GetTotalAfterTax(localizationCountry, cart),
                TotalToBePaid = this.GetTotalToBePaid(localizationCountry, cart),
                Status = OrderStatusEnum.Placed
            };

            // --------------------------------------------------------
            // COUPON
            // --------------------------------------------------------

            var coupon = cart.Coupon;

            if (coupon != null)
            {
                var couponAppliedValue = cart.GetCartItems().Sum(ci => this.GetCartItemCouponValue(localizationCountry, ci));

                if (coupon.Amount > couponAppliedValue)
                {
                    coupon.ValidityTimes += 1;
                    coupon.Amount -= couponAppliedValue;
                }

                order.OrderCoupon = new OrderCoupon
                {
                    Amount = this.GetCouponValue(localizationCountry, cart),
                    Code = coupon.Code
                };
            }

            // --------------------------------------------------------
            // CREDIT
            // --------------------------------------------------------

            if (cart.UserCredit != null)
            {
                order.OrderCredit = new OrderCredit
                {
                    Amount = cart.UserCredit.Amount,
                    Reference = cart.UserCredit.Reference
                };
            }

            // --------------------------------------------------------
            // USER
            // --------------------------------------------------------

            if (cart.User != null)
            {
                order.UserId = cart.User.Id;
                order.UserEmail = cart.User.Email;
                order.UserFirstName = cart.User.FirstName;
                order.UserLastName = cart.User.LastName;
            }

            // --------------------------------------------------------
            // SHIPPING ADDRESS
            // --------------------------------------------------------

            if (cart.ShippingAddress != null)
            {
                order.ShippingAddress = new OrderAddress
                {
                    AddressFirstName = cart.ShippingAddress.FirstName,
                    AddressLastName = cart.ShippingAddress.LastName,
                    AddressLine1 = cart.ShippingAddress.AddressLine1,
                    AddressLine2 = cart.ShippingAddress.AddressLine2,
                    City = cart.ShippingAddress.City,
                    Postcode = cart.ShippingAddress.Postcode,
                    StateCountyProvince = cart.ShippingAddress.StateCountyProvince,
                    CountryName = cart.ShippingAddress.Country?.Name,
                    PhoneNumber = cart.ShippingAddress.PhoneNumber
                };
            }

            // --------------------------------------------------------
            // BILLING ADDRESS
            // --------------------------------------------------------

            if (cart.BillingAddress != null && cart.BillingAddress.Id != cart.ShippingAddress?.Id)
            {
                order.BillingAddress = new OrderAddress
                {
                    AddressFirstName = cart.BillingAddress.FirstName,
                    AddressLastName = cart.BillingAddress.LastName,
                    AddressLine1 = cart.BillingAddress.AddressLine1,
                    AddressLine2 = cart.BillingAddress.AddressLine2,
                    City = cart.BillingAddress.City,
                    Postcode = cart.BillingAddress.Postcode,
                    StateCountyProvince = cart.BillingAddress.StateCountyProvince,
                    CountryName = cart.BillingAddress?.Country?.Name,
                    PhoneNumber = cart.BillingAddress.PhoneNumber
                };
            }

            // --------------------------------------------------------
            // SHIPPING BOXES
            // --------------------------------------------------------

            foreach (var cartShippingBox in cart.CartShippingBoxes.Where(x => x.CartItems.Any()))
            {
                var orderShippingBox = new OrderShippingBox
                {
                    Order = order,
                    Name = cartShippingBox.ShippingBox?.Name,
                    Title = cartShippingBox.ShippingBox?.GetLocalizedTitle(localizationCountry),
                    Description = cartShippingBox.ShippingBox?.GetLocalizedDescription(localizationCountry),
                    InternalDescription = cartShippingBox.ShippingBox?.GetLocalizedInternalDescription(localizationCountry),
                    MinDays = cartShippingBox.ShippingBox?.GetLocalizedMinDays(localizationCountry) ?? 0,
                    MaxDays = cartShippingBox.ShippingBox?.GetLocalizedMaxDays(localizationCountry) ?? 0,
                    CountWeekends = cartShippingBox.ShippingBox?.GetLocalizedCountWeekends(localizationCountry) ?? false,
                    ShippingPriceBeforeTax = this.GetShippingPriceBeforeTax(localizationCountry, cartShippingBox),
                    ShippingPriceAfterTax = this.GetShippingPriceAfterTax(localizationCountry, cartShippingBox),
                    Status = OrderShippingBoxStatusEnum.Preparing
                };

                // --------------------------------------------------------
                // ORDER ITEMS
                // --------------------------------------------------------

                foreach (var cartItem in cartShippingBox.CartItems)
                {
                    var product = cartItem.Product.AsNotNull();
                    var productStockUnit = cartItem.GetProductStockUnit().AsNotNull();

                    var orderItem = new OrderItem
                    {
                        OrderShippingBox = orderShippingBox,
                        ProductId = product.Id,
                        ProductName = product.Name,
                        ProductUrl = product.Url,
                        ProductCode = product.Code,
                        ProductQuantity = cartItem.Quantity,
                        ProductTitle = product.GetLocalizedTitle(localizationCountry),
                        ProductStockUnitId = productStockUnit.Id,
                        ProductStockUnitCode = productStockUnit.Code,
                        ProductStockUnitReleaseDate = productStockUnit.DispatchDate,
                        ProductStockUnitEnablePreorder = productStockUnit.EnablePreorder,
                        ProductStockUnitShipsIn = productStockUnit.DispatchTime,
                        ProductStockUnitBasePrice = productStockUnit.GetLocalizedBasePrice(localizationCountry),
                        ProductStockUnitSalePrice = productStockUnit.GetLocalizedSalePrice(localizationCountry),
                        ProductStockUnitMembershipSalePrice = productStockUnit.GetLocalizedMembershipSalePrice(localizationCountry),
                        Image = cartItem.Image,
                        Status = OrderItemStatusEnum.Default,
                        MinEta = cartItem.GetMinEta(localizationCountry),
                        MaxEta = cartItem.GetMaxEta(localizationCountry),
                        SubtotalBeforeTax = this.GetCartItemSubtotalBeforeTax(localizationCountry, cartItem),
                        SubtotalAfterTax = this.GetCartItemSubtotalAfterTax(localizationCountry, cartItem),
                        TotalBeforeTax = this.GetCartItemTotalBeforeTax(localizationCountry, cartItem),
                        TotalAfterTax = this.GetCartItemTotalAfterTax(localizationCountry, cartItem),
                    };

                    foreach (var cartItemVariant in cartItem.CartItemVariants)
                    {
                        var orderItemProductVariant = new OrderItemVariant
                        {
                            OrderItem = orderItem,

                            VariantName = cartItemVariant.Variant?.Name,
                            VariantUrl = cartItemVariant.Variant?.Url,
                            VariantType = cartItemVariant.Variant?.Type ?? VariantTypesEnum.Boolean,
                            VariantLocalizedTitle = cartItemVariant.Variant?.GetLocalizedTitle(localizationCountry),
                            VariantLocalizedDescription = cartItemVariant.Variant?.GetLocalizedDescription(localizationCountry),

                            VariantOptionName = cartItemVariant.VariantOptionValue?.Name,
                            VariantOptionUrl = cartItemVariant.VariantOptionValue?.Url,
                            VariantOptionCode = cartItemVariant.VariantOptionValue?.Code,
                            VariantOptionLocalizedTitle = cartItemVariant.VariantOptionValue?.GetLocalizedTitle(localizationCountry),
                            VariantOptionLocalizedDescription = cartItemVariant.VariantOptionValue?.GetLocalizedDescription(localizationCountry),
                            VariantOptionLocalizedPriceModifier = cartItemVariant.VariantOptionValue?.GetLocalizedPriceModifier(localizationCountry) ?? 0,

                            BooleanValue = cartItemVariant.BooleanValue,
                            DoubleValue = cartItemVariant.DoubleValue,
                            IntegerValue = cartItemVariant.IntegerValue,
                            StringValue = cartItemVariant.StringValue,
                            JsonValue = cartItemVariant.JsonValue
                        };

                        orderItem.OrderItemVariants.Add(orderItemProductVariant);
                    }

                    orderShippingBox.OrderItems.Add(orderItem);
                }

                order.OrderShippingBoxes.Add(orderShippingBox);
            }

            order.AutoTags = this.autoTagService.GetAutoTags(order);

            this.dbContext.Orders.Add(order);

            // -----------------------------------------------------------
            // Decrease product stock
            // -----------------------------------------------------------

            foreach (var cartItem in cart.CartShippingBoxes.SelectMany(x => x.CartItems))
            {
                var productStockUnit = cartItem.GetProductStockUnit();

                if (productStockUnit == null)
                {
                    continue;
                }

                productStockUnit.Stock -= 1;
            }

            this.dbContext.SaveChanges();

            return order;
        }

        /// <inheritdoc />
        public virtual List<CartError> GetErrors(Country localizationCountry, Cart cart)
        {
            var errors = new List<CartError>();

            var shippingCountryOrLocalizationCountry = cart.ShippingAddress?.Country ?? localizationCountry;

            var contentSectionFallBackValue = new Hiro_CartErrors
            {
                CartItemWithDisabledProduct = "The product [[Product]] is no longer available.",
                CartItemWithOutOfStockProduct = "The product [[Product]] is out of stock.",
                CartItemWithInvalidShippingBox = "The product [[Product]] cannot be shipped to [[ShippingCountry]] using the current shipping method.",
                InvalidCoupon = "Invalid Coupon.",
                InvalidTaxOverride = "The current Tax Override is obsolete.",
                InvalidUserCredit = "Your User Credits cannot be used in the current country.",
                LocalizedShippingAddressCountryNotMatchingCurrentCountry = "When the ShippingAddress Country is localized, the current Country should match.",
                NonLocalizedShippingAddressCountryNotMatchingDefaultCountry = "When the ShippingAddress Country is not localized, the current Country should be the default one.",
                ShippingAddressRequired = "Please select a shipping address."
            }.ToCamelCaseJson();

            var contentSection = this.contentSectionService.GetOrSetLocalizedContentAs<Hiro_CartErrors>(
                localizationCountry,
                ContentSectionNames.Cms_CartErrors,
                contentSectionFallBackValue);

            // -------------------------------------------------------
            // Check if the ShippingAddress is required
            // -------------------------------------------------------

            if (cart.ShippingAddress == null && cart.CartShippingBoxes.Any(csb => csb.ShippingBox?.RequiresShippingAddress ?? false))
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.ShippingAddressRequired,
                    Message = contentSection.ShippingAddressRequired
                });
            }

            // -------------------------------------------------------
            // Check if any Product has been disabled
            // -------------------------------------------------------

            var cartItemsWithDisabledProducts = cart.CartShippingBoxes
                .SelectMany(c => c.CartItems)
                .Where(ci =>
                    ci.Product == null ||
                    ci.Product.IsDisabled ||
                    ci.Product.GetLocalizedIsDisabled(localizationCountry))
                .ToList();

            foreach (var cartItemWithDisabledProduct in cartItemsWithDisabledProducts)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.CartItemWithDisabledProduct,
                    Message = contentSection.CartItemWithDisabledProduct?
                        .Replace("[[Product]]", cartItemWithDisabledProduct.Product.GetLocalizedTitle(localizationCountry))
                        .Replace("[[ShippingCountry]]", shippingCountryOrLocalizationCountry.Name),
                    TargetId = cartItemWithDisabledProduct.Id
                });
            }

            // -------------------------------------------------------
            // Check if any CartItem is out of stock
            // -------------------------------------------------------

            var cartItemWithOutOfStockProducts = cart.CartShippingBoxes
                .SelectMany(c => c.CartItems)
                .Where(ci => ci.GetStock() < 1)
                .ToList();

            foreach (var cartItemWithOutOfStockProduct in cartItemWithOutOfStockProducts)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.CartItemWithOutOfStockProduct,
                    Message = contentSection.CartItemWithOutOfStockProduct?.Replace("[[Product]]", cartItemWithOutOfStockProduct.Product.GetLocalizedTitle(localizationCountry)),
                    TargetId = cartItemWithOutOfStockProduct.Id,
                });
            }

            // -------------------------------------------------------
            // Check if the ShippingBoxes are
            // compatible with the shipping Country.
            // -------------------------------------------------------

            var cartItemsWithInvalidShippingBox = cart.CartShippingBoxes
                .Where(x =>
                    x.ShippingBox == null ||
                    !x.ShippingBox.Countries.ContainsById(shippingCountryOrLocalizationCountry))
                .SelectMany(x => x.CartItems)
                .ToList();

            foreach (var cartItemWithInvalidShippingBox in cartItemsWithInvalidShippingBox)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.CartItemWithInvalidShippingBox,
                    Message = contentSection.CartItemWithInvalidShippingBox?.Replace("[[Product]]", cartItemWithInvalidShippingBox.Product.GetLocalizedTitle(localizationCountry)),
                    TargetId = cartItemWithInvalidShippingBox.Id
                });
            }

            // -------------------------------------------------------
            // Check if coupon is still valid
            // -------------------------------------------------------

            var coupon = cart.Coupon;

            if (coupon != null)
            {
                if ((coupon.Country != null && coupon.Country.Id != localizationCountry.Id) ||
                    coupon.ExpirationDate < DateTime.Now ||
                    !coupon.Published)
                {
                    errors.Add(new CartError
                    {
                        Code = CartErrorCode.InvalidCoupon,
                        Message = contentSection.InvalidCoupon,
                        TargetId = coupon.Id
                    });
                }
            }

            // -------------------------------------------------------
            // Check if the Shipping country is
            // compatible with the current Country.
            // -------------------------------------------------------

            if (shippingCountryOrLocalizationCountry.Localize &&
                shippingCountryOrLocalizationCountry.Id != localizationCountry.Id &&
                !shippingCountryOrLocalizationCountry.IsDisabled)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.LocalizedShippingAddressCountryNotMatchingCurrentCountry,
                    Message = contentSection.LocalizedShippingAddressCountryNotMatchingCurrentCountry,
                    TargetId = null
                });
            }

            if ((shippingCountryOrLocalizationCountry.IsDisabled || !shippingCountryOrLocalizationCountry.Localize) &&
                !localizationCountry.IsDefault)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.NonLocalizedShippingAddressCountryNotMatchingDefaultCountry,
                    Message = contentSection.NonLocalizedShippingAddressCountryNotMatchingDefaultCountry,
                    TargetId = null
                });
            }

            // -------------------------------------------------------
            // Check if the TaxesOverride are still valid
            // -------------------------------------------------------

            var invalidTaxesOverride = cart.CartShippingBoxes
                .SelectMany(x => x.CartItems)
                .SelectMany(c => c.TaxesOverride)
                .Where(t =>
                    t.ShippingAddress?.Id != cart.ShippingAddress?.Id ||
                    t.ShippingAddress?.ModifiedDate > cart.ShippingAddress?.ModifiedDate)
                .ToList();

            foreach (var tax in invalidTaxesOverride)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.InvalidTaxOverride,
                    Message = contentSection.InvalidTaxOverride,
                    TargetId = tax.Id
                });
            }

            // -------------------------------------------------------
            // Check if the UserCredit localized kit Country
            // matched the current country
            // -------------------------------------------------------

            var userCreditLocalizedKitCountryId = cart.UserCredit?.UserLocalizedKit?.Country?.Id;

            if (userCreditLocalizedKitCountryId != null && userCreditLocalizedKitCountryId != localizationCountry.Id)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.InvalidUserCredit,
                    Message = contentSection.InvalidUserCredit,
                    TargetId = cart.UserCredit.Id
                });
            }

            return errors;
        }

        /// <inheritdoc />
        public virtual double GetCouponValue(Country localizationCountry, Cart cart)
        {
            var coupon = cart.Coupon;

            if (coupon == null)
            {
                return 0;
            }

            var couponCartItems = cart
                .GetCartItems()
                .Where(ci => this.GetCartItemCouponApplies(localizationCountry, ci));

            var couponCartItemsSubtotalBeforeTax = couponCartItems.Sum(ci => this.GetCartItemSubtotalBeforeTax(localizationCountry, ci));

            if (coupon.Amount > 0)
            {
                return coupon.Amount.ToMaximum(couponCartItemsSubtotalBeforeTax);
            }

            return couponCartItemsSubtotalBeforeTax.CalculatePercentage(coupon.Percentage);
        }

        /// <inheritdoc />
        public virtual double GetTotalBeforeTax(Country localizationCountry, Cart cart)
        {
            var totalBeforeTax = cart.CartShippingBoxes.Sum(csb => this.GetCartShippingBoxTotalBeforeTax(localizationCountry, csb));

            return totalBeforeTax.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(Country localizationCountry, Cart cart)
        {
            var totalAfterTax = cart.CartShippingBoxes.Sum(csb => this.GetCartShippingBoxTotalAfterTax(localizationCountry, csb));

            return totalAfterTax.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalToBePaid(Country localizationCountry, Cart cart)
        {
            var userCreditAmount = cart.UserCredit?.Amount ?? 0;

            var total = this.GetTotalAfterTax(localizationCountry, cart) + userCreditAmount;

            return total.ToMinimum(0);
        }

        // ------------------------------------------------------------------------------
        // CartShippingBox methods
        // ------------------------------------------------------------------------------

        /// <inheritdoc />
        public virtual double GetShippingPriceBeforeTax(Country localizationCountry, CartShippingBox cartShippingBox)
        {
            var freeShippingMinimumPrice = cartShippingBox.ShippingBox.GetLocalizedFreeShippingMinimumPrice(localizationCountry);

            if (freeShippingMinimumPrice > 0)
            {
                var cartItemsTotalBeforeTax = cartShippingBox.Cart?.GetCartItems().Sum(cartItem => this.GetCartItemTotalBeforeTax(localizationCountry, cartItem)) ?? 0;

                if (cartItemsTotalBeforeTax > freeShippingMinimumPrice)
                {
                    return 0;
                }
            }

            return cartShippingBox.ShippingBox.GetLocalizedPrice(localizationCountry);
        }

        /// <inheritdoc />
        public virtual double GetShippingPriceAfterTax(Country localizationCountry, CartShippingBox cartShippingBox)
        {
            var shippingPriceBeforeTax = this.GetShippingPriceBeforeTax(localizationCountry, cartShippingBox);

            var taxes = (cartShippingBox.Cart?.ShippingAddress?.Country ?? localizationCountry).Taxes.Where(x => x.CartItem == null && x.ApplyToShippingPrice).ToList();

            return shippingPriceBeforeTax.AddTaxes(taxes);
        }

        /// <inheritdoc />
        public virtual double GetCartShippingBoxTotalBeforeTax(Country localizationCountry, CartShippingBox cartShippingBox)
        {
            var cartItemsTotalBeforeTax = cartShippingBox.CartItems.Sum(cartItem => this.GetCartItemTotalBeforeTax(localizationCountry, cartItem));

            var shippingPriceBeforeTax = this.GetShippingPriceBeforeTax(localizationCountry, cartShippingBox);

            return cartItemsTotalBeforeTax + shippingPriceBeforeTax;
        }

        /// <inheritdoc />
        public virtual double GetCartShippingBoxTotalAfterTax(Country localizationCountry, CartShippingBox cartShippingBox)
        {
            var cartItemsTotalAfterTax = cartShippingBox.CartItems.Sum(cartItem => this.GetCartItemTotalAfterTax(localizationCountry, cartItem));

            var shippingPriceAfterTax = this.GetShippingPriceAfterTax(localizationCountry, cartShippingBox);

            return cartItemsTotalAfterTax + shippingPriceAfterTax;
        }

        // ------------------------------------------------------------------------------
        // CartItem methods
        // ------------------------------------------------------------------------------

        /// <inheritdoc />
        public virtual void AddCartItemToCart(Country localizationCountry, Cart cart, CartItem cartItem)
        {
            if (localizationCountry == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(localizationCountry)}' cannot be null.");
            }

            if (cart == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cart)}' cannot be null.");
            }

            if (cartItem == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cartItem)}' cannot be null.");
            }

            if (this.dbContext.Contains(cartItem))
            {
                throw new ArgumentException($"The parameter '{nameof(cartItem)}' cannot be attached to the context.");
            }

            var product = this.dbContext.Products.Find(cartItem.Product?.Id);

            if (product == null)
            {
                throw new ArgumentNullException($"The property 'Product' of the parameter '{nameof(cartItem)}' cannot be null.");
            }

            var shippingBox = this.dbContext.ShippingBoxes.Find(cartItem.CartShippingBox?.ShippingBox?.Id);

            if (shippingBox == null)
            {
                throw new ArgumentNullException($"The property 'ShippingBox' of the parameter '{nameof(cartItem)}' cannot be null.");
            }

            cartItem.Product = product;

            cartItem.CartShippingBox = this.GetCartShippingBox(cart, shippingBox.Id) ??
                                       this.CreateCartShippingBox(cart, shippingBox.Id);

            foreach (var cartItemVariant in cartItem.CartItemVariants)
            {
                cartItemVariant.CartItem = cartItem;
                cartItemVariant.Variant = this.dbContext.Variants.Find(cartItemVariant.Variant?.Id);
                cartItemVariant.VariantOptionValue = this.dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id);
            }

            this.dbContext.CartItems.Add(cartItem);

            this.dbContext.SaveChanges();
        }

        public virtual void UpdateCartItem([NotNull]Cart cart, [NotNull]CartItem cartItem)
        {
            if (cart == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cart)}' cannot be null.");
            }

            if (cartItem == null)
            {
                throw new ArgumentNullException($"The parameter '{nameof(cartItem)}' cannot be null.");
            }

            var shippingBoxId = cartItem.CartShippingBox?.ShippingBox?.Id;

            if (shippingBoxId == null)
            {
                throw new NullReferenceException("The ShippingBox was null");
            }

            cartItem.CartShippingBox = this.GetCartShippingBox(cart, shippingBoxId.Value) ?? this.CreateCartShippingBox(cart, shippingBoxId.Value);

            var dbCartItem = this.dbContext.CartItems.Find(cartItem.Id);

            if (dbCartItem == null)
            {
                throw new NullReferenceException("The cartItem was null");
            }

            dbCartItem.CartShippingBox = this.GetCartShippingBox(cart, shippingBoxId.Value) ?? this.CreateCartShippingBox(cart, shippingBoxId.Value);

            foreach (var cartItemVariant in cartItem.CartItemVariants.ToList())
            {
                var dbCartItemVariant = dbCartItem.CartItemVariants.FirstOrDefault(x => x.Id == cartItemVariant.Id);

                if (dbCartItemVariant == null)
                {
                    dbCartItemVariant = new CartItemVariant
                    {
                        CartItem = dbCartItem,
                        BooleanValue = cartItemVariant.BooleanValue,
                        DoubleValue = cartItemVariant.DoubleValue,
                        IntegerValue = cartItemVariant.IntegerValue,
                        JsonValue = cartItemVariant.JsonValue,
                        StringValue = cartItemVariant.StringValue,
                        Variant = this.dbContext.Variants.Find(cartItemVariant.Variant?.Id),
                        VariantOptionValue = this.dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id)
                    };

                    cartItem.CartItemVariants.Add(dbCartItemVariant);
                }
                else
                {
                    dbCartItemVariant.BooleanValue = cartItemVariant.BooleanValue;
                    dbCartItemVariant.DoubleValue = cartItemVariant.DoubleValue;
                    dbCartItemVariant.IntegerValue = cartItemVariant.IntegerValue;
                    dbCartItemVariant.JsonValue = cartItemVariant.JsonValue;
                    dbCartItemVariant.StringValue = cartItemVariant.StringValue;
                    dbCartItemVariant.Variant = this.dbContext.Variants.Find(cartItemVariant.Variant?.Id);
                    dbCartItemVariant.VariantOptionValue = this.dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id);
                }
            }

            cartItem.TaxesOverride.Clear();
            cartItem.TaxesOverride.AddRange(cartItem.TaxesOverride);

            this.dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public virtual void UpdateCartItemQuantity(Guid cartItemId, int quantity)
        {
            var cartItem = this.dbContext.CartItems.Find(cartItemId);

            if (cartItem == null)
            {
                return;
            }

            cartItem.Quantity = quantity.ToMinimum(1);

            this.dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public virtual void DeleteCartItem(Guid cartItemId)
        {
            var cartItem = this.dbContext.CartItems.Find(cartItemId);
            var cartShippingBox = cartItem?.CartShippingBox;

            if (cartShippingBox == null)
            {
                return;
            }

            if (cartShippingBox.CartItems.Count > 1)
            {
                this.dbContext.CartItems.Delete(cartItemId, true);
            }
            else
            {
                this.dbContext.CartShippingBoxes.Delete(cartShippingBox.Id, true);
            }
        }

        /// <inheritdoc />
        public virtual bool GetCartItemCouponApplies(Country localizationCountry, CartItem cartItem)
        {
            var coupon = cartItem.CartShippingBox?.Cart?.Coupon;

            if (coupon == null)
            {
                return false;
            }

            if ((!coupon.AllowOnSale && cartItem.GetProductStockUnit().GetLocalizedSalePrice(localizationCountry) > 0) ||
                (cartItem.GetProductStockUnit().GetLocalizedMembershipSalePrice(localizationCountry) > 0 && cartItem.CartShippingBox?.Cart?.User?.ShowMembershipSalePrice == true))
            {
                return false;
            }

            if (!coupon.Categories.Any())
            {
                return true;
            }

            foreach (var couponCategory in coupon.Categories)
            {
                if (cartItem.Product != null && cartItem.Product.HasCategories(couponCategory))
                {
                    return true;
                }

                var productImageKit = cartItem.GetProductImageKit();

                if (productImageKit != null && productImageKit.HasCategories(couponCategory))
                {
                    return true;
                }

                var productStockUnit = cartItem.GetProductStockUnit();

                if (productStockUnit != null && productStockUnit.HasCategories(couponCategory))
                {
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc />
        public virtual double GetCartItemCouponValue(Country localizationCountry, CartItem cartItem)
        {
            var coupon = cartItem.CartShippingBox?.Cart?.Coupon;

            if (coupon == null)
            {
                return 0;
            }

            if (this.GetCartItemCouponApplies(localizationCountry, cartItem))
            {
                var couponCartItems = cartItem.CartShippingBox.Cart
                    .GetCartItems()
                    .Where(ci => this.GetCartItemCouponApplies(localizationCountry, ci));

                var couponCartItemsSubtotalBeforeTax = couponCartItems.Sum(ci => this.GetCartItemSubtotalBeforeTax(localizationCountry, ci));

                var coefficient = this.GetCartItemSubtotalBeforeTax(localizationCountry, cartItem) / couponCartItemsSubtotalBeforeTax;

                return this.GetCouponValue(localizationCountry, cartItem.CartShippingBox.Cart) * coefficient;
            }

            return 0;
        }

        /// <inheritdoc />
        public virtual List<Tax> GetCartItemTaxes(Country localizationCountry, CartItem cartItem)
        {
            if (cartItem.TaxesOverride.Any())
            {
                return cartItem.TaxesOverride;
            }

            return (cartItem.CartShippingBox?.Cart?.ShippingAddress?.Country ?? localizationCountry).Taxes.Where(x => x.CartItem == null && x.ApplyToProductPrice).ToList();
        }

        /// <inheritdoc />
        public virtual double GetCartItemSubtotalBeforeTax(Country localizationCountry, CartItem cartItem)
        {
            var selectedVariantOptions = cartItem.CartItemVariants.Where(x => x.VariantOptionValue != null).Select(x => x.VariantOptionValue).ToList();

            var productTotalBeforeTax = this.productService.GetTotalBeforeTax(localizationCountry, cartItem.CartShippingBox?.Cart?.User, cartItem.Product, selectedVariantOptions);

            var subtotalBeforeTax = productTotalBeforeTax * cartItem.Quantity;

            return subtotalBeforeTax.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetCartItemSubtotalAfterTax(Country localizationCountry, CartItem cartItem)
        {
            var taxes = this.GetCartItemTaxes(localizationCountry, cartItem);

            return this.GetCartItemSubtotalBeforeTax(localizationCountry, cartItem).AddTaxes(taxes);
        }

        /// <inheritdoc />
        public virtual double GetCartItemTotalBeforeTax(Country localizationCountry, CartItem cartItem)
        {
            var subtotalBeforeTax = this.GetCartItemSubtotalBeforeTax(localizationCountry, cartItem);

            var couponValue = this.GetCartItemCouponValue(localizationCountry, cartItem);

            var total = subtotalBeforeTax - couponValue;

            return total.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetCartItemTotalAfterTax(Country localizationCountry, CartItem cartItem)
        {
            var taxes = this.GetCartItemTaxes(localizationCountry, cartItem);

            return this.GetCartItemTotalBeforeTax(localizationCountry, cartItem).AddTaxes(taxes);
        }

        /// <inheritdoc />
        public virtual double GetCartItemSubtotalBeforeTaxWithoutSalePrice(Country localizationCountry, CartItem cartItem)
        {
            var selectedVariantOptions = cartItem.CartItemVariants.Where(x => x.VariantOptionValue != null).Select(x => x.VariantOptionValue).ToList();

            var productTotalBeforeTaxWithoutSalePrice = this.productService.GetTotalBeforeTaxWithoutSalePrice(localizationCountry, cartItem.CartShippingBox?.Cart?.User, cartItem.Product, selectedVariantOptions);

            return productTotalBeforeTaxWithoutSalePrice * cartItem.Quantity;
        }

        /// <inheritdoc />
        public virtual double GetCartItemSubtotalAfterTaxWithoutSalePrice(Country localizationCountry, CartItem cartItem)
        {
            var taxes = this.GetCartItemTaxes(localizationCountry, cartItem);

            return this.GetCartItemSubtotalBeforeTaxWithoutSalePrice(localizationCountry, cartItem).AddTaxes(taxes);
        }
    }
}