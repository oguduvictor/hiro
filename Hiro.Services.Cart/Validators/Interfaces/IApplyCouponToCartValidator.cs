﻿namespace Hiro.Services.Validators.Interfaces
{
    using System.Collections.Generic;
    using Hiro.Domain;
    using Hiro.Shared.Dtos;

    public interface IApplyCouponToCartValidator
    {
        List<OperationError> Validate(Country localizationCountry, Coupon coupon);
    }
}