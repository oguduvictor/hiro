﻿namespace Hiro.Services.Validators.Interfaces
{
    using System.Collections.Generic;
    using Hiro.Domain;
    using Hiro.Shared.Dtos;

    public interface IAddOrUpdateAddressValidator
    {
        List<OperationError> Validate(Address address);
    }
}
