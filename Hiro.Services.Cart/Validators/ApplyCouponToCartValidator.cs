﻿namespace Hiro.Services.Validators
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain;
    using Hiro.Services.Interfaces;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Validation;

    public class ApplyCouponToCartValidator : HiroValidator, IApplyCouponToCartValidator
    {
        private readonly IAuthenticationService authenticationService;

        public ApplyCouponToCartValidator(
            IAuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        public List<OperationError> Validate(Country localizationCountry, Coupon coupon)
        {
            if (coupon == null)
            {
                this.Errors.Add(new OperationError("This Coupon code is invalid."));
            }
            else
            {
                this.Must(coupon.Country == null || coupon.Country.Id == localizationCountry.Id, "This Coupon cannot be used in the current Country.");
                this.Must(coupon.ExpirationDate > DateTime.Now, "This Coupon is expired.");
                this.Must(coupon.ValidityTimes > 0, "This Coupon has already been used.");
                this.Must(coupon.Published, "This Coupon code is invalid.");

                if (coupon.Referee != null)
                {
                    var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

                    this.Must(authenticatedUser != null, "Please login before using this Coupon.");
                    this.Must(coupon.Referee.Id != authenticatedUser?.Id, "A Coupon cannot be used by the same person who generated it.");
                }
            }

            return this.Errors;
        }
    }
}