﻿namespace Hiro.Services.Validators
{
    using System.Collections.Generic;
    using Hiro.Domain;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Validation;

    public class AddOrUpdateAddressValidator : HiroValidator, IAddOrUpdateAddressValidator
    {
        public List<OperationError> Validate(Address address)
        {
            this.MustNotBeEmpty(address.FirstName, "'First Name' should not be empty.", nameof(address.FirstName));
            this.MustNotBeEmpty(address.LastName, "'Last Name' should not be empty.", nameof(address.LastName));
            this.MustNotBeEmpty(address.AddressLine1, "'Address Line1' should not be empty.", nameof(address.AddressLine1));
            this.MustNotBeEmpty(address.Postcode, "'Postcode' should not be empty.", nameof(address.Postcode));
            this.MustNotBeEmpty(address.City, "'City' should not be empty.", nameof(address.City));
            this.Must(address.Country != null, "'Country' should not be empty.", nameof(address.Country));
            this.MustNotBeEmpty(address.PhoneNumber, "'PhoneNumber' should not be empty.", nameof(address.PhoneNumber));

            return this.Errors;
        }
    }
}
