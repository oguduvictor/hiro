﻿namespace Hiro.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain;
    using Hiro.Shared.Dtos;
    using JetBrains.Annotations;

    public interface ICartService
    {
        bool AddUserCredit(Country localizationCountry, User user, double amount, string reference = null);

        OperationResult<Address> AddOrUpdateAddress(Cart cart, Address address, bool setUser = false, bool setAsCartShippingAddress = false, bool setAsCartBillingAddress = false);

        OperationResult SetCartShippingAddress(Cart cart, Guid? addressId);

        OperationResult SetCartBillingAddress(Cart cart, Guid? addressId);

        /// <summary>
        /// Generates an in-memory CartItem object from a Product.
        /// </summary>
        /// <param name="localizationCountry">The Localization Country.</param>
        /// <param name="shippingCountry">The shipping Country. Can be null.</param>
        /// <param name="product">The Product. Cannot be null.</param>
        /// <param name="selectedVariantOptions">The selected VariantOptions.</param>
        /// <returns>Returns a CartItem generated from the Product.</returns>
        CartItem GetCartItemFromProduct([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, Product product, List<VariantOption> selectedVariantOptions = null);

        [NotNull]
        Cart CreateCart(User user);

        void DeleteCart(Guid? cartId = null);

        CartShippingBox GetCartShippingBox(Cart cart, Guid shippingBoxId);

        CartShippingBox CreateCartShippingBox(Cart cart, Guid shippingBoxId);

        void SetShippingBox(Country localizationCountry, Cart cart, Guid cartItemId, string shippingBoxName);

        void AddCartItemToCart(Country localizationCountry, [NotNull] Cart cart, CartItem cartItem);

        void UpdateCartItem([NotNull]Cart cart, [NotNull]CartItem cartItem);

        void UpdateCartItemQuantity(Guid cartItemId, int quantity);

        void DeleteCartItem(Guid cartItemId);

        OperationResult<double> ApplyUserCredit(Country localizationCountry, Cart cart, double amount, string reference = null);

        OperationResult<double> RemoveCredit(Country localizationCountry, Cart cart);

        OperationResult ApplyCoupon(Country localizationCountry, Cart cart, string couponCode);

        void RemoveCoupon(Country localizationCountry, Cart cart);

        Order CreateOrder(Country localizationCountry, Cart cart, string transactionId = null);

        List<CartError> GetErrors(Country localizationCountry, Cart cart);

        double GetCouponValue(Country localizationCountry, Cart cart);

        double GetTotalBeforeTax(Country localizationCountry, Cart cart);

        double GetTotalAfterTax(Country localizationCountry, Cart cart);

        double GetTotalToBePaid(Country localizationCountry, Cart cart);

        // -----------------------------------------------------------------------------------
        // CartShippingBox methods
        // -----------------------------------------------------------------------------------

        double GetShippingPriceBeforeTax(Country localizationCountry, CartShippingBox cartShippingBox);

        double GetShippingPriceAfterTax(Country localizationCountry, CartShippingBox cartShippingBox);

        double GetCartShippingBoxTotalBeforeTax(Country localizationCountry, CartShippingBox cartShippingBox);

        double GetCartShippingBoxTotalAfterTax(Country localizationCountry, CartShippingBox cartShippingBox);

        // -----------------------------------------------------------------------------------
        // CartItem methods
        // -----------------------------------------------------------------------------------

        bool GetCartItemCouponApplies(Country localizationCountry, CartItem cartItem);

        double GetCartItemCouponValue(Country localizationCountry, CartItem cartItem);

        List<Tax> GetCartItemTaxes(Country localizationCountry, CartItem cartItem);

        double GetCartItemSubtotalBeforeTax(Country localizationCountry, CartItem cartItem);

        double GetCartItemSubtotalAfterTax(Country localizationCountry, CartItem cartItem);

        double GetCartItemTotalBeforeTax(Country localizationCountry, CartItem cartItem);

        double GetCartItemTotalAfterTax(Country localizationCountry, CartItem cartItem);

        double GetCartItemSubtotalBeforeTaxWithoutSalePrice(Country localizationCountry, CartItem cartItem);

        double GetCartItemSubtotalAfterTaxWithoutSalePrice(Country localizationCountry, CartItem cartItem);
    }
}