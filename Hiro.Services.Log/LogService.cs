﻿namespace Hiro.Services
{
    using Hiro.DataAccess;
    using Hiro.Domain;
    using Hiro.Domain.Enums;
    using Hiro.Services.Interfaces;
    using System.Threading.Tasks;

    public class LogService : ILogService
    {
        private readonly HiroDbContext dbContext;

        public LogService(HiroDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Log(LogTypesEnum type, string message, string details = null, User user = null)
        {
            this.dbContext.Logs.Add(CreateLog(type, message, details, user));

            this.dbContext.SaveChanges();
        }

        public async Task LogAsync(LogTypesEnum type, string message, string details = null, User user = null)
        {
            this.dbContext.Logs.Add(CreateLog(type, message, details, user));

            await this.dbContext.SaveChangesAsync();
        }

        private static Log CreateLog(LogTypesEnum type, string message, string details, User user)
        {
            return new Log
            {
                Type = type,
                Message = message,
                Details = details,
                User = user
            };
        }
    }
}