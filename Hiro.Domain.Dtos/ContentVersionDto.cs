﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using JetBrains.Annotations;

    public class ContentVersionDto : BaseEntityDto
    {
        [CanBeNull]
        public virtual ContentSectionLocalizedKitDto ContentSectionLocalizedKit { get; set; }

        [CanBeNull]
        public string Content { get; set; }
    }
}