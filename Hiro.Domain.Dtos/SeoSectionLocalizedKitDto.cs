﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using JetBrains.Annotations;

    public class SeoSectionLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        [CanBeNull]
        public virtual CountryDto Country { get; set; }

        [CanBeNull]
        public virtual SeoSectionDto SeoSection { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string Image { get; set; }
    }
}