﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using JetBrains.Annotations;

    public class BlogCategoryLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public BlogCategoryDto BlogCategory { get; set; }

        public CountryDto Country { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }
    }
}