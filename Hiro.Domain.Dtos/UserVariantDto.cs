﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class UserVariantDto : BaseEntityDto
    {
        public UserVariantDto()
        {
        }

        public UserVariantDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public UserDto User { get; set; }

        [CanBeNull]
        public VariantDto Variant { get; set; }

        public bool BooleanValue { get; set; }

        public double DoubleValue { get; set; }

        public int IntegerValue { get; set; }

        [CanBeNull]
        public string StringValue { get; set; }

        [CanBeNull]
        public string JsonValue { get; set; }

        [CanBeNull]
        public VariantOptionDto VariantOptionValue { get; set; }
    }
}