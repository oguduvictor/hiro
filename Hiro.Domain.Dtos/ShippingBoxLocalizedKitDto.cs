﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class ShippingBoxLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public ShippingBoxLocalizedKitDto()
        {
        }

        public ShippingBoxLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        public CountryDto Country { get; set; }

        [CanBeNull]
        public ShippingBoxDto ShippingBox { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string InternalDescription { get; set; }

        [CanBeNull]
        public string Carrier { get; set; }

        public int MinDays { get; set; }

        public int MaxDays { get; set; }

        public bool CountWeekends { get; set; }

        public double Price { get; set; }

        public double FreeShippingMinimumPrice { get; set; }
    }
}