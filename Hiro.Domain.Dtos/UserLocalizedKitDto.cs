﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class UserLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public UserLocalizedKitDto()
        {
        }

        public UserLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public UserDto User { get; set; }

        [NotNull]
        public List<UserCreditDto> UserCredits { get; set; } = new List<UserCreditDto>();
    }
}