﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class AttributeOptionLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public AttributeOptionLocalizedKitDto()
        {
        }

        public AttributeOptionLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public AttributeOptionDto AttributeOption { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }
    }
}