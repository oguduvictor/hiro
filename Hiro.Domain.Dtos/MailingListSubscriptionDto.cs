﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System.Collections.Generic;
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public class MailingListSubscriptionDto
    {
        [NotNull]
        public MailingListDto MailingList { get; set; }

        [NotNull]
        public UserDto User { get; set; }

        public MailingListSubscriptionStatusEnum Status { get; set; }

        public List<string> StatusDescriptions { get; set; }
    }
}
