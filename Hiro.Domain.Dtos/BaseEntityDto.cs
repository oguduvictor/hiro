﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;

    public class BaseEntityDto
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public bool? IsDisabled { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets whether the entity is new or it has already been saved into the database.
        /// </summary>
        [Obsolete]
        public bool? IsNew { get; set; }

        /// <summary>
        /// Gets or sets whether the entity has been deleted.
        /// </summary>
        public bool? IsDeleted { get; set; }
    }
}