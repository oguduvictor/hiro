﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class BlogPostDto : BaseEntityDto
    {
        public BlogPostDto()
        {
        }

        public BlogPostDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Content { get; set; }

        public bool IsPublished { get; set; }

        public DateTime PublicationDate { get; set; } = DateTime.Now;

        [CanBeNull]
        public UserDto Author { get; set; }

        [CanBeNull]
        public string FeaturedImage { get; set; }

        [CanBeNull]
        public string FeaturedImageHorizontalCrop { get; set; } = "50%";

        [CanBeNull]
        public string FeaturedImageVerticalCrop { get; set; } = "50%";

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string MetaTitle { get; set; }

        [CanBeNull]
        public string MetaDescription { get; set; }

        [CanBeNull]
        public string Excerpt { get; set; }

        [NotNull]
        public List<BlogCategoryDto> BlogCategories { get; set; } = new List<BlogCategoryDto>();

        [NotNull]
        public List<BlogCommentDto> BlogComments { get; set; } = new List<BlogCommentDto>();

        [CanBeNull]
        public string ContentPreview { get; set; }
    }
}