﻿

#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    public interface ISortableDto
    {
        int SortOrder { get; set; }

    }
}
