﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public interface ILocalizedKitDto
    {
        Guid Id { get; set; }

        [CanBeNull]
        CountryDto Country { get; set; }
    }
}
