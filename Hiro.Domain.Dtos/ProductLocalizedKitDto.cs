﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class ProductLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public ProductLocalizedKitDto()
        {
        }

        public ProductLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public ProductDto Product { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string MetaTitle { get; set; }

        [CanBeNull]
        public string MetaDescription { get; set; }
    }
}