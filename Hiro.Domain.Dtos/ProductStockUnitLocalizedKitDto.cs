﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using JetBrains.Annotations;

    public class ProductStockUnitLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        [CanBeNull]
        public ProductStockUnitDto ProductStockUnit { get; set; }

        [CanBeNull]
        public CountryDto Country { get; set; }

        public double BasePrice { get; set; }

        public double SalePrice { get; set; }

        public double MembershipSalePrice { get; set; }
    }
}
