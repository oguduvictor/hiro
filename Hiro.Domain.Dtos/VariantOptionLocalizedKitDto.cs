﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class VariantOptionLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public VariantOptionLocalizedKitDto()
        {
        }

        public VariantOptionLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public VariantOptionDto VariantOption { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        public double PriceModifier { get; set; }
    }
}