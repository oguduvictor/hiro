﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class CountryDto : BaseEntityDto
    {
        public CountryDto()
        {
        }

        public CountryDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string FlagIcon { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string LanguageCode { get; set; }

        public bool IsDefault { get; set; }

        public bool Localize { get; set; }

        [CanBeNull]
        public string PaymentAccountId { get; set; }

        [NotNull]
        public List<TaxDto> Taxes { get; set; } = new List<TaxDto>();

        [NotNull]
        public List<ShippingBoxDto> ShippingBoxes { get; set; } = new List<ShippingBoxDto>();

        [CanBeNull]
        public string CurrencySymbol { get; set; }

        [CanBeNull]
        public string Iso4217CurrencySymbol { get; set; }
    }
}