﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public class AttributeDto : BaseEntityDto, ISortableDto
    {
        public AttributeDto()
        {
        }

        public AttributeDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        public AttributeTypesEnum Type { get; set; }

        [NotNull]
        public List<string> TypeDescriptions { get; set; } = new List<string>();

        [NotNull]
        public List<AttributeOptionDto> AttributeOptions { get; set; } = new List<AttributeOptionDto>();

        [NotNull]
        public List<AttributeLocalizedKitDto> AttributeLocalizedKits { get; set; } = new List<AttributeLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedTitle { get; set; }
    }
}