﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ContentSectionLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public ContentSectionLocalizedKitDto()
        {
        }

        public ContentSectionLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        public CountryDto Country { get; set; }

        public ContentSectionDto ContentSection { get; set; }

        [CanBeNull]
        public string Content { get; set; }

        public virtual List<ContentVersionDto> ContentVersions { get; set; } = new List<ContentVersionDto>();

    }
}