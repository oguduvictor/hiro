﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class EmailLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public EmailLocalizedKitDto()
        {
        }

        public EmailLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public EmailDto Email { get; set; }

        [CanBeNull]
        public string From { get; set; }

        [CanBeNull]
        public string ReplyTo { get; set; }

        [CanBeNull]
        public string DisplayName { get; set; }
    }
}