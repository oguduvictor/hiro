﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class CategoryLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public CategoryLocalizedKitDto()
        {
        }

        public CategoryLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public CategoryDto Category { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string FeaturedTitle { get; set; }

        [CanBeNull]
        public string FeaturedDescription { get; set; }

        [CanBeNull]
        public string FeaturedImage { get; set; }

        [CanBeNull]
        public string MetaTitle { get; set; }

        [CanBeNull]
        public string MetaDescription { get; set; }

        [CanBeNull]
        public string Image { get; set; }
    }
}