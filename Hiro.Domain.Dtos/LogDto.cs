﻿#pragma warning disable 1591
namespace Hiro.Domain.Dtos
{
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public class LogDto : BaseEntityDto
    {
        public LogTypesEnum Type { get; set; }

        [CanBeNull]
        public string Message { get; set; }

        [CanBeNull]
        public string Details { get; set; }

        public UserDto User { get; set; }
    }
}