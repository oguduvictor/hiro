﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class AttributeLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public AttributeLocalizedKitDto()
        {
        }

        public AttributeLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public AttributeDto Attribute { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }
    }
}