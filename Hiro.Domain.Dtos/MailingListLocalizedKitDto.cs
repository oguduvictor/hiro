﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    public class MailingListLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public MailingListDto MailingList { get; set; }

        public CountryDto Country { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
