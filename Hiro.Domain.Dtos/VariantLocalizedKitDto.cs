﻿#pragma warning disable 1591

namespace Hiro.Domain.Dtos
{
    using System;
    using JetBrains.Annotations;

    public class VariantLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public VariantLocalizedKitDto()
        {
        }

        public VariantLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public VariantDto Variant { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }
    }
}