﻿namespace Hiro.Services.Interfaces
{
    public interface  IIpGeolocalizationService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns>string.</returns>
        string GetCountryFromIp(string ipAddress);
    }
}
