﻿namespace Hiro.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using Hiro.Services.Interfaces;

    public class IpGeolocalizationService : IIpGeolocalizationService
    {
        /// <inheritdoc />
        public virtual string GetCountryFromIp(string ipAddress)
        {
            const string csvPath = "Hiro.Services.Csv.Output.csv";

            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(csvPath))
            {
                if (stream == null)
                {
                    throw new NullReferenceException("No IP database was found at " + csvPath);
                }

                var ip = IPAddress.Parse(ipAddress);

                var rows = GetRows(() => stream, Encoding.UTF8);

                foreach (var row in rows)
                {
                    var columns = row.Split(';');

                    var addressRange = new IpAddressRange(IPAddress.Parse(columns[0]), IPAddress.Parse(columns[1]));

                    if (addressRange.IsInRange(ip))
                    {
                        return columns[2];
                    }
                }
            }

            return string.Empty;
        }

        private static IEnumerable<string> GetRows(Func<Stream> streamProvider, Encoding encoding)
        {
            using var stream = streamProvider();
            using var reader = new StreamReader(stream, encoding);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                yield return line;
            }
        }
    }
}
