﻿namespace Hiro.Services
{
    using System.Net;
    using System.Net.Sockets;

    internal class IpAddressRange
    {
        private readonly AddressFamily _addressFamily;
        private readonly byte[] _lowerBytes;
        private readonly byte[] _upperBytes;

        public IpAddressRange(IPAddress lowerInclusive, IPAddress upperInclusive)
        {
            _addressFamily = lowerInclusive.AddressFamily;
            _lowerBytes = lowerInclusive.GetAddressBytes();
            _upperBytes = upperInclusive.GetAddressBytes();
        }

        public bool IsInRange(IPAddress address)
        {
            if (address.AddressFamily != _addressFamily)
            {
                return false;
            }

            var addressBytes = address.GetAddressBytes();

            bool lowerBoundary = true, upperBoundary = true;

            for (var i = 0; i < _lowerBytes.Length && (lowerBoundary || upperBoundary); i++)
            {
                if (lowerBoundary && addressBytes[i] < _lowerBytes[i] ||
                    upperBoundary && addressBytes[i] > _upperBytes[i])
                {
                    return false;
                }

                lowerBoundary &= addressBytes[i] == _lowerBytes[i];
                upperBoundary &= addressBytes[i] == _upperBytes[i];
            }

            return true;
        }
    }
}
