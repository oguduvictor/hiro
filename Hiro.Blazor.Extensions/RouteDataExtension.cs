﻿using Microsoft.AspNetCore.Components;

namespace Hiro.Blazor.Extensions
{
    public static class RouteDataExtension
    {
        public static string TryGetRouteValue(this RouteData routeData, string key)
        {
            if (routeData == null)
            {
                return null;
            }

            routeData.RouteValues.TryGetValue(key, out var value);

            return value?.ToString();
        }
    }
}