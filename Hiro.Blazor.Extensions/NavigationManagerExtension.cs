﻿namespace Hiro.Blazor.Extensions
{
    using System.Text.RegularExpressions;
    using Microsoft.AspNetCore.Components;

    public static class NavigationManagerExtension
    {
        /// <summary>
        /// Returns a Blazor route from a template replacing the template variables with the provided values.
        /// </summary>
        public static string GetBlazorRoute(this NavigationManager navigationManager, string routeTemplate, object routeTemplateParameters = null)
        {
            var routeUrl = routeTemplate;

            if (routeTemplateParameters != null)
            {
                foreach (var property in routeTemplateParameters.GetType().GetProperties())
                {
                    var propertyName = property.Name;
                    var propertyValue = property.GetValue(routeTemplateParameters);

                    var regexPattern = $@"{{{propertyName}(:[^}}]*)?}}";

                    var regex = new Regex(regexPattern);

                    routeUrl = regex.Replace(routeUrl, propertyValue?.ToString() ?? string.Empty);
                }
            }

            routeUrl = Regex.Replace(routeUrl, @"{(.*)}", string.Empty);
            routeUrl = routeUrl.Replace("//", "/");

            return routeUrl;
        }

        public static void NavigateToBlazorRoute(this NavigationManager navigationManager, string routeTemplate, object routeTemplateParameters = null, bool forceReload = false)
        {
            navigationManager.NavigateTo(navigationManager.GetBlazorRoute(routeTemplate, routeTemplateParameters), forceReload);
        }
    }
}
