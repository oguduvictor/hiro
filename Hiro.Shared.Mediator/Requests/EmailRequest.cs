﻿using System.Collections.Generic;
using Hiro.Shared.Dtos;
using MediatR;

namespace Hiro.Shared.Mediator.Requests
{
    public class EmailRequest : IRequest<OperationResult>
    {
        public string EmailName { get; set; }

        public IEnumerable<string> To { get; set; }

        public object Data { get; set; }
    }
}