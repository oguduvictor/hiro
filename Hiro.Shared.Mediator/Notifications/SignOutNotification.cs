﻿using MediatR;

namespace Hiro.Shared.Mediator.Notifications
{
    public class SignOutNotification : INotification
    {
    }
}