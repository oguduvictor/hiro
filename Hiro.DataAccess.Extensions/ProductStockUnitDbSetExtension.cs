namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class ProductStockUnitDbSetExtension
    {
        public static void Delete(this DbSet<ProductStockUnit> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbEntity.VariantOptions.Clear();

            dbContext.ProductStockUnitLocalizedKits.RemoveRange(dbEntity.ProductStockUnitLocalizedKits);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}