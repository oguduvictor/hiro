namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Hiro.Domain;

    public static class ContentSectionDbSetExtension
    {
        public static void Delete(this DbSet<ContentSection> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var productAttributes = dbEntity.ProductAttributes.ToList();

            foreach (var productAttribute in productAttributes)
            {
                dbContext.ProductAttributes.Delete(productAttribute.Id, false);
            }

            var contentSectionEmails = dbContext.Emails.Where(x => x.ContentSection.Id == entityId).ToList();

            foreach (var email in contentSectionEmails)
            {
                email.ContentSection = null;
            }

            var contentSectionLocalizedKits = dbEntity.ContentSectionLocalizedKits.ToList();

            foreach (var contentSectionLocalizedKit in contentSectionLocalizedKits)
            {
                dbContext.ContentSectionLocalizedKits.Delete(contentSectionLocalizedKit.Id, false);
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}