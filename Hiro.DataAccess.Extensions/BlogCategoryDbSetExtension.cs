namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Hiro.Domain;

    public static class BlogCategoryDbSetExtension
    {
        public static void Delete(this DbSet<BlogCategory> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var blogPosts = dbEntity.BlogPosts.ToList();

            foreach (var blogPost in blogPosts)
            {
                blogPost.BlogCategories.Remove(dbEntity);
            }

            var blogCategoryLocalizedKits = dbEntity.BlogCategoryLocalizedKits.ToList();

            foreach (var blogCategoryLocalizedKit in blogCategoryLocalizedKits)
            {
                dbContext.BlogCategoryLocalizedKits.Delete(blogCategoryLocalizedKit.Id, false);
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}