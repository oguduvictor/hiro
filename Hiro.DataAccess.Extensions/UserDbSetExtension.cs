#pragma warning disable 1591

namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Hiro.Domain;

    public static class UserDbSetExtension
    {
        public static void Delete(this DbSet<User> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbEntity.MailingListSubscriptions.Clear();

            var logs = dbContext.Logs.Where(x => x.User.Id == entityId).ToList();

            foreach (var log in logs)
            {
                log.User = null;
            }

            var coupons = dbContext.Coupons.Where(x => x.Referee != null && x.Referee.Id == entityId).ToList();

            foreach (var coupon in coupons)
            {
                coupon.Referee = null;
            }

            var userAttributes = dbEntity.UserAttributes.ToList();

            foreach (var userAttribute in userAttributes)
            {
                dbContext.UserAttributes.Delete(userAttribute.Id, false);
            }

            var userLocalizedKits = dbEntity.UserLocalizedKits.ToList();

            foreach (var userLocalizedKit in userLocalizedKits)
            {
                dbContext.UserLocalizedKits.Delete(userLocalizedKit.Id, false);
            }

            var carts = dbContext.Carts.Where(x => x.User.Id == entityId).ToList();

            foreach (var cart in carts)
            {
                dbContext.Carts.Delete(cart.Id, false);
            }

            var addresses = dbContext.Addresses.Where(x => x.User.Id == entityId).ToList();

            foreach (var address in addresses)
            {
                dbContext.Addresses.Delete(address.Id, false);
            }

            var sentEmails = dbContext.SentEmails.Where(x => x.User.Id == entityId).ToList();

            dbContext.SentEmails.RemoveRange(sentEmails);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}