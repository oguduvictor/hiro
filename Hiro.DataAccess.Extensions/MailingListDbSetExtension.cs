#pragma warning disable 1591

namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class MailingListDbSetExtension
    {
        public static void Delete(this DbSet<MailingList> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbEntity.MailingListSubscriptions.Clear();

            dbContext.MailingListLocalizedKits.RemoveRange(dbEntity.MailingListLocalizedKits);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}
