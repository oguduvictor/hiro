﻿namespace Hiro.DataAccess.Extensions.Enums
{
    public enum PreviousValueMatch
    {
        Exact = 0,
        Contains = 1,
        StartsWith = 2,
        EndsWith = 3
    }
}