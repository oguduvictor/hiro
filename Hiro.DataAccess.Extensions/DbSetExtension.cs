﻿#pragma warning disable 1591

namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection;
    using Hiro.DataAccess.Extensions.Enums;
    using Hiro.Shared.Extensions;

    /// <summary>
    ///
    /// </summary>
    public static class DbSetExtension
    {
        /// <summary>
        /// Regurns whether an entity is attached to the DbSet.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool Contains<TEntity>(this DbSet<TEntity> dbSet, TEntity entity) where TEntity : class
        {
            return dbSet.Local.Any(e => e == entity);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <returns></returns>
        public static HiroDbContext GetContext<TEntity>(this DbSet<TEntity> dbSet) where TEntity : class
        {
            var internalSet = dbSet.GetType().GetField("_internalSet", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(dbSet);

            var internalContext = internalSet?.GetType().BaseType?.GetField("_internalContext", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(internalSet);

            return (HiroDbContext)internalContext?.GetType().GetProperty("Owner", BindingFlags.Instance | BindingFlags.Public)?.GetValue(internalContext, null);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <param name="propertyName"></param>
        /// <param name="previousValue"></param>
        /// <param name="newValue"></param>
        /// <param name="match"></param>
        public static void BulkUpdate<TEntity>(this DbSet<TEntity> dbSet, string propertyName, string previousValue, string newValue, PreviousValueMatch match = PreviousValueMatch.Exact) where TEntity : class
        {
            if (match == PreviousValueMatch.Exact && previousValue == newValue)
            {
                return;
            }

            var tableName = dbSet.ToString()
                .SubstringFromFirst("[dbo].[")
                .SubstringUpToFirst("]");

            var sqlCommand = $"UPDATE [{tableName}]\n";

            if (newValue == null)
            {
                sqlCommand += $"SET [{propertyName}] = NULL\n";
            }
            else
            {
                sqlCommand += $"SET [{propertyName}] = '{newValue}'\n";
            }

            if (previousValue == null)
            {
                if (match == PreviousValueMatch.Exact)
                {
                    sqlCommand += $"WHERE [{propertyName}] IS NULL";
                }
                else
                {
                    throw new Exception("When 'previousValue' is null, the match type can only be PreviousValueMatch.Exact");
                }
            }
            else
            {
                switch (match)
                {
                    case PreviousValueMatch.Exact:
                        sqlCommand += $"WHERE [{propertyName}] = '{previousValue}'";
                        break;
                    case PreviousValueMatch.Contains:
                        sqlCommand += $"WHERE [{propertyName}] LIKE '%{previousValue}%'";
                        break;
                    case PreviousValueMatch.StartsWith:
                        sqlCommand += $"WHERE [{propertyName}] LIKE '{previousValue}%'";
                        break;
                    case PreviousValueMatch.EndsWith:
                        sqlCommand += $"WHERE [{propertyName}] LIKE '%{previousValue}'";
                        break;
                }
            }

            dbSet.GetContext().Database.ExecuteSqlCommand(sqlCommand);
        }
    }
}
