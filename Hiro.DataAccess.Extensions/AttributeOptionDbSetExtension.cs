namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Hiro.Domain;

    public static class AttributeOptionDbSetExtension
    {
        public static void Delete(this DbSet<AttributeOption> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var productAttributesWhereAttributeOptionValue = dbContext.ProductAttributes
                .Where(x => x.AttributeOptionValue.Id == entityId).ToList();

            foreach (var productAttribute in productAttributesWhereAttributeOptionValue)
            {
                productAttribute.AttributeOptionValue = null;
            }

            var attributeOptionLocalizedKits = dbContext.AttributeOptionLocalizedKits
                .Where(x => x.AttributeOption.Id == dbEntity.Id).ToList();

            dbContext.AttributeOptionLocalizedKits.RemoveRange(attributeOptionLocalizedKits);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}