namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class ContentSectionLocalizedKitDbSetExtension
    {
        public static void Delete(this DbSet<ContentSectionLocalizedKit> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbContext.ContentVersions.RemoveRange(dbEntity.ContentVersions);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}