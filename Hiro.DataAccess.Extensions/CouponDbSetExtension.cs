namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Hiro.Domain;

    public static class CouponDbSetExtension
    {
        public static void Delete(this DbSet<Coupon> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var carts = dbContext.Carts.Where(x => x.Coupon != null && x.Coupon.Id == entityId);
            foreach (var cart in carts)
            {
                cart.Coupon = null;
            }

            var sentEmails = dbContext.SentEmails.Where(x => x.Coupon != null && x.Coupon.Id == entityId);
            foreach (var sentEmail in sentEmails)
            {
                sentEmail.Coupon = null;
            }

            dbEntity.Categories.Clear();

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}