namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class SeoSectionDbSetExtension
    {
        public static void Delete(this DbSet<SeoSection> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbContext.SeoSectionLocalizedKits.RemoveRange(dbEntity.SeoSectionLocalizedKits);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}