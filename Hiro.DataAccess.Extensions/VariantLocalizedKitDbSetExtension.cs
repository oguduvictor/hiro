﻿namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class VariantLocalizedKitDbSetExtension
    {
        public static void Delete(this DbSet<VariantLocalizedKit> variantLocalizedKits, Guid? variantLocalizedKitId, bool saveChanges)
        {
            var dbContext = variantLocalizedKits.GetContext();

            if (variantLocalizedKitId == null)
            {
                return;
            }

            var variantLocalizedKit = dbContext.VariantLocalizedKits.Find(variantLocalizedKitId);

            if (variantLocalizedKit == null)
            {
                return;
            }

            dbContext.VariantLocalizedKits.Remove(variantLocalizedKit);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}