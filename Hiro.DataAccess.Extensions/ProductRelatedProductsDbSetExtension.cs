namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class ProductRelatedProductsDbSetExtension
    {
        public static void Delete(this DbSet<ProductRelatedProducts> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}