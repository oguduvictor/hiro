#pragma warning disable 1591

namespace Hiro.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using Hiro.Domain;

    public static class BlogCategoryLocalizedKitDbSetExtension
    {
        public static void Delete(this DbSet<BlogCategoryLocalizedKit> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}