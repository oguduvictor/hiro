@echo off

SET "CDIR=%~dp0"

:: for loop requires removing trailing backslash from %~dp0 output
SET "CDIR=%CDIR:~0,-1%"

FOR %%i IN ("%CDIR%") DO SET "package=%%~nxi"

SET /P packageVersion=Enter %package% Nuget Package Version to upload: 

SET package=%package%.%packageVersion%.nupkg

.\NuGet push %package% -Source http://sumosoft-nuget.azurewebsites.net/nuget -ApiKey Sumosoft007

PAUSE
