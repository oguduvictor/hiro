﻿namespace Hiro.Services.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.DataAccess;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Validation;

    public class RegisterUserValidator : HiroValidator, IRegisterUserValidator
    {
        private readonly HiroDbContext dbContext;

        public RegisterUserValidator(HiroDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<OperationError> Validate(string firstName, string lastName, string email, string password)
        {
            this.CascadeMode = true;

            this.MustBeValidEmailAddress(email, "Please enter a valid email address.", "Email");
            this.MustNotBeEmpty(firstName, "Please enter your first name.", "FirstName");
            this.MustNotBeEmpty(lastName, "Please enter your last name.", "LastName");
            this.MustNotBeEmpty(password, "Please enter your password.", "Password");
            this.Must(!this.dbContext.Users.Any(x => x.Email == email), "Email has already been used", "Email");

            return this.Errors;
        }
    }
}