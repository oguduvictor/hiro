﻿namespace Hiro.Services.Validators
{
    using System.Collections.Generic;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Validation;

    public class EmailIsRegisteredValidator : HiroValidator, IEmailIsRegisteredValidator
    {
        public List<OperationError> Validate(string email)
        {
            this.MustNotBeEmpty(email, "Please, enter an email address", "Email");
            this.MustBeValidEmailAddress(email, "Please, enter a valid email address", "Email");

            return this.Errors;
        }
    }
}
