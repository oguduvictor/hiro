﻿namespace Hiro.Services.Validators.Interfaces
{
    using System.Collections.Generic;
    using Hiro.Shared.Dtos;

    public interface IRegisterUserValidator
    {
        List<OperationError> Validate(string firstName, string lastName, string email, string password);
    }
}
