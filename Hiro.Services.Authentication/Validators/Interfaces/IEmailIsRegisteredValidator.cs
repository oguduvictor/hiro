﻿namespace Hiro.Services.Validators.Interfaces
{
    using System.Collections.Generic;
    using Hiro.Shared.Dtos;

    public interface IEmailIsRegisteredValidator
    {
        List<OperationError> Validate(string email);
    }
}
