﻿namespace Hiro.Services.Validators.Interfaces
{
    using System.Collections.Generic;
    using Hiro.Shared.Dtos;

    public interface IAuthenticateUserValidator
    {
        List<OperationError> Validate(string email, string password);
    }
}