﻿namespace Hiro.Services.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.DataAccess;
    using Hiro.Services.Interfaces;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Validation;

    public class AuthenticateUserValidator : HiroValidator, IAuthenticateUserValidator
    {
        private readonly HiroDbContext dbContext;
        private readonly IEncryptionService encryptionService;

        public AuthenticateUserValidator(
            HiroDbContext dbContext,
            IEncryptionService encryptionService)
        {
            this.dbContext = dbContext;
            this.encryptionService = encryptionService;
        }

        public List<OperationError> Validate(string email, string password)
        {
            this.CascadeMode = true;

            this.MustBeValidEmailAddress(email, "Please enter a valid email address.", "Email");
            this.MustNotBeEmpty(password, "Please enter your password", "Password");
            var encryptedPassword = this.encryptionService.Encrypt(password);

            this.Must(
                this.dbContext.Users.Any(x => x.Email.Equals(email) && x.Password.Equals(encryptedPassword)),
                "Wrong username or password.");

            return this.Errors;
        }
    }
}