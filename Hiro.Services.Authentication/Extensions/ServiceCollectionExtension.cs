﻿namespace Hiro.Services.Extensions
{
    using System;
    using System.Linq;
    using System.Text;
    using Hiro.Domain.Dtos;
    using Hiro.Services.Authorization.Requirements;
    using Hiro.Shared.Extensions;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;

    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddHiroJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services
               .AddAuthentication(options =>
               {
                   options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                   options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                   options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
               })
               .AddJwtBearer(options =>
               {
                   options.RequireHttpsMetadata = false;
                   options.SaveToken = true;
                   var jwtConfigSection = configuration.GetSection("JwtOptions");
                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfigSection["SecretKey"])),
                       ValidIssuer = jwtConfigSection["Issuer"],
                       ValidAudience = jwtConfigSection["Audience"],
                       ClockSkew = TimeSpan.Zero,
                       ValidateIssuer = true,
                       ValidateAudience = true,
                       ValidateLifetime = true,
                       ValidateIssuerSigningKey = true,
                   };
               });

            return services;
        }

        public static IServiceCollection AddHiroAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                foreach (var property in typeof(UserRoleDto).GetProperties().Where(x => x.GetDisplayAttribute()?.Name == "Permission"))
                {
                    options.AddPolicy(property.Name, policy => policy.Requirements.Add(new UserRolePermissionRequirement(property.Name)));
                }
            });

            return services;
        }
    }
}
