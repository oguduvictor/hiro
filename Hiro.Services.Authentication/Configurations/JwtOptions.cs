﻿namespace Hiro.Services.Configurations
{
    public class JwtOptions
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string SecretKey { get; set; }
        public int ValidityInDays { get; set; }
    }
}
