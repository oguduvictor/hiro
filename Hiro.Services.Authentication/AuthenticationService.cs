﻿using Hiro.Shared.Constants;

namespace Hiro.Services
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using Hiro.DataAccess;
    using Hiro.Domain;
    using Hiro.Services.Configurations;
    using Hiro.Services.Enums;
    using Hiro.Services.Interfaces;
    using Hiro.Services.Validators.Interfaces;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Extensions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;

    public class AuthenticationService : IAuthenticationService
    {
        private readonly JwtOptions jwtConfig;
        private readonly HiroDbContext dbContext;
        private readonly IAuthenticateUserValidator authenticateUserValidator;
        private readonly IEncryptionService encryptionService;
        private readonly IRegisterUserValidator registerUserValidator;
        private readonly HttpContext httpContext;

        public AuthenticationService(
            HiroDbContext dbContext,
            IOptionsMonitor<JwtOptions> jwtConfigOptions,
            IAuthenticateUserValidator authenticateUserValidator,
            IEncryptionService encryptionService,
            IRegisterUserValidator registerUserValidator,
            IHttpContextAccessor httpContextAccessor)
        {
            this.dbContext = dbContext;
            this.jwtConfig = jwtConfigOptions.CurrentValue;
            this.authenticateUserValidator = authenticateUserValidator;
            this.encryptionService = encryptionService;
            this.registerUserValidator = registerUserValidator;
            this.httpContext = httpContextAccessor.HttpContext;
        }

        public virtual User GetAuthenticatedUser()
        {
            var userIdClaim = this.httpContext.User.FindFirst(HiroJwtRegisteredClaimNames.UserId)?.Value;

            if (string.IsNullOrEmpty(userIdClaim))
            {
                return null;
            }

            try
            {
                return this.dbContext.Users.Find(Guid.Parse(userIdClaim));
            }
            catch
            {
                throw new Exception("Invalid UserId Claim Value");
            }
        }

        public virtual AuthenticationType GetAuthenticationType()
        {
            var authenticationTypeClaim = this.httpContext.User.Claims.FirstOrDefault(x => x.Type == HiroJwtRegisteredClaimNames.AuthenticationType);

            var parsed = Enum.TryParse<AuthenticationType>(authenticationTypeClaim?.Value, out var authenticationType);

            if (parsed)
            {
                return authenticationType;
            }

            return AuthenticationType.None;
        }

        public virtual OperationResult<User> AuthenticateUser(string email, string password)
        {
            var errors = this.authenticateUserValidator.Validate(email, password);

            if (errors.Any())
            {
                return new OperationResult<User>(errors);
            }

            var user = this.dbContext.Users.First(x => x.Email.Equals(email));

            var authClaims = this.CreateAuthenticationClaims(user, AuthenticationType.Full);

            this.httpContext.User = new ClaimsPrincipal(new ClaimsIdentity(authClaims, "jwt"));

            return new OperationResult<User>
            {
                Data = user
            };
        }

        public OperationResult<User> PartiallyAuthenticateUser(User user)
        {
            var authClaims = this.CreateAuthenticationClaims(user, AuthenticationType.Partial);

            this.httpContext.User = new ClaimsPrincipal(new ClaimsIdentity(authClaims, "jwt"));

            return new OperationResult<User>
            {
                Data = user
            };
        }

        public virtual OperationResult<User> RegisterUser(string firstName, string lastName, string email, string password, UserRole role, Country country)
        {
            var operationResult = new OperationResult<User>
            {
                Errors = this.registerUserValidator.Validate(firstName, lastName, email, password)
            };

            if (operationResult.Errors.Any())
            {
                return operationResult;
            }

            var user = new User
            {
                FirstName = firstName?.Trim().ToLower().ToUpperFirstLetter(),
                LastName = lastName?.Trim().ToLower().ToUpperFirstLetter(),
                Email = email?.ToPrettyEmailAddress(),
                Password = this.encryptionService.Encrypt(password),
                LastLogin = DateTime.Now,
                Role = role,
                Country = country
            };

            this.dbContext.Users.Add(user);

            this.dbContext.SaveChanges();

            operationResult.Data = user;

            return operationResult;
        }

        public OperationResult<User> PartiallyRegisterUser(string firstName = null, string lastName = null, string email = null, Guid? countryId = null)
        {
            var operationResult = new OperationResult<User>
            {
                //todo
                //Errors = this.partiallyRegisterUserValidator.Validate(firstName, lastName, email, countryId)
            };

            if (operationResult.Errors.Any())
            {
                return operationResult;
            }

            var user = new User
            {
                FirstName = firstName?.Trim().ToLower().ToUpperFirstLetter(),
                LastName = lastName?.Trim().ToLower().ToUpperFirstLetter(),
                Email = email?.ToPrettyEmailAddress(),
                LastLogin = DateTime.Now,
                Role = this.dbContext.UserRoles.FirstOrDefault(x => x.Name.Equals(UserRoleNames.Default)),
                Country = countryId == null ? null : this.dbContext.Countries.Find(countryId)
            };

            this.dbContext.Users.Add(user);

            this.dbContext.SaveChanges();

            operationResult.Data = user;

            return operationResult;
        }

        public virtual Claim[] CreateAuthenticationClaims(User user, AuthenticationType authType)
        {
            return new[]
            {
                new Claim(HiroJwtRegisteredClaimNames.UserId, user.Id.ToString()),
                new Claim(HiroJwtRegisteredClaimNames.AuthenticationType, authType.ToString())
            };
        }

        public virtual string CreateAuthenticationToken(User user, AuthenticationType authType)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.jwtConfig.SecretKey));

            var expirationDate = DateTime.Now.AddDays(this.jwtConfig.ValidityInDays);

            var jwtSecurityToken = new JwtSecurityToken(
                this.jwtConfig.Issuer,
                this.jwtConfig.Audience,
                expires: expirationDate,
                claims: this.CreateAuthenticationClaims(user, authType),
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }
    }
}