﻿namespace Hiro.Services.Enums
{
    public enum AuthenticationType
    {
        /// <summary>
        /// No authentication Token found.
        /// </summary>
        None,

        /// <summary>
        /// Partial authentication Token.
        /// </summary>
        Partial,

        /// <summary>
        /// Full authentication Token.
        /// </summary>
        Full
    }
}