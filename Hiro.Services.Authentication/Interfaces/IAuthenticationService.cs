﻿#pragma warning disable 1591

namespace Hiro.Services.Interfaces
{
    using System;
    using System.Security.Claims;
    using Hiro.Domain;
    using Hiro.Services.Enums;
    using Hiro.Shared.Dtos;
    using JetBrains.Annotations;

    public interface IAuthenticationService
    {
        [CanBeNull]
        User GetAuthenticatedUser();

        AuthenticationType GetAuthenticationType();

        OperationResult<User> AuthenticateUser(string email, string password);

        OperationResult<User> PartiallyAuthenticateUser(User user);

        OperationResult<User> RegisterUser(string firstName, string lastName, string email, string password, UserRole role, Country country);

        OperationResult<User> PartiallyRegisterUser(string firstName = null, string lastName = null, string email = null, Guid? countryId = null);

        Claim[] CreateAuthenticationClaims(User user, AuthenticationType authType);

        string CreateAuthenticationToken(User user, AuthenticationType authType);
    }
}