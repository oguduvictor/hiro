﻿namespace Hiro.Services.Authorization.Requirements
{
    using Microsoft.AspNetCore.Authorization;

    public class UserRolePermissionRequirement : IAuthorizationRequirement
    {
        public UserRolePermissionRequirement(string permissionName)
        {
            this.PermissionName = permissionName;
        }

        public string PermissionName { get; set; }
    }
}
