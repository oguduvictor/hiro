﻿namespace Hiro.Services.Authorization.Handlers
{
    using System;
    using System.Threading.Tasks;
    using Hiro.DataAccess;
    using Hiro.Services.Authorization.Requirements;
    using Hiro.Services.Enums;
    using Hiro.Shared.Constants;
    using Hiro.Shared.Extensions;
    using Microsoft.AspNetCore.Authorization;

    public class UserRolePermissionHandler : AuthorizationHandler<UserRolePermissionRequirement>
    {
        private readonly HiroDbContext dbContext;

        public UserRolePermissionHandler(HiroDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserRolePermissionRequirement requirement)
        {
            var userIdClaim = context.User.FindFirst(HiroJwtRegisteredClaimNames.UserId);
            var authTypeClaim = context.User.FindFirst(HiroJwtRegisteredClaimNames.AuthenticationType);

            if (userIdClaim != null && authTypeClaim?.Value == AuthenticationType.Full.ToString())
            {
                Guid.TryParse(userIdClaim.Value, out var userId);

                var userRole = this.dbContext.Users.Find(userId)?.Role;

                if (userRole != null)
                {
                    var property = userRole.GetType().GetProperty(requirement.PermissionName);

                    if (property != null)
                    {
                        var value = property.GetValue(userRole).TryCast<bool>();

                        if (value)
                        {
                            context.Succeed(requirement);
                        }
                    }
                }
            }

            return Task.CompletedTask;
        }
    }
}
