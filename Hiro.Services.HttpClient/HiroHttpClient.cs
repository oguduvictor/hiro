﻿#pragma warning disable 1591
namespace Hiro.Services
{
    using System;
    using System.Net.Http;
    using System.Net.Mime;
    using System.Text;
    using System.Threading.Tasks;
    using Hiro.Admin.Shared.Constants;
    using Hiro.Services.Options;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Extensions;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class HiroHttpClient
    {
        private readonly HttpClient httpClient;
        private readonly HiroHttpClientOptions httpClientOptions;

        public HiroHttpClient(
            HttpClient httpClient,
            IOptionsMonitor<HiroHttpClientOptions> httpClientOptionsMonitor)
        {
            this.httpClient = httpClient;
            this.httpClientOptions = httpClientOptionsMonitor.CurrentValue;
        }

        public async Task<T> PostAsync<T>(string path, object query, object body)
        {
            var requestUri = new Uri($"{this.httpClientOptions.ApiBaseUrl}/{path}{query?.ToQueryString()}");

            var response = await this.httpClient.PostAsync(requestUri, GetJsonData(body));

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();

                return string.IsNullOrEmpty(responseContent) ? default : JsonConvert.DeserializeObject<T>(responseContent);
            }

            throw new HttpRequestException(response.StatusCode.ToString());
        }

        public async Task<OperationResult> PostFilesAsync(MultipartFormDataContent formDataContent)
        {
            var requestUri = new Uri($"{this.httpClientOptions.ApiBaseUrl}/{HiroAdminApiRoutes.AdminMediaUploadFileForm}");

            var response = await this.httpClient.PostAsync(requestUri, formDataContent);

            var responseContent = await response.Content.ReadAsStringAsync();

            response.EnsureSuccessStatusCode();

            if (!string.IsNullOrEmpty(responseContent))
            {
                return JsonConvert.DeserializeObject<OperationResult>(responseContent);
            }

            return null;
        }

        private static StringContent GetJsonData(object data)
        {
            if (data == null)
            {
                return null;
            }

            return new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, MediaTypeNames.Application.Json);
        }
    }
}