﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiro.Services.Options
{
    public class HiroHttpClientOptions
    {
        public string ApiBaseUrl { get; set; }
    }
}
