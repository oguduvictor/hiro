﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class CartListDto : PaginatedListDto
    {
        [NotNull]
        public List<CartDto> Carts { get; set; } = new List<CartDto>();

        [NotNull]
        public List<ShippingBoxDto> ShippingBoxes { get; set; } = new List<ShippingBoxDto>();

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();
    }
}