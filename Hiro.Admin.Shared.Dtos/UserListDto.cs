﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class UserListDto : PaginatedListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<UserDto> Users { get; set; } = new List<UserDto>();
    }
}