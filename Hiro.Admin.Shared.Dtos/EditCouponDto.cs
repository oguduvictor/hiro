﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditCouponDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public CouponDto Coupon { get; set; }

        public int Quantity { get; set; }

        [NotNull]
        public List<CategoryDto> AllCategories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<CountryDto> AllCountries { get; set; } = new List<CountryDto>();

        [NotNull]
        public List<UserDto> AllUsers { get; set; } = new List<UserDto>();

    }
}