﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditCountryDto
    {
        public List<CountryDto> AllLocalizedCountries { get; set; }

        public List<CategoryDto> AllCategories { get; set; }

        public UserDto AuthenticatedUser { get; set; }

        public CountryDto Country { get; set; }

        public IEnumerable<string> FlagIcons { get; set; }

        public IEnumerable<string> Cultures { get; set; }
    }
}