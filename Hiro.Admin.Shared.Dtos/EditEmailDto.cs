﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditEmailDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public EmailDto Email { get; set; }

        [NotNull]
        public List<string> ViewNames { get; set; } = new List<string>();

        public List<ContentSectionDto> AllContentSections { get; set; } = new List<ContentSectionDto>();

        public List<string> ContentSectionsSchemaNames { get; set; } = new List<string>();
    }
}