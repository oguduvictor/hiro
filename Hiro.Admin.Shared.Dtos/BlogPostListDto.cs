﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class BlogPostListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<BlogPostDto> BlogPosts { get; set; }
    }
}