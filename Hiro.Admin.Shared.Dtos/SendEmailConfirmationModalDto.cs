﻿using System;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class SendEmailConfirmationModalDto
    {
        /// <summary>
        /// Gets or Sets the Id of the entity used as data context when sending the email.
        /// </summary>
        [NotNull]
        public Guid EntityId { get; set; }

        [NotNull]
        public string EmailName { get; set; }

        public string SendTo { get; set; }
    }
}
