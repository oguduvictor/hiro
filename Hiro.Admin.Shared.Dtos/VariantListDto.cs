﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class VariantListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<VariantDto> Variants { get; set; }
    }
}