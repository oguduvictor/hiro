﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditBlogPostDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public BlogPostDto BlogPost { get; set; }

        public List<UserDto> Users { get; set; }

        public List<BlogCategoryDto> AllBlogCategories { get; set; }

        [CanBeNull]
        public string PostPreviewLink { get; set; }
    }
}