﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditVariantOptionDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<string> AllTags { get; set; }

        public VariantOptionDto VariantOption { get; set; }
    }
}