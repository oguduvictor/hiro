﻿#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class RenameFileDto
    {
        public string LocalPath { get; set; }

        public string NewName { get; set; }
    }
}