﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditOrderDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public OrderDto Order { get; set; }

        [NotNull]
        public List<EmailDto> AllEmails { get; set; } = new List<EmailDto>();
    }
}