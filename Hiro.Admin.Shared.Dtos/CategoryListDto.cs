﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class CategoryListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();
    }
}