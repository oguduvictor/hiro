﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Shared.Dtos
{
    public class SendEmailDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<CountryDto> AllCountries { get; set; } = new List<CountryDto>();

        public List<MailingListDto> AllMailingLists { get; set; } = new List<MailingListDto>();

        public List<UserDto> AllUsers { get; set; } = new List<UserDto>();
    }
}