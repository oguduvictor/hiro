﻿using System.Collections.Generic;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class ResizeFileDto
    {
        public IEnumerable<string> LocalPaths { get; set; }

        public int Width { get; set; }

        public int Compression { get; set; }

        [CanBeNull]
        public string Destination { get; set; }

        public bool KeepOriginal { get; set; }
    }
}