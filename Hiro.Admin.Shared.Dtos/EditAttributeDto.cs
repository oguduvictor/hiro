﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditAttributeDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<string> AllTags { get; set; }

        public AttributeDto Attribute { get; set; }

        public AttributeOptionDto NewAttributeOption { get; set; }
    }
}