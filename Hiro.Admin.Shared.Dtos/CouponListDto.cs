﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class CouponListDto : PaginatedListDto
    {
        public List<CouponDto> Coupons { get; set; }
    }
}