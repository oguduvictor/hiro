﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditCategoryDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public CategoryDto Category { get; set; }

        public List<string> AllTags { get; set; }

        [NotNull]
        public List<CategoryDto> AllCategories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<ProductDto> AllProducts { get; set; } = new List<ProductDto>();

        [NotNull]
        public List<ProductImageKitDto> AllProductImageKits { get; set; } = new List<ProductImageKitDto>();

        [NotNull]
        public List<ProductStockUnitDto> AllProductStockUnits { get; set; } = new List<ProductStockUnitDto>();
    }
}