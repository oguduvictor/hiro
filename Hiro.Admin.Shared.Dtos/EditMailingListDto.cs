﻿using Hiro.Domain.Dtos;

namespace Hiro.Admin.Shared.Dtos
{
    public class EditMailingListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public MailingListDto MailingList { get; set; }
    }
}