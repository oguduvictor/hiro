﻿using System;
using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditProductStockUnitsDto
    {
        [NotNull]
        public List<ProductStockUnitDto> ProductStockUnits { get; set; } = new List<ProductStockUnitDto>();

        /// <summary>
        /// Gets or sets the Variants affecting the stock.
        /// </summary>
        public List<VariantDto> Variants { get; set; }

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }
    }
}