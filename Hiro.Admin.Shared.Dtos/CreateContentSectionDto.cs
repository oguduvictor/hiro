﻿using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class CreateContentSectionDto
    {
        public OperationResult OperationResult { get; set; }

        public ContentSectionDto SimilarContentSection { get; set; }

        public ContentSectionDto SavedContentSection { get; set; }
    }
}