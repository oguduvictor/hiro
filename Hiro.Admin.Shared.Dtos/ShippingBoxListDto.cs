﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class ShippingBoxListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<ShippingBoxDto> ShippingBoxes { get; set; }
    }
}