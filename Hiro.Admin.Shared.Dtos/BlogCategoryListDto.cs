﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class BlogCategoryListDto
    {
        public List<BlogCategoryDto> BlogCategories { get; set; }
    }
}