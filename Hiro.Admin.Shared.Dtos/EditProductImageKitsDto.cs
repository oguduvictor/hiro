﻿using System;
using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditProductImageKitsDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ProductImageKitDto> ProductImageKits { get; set; } = new List<ProductImageKitDto>();

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }
    }
}