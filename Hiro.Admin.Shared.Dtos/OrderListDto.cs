﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class OrderListDto : PaginatedListDto
    {
        [NotNull]
        public List<OrderDto> Orders { get; set; } = new List<OrderDto>();

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();
    }
}