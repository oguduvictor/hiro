﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditVariantDto
    {
        public List<string> AllVariantSelectors { get; set; }

        public UserDto AuthenticatedUser { get; set; }

        public VariantDto Variant { get; set; }
    }
}