﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class ProductListDto : PaginatedListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<CategoryDto> AllCategories { get; set; }

        public List<ProductDto> Products { get; set; } = new List<ProductDto>();

        public List<string> LocalizedCountries { get; set; } = new List<string>();
    }
}