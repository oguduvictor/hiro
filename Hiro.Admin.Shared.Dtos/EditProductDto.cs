﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditProductDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public ProductDto Product { get; set; }

        public List<string> AllTags { get; set; }

        [NotNull]
        public List<ShippingBoxDto> AllShippingBoxes { get; set; } = new List<ShippingBoxDto>();

        [NotNull]
        public List<ProductDto> AllProducts { get; set; } = new List<ProductDto>();

        [NotNull]
        public List<CategoryDto> AllCategories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<ProductImageKitDto> AllProductImageKits { get; set; } = new List<ProductImageKitDto>();
    }
}