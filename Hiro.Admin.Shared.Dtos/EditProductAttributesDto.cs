﻿using System;
using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditProductAttributesDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ProductAttributeDto> ProductAttributes { get; set; } = new List<ProductAttributeDto>();

        public List<AttributeDto> AllAttributes { get; set; } = new List<AttributeDto>();

        public List<ContentSectionDto> AllContentSections { get; set; } = new List<ContentSectionDto>();

        public List<string> ContentSectionsSchemaNames { get; set; } = new List<string>();

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }
    }
}