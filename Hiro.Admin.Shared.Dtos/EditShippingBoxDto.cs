﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditShippingBoxDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public ShippingBoxDto ShippingBox { get; set; }

        public List<CountryDto> AllCountries { get; set; }

        public List<ProductDto> AllProducts { get; set; }
    }
}