﻿using System;
using System.ComponentModel.DataAnnotations;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class ResetUserPasswordDto : BaseEntityDto
    {
        public Guid UserId { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}