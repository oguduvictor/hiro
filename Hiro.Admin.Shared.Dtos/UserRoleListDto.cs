﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class UserRoleListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<UserRoleDto> UserRoles { get; set; } = new List<UserRoleDto>();
    }
}