﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditCartDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public CartDto Cart { get; set; }

        [NotNull]
        public List<EmailDto> AllEmails { get; set; } = new List<EmailDto>();
    }
}