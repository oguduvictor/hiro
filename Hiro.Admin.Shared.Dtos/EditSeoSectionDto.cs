﻿using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditSeoSectionDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public SeoSectionDto SeoSection { get; set; }
    }
}