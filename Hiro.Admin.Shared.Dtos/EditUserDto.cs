﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using JetBrains.Annotations;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditUserDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public UserDto User { get; set; }

        [NotNull]
        public List<UserRoleDto> UserRoles { get; set; } = new List<UserRoleDto>();

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();

        [NotNull]
        public List<OrderDto> Orders { get; set; } = new List<OrderDto>();

        [NotNull]
        public virtual List<UserLocalizedKitDto> UserLocalizedKits { get; set; } = new List<UserLocalizedKitDto>();

    }
}