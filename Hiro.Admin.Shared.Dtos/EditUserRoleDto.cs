﻿using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Shared.Dtos
{
    public class EditUserRoleDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public UserRoleDto UserRole { get; set; }
    }
}