﻿using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Api.Services.Interfaces
{
    public interface ICartMappingService
    {
        CartDto GetCartDto(Country localizationCountry, Cart cart);
        CartItemDto GetCartItemDto(Country localizationCountry, CartItem cartItem);
    }
}