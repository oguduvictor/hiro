﻿using System.Linq;
using Hiro.Api.Services.Interfaces;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Services.Interfaces;
using Hiro.Shared.Extensions;

namespace Hiro.Api.Services
{
    public class CartMappingService : ICartMappingService
    {
        private readonly ICartService cartService;

        public CartMappingService(ICartService cartService)
        {
            this.cartService = cartService;
        }

        public virtual CartDto GetCartDto(Country localizationCountry, Cart cart)
        {
            return cart == null ? null : new CartDto
            {
                Id = cart.Id,
                CouponValue = this.cartService.GetCouponValue(localizationCountry, cart),
                ShippingAddress = cart.ShippingAddress == null ? null : new AddressDto
                {
                    Id = cart.ShippingAddress.Id,
                    FirstName = cart.ShippingAddress.FirstName,
                    LastName = cart.ShippingAddress.LastName,
                    AddressLine1 = cart.ShippingAddress.AddressLine1,
                    AddressLine2 = cart.ShippingAddress.AddressLine2,
                    City = cart.ShippingAddress.City,
                    Country = cart.ShippingAddress.Country == null ? null : new CountryDto
                    {
                        Id = cart.ShippingAddress.Country.Id,
                        Name = cart.ShippingAddress.Country.Name,
                        LanguageCode = cart.ShippingAddress.Country.LanguageCode,
                        IsDefault = cart.ShippingAddress.Country.IsDefault,
                        Localize = cart.ShippingAddress.Country.Localize,
                        Url = cart.ShippingAddress.Country.Url,
                        CurrencySymbol = cart.ShippingAddress.Country.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = cart.ShippingAddress.Country.GetIso4217CurrencySymbol(),
                        Taxes = cart.ShippingAddress.Country.Taxes.Select(tax => new TaxDto
                        {
                            Id = tax.Id,
                            Amount = tax.Amount,
                            ApplyToProductPrice = tax.ApplyToProductPrice,
                            ApplyToShippingPrice = tax.ApplyToShippingPrice,
                            CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                            Code = tax.Code,
                            Country = tax.Country == null ? null : new CountryDto
                            {
                                Id = tax.Country.Id,
                                CurrencySymbol = tax.Country.GetCurrencySymbol(),
                                IsDefault = tax.Country.IsDefault,
                                Iso4217CurrencySymbol = tax.Country.GetIso4217CurrencySymbol(),
                                LanguageCode = tax.Country.LanguageCode,
                                Localize = tax.Country.Localize,
                                Name = tax.Country.Name,
                                Url = tax.Country.Url
                            },
                            Percentage = tax.Percentage,
                            Name = tax.Name
                        }).ToList()
                    },
                    StateCountyProvince = cart.ShippingAddress.StateCountyProvince,
                    Postcode = cart.ShippingAddress.Postcode,
                    PhoneNumber = cart.ShippingAddress.PhoneNumber
                },
                BillingAddress = cart.BillingAddress == null ? null : new AddressDto
                {
                    Id = cart.BillingAddress.Id,
                    FirstName = cart.BillingAddress.FirstName,
                    LastName = cart.BillingAddress.LastName,
                    AddressLine1 = cart.BillingAddress.AddressLine1,
                    AddressLine2 = cart.BillingAddress.AddressLine2,
                    City = cart.BillingAddress.City,
                    Country = cart.BillingAddress.Country == null ? null : new CountryDto
                    {
                        Name = cart.BillingAddress.Country.Name,
                        LanguageCode = cart.BillingAddress.Country.LanguageCode,
                        IsDefault = cart.BillingAddress.Country.IsDefault,
                        Localize = cart.BillingAddress.Country.Localize,
                        Url = cart.BillingAddress.Country.Url,
                        CurrencySymbol = cart.BillingAddress.Country.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = cart.BillingAddress.Country.GetIso4217CurrencySymbol(),
                        Taxes = cart.BillingAddress.Country.Taxes.Select(tax => new TaxDto
                        {
                            Id = tax.Id,
                            Amount = tax.Amount,
                            ApplyToProductPrice = tax.ApplyToProductPrice,
                            ApplyToShippingPrice = tax.ApplyToShippingPrice,
                            CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                            Code = tax.Code,
                            Country = tax.Country == null ? null : new CountryDto
                            {
                                Id = tax.Country.Id,
                                CurrencySymbol = tax.Country.GetCurrencySymbol(),
                                IsDefault = tax.Country.IsDefault,
                                Iso4217CurrencySymbol = tax.Country.GetIso4217CurrencySymbol(),
                                LanguageCode = tax.Country.LanguageCode,
                                Localize = tax.Country.Localize,
                                Name = tax.Country.Name,
                                Url = tax.Country.Url
                            },
                            Percentage = tax.Percentage,
                            Name = tax.Name
                        }).ToList()
                    },
                    StateCountyProvince = cart.BillingAddress.StateCountyProvince,
                    Postcode = cart.BillingAddress.Postcode,
                    PhoneNumber = cart.BillingAddress.PhoneNumber
                },
                CartShippingBoxes = cart.CartShippingBoxes.Select(cartShippingBox => new CartShippingBoxDto
                {
                    Id = cartShippingBox.Id,
                    ShippingBox = cartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                    {
                        Id = cartShippingBox.ShippingBox.Id,
                        Name = cartShippingBox.ShippingBox.Name,
                        RequiresShippingAddress = cartShippingBox.ShippingBox.RequiresShippingAddress,
                        Countries = cartShippingBox.ShippingBox.Countries.Select(country => new CountryDto
                        {
                            Id = country.Id
                        }).ToList(),
                        LocalizedPrice = cartShippingBox.ShippingBox.GetLocalizedPrice(localizationCountry),
                        LocalizedTitle = cartShippingBox.ShippingBox.GetLocalizedTitle(localizationCountry),
                        LocalizedDescription = cartShippingBox.ShippingBox.GetLocalizedDescription(localizationCountry),
                        LocalizedMinDays = cartShippingBox.ShippingBox.GetLocalizedMinDays(localizationCountry),
                        LocalizedMaxDays = cartShippingBox.ShippingBox.GetLocalizedMaxDays(localizationCountry),
                        LocalizedCountWeekends = cartShippingBox.ShippingBox.GetLocalizedCountWeekends(localizationCountry),
                        LocalizedFreeShippingMinimumPrice = cartShippingBox.ShippingBox.GetLocalizedFreeShippingMinimumPrice(localizationCountry)
                    },
                    CartItems = cartShippingBox.CartItems.Select(cartItem => this.GetCartItemDto(localizationCountry, cartItem)).ToList(),
                    ShippingPriceBeforeTax = this.cartService.GetShippingPriceBeforeTax(localizationCountry, cartShippingBox),
                    ShippingPriceAfterTax = this.cartService.GetShippingPriceAfterTax(localizationCountry, cartShippingBox),
                    TotalBeforeTax = this.cartService.GetCartShippingBoxTotalBeforeTax(localizationCountry, cartShippingBox),
                    TotalAfterTax = this.cartService.GetCartShippingBoxTotalAfterTax(localizationCountry, cartShippingBox)
                }).ToList(),
                Coupon = cart.Coupon == null ? null : new CouponDto
                {
                    Id = cart.Coupon.Id,
                    Amount = cart.Coupon.Amount,
                    Percentage = cart.Coupon.Percentage,
                    Code = cart.Coupon.Code,
                    Categories = cart.Coupon.Categories.Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        Type = x.Type
                    }).ToList()
                },
                UserCredit = cart.UserCredit == null ? null : new UserCreditDto
                {
                    Id = cart.UserCredit.Id,
                    Amount = cart.UserCredit.Amount
                },
                Errors = this.cartService.GetErrors(localizationCountry, cart),
                TotalBeforeTax = this.cartService.GetTotalBeforeTax(localizationCountry, cart),
                TotalAfterTax = this.cartService.GetTotalAfterTax(localizationCountry, cart),
                TotalToBePaid = this.cartService.GetTotalToBePaid(localizationCountry, cart)
            };
        }

        public virtual CartItemDto GetCartItemDto(Country localizationCountry, CartItem cartItem)
        {
            if (cartItem == null)
            {
                return null;
            }

            return new CartItemDto
            {
                Id = cartItem.Id,
                Quantity = cartItem.Quantity,
                Image = cartItem.Image,
                MinEta = cartItem.GetMinEta(localizationCountry),
                MaxEta = cartItem.GetMaxEta(localizationCountry),
                CouponApplies = this.cartService.GetCartItemCouponApplies(localizationCountry, cartItem),
                CartShippingBox = cartItem.CartShippingBox == null ? null : new CartShippingBoxDto
                {
                    Id = cartItem.CartShippingBox.Id,
                    ShippingBox = cartItem.CartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                    {
                        Id = cartItem.CartShippingBox.ShippingBox.Id,
                        Name = cartItem.CartShippingBox.ShippingBox.Name,
                        RequiresShippingAddress = cartItem.CartShippingBox.ShippingBox.RequiresShippingAddress,
                        LocalizedTitle = cartItem.CartShippingBox.ShippingBox.GetLocalizedTitle(localizationCountry),
                        LocalizedDescription = cartItem.CartShippingBox.ShippingBox.GetLocalizedDescription(localizationCountry),
                        LocalizedPrice = cartItem.CartShippingBox.ShippingBox.GetLocalizedPrice(localizationCountry)
                    }
                },
                Product = cartItem.Product == null ? null : new ProductDto
                {
                    Id = cartItem.Product.Id,
                    Name = cartItem.Product.Name,
                    Url = cartItem.Product.Url,
                    Code = cartItem.Product.Code,
                    TaxCode = cartItem.Product.TaxCode,
                    Categories = cartItem.Product.Categories.Select(category => new CategoryDto
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Url = category.Url,
                        LocalizedTitle = category.GetLocalizedTitle(localizationCountry)
                    }).ToList(),
                    ProductAttributes = cartItem.Product.ProductAttributes.Select(productAttribute => new ProductAttributeDto
                    {
                        Id = productAttribute.Id,
                        Attribute = productAttribute.Attribute == null ? null : new AttributeDto
                        {
                            Id = productAttribute.Attribute.Id,
                            Name = productAttribute.Attribute.Name,
                            Url = productAttribute.Attribute.Url,
                            Type = productAttribute.Attribute.Type
                        },
                        AttributeOptionValue = productAttribute.AttributeOptionValue == null ? null : new AttributeOptionDto
                        {
                            Id = productAttribute.AttributeOptionValue.Id,
                            Name = productAttribute.AttributeOptionValue.Name,
                            Url = productAttribute.AttributeOptionValue.Url,
                            LocalizedTitle = productAttribute.AttributeOptionValue.GetLocalizedTitle(localizationCountry)
                        },
                        ContentSectionValue = productAttribute.ContentSectionValue == null ? null : new ContentSectionDto
                        {
                            Id = productAttribute.ContentSectionValue.Id,
                            Name = productAttribute.ContentSectionValue.Name,
                            LocalizedContent = productAttribute.ContentSectionValue.GetLocalizedContent(localizationCountry)
                        },
                        DoubleValue = productAttribute.DoubleValue,
                        StringValue = productAttribute.StringValue,
                        ImageValue = productAttribute.ImageValue,
                        BooleanValue = productAttribute.BooleanValue,
                        DateTimeValue = productAttribute.DateTimeValue
                    }).ToList(),
                    ProductVariants = cartItem.Product.ProductVariants.Select(productVariant => new ProductVariantDto
                    {
                        Id = productVariant.Id,
                        Variant = productVariant.Variant == null ? null : new VariantDto
                        {
                            Id = productVariant.Variant.Id,
                            Name = productVariant.Variant.Name,
                            Type = productVariant.Variant.Type,
                            Url = productVariant.Variant.Url,
                            CreateProductImageKits = productVariant.Variant.CreateProductImageKits,
                            CreateProductStockUnits = productVariant.Variant.CreateProductStockUnits
                        },
                        VariantOptions = productVariant
                                .VariantOptions
                                .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled(localizationCountry))
                                .OrderBySortOrder()
                                .ThenBy(x => x.Name)
                                .Select(variantOption => new VariantOptionDto
                                {
                                    Id = variantOption.Id,
                                    Name = variantOption.Name,
                                    Url = variantOption.Url,
                                    Image = variantOption.Image,
                                    LocalizedTitle = variantOption.GetLocalizedTitle(localizationCountry),
                                    LocalizedDescription = variantOption.GetLocalizedDescription(localizationCountry),
                                    LocalizedPriceModifier = variantOption.GetLocalizedPriceModifier(localizationCountry)
                                }).ToList()
                    }).ToList(),
                    ShippingBoxes = cartItem.Product.ShippingBoxes.OrderBySortOrder().Select(shippingBox => new ShippingBoxDto
                    {
                        Id = shippingBox.Id,
                        Name = shippingBox.Name,
                        RequiresShippingAddress = shippingBox.RequiresShippingAddress,
                        LocalizedTitle = shippingBox.GetLocalizedTitle(localizationCountry),
                        LocalizedDescription = shippingBox.GetLocalizedDescription(localizationCountry),
                        LocalizedPrice = shippingBox.GetLocalizedPrice(localizationCountry),
                        LocalizedFreeShippingMinimumPrice = shippingBox.GetLocalizedFreeShippingMinimumPrice(localizationCountry),
                        LocalizedMinDays = shippingBox.GetLocalizedMinDays(localizationCountry),
                        LocalizedMaxDays = shippingBox.GetLocalizedMaxDays(localizationCountry),
                        Countries = shippingBox.Countries.Select(country => new CountryDto
                        {
                            Id = country.Id,
                            Name = country.Name
                        }).ToList()
                    }).ToList(),
                    ProductStockUnits = cartItem.Product.ProductStockUnits.Select(productStockUnit => new ProductStockUnitDto
                    {
                        Id = productStockUnit.Id,
                        DispatchDate = productStockUnit.DispatchDate,
                        EnablePreorder = productStockUnit.EnablePreorder,
                        DispatchTime = productStockUnit.DispatchTime,
                        Stock = productStockUnit.Stock,
                        Code = productStockUnit.Code,
                        LocalizedBasePrice = productStockUnit.GetLocalizedBasePrice(localizationCountry),
                        LocalizedSalePrice = productStockUnit.GetLocalizedSalePrice(localizationCountry),
                        VariantOptions = productStockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Name = variantOption.Name,
                            Url = variantOption.Url,
                            Variant = variantOption.Variant == null ? null : new VariantDto
                            {
                                Id = variantOption.Variant.Id
                            }
                        }).ToList()
                    }).ToList(),
                    ProductImageKits = cartItem.Product.ProductImageKits.Select(productImageKit => new ProductImageKitDto
                    {
                        Id = productImageKit.Id,
                        VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Name = variantOption.Name,
                            Url = variantOption.Url
                        }).ToList(),
                        ProductImages = productImageKit.ProductImages.OrderBySortOrder().Select(productImage => new ProductImageDto
                        {
                            Id = productImage.Id,
                            Name = productImage.Name,
                            Url = productImage.Url,
                            SortOrder = productImage.SortOrder
                        }).ToList()
                    }).ToList(),
                    LocalizedTitle = cartItem.Product.GetLocalizedTitle(localizationCountry),
                    LocalizedDescription = cartItem.Product.GetLocalizedDescription(localizationCountry),
                    LocalizedMetaTitle = cartItem.Product.GetLocalizedMetaTitle(localizationCountry),
                    LocalizedMetaDescription = cartItem.Product.GetLocalizedMetaDescription(localizationCountry)
                },
                CartItemVariants = cartItem.CartItemVariants.Select(cartItemVariant => new CartItemVariantDto
                {
                    Id = cartItemVariant.Id,
                    CartItem = cartItemVariant.CartItem == null ? null : new CartItemDto
                    {
                        Id = cartItemVariant.CartItem.Id
                    },
                    Variant = cartItemVariant.Variant == null ? null : new VariantDto
                    {
                        Id = cartItemVariant.Variant.Id,
                        Name = cartItemVariant.Variant.Name,
                        Type = cartItemVariant.Variant.Type,
                        Url = cartItemVariant.Variant.Url,
                        LocalizedTitle = cartItemVariant.Variant.GetLocalizedTitle(localizationCountry)
                    },
                    VariantOptionValue = cartItemVariant.VariantOptionValue == null ? null : new VariantOptionDto
                    {
                        Id = cartItemVariant.VariantOptionValue.Id,
                        Name = cartItemVariant.VariantOptionValue.Name,
                        Url = cartItemVariant.VariantOptionValue.Url,
                        Image = cartItemVariant.VariantOptionValue.Image,
                        LocalizedTitle = cartItemVariant.VariantOptionValue.GetLocalizedTitle(localizationCountry),
                        LocalizedDescription = cartItemVariant.VariantOptionValue.GetLocalizedDescription(localizationCountry),
                        LocalizedPriceModifier = cartItemVariant.VariantOptionValue.GetLocalizedPriceModifier(localizationCountry)
                    },
                    BooleanValue = cartItemVariant.BooleanValue,
                    DoubleValue = cartItemVariant.DoubleValue,
                    IntegerValue = cartItemVariant.IntegerValue,
                    StringValue = cartItemVariant.StringValue,
                    JsonValue = cartItemVariant.JsonValue
                }).ToList(),
                SubtotalBeforeTax = this.cartService.GetCartItemSubtotalBeforeTax(localizationCountry, cartItem),
                SubtotalAfterTax = this.cartService.GetCartItemSubtotalAfterTax(localizationCountry, cartItem),
                SubtotalBeforeTaxWithoutSalePrice = this.cartService.GetCartItemSubtotalBeforeTaxWithoutSalePrice(localizationCountry, cartItem),
                SubtotalAfterTaxWithoutSalePrice = this.cartService.GetCartItemSubtotalAfterTaxWithoutSalePrice(localizationCountry, cartItem),
                TotalBeforeTax = this.cartService.GetCartItemTotalBeforeTax(localizationCountry, cartItem),
                TotalAfterTax = this.cartService.GetCartItemTotalAfterTax(localizationCountry, cartItem)
            };
        }
    }
}