﻿namespace Hiro.Admin.Shared.Constants
{
    public class HiroAdminRoutes
    {
        public const string AdminAuthentication = "/admin/authentication";
        public const string AdminRegistration = "/admin/registration";
        public const string CountryUrlAdminAuthentication = "/{CountryUrl}/admin/authentication";
        public const string CountryUrlAdminRegistration = "/{CountryUrl}/admin/registration";

        public const string AdminContentSections = "/admin/contentsections";
        public const string AdminContentSection = "/admin/contentsection";
        public const string AdminContentSectionId = "/admin/contentsection/{Id:guid}";
        public const string CountryUrlAdminContentSections = "/{CountryUrl}/admin/contentsections";
        public const string CountryUrlAdminContentSection = "/{CountryUrl}/admin/contentsection";
        public const string CountryUrlAdminContentSectionId = "/{CountryUrl}/admin/contentsection/{Id:guid}";

        public const string AdminNotificationId = "/admin/notification/{Id:guid}";
        public const string AdminNotifications = "/admin/notifications";
        public const string CountryUrlAdminNotificationId = "/{CountryUrl}/admin/notification/{Id:guid}";
        public const string CountryUrlAdminNotifications = "/{CountryUrl}/admin/notifications";

        public const string AdminCartId = "/admin/carts/{Id:guid}";
        public const string AdminCarts = "/admin/carts";
        public const string CountryUrlAdminCartId = "/{CountryUrl}/admin/carts/{Id:guid}";
        public const string CountryUrlAdminCarts = "/{CountryUrl}/admin/carts";

        public const string AdminOrderId = "/admin/order/{Id:guid}";
        public const string AdminOrders = "/admin/orders";
        public const string CountryUrlAdminOrderId = "/{CountryUrl}/admin/order/{Id:guid}";
        public const string CountryUrlAdminOrders = "/{CountryUrl}/admin/orders";

        public const string AdminProduct = "/admin/product";
        public const string AdminProductAttributes = "/admin/product/{Id:guid}/attributes";
        public const string AdminProductVariants = "/admin/product/{Id:guid}/variants";
        public const string AdminProductStockUnits = "/admin/product/{Id:guid}/stock-units";
        public const string AdminProductImageKits = "/admin/product/{Id:guid}/image-kits";
        public const string AdminProductId = "/admin/product/{Id:guid}";
        public const string AdminProducts = "/admin/products";

        public const string CountryUrlAdminProduct = "/{CountryUrl}/admin/product";
        public const string CountryUrlAdminProductAttributes = "/{CountryUrl}/admin/product/{Id:guid}/attributes";
        public const string CountryUrlAdminProductVariants = "/{CountryUrl}/admin/product/{Id:guid}/variants";
        public const string CountryUrlAdminProductStockUnits = "/{CountryUrl}/admin/product/{Id:guid}/stock-units";
        public const string CountryUrlAdminProductImageKits = "/{CountryUrl}/admin/product/{Id:guid}/image-kits";
        public const string CountryUrlAdminProductId = "/{CountryUrl}/admin/product/{Id:guid}";
        public const string CountryUrlAdminProducts = "/{CountryUrl}/admin/products";

        public const string AdminCoupon = "/admin/coupon";
        public const string AdminCouponId = "/admin/coupon/{Id:guid}";
        public const string AdminCoupons = "/admin/coupons";
        public const string CountryUrlAdminCoupon = "/{CountryUrl}/admin/coupon";
        public const string CountryUrlAdminCouponId = "/{CountryUrl}/admin/coupon/{Id:guid}";
        public const string CountryUrlAdminCoupons = "/{CountryUrl}/admin/coupons";

        public const string AdminVariant = "/admin/variant";
        public const string AdminVariantId = "/admin/variant/{Id:guid}";
        public const string AdminVariants = "/admin/variants";
        public const string AdminVariantOption = "/admin/variant/{variantId:guid}/variantOption";
        public const string AdminVariantOptionId = "/admin/variant/{variantId:guid}/variantOption/{Id:guid}";
        public const string CountryUrlAdminVariant = "/{CountryUrl}/admin/variant";
        public const string CountryUrlAdminVariantId = "/{CountryUrl}/admin/variant/{Id:guid}";
        public const string CountryUrlAdminVariants = "/{CountryUrl}/admin/variants";
        public const string CountryUrlAdminVariantOption = "/{CountryUrl}/admin/variant/{variantId:guid}/variantOption";
        public const string CountryUrlAdminVariantOptionId = "/{CountryUrl}/admin/variant/{variantId:guid}/variantOption/{Id:guid}";

        public const string AdminSeoSection = "/admin/seo-section";
        public const string AdminSeoSectionId = "/admin/seo-section/{Id:guid}";
        public const string AdminSeoSections = "/admin/seo-sections";
        public const string CountryUrlAdminSeoSection = "/{CountryUrl}/admin/seo-section";
        public const string CountryUrlAdminSeoSectionId = "/{CountryUrl}/admin/seo-section/{Id:guid}";
        public const string CountryUrlAdminSeoSections = "/{CountryUrl}/admin/seo-sections";

        public const string AdminUser = "/admin/user";
        public const string AdminUserId = "/admin/user/{Id:guid}";
        public const string AdminUsers = "/admin/users";
        public const string CountryUrlAdminUser = "/{CountryUrl}/admin/user";
        public const string CountryUrlAdminUserId = "/{CountryUrl}/admin/user/{Id:guid}";
        public const string CountryUrlAdminUsers = "/{CountryUrl}/admin/users";

        public const string AdminCountry = "/admin/country";
        public const string AdminCountryId = "/admin/country/{Id:guid}";
        public const string AdminCountries = "/admin/countries";
        public const string CountryUrlAdminCountry = "/{CountryUrl}/admin/country";
        public const string CountryUrlAdminCountryId = "/{CountryUrl}/admin/country/{Id:guid}";
        public const string CountryUrlAdminCountries = "/{CountryUrl}/admin/countries";

        public const string AdminUserRole = "/admin/user/role";
        public const string AdminUserRoleId = "/admin/user/role/{Id:guid}";
        public const string AdminUserRoles = "/admin/user/roles";
        public const string CountryUrlAdminUserRole = "/{CountryUrl}/admin/user/role";
        public const string CountryUrlAdminUserRoleId = "/{CountryUrl}/admin/user/role/{Id:guid}";
        public const string CountryUrlAdminUserRoles = "/{CountryUrl}/admin/user/roles";

        public const string AdminAttribute = "/admin/attribute";
        public const string AdminAttributeId = "/admin/attribute/{Id:guid}";
        public const string AdminAttributes = "/admin/attributes";
        public const string CountryUrlAdminAttribute = "/{CountryUrl}/admin/attribute";
        public const string CountryUrlAdminAttributeId = "/{CountryUrl}/admin/attribute/{Id:guid}";
        public const string CountryUrlAdminAttributes = "/{CountryUrl}/admin/attributes";

        public const string AdminShippingBox = "/admin/shipping-box";
        public const string AdminShippingBoxes = "/admin/shipping-boxes";
        public const string AdminShippingBoxId = "/admin/shipping-box/{Id:guid}";
        public const string CountryUrlAdminShippingBox = "/{CountryUrl}/admin/shipping-box";
        public const string CountryUrlAdminShippingBoxes = "/{CountryUrl}/admin/shipping-boxes";
        public const string CountryUrlAdminShippingBoxId = "/{CountryUrl}/admin/shipping-box/{Id:guid}";

        public const string AdminCategory = "/admin/category";
        public const string AdminCategories = "/admin/categories";
        public const string AdminCategoryId = "/admin/category/{Id:guid}";
        public const string CountryUrlAdminCategory = "/{CountryUrl}/admin/category";
        public const string CountryUrlAdminCategories = "/{CountryUrl}/admin/categories";
        public const string CountryUrlAdminCategoryId = "/{CountryUrl}/admin/category/{Id:guid}";

        public const string AdminEmail = "/admin/email";
        public const string AdminEmailId = "/admin/email/{Id:guid}";
        public const string AdminEmails = "/admin/emails";
        public const string CountryUrlAdminEmail = "/{CountryUrl}/admin/email";
        public const string CountryUrlAdminEmailId = "/{CountryUrl}/admin/email/{Id:guid}";
        public const string CountryUrlAdminEmails = "/{CountryUrl}/admin/emails";

        public const string AdminBlogCategory = "/admin/blog-category";
        public const string AdminBlogCategories = "/admin/blog-categories";
        public const string AdminBlogCategoryId = "/admin/blog-category/{Id:guid}";
        public const string CountryUrlAdminBlogCategory = "/{CountryUrl}/admin/blog-category";
        public const string CountryUrlAdminBlogCategories = "/{CountryUrl}/admin/blog-categories";
        public const string CountryUrlAdminBlogCategoryId = "/{CountryUrl}/admin/blog-category/{Id:guid}";

        public const string AdminBlogPost = "/admin/blog-post";
        public const string AdminBlogPostId = "/admin/blog-post/{Id:guid}";
        public const string AdminBlogPosts = "/admin/blog-posts";
        public const string CountryUrlAdminBlogPost = "/{CountryUrl}/admin/blog-post";
        public const string CountryUrlAdminBlogPostId = "/{CountryUrl}/admin/blog-post/{Id:guid}";
        public const string CountryUrlAdminBlogPosts = "/{CountryUrl}/admin/blog-posts";

        public const string AdminMedia = "/admin/media";
        public const string CountryUrlAdminMedia = "/{CountryUrl}/admin/media";

        public const string AdminMailingList = "/admin/mailing-list";
        public const string AdminMailingListId = "/admin/mailing-list/{Id:guid}";
        public const string AdminMailingLists = "/admin/mailing-lists";
        public const string CountryUrlAdminMailingList = "/{CountryUrl}/admin/mailing-list";
        public const string CountryUrlAdminMailingListId = "/{CountryUrl}/admin/mailing-list/{Id:guid}";
        public const string CountryUrlAdminMailingLists = "/{CountryUrl}/admin/mailing-lists";

        public const string AdminSettings = "/admin/settings";
        public const string CountryUrlAdminSettings = "/{CountryUrl}/admin/settings";
    }
}