﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hiro.Domain;
using Hiro.Services.Dto;

namespace Hiro.Services.Interfaces
{
    public interface ITagSearchService
    {
        /// <summary>
        ///
        /// </summary>
        IOrderedQueryable<ProductImageKit> GetProductImageKits(Country localizationCountry, List<Tag> tags);

        /// <summary>
        ///
        /// </summary>
        TagGroup GetAttributeTagGroup(Country localizationCountry, IEnumerable<ProductImageKit> productImageKits, string attributeName, int sortOrder);

        /// <summary>
        ///
        /// </summary>
        TagGroup GetVariantTagGroup(Country localizationCountry, IEnumerable<ProductImageKit> productImageKits, string variantName, int sortOrder);

        /// <summary>
        ///
        /// </summary>
        TagGroup GetCategoryTagGroup(Country localizationCountry, IEnumerable<ProductImageKit> productImageKits, int sortOrder);

        /// <summary>
        ///
        /// </summary>
        /// <param name="rootCategoryUrl"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        TagGroup GetCategoryTagGroup(Country localizationCountry, string rootCategoryUrl, int sortOrder);
    }
}
