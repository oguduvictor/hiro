﻿namespace Hiro.Services.Dto
{
    using System.Collections.Generic;

    /// <summary>
    ///
    /// </summary>
    public class TagGroup
    {
        /// <summary>
        ///
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        public List<Tag> Tags { get; set; }
    }
}