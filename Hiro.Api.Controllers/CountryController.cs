﻿using System.Linq;
using Hiro.Api.Shared.Constants;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Hiro.Api.Controllers
{
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly HiroDbContext dbContext;

        public CountryController(HiroDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCountryGetLocalizationCountries)]
        public IActionResult GetLocalizationCountries()
        {
            var countries = this.dbContext.Countries
                    .Where(x => !x.IsDisabled && x.Localize)
                    .OrderBy(x => x.Name)
                    .AsEnumerable()
                    .Select(x => new CountryDto
                    {
                        Id = x.Id,
                        IsDefault = x.IsDefault,
                        FlagIcon = x.FlagIcon,
                        Name = x.Name,
                        Localize = x.Localize,
                        Url = x.Url,
                        LanguageCode = x.LanguageCode,
                        PaymentAccountId = x.PaymentAccountId.IfNullOrEmptyConvertTo("-"),
                        CurrencySymbol = x.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = x.GetIso4217CurrencySymbol()
                    }).ToList();

            return this.Ok(countries);
        }
    
        [HttpPost]
        [Route(HiroApiRoutes.ApiCountryGetCountries)]
        public IActionResult GetCountries()
        {
            var countryDtos = this.dbContext.Countries
                .AsEnumerable()
                .OrderBy(x => x.Name).Select(c => new CountryDto
                {
                    Id = c.Id,
                    LanguageCode = c.LanguageCode,
                    CurrencySymbol = c.GetCurrencySymbol(),
                    FlagIcon = c.FlagIcon,
                    Iso4217CurrencySymbol = c.GetIso4217CurrencySymbol(),
                    Name = c.Name,
                    Localize = c.Localize,
                    IsDisabled = c.IsDisabled,
                    IsDefault = c.IsDefault,
                    Url = c.Url
                });

            return this.Ok(countryDtos);
        }
    }
}