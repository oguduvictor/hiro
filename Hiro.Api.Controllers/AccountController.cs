﻿using System;
using System.Linq;
using Hiro.Api.Shared.Constants;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Services.Enums;
using Hiro.Services.Interfaces;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Hiro.Api.Controllers
{
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAuthenticationService authenticationService;

        public AccountController(
            HiroDbContext dbContext,
            IAuthenticationService authenticationService)
        {
            this.dbContext = dbContext;
            this.authenticationService = authenticationService;
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationGetAuthenticatedUser)]
        public virtual IActionResult GetAuthenticatedUser()
        {
            var user = this.authenticationService.GetAuthenticatedUser();

            if (user == null)
            {
                return this.Ok(user);
            }

            var userDto = new UserDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password,
                Role = user.Role == null ? null : new UserRoleDto
                {
                    Id = user.Role.Id,
                    Name = user.Role.Name,
                    AccessAdmin = user.Role.AccessAdmin
                },
                LastLogin = user.LastLogin,
                Gender = user.Gender,
                LoginProvider = user.LoginProvider,
                Country = user.Country == null ? null : new CountryDto
                {
                    Id = user.Country.Id,
                    Name = user.Country.Name,
                    FlagIcon = user.Country.FlagIcon,
                    Url = user.Country.Url,
                    LanguageCode = user.Country.LanguageCode,
                    IsDefault = user.Country.IsDefault,
                    Localize = user.Country.Localize,
                    CurrencySymbol = user.Country.GetCurrencySymbol(),
                    Iso4217CurrencySymbol = user.Country.GetIso4217CurrencySymbol()
                }
            };

            return this.Ok(userDto);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationGetUserAddresses)]
        public virtual IActionResult GetUserAddresses()
        {
            var user = this.authenticationService.GetAuthenticatedUser().AsNotNull();

            var addresses = user.Addresses
                .OrderByDescending(x => x.Id == user.Cart?.ShippingAddress?.Id)
                .ThenByDescending(x => x.Id == user.Cart?.BillingAddress?.Id)
                .ThenByDescending(x => x.ModifiedDate)
                .Select(x => new AddressDto
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    AddressLine1 = x.AddressLine1,
                    AddressLine2 = x.AddressLine2,
                    City = x.City,
                    Country = x.Country == null ? new CountryDto() : new CountryDto
                    {
                        Id = x.Country.Id,
                        Name = x.Country.Name,
                        IsDefault = x.Country.IsDefault,
                        Localize = x.Country.Localize,
                        IsDisabled = x.Country.IsDisabled
                    },
                    StateCountyProvince = x.StateCountyProvince,
                    Postcode = x.Postcode,
                    PhoneNumber = x.PhoneNumber,
                    IsDisabled = x.IsDisabled,
                    User = x.User == null ? null : new UserDto
                    {
                        Id = x.User.Id,
                        FirstName = x.User.FirstName,
                        LastName = x.User.LastName,
                        Email = x.User.Email
                    }
                }).ToList();

            return this.Ok(addresses);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationGetUserCreditTotal)]
        public virtual IActionResult GetUserCreditTotal(string localizationCountryUrl)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var user = this.authenticationService.GetAuthenticatedUser();

            var creditsTotal = user?.UserLocalizedKits.GetByCountry(localizationCountry)?.UserCredits.Sum(x => x.Amount) ?? 0;

            return this.Ok(creditsTotal);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationAuthenticateUser)]
        public virtual IActionResult AuthenticateUser(string email, string password)
        {
            var authenticateUserOperationResult = this.authenticationService.AuthenticateUser(email, password);

            if (authenticateUserOperationResult.Errors.Any())
            {
                return this.Ok(authenticateUserOperationResult);
            }

            var createAuthenticationTokenOperationResult = new OperationResult<string>
            {
                Data = this.authenticationService.CreateAuthenticationToken(authenticateUserOperationResult.Data, AuthenticationType.Full)
            };

            return this.Ok(createAuthenticationTokenOperationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationPartiallyAuthenticateUser)]
        public virtual IActionResult PartiallyAuthenticateUser(Guid userId)
        {
            var user = this.dbContext.Users.Find(userId);

            if (user == null)
            {
                return this.NotFound();
            }

            var partiallyAuthenticateUserOperationResult = this.authenticationService.PartiallyAuthenticateUser(user);

            if (partiallyAuthenticateUserOperationResult.Errors.Any())
            {
                return this.Ok(partiallyAuthenticateUserOperationResult);
            }

            var createAuthenticationTokenOperationResult = new OperationResult<string>
            {
                Data = this.authenticationService.CreateAuthenticationToken(partiallyAuthenticateUserOperationResult.Data, AuthenticationType.Partial)
            };

            return this.Ok(createAuthenticationTokenOperationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationRegisterUser)]
        public virtual IActionResult RegisterUser(string firstName, string lastName, string email, string password, Guid countryId)
        {
            var registerUserOperationResult = this.authenticationService.RegisterUser(
                firstName,
                lastName,
                email,
                password,
                this.dbContext.UserRoles.FirstOrDefault(x => x.Name == UserRoleNames.Default),
                this.dbContext.Countries.Find(countryId));

            if (registerUserOperationResult.Errors.Any())
            {
                return this.Ok(registerUserOperationResult);
            }

            var createAuthenticationTokenOperationResult = new OperationResult<string>
            {
                Data = this.authenticationService.CreateAuthenticationToken(registerUserOperationResult.Data, AuthenticationType.Full)
            };

            return this.Ok(createAuthenticationTokenOperationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiAuthenticationPartiallyRegisterUser)]
        public virtual IActionResult PartiallyRegisterUser(string firstName, string lastName, string email, Guid countryId)
        {
            var partiallyRegisterUserOperationResult = this.authenticationService.PartiallyRegisterUser(
                firstName,
                lastName,
                email,
                countryId);

            if (partiallyRegisterUserOperationResult.Errors.Any())
            {
                return this.Ok(partiallyRegisterUserOperationResult);
            }

            var createAuthenticationTokenOperationResult = new OperationResult<string>
            {
                Data = this.authenticationService.CreateAuthenticationToken(partiallyRegisterUserOperationResult.Data, AuthenticationType.Full)
            };

            return this.Ok(createAuthenticationTokenOperationResult);
        }
    }
}