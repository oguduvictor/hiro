﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Api.Services.Interfaces;
using Hiro.Api.Shared.Constants;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Services.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Hiro.Api.Controllers
{
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartService cartService;
        private readonly ICartMappingService cartMappingService;
        private readonly HiroDbContext dbContext;
        private readonly IAuthenticationService authenticationService;

        public CartController(
            ICartService cartService,
            HiroDbContext dbContext,
            IAuthenticationService authenticationService,
            ICartMappingService cartMappingService)
        {
            this.cartService = cartService;
            this.dbContext = dbContext;
            this.authenticationService = authenticationService;
            this.cartMappingService = cartMappingService;
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartGetCart)]
        public IActionResult GetCart(string localizationCountryUrl)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart;

            if (cart == null)
            {
                return this.Ok(null);
            }

            var cartDto = this.cartMappingService.GetCartDto(localizationCountry, cart);

            return this.Ok(cartDto);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartGetCartItemFromProduct)]
        public IActionResult GetCartItemFromProduct(string localizationCountryUrl, string productUrl, List<string> selectedVariantOptionUrls = null)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var product = this.dbContext.Products.FirstOrDefault(x => x.Url.Equals(productUrl));

            if (product == null)
            {
                return this.NotFound();
            }

            var variantOptions = selectedVariantOptionUrls?
                .Select(x => this.dbContext.VariantOptions.FirstOrDefault(vo => vo.Url.Equals(x)))
                .Where(x => x != null)
                .ToList();

            var shippingCountry = this.authenticationService.GetAuthenticatedUser()?.Cart?.ShippingAddress?.Country;

            var cartItem = this.cartService.GetCartItemFromProduct(localizationCountry, shippingCountry, product, variantOptions);

            var cartItemDto = this.cartMappingService.GetCartItemDto(localizationCountry, cartItem);

            return this.Ok(cartItemDto);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartAddCartItemToCart)]
        public IActionResult AddCartItemToCart(string localizationCountryUrl, CartItem cartItem)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                       this.cartService.CreateCart(authenticatedUser);

            this.cartService.AddCartItemToCart(localizationCountry, cart, cartItem);

            return this.Ok();
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartUpdateCartItemQuantity)]
        public IActionResult UpdateCartItemQuantity(Guid cartItemId, int quantity)
        {
            this.cartService.UpdateCartItemQuantity(cartItemId, quantity);

            return this.Ok();
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartUpdateCartItem)]
        public IActionResult UpdateCartItem(CartItem cartItem)
        {
            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                       this.cartService.CreateCart(authenticatedUser);

            this.cartService.UpdateCartItem(cart, cartItem);

            return this.Ok();
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartDeleteCartItem)]
        public IActionResult DeleteCartItem(Guid cartItemId)
        {
            this.cartService.DeleteCartItem(cartItemId);

            return this.Ok();
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartApplyCoupon)]
        public IActionResult ApplyCoupon(string localizationCountryUrl, string couponCode)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                this.cartService.CreateCart(authenticatedUser);

            var operationResult = this.cartService.ApplyCoupon(localizationCountry, cart, couponCode);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartRemoveCoupon)]
        public IActionResult RemoveCoupon(string localizationCountryUrl)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                this.cartService.CreateCart(authenticatedUser);

            this.cartService.RemoveCoupon(localizationCountry, cart);

            return this.Ok();
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartApplyUserCredit)]
        public IActionResult ApplyUserCredit(string localizationCountryUrl, double amount)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                this.cartService.CreateCart(authenticatedUser);

            var operationResult = this.cartService.ApplyUserCredit(localizationCountry, cart, amount);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartRemoveCredit)]
        public IActionResult RemoveCredit(string localizationCountryUrl)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                this.cartService.CreateCart(authenticatedUser);

            var operationResult = this.cartService.RemoveCredit(localizationCountry, cart);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartAddOrUpdateAddress)]
        public virtual IActionResult AddOrUpdateAddress(Address address, bool setUser = false, bool setAsCartShippingAddress = false, bool setAsCartBillingAddress = false)
        {
            var authenticatedUser = this.authenticationService.GetAuthenticatedUser();

            if (authenticatedUser == null)
            {
                return this.Unauthorized();
            }

            var cart = authenticatedUser.Cart ??
                       this.cartService.CreateCart(authenticatedUser);

            var addOrUpdateAddressOperationResult = this.cartService.AddOrUpdateAddress(cart, address, setUser, setAsCartShippingAddress, setAsCartBillingAddress);

            if (addOrUpdateAddressOperationResult.Errors.Any())
            {
                return this.Ok(new OperationResult(addOrUpdateAddressOperationResult.Errors));
            }
            else
            {
                var operationResultDto = new OperationResult<AddressDto>();

                var dbAddress = addOrUpdateAddressOperationResult.Data.AsNotNull();

                operationResultDto.Data = new AddressDto
                {
                    Id = dbAddress.Id,
                    FirstName = dbAddress.FirstName,
                    LastName = dbAddress.LastName,
                    AddressLine1 = dbAddress.AddressLine1,
                    AddressLine2 = dbAddress.AddressLine2,
                    City = dbAddress.City,
                    Country = dbAddress.Country == null ? new CountryDto() : new CountryDto
                    {
                        Id = dbAddress.Country.Id,
                        Name = dbAddress.Country.Name,
                        IsDefault = dbAddress.Country.IsDefault,
                        Localize = dbAddress.Country.Localize,
                        IsDisabled = dbAddress.Country.IsDisabled
                    },
                    StateCountyProvince = dbAddress.StateCountyProvince,
                    Postcode = dbAddress.Postcode,
                    PhoneNumber = dbAddress.PhoneNumber,
                    IsDisabled = dbAddress.IsDisabled,
                    User = dbAddress.User == null ? null : new UserDto
                    {
                        Id = dbAddress.User.Id,
                        FirstName = dbAddress.User.FirstName,
                        LastName = dbAddress.User.LastName,
                        Email = dbAddress.User.Email
                    }
                };

                return this.Ok(operationResultDto);
            }
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartDeleteAddress)]
        public virtual IActionResult DeleteAddress(Guid addressId)
        {
            this.dbContext.Addresses.Delete(addressId, true);

            return this.Ok(new OperationResult());
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartSetCartShippingAddress)]
        public virtual IActionResult SetCartShippingAddress(Guid? addressId)
        {
            var operationResult = new OperationResult();

            var user = this.authenticationService.GetAuthenticatedUser();

            var cart = user?.Cart ?? this.cartService.CreateCart(user);

            if (addressId == null)
            {
                if (cart.ShippingAddress == null)
                {
                    return this.Ok(operationResult);
                }

                cart.ShippingAddress = null;

                this.dbContext.SaveChanges();
            }
            else
            {
                cart.ShippingAddress = dbContext.Addresses.Find(addressId);

                dbContext.SaveChanges();
            }

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartSetCartBillingAddress)]
        public virtual IActionResult SetCartBillingAddress(Guid addressId)
        {
            var operationResult = new OperationResult();

            var user = this.authenticationService.GetAuthenticatedUser();

            var cart = user?.Cart ?? this.cartService.CreateCart(user);

            cart.BillingAddress = this.dbContext.Addresses.Find(addressId);

            this.dbContext.SaveChanges();

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiCartSetShippingBox)]
        public virtual IActionResult SetShippingBox(string localizationCountryUrl, Guid cartItemId, string shippingBoxName)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            var cart = this.authenticationService.GetAuthenticatedUser()?.Cart;

            var operationResult = new OperationResult();

            this.cartService.SetShippingBox(localizationCountry, cart, cartItemId, shippingBoxName);

            return this.Ok(operationResult);
        }
    }
}