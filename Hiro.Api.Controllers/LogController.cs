﻿using System;
using System.Collections.Generic;
using System.Text;
using Hiro.Api.Shared.Constants;
using Hiro.Domain.Enums;
using Hiro.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Hiro.Api.Controllers
{
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly IAuthenticationService authenticationService;
        private readonly ILogService logService;

        public LogController(IAuthenticationService authenticationService, ILogService logService)
        {
            this.authenticationService = authenticationService;
            this.logService = logService;
        }


        [HttpPost]
        [Route(HiroApiRoutes.ApiLogCreateLog)]
        public virtual IActionResult CreateLog(LogTypesEnum type, string message, string details = null)
        {
            var user = this.authenticationService.GetAuthenticatedUser();

            this.logService.Log(type, message, details, user);

            return this.Ok();
        }
    }
}
