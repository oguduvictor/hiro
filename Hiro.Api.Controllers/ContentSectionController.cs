﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Api.Shared.Constants;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Hiro.Api.Controllers
{
    [ApiController]
    public class ContentSectionController : ControllerBase
    {
        private readonly IContentSectionService contentSectionService;
        private readonly HiroDbContext dbContext;

        public ContentSectionController(
            IContentSectionService contentSectionService, 
            HiroDbContext dbContext)
        {
            this.contentSectionService = contentSectionService;
            this.dbContext = dbContext;
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiContentSectionGetContentSections)]
        public IActionResult GetContentSections(string localizationCountryUrl, List<string> contentSectionNames)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var contentSections = contentSectionNames.Select(contentSectionName => new ContentSectionDto
            {
                Name = contentSectionName,
                LocalizedContent = this.contentSectionService.GetLocalizedContent(localizationCountry, contentSectionName)
            });

            return this.Ok(contentSections);
        }

        [HttpPost]
        [Route(HiroApiRoutes.ApiContentSectionGetContentSection)]
        public IActionResult GetContentSection(string localizationCountryUrl, string contentSectionName)
        {
            var localizationCountry = this.dbContext.Countries.FirstOrDefault(x => x.Url.Equals(localizationCountryUrl));

            if (localizationCountry == null)
            {
                return this.NotFound();
            }

            var contentSection =  new ContentSectionDto
            {
                Name = contentSectionName,
                LocalizedContent = this.contentSectionService.GetLocalizedContent(localizationCountry, contentSectionName)
            };

            return this.Ok(contentSection);
        }
    }
}