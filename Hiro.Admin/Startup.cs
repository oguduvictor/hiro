#pragma warning disable 1591

namespace Hiro.Admin
{
    using System.IdentityModel.Tokens.Jwt;
    using System.Net.Http;
    using Blazored.LocalStorage;
    using Hiro.Admin.Controllers.Validators;
    using Hiro.Admin.Controllers.Validators.Interfaces;
    using Hiro.Admin.Server.Services;
    using Hiro.Admin.Server.Services.Interfaces;
    using Hiro.Admin.States;
    using Hiro.Blazor.Services;
    using Hiro.DataAccess;
    using Hiro.Imagik.Blazor.Helpers;
    using Hiro.Services;
    using Hiro.Services.Authorization.Handlers;
    using Hiro.Services.Configurations;
    using Hiro.Services.Email;
    using Hiro.Services.Email.Interfaces;
    using Hiro.Services.Extensions;
    using Hiro.Services.Interfaces;
    using Hiro.Services.Options;
    using Hiro.Services.Validators;
    using Hiro.Services.Validators.Interfaces;
    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Components.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(Startup));
            services.AddRazorPages();
            services.AddBlazoredLocalStorage();
            services.AddHttpContextAccessor();
            services
                .AddServerSideBlazor()
                .AddCircuitOptions(options => options.DetailedErrors = true);
            services.AddHiroJwtAuthentication(this.Configuration);
            services.AddHiroAuthorization();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            // -------------------------------------------------------------
            // Options
            // -------------------------------------------------------------

            services.Configure<JwtOptions>(this.Configuration.GetSection("JwtOptions"));
            services.Configure<AzureMediaOptions>(this.Configuration.GetSection("AzureMedia"));
            services.Configure<HiroHttpClientOptions>(this.Configuration.GetSection("HttpClient"));

            // -------------------------------------------------------------
            // Validators
            // -------------------------------------------------------------

            services.AddScoped<IAddOrUpdateAddressValidator, AddOrUpdateAddressValidator>();
            services.AddScoped<IAddressDtoValidator, AddressDtoValidator>();
            services.AddScoped<IAttributeDtoValidator, AttributeDtoValidator>();
            services.AddScoped<IEmailIsRegisteredValidator, EmailIsRegisteredValidator>();
            services.AddScoped<IAttributeLocalizedKitDtoValidator, AttributeLocalizedKitDtoValidator>();
            services.AddScoped<IAttributeOptionDtoValidator, AttributeOptionDtoValidator>();
            services.AddScoped<IAttributeOptionLocalizedKitDtoValidator, AttributeOptionLocalizedKitDtoValidator>();
            services.AddScoped<IAuthenticateUserValidator, AuthenticateUserValidator>();
            services.AddScoped<IBatchRenameFilesDtoValidator, BatchRenameFilesDtoValidator>();
            services.AddScoped<IBlogCategoryDtoValidator, BlogCategoryDtoValidator>();
            services.AddScoped<IBlogCategoryLocalizedKitDtoValidator, BlogCategoryLocalizedKitDtoValidator>();
            services.AddScoped<IBlogPostDtoValidator, BlogPostDtoValidator>();
            services.AddScoped<ICategoryDtoValidator, CategoryDtoValidator>();
            services.AddScoped<ICategoryLocalizedKitDtoValidator, CategoryLocalizedKitDtoValidator>();
            services.AddScoped<ICmsSettingsDtoValidator, CmsSettingsDtoValidator>();
            services.AddScoped<IContentSectionDtoValidator, ContentSectionDtoValidator>();
            services.AddScoped<IContentSectionLocalizedKitDtoValidator, ContentSectionLocalizedKitDtoValidator>();
            services.AddScoped<ICountryDtoValidator, CountryDtoValidator>();
            services.AddScoped<ICouponDtoValidator, CouponDtoValidator>();
            services.AddScoped<IDirectoryDtoValidator, DirectoryDtoValidator>();
            services.AddScoped<IEditCouponDtoValidator, EditCouponDtoValidator>();
            services.AddScoped<IEditProductAttributesDtoValidator, EditProductAttributesDtoValidator>();
            services.AddScoped<IEditProductImageKitsDtoValidator, EditProductImageKitsDtoValidator>();
            services.AddScoped<IEditProductStockUnitsDtoValidator, EditProductStockUnitsDtoValidator>();
            services.AddScoped<IEditProductVariantsDtoValidator, EditProductVariantsDtoValidator>();
            services.AddScoped<IEmailDtoValidator, EmailDtoValidator>();
            services.AddScoped<IEmailLocalizedKitDtoValidator, EmailLocalizedKitDtoValidator>();
            services.AddScoped<IMailingListDtoValidator, MailingListDtoValidator>();
            services.AddScoped<IMailingListLocalizedKitDtoValidator, MailingListLocalizedKitDtoValidator>();
            services.AddScoped<INameValidator, NameValidator>();
            services.AddScoped<IProductAttributeDtoValidator, ProductAttributeDtoValidator>();
            services.AddScoped<IProductDtoValidator, ProductDtoValidator>();
            services.AddScoped<IProductImageDtoValidator, ProductImageDtoValidator>();
            services.AddScoped<IProductImageKitDtoValidator, ProductImageKitDtoValidator>();
            services.AddScoped<IProductLocalizedKitDtoValidator, ProductLocalizedKitDtoValidator>();
            services.AddScoped<IProductStockUnitDtoValidator, ProductStockUnitDtoValidator>();
            services.AddScoped<IProductStockUnitLocalizedKitDtoValidator, ProductStockUnitLocalizedKitDtoValidator>();
            services.AddScoped<IProductVariantDtoValidator, ProductVariantDtoValidator>();
            services.AddScoped<IOrderDtoValidator, OrderDtoValidator>();
            services.AddScoped<IOrderItemDtoValidator, OrderItemDtoValidator>();
            services.AddScoped<IOrderShippingBoxDtoValidator, OrderShippingBoxDtoValidator>();
            services.AddScoped<IRegisterUserValidator, RegisterUserValidator>();
            services.AddScoped<IRenameFileDtoValidator, RenameFileDtoValidator>();
            services.AddScoped<IResizeFileDtoValidator, ResizeFileDtoValidator>();
            services.AddScoped<ISendEmailConfirmationModalDtoValidator, SendEmailConfirmationModalDtoValidator>();
            services.AddScoped<ISeoSectionDtoValidator, SeoSectionDtoValidator>();
            services.AddScoped<ISeoSectionLocalizedKitDtoValidator, SeoSectionLocalizedKitDtoValidator>();
            services.AddScoped<IShippingBoxDtoValidator, ShippingBoxDtoValidator>();
            services.AddScoped<IShippingBoxLocalizedKitDtoValidator, ShippingBoxLocalizedKitDtoValidator>();
            services.AddScoped<ITaxDtoValidator, TaxDtoValidator>();
            services.AddScoped<IUrlValidator, UrlValidator>();
            services.AddScoped<IUserDtoValidator, UserDtoValidator>();
            services.AddScoped<IUserRoleDtoValidator, UserRoleDtoValidator>();
            services.AddScoped<IApplyCouponToCartValidator, ApplyCouponToCartValidator>();
            services.AddScoped<IVariantOptionLocalizedKitDtoValidator, VariantOptionLocalizedKitDtoValidator>();
            services.AddScoped<IVariantOptionDtoValidator, VariantOptionDtoValidator>();
            services.AddScoped<IVariantDtoValidator, VariantDtoValidator>();
            services.AddScoped<IVariantLocalizedKitDtoValidator, VariantLocalizedKitDtoValidator>();

            // -------------------------------------------------------------
            // Services
            // -------------------------------------------------------------

            services.AddScoped<HttpClient>();
            services.AddScoped<IAutoTagService, AutoTagService>();
            services.AddScoped<IBitmapService, BitmapService>();
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddScoped<IMediaService, AzureMediaService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<IWishListService, WishListService>();
            services.AddScoped<IContentSectionService, ContentSectionService>();
            services.AddScoped<ILogService, LogService>();
            services.AddScoped<IAdminCategoryService, AdminCategoryService>();
            services.AddScoped<IAdminCartService, AdminCartService>();
            services.AddScoped<IAdminBlogCategoryService, AdminBlogCategoryService>();
            services.AddScoped<IAdminContentSectionService, AdminContentSectionService>();
            services.AddScoped<IAdminCountryService, AdminCountryService>();
            services.AddScoped<IAdminCouponService, AdminCouponService>();
            services.AddScoped<IAdminEmailService, AdminEmailService>();
            services.AddScoped<IAdminMailingListService, AdminMailingListService>();
            services.AddScoped<IAdminNotificationService, AdminNotificationService>();
            services.AddScoped<IAdminOrderService, AdminOrderService>();
            services.AddScoped<IAdminPostsService, AdminPostsService>();
            services.AddScoped<IAdminProductService, AdminProductService>();
            services.AddScoped<IAdminSeoService, AdminSeoService>();
            services.AddScoped<IAdminSettingService, AdminSettingService>();
            services.AddScoped<IAdminShippingBoxService, AdminShippingBoxService>();
            services.AddScoped<IAdminUserService, AdminUserService>();
            services.AddScoped<IAdminMediaService, AdminMediaService>();
            services.AddScoped<IAdminAttributeService, AdminAttributeService>();
            services.AddScoped<IAdminUtilityService, AdminUtilityService>();
            services.AddScoped<IAdminVariantService, AdminVariantService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IAuthorizationHandler, UserRolePermissionHandler>();
            services.AddScoped<AuthenticationStateProvider, JwtAuthenticationStateProvider>();
            services.AddScoped(_ => new HiroDbContext(this.Configuration.GetConnectionString("DbConnectionString")));
            services.AddScoped<HiroHttpClient>();
            services.AddScoped<AdminSessionState>();
            services.AddScoped<AdminAuthenticationState>();
            services.AddScoped<AdminContentSectionState>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}