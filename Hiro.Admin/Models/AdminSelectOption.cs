﻿namespace Hiro.Admin.Models
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain.Dtos;
    using Hiro.Shared.Extensions;

    public class AdminSelectOption
    {
        public AdminSelectOption()
        {
        }

        public AdminSelectOption(Enum value, string title = null, string description = null)
        {
            this.Title = title ?? value.ToString();
            this.Description = description;
            this.Key = Convert.ToInt32(value).ToString();
            this.Value = value;
        }

        public AdminSelectOption(string value, string title = null, string description = null)
        {
            this.Title = title ?? value;
            this.Description = description;
            this.Key = value;
            this.Value = value;
        }

        public AdminSelectOption(BaseEntityDto value, string title, string description = null)
        {
            this.Title = title ?? string.Empty;
            this.Description = description;
            this.Key = value.Id.ToString();
            this.Value = value;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Key { get; set; }

        public object Value { get; set; }

        public static List<AdminSelectOption> CreateInList(Enum value, string title = null, string description = null)
        {
            if (value == null)
            {
                return new List<AdminSelectOption>();
            }

            return new AdminSelectOption(value, title, description).InList();
        }

        public static List<AdminSelectOption> CreateInList(string value, string title = null, string description = null)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new List<AdminSelectOption>();
            }

            return new AdminSelectOption(value, title, description).InList();
        }

        public static List<AdminSelectOption> CreateInList(BaseEntityDto value, string title = null, string description = null)
        {
            if (value == null)
            {
                return new List<AdminSelectOption>();
            }

            return new AdminSelectOption(value, title, description).InList();
        }

        public T GetValueAs<T>() where T : class
        {
            return this.Value as T;
        }
    }
}
