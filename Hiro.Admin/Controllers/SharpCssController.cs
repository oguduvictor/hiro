﻿namespace Hiro.Admin.Controllers
{
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Hiro.Shared.Extensions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Abstractions;
    using Microsoft.AspNetCore.Mvc.Razor;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    using Microsoft.AspNetCore.Routing;

    public class SharpCssController : Controller
    {
        private readonly IRazorViewEngine razorViewEngine;

        public SharpCssController(
            IRazorViewEngine razorViewEngine)
        {
            this.razorViewEngine = razorViewEngine;
        }

        [Route("SharpCss")]
        public async Task<IActionResult> Index()
        {
            var stringifiedView = await this.RenderViewAsync("~/SharpCss/Index.cshtml");

            stringifiedView = stringifiedView.Replace("<style>", string.Empty);
            stringifiedView = stringifiedView.Replace("</style>", string.Empty);

            // ---------------------------------------------------------------------
            // Minify
            // ---------------------------------------------------------------------

            stringifiedView = Regex.Replace(stringifiedView, @"<!--(.*)-->", string.Empty);
            stringifiedView = Regex.Replace(stringifiedView, @"[a-zA-Z]+#", "#");
            stringifiedView = Regex.Replace(stringifiedView, @"[\n\r]+\s*", string.Empty);
            stringifiedView = Regex.Replace(stringifiedView, @"\s+", " ");
            stringifiedView = Regex.Replace(stringifiedView, @"\s?([:,;{}])\s?", "$1");
            stringifiedView = stringifiedView.Replace(";}", "}");
            stringifiedView = Regex.Replace(stringifiedView, @"([\s:]0)(px|pt|%|em)", "$1");
            stringifiedView = Regex.Replace(stringifiedView, @"/\*[\d\D]*?\*/", string.Empty);

            return this.Content(stringifiedView, "text/css");
        }
    }
}