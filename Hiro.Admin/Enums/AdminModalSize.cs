﻿namespace Hiro.Admin.Enums
{
    public enum AdminModalSize
    {
        Default = 0,
        Small = 1,
        Big = 2
    }
}
