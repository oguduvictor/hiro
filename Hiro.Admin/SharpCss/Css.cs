﻿using System.Runtime.CompilerServices;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1310
#pragma warning disable SA1303

[assembly: InternalsVisibleTo("Hiro.Admin.Views")]

namespace Hiro.Admin.SharpCss
{
    internal class Css
    {
        public const string admin_contentsection_editor = "admin-contentsection-editor";
        public const string admin_contentsectionlocalizedkit_editor = "admin-contentsectionlocalizedkit-editor";
        public const string admin_contentsectionlocalizedkit_editor_style_1 = "admin-contentsectionlocalizedkit-editor-style-1";

        public const string admin_layout = "admin-layout";

        public const string admin_loading = "admin-loading";
        public const string admin_loading_style_1 = "admin-loading-style-1";

        public const string admin_categories_table = "admin-categories-table";
        public const string admin_categories_table_style_1 = "admin-categories-table-style-1";

        public const string admin_sidebar = "admin-sidebar";
        public const string admin_sidebar_logo = "admin-sidebar-logo";
        public const string admin_sidebar_navlinks = "admin-sidebar-navlinks";
        public const string admin_sidebar_navlinks_group_title = "admin-sidebar-navlinks-group-title";

        public const string admin_page_template = "admin-page-template";
        public const string admin_page_template_style_1 = "admin-page-template-style-1";
        public const string admin_page_template_top_bar = "admin-page-template-top-bar";
        public const string admin_page_template_top_bar_title = "admin-page-template-top-bar-title";
        public const string admin_page_template_top_bar_controls = "admin-page-template-top-bar-controls";
        public const string admin_page_template_operation_result = "admin-page-template-form-response";
        public const string admin_page_template_form_operation_result_success = "admin-page-template-form-response-success";
        public const string admin_page_template_operation_result_error = "admin-page-template-form-response-error";
        public const string admin_page_template_operation_result_close_button = "admin-page-template-form-response-close-button";
        public const string admin_page_template_content = "admin-page-template-content";
        public const string admin_page_template_content_columns = "admin-page-template-content-columns";
        public const string admin_page_template_content_column_left = "admin-page-template-content-column-left";
        public const string admin_page_template_content_column_right = "admin-page-template-content-column-right";

        public const string admin_card = "admin-card";
        public const string admin_card_style_1 = "admin-card-style-1";
        public const string admin_card_header = "admin-card-header";
        public const string admin_card_header_title = "admin-card-header-title";
        public const string admin_card_header_controls = "admin-card-header-controls";

        public const string admin_table = "admin-table";
        public const string admin_table_style_1 = "admin-table-style-1";
        public const string admin_table_dropzone = "admin-table-dropzone";
        public const string admin_table_dragged_item = "admin-table-dragged-item";

        public const string admin_button = "admin-button";
        public const string admin_button_text = "admin-button-text";
        public const string admin_button_disabled = "admin-button-disabled";
        public const string admin_button_loading_icon = "admin-button-loading-icon";
        public const string admin_button_style_1 = "admin-button-style-1";
        public const string admin_button_style_2 = "admin-button-style-2";

        public const string admin_input = "admin-input";
        public const string admin_input_style_1 = "admin-input-style-1";
        public const string admin_input_error = "admin-input-error";
        public const string admin_input_error_message = "admin-input-error-message";
        public const string admin_input_style_2 = "admin-input-style-2";
        public const string admin_input_style_3 = "admin-input-style-3";

        public const string admin_label = "admin-label";
        public const string admin_label_style_1 = "admin-label-style-1";
        public const string admin_label_style_2 = "admin-label-style-2";
        public const string admin_label_help_icon = "admin-help-icon";

        public const string admin_select = "admin-select";
        public const string admin_select_style_1 = "admin-select-style-1";
        public const string admin_select_hide_selected_options = "admin-select-hide-selected-options";
        public const string admin_select_search_container = "admin-select-search-container";
        public const string admin_select_search_selected_option = "admin-select-search-selected-option";
        public const string admin_select_search_results = "admin-select-search-results";
        public const string admin_select_icon = "admin-select-icon";

        public const string admin_textarea = "admin-textarea";
        public const string admin_textarea_style_1 = "admin-textarea-style-1";
        public const string admin_textarea_style_2 = "admin-textarea-style-2";

        public const string admin_localizedkits = "admin-localizedkits";
        public const string admin_localizedkits_style_1 = "admin-localizedkits-style-1";
        public const string admin_localizedkits_localizedkit = "admin-localizedkits-localizedkit";
        public const string admin_localizedkits_localizedkit_country = "admin-localizedkits-localizedkit-country";
        public const string admin_localizedkits_localizedkit_content = "admin-localizedkits-localizedkit-content";
        public const string admin_localizedkits_localizedkit_content_expanded = "admin-localizedkits-localizedkit-content-expanded";

        public const string admin_product_stock_units_table = "admin-product-stock-units-table";
        public const string admin_product_stock_units_table_hide_prices = "admin-product-stock-units-table-hide-prices";
        public const string admin_product_stock_units_table_psu_first_row = "admin-product-stock-units-table-psu-first-row";
        public const string admin_product_stock_units_table_localizedkit_td = "admin-product-stock-units-table-localizedkit-td";

        public const string admin_modal = "admin-modal";
        public const string admin_modal_style_1 = "admin-modal-style-1";
        public const string admin_modal_overlay = "admin-modal-overlay";
        public const string admin_modal_content = "admin-modal-content";
        public const string admin_modal_content_small = "admin-modal-content-small";
        public const string admin_modal_content_big = "admin-modal-content-big";
        public const string admin_modal_content_header = "admin-modal-content-header";
        public const string admin_modal_content_body = "admin-modal-content-body";
        public const string admin_modal_content_footer = "admin-modal-content-footer";

        public const string admin_media_thumbnail = "admin-media-thumbnail";
        public const string admin_media_thumbnail_style_1 = "admin-media-thumbnail-style-1";
        public const string admin_media_thumbnail_image = "admin-media-thumbnail-image";

        public const string admin_file_picker = "admin-file-picker";
        public const string admin_file_picker_style_1 = "admin-file-picker-style-1";
        public const string admin_file_picker_modal_button = "admin-file-picker-modal-button";
        public const string admin_file_picker_directory = "admin-file-picker-directory";
        public const string admin_file_picker_directory_image = "admin-file-picker-directory-image";
        public const string admin_file_picker_file = "admin-file-picker-file";
        public const string admin_file_picker_file_image = "admin-file-picker-file-image";

        public const string admin_dropdown = "admin-dropdown";
        public const string admin_dropdown_style_1 = "admin-dropdown-style-1";
        public const string admin_dropdown_list = "admin-dropdown-list";
        public const string admin_dropdown_list_push_left = "admin-dropdown-list-push-left";
        public const string admin_dropdown_item = "admin-dropdown-item";
        public const string admin_dropdown_item_style_1 = "admin-dropdown-item-style-1";
        public const string admin_dropdown_overlay = "admin-dropdown-overlay";

        public const string admin_authentication = "admin-authentication";
        public const string admin_authentication_container = "admin-authentication-container";
        public const string admin_authentication_back_button = "admin-authentication-back-button";
        public const string admin_authentication_content = "admin-authentication-content";
        public const string admin_authentication_error_list = "admin-authentication-error-list";
        public const string admin_authentication_footer = "admin-authentication-footer";
        public const string admin_authentication_form = "admin-authentication-form";
        public const string admin_authentication_form_email_step = "admin-authentication-form-email-step";
        public const string admin_authentication_form_password_step = "admin-authentication-form-password-step";
        public const string admin_authentication_form_active_step = "admin-authentication-form-active-step";
        public const string admin_authentication_form_email_badge = "admin-authentication-form-email-badge";
        public const string admin_authentication_form_forgot_password_link = "admin-authentication-form-forgot-password-link";

        public const string admin_registration = "admin-registration";
        public const string admin_registration_container = "admin-registration-container";
        public const string admin_registration_content = "admin-registration-content";
        public const string admin_registration_form = "admin-registration-form";
        public const string admin_registration_footer = "admin-registration-footer";

        public const string admin_form_line = "admin-form-line";

        public const string admin_cart_summary = "admin-ecommerce-cart-summary-section";
        public const string admin_cart_product_item = "admin-ecommerce-cart-product-item";

        public const string admin_media_page_media_table = "admin-media-page-media-table";
        public const string admin_media_page_media_table_title = "admin-media-page-media-table-title";
        public const string admin_media_page_media_link = "admin-media-page-media-link";

        public const string admin_inline_wrapper = "admin-inline-wrapper";
        public const string admin_inline_wrapper_style_1 = "admin-inline-wrapper-style-1";
        public const string admin_inline_wrapper_left_column = "admin-inline-wrapper-left-column";
        public const string admin_inline_wrapper_right_column = "admin-inline-wrapper-right-column";

        // ---------------------------------------------------------------------------
        // Icon classes
        // ---------------------------------------------------------------------------

        public const string admin_icon_alert_triangle = "icon-alert-triangle";
        public const string admin_icon_at_sign = "icon-at-sign";
        public const string admin_icon_th = "icon-th";
        public const string admin_icon_cart = "icon-shopping-cart1";
        public const string admin_icon_checkbox_checked = "icon-checkbox-checked";
        public const string admin_icon_checkbox_unchecked = "icon-checkbox-unchecked";
        public const string admin_icon_checkmark = "icon-checkmark";
        public const string admin_icon_chevron_thin_left = "icon-chevron-thin-left";
        public const string admin_icon_chevron_thin_right = "icon-chevron-thin-right";
        public const string admin_icon_documents = "icon-documents";
        public const string admin_icon_edit = "icon-edit";
        public const string admin_icon_envelop = "icon-envelop";
        public const string admin_icon_eye = "icon-eye";
        public const string admin_icon_eye_blocked = "icon-eye-blocked";
        public const string admin_icon_file_picture = "icon-file-picture";
        public const string admin_icon_file_text2 = "icon-file-text2";
        public const string admin_icon_gear = "icon-gear";
        public const string admin_icon_help_circle = "icon-help-circle";
        public const string admin_icon_list = "icon-list";
        public const string admin_icon_truck1 = "icon-truck1";
        public const string admin_icon_key = "icon-key";
        public const string admin_icon_clipboard = "icon-clipboard";
        public const string admin_icon_tags = "icon-tags";
        public const string admin_icon_table = "icon-table";
        public const string admin_icon_archive = "icon-archive";
        public const string admin_icon_spinner6 = "icon-spinner6";
        public const string admin_icon_trash = "icon-trash";
        public const string admin_icon_tree = "icon-tree";
        public const string admin_icon_triangle_down = "icon-triangle-down";
        public const string admin_icon_triangle_up = "icon-triangle-up";
        public const string admin_icon_user = "icon-user";
        public const string admin_icon_users = "icon-users";
        public const string admin_icon_earth = "icon-earth";
        public const string admin_icon_upload_cloud = "icon-upload-cloud";
        public const string admin_icon_plus = "icon-plus";

        // ---------------------------------------------------------------------------
        // Variables
        // ---------------------------------------------------------------------------

        public const string admin_sidebar_bg_color = "#0e2d3b";
        public const string admin_label_color = "#356ea0";
        public const string admin_table_border_color = "#e4e4e4";
    }
}