namespace Hiro.Admin
{
    using System.Data.Entity;
    using Hiro.DataAccess;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) => Host
            .CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
                Database.SetInitializer(new MigrateDatabaseToLatestVersion<HiroDbContext, DataAccess.Migrations.Configuration>());
            });
    }
}