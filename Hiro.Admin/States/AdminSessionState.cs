﻿#pragma warning disable 1591
namespace Hiro.Admin.States
{
    using System;
    using System.Threading.Tasks;
    using Hiro.Shared.Dtos;
    using JetBrains.Annotations;

    public class AdminSessionState
    {
        /// <summary>
        /// Gets or sets a value indicating whether the current page is loading.
        /// </summary>
        public bool IsLoading { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current page contains unsaved changes.
        /// </summary>
        public bool ContainsUnsavedChanges { get; set; }

        [CanBeNull]
        public OperationResult OperationResult { get; set; }

        public bool ShowConfirmationModal { get; set; }

        public Func<Task> ConfirmationModalTask { get; set; } = () => null;
    }
}
