﻿namespace Hiro.Admin.States
{
    using System.Collections.Generic;
    using Hiro.Domain.Dtos;

    public class AdminContentSectionState
    {
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();

        public List<ContentSectionDto> ContentSections { get; set; } = new List<ContentSectionDto>();
    }
}