﻿namespace Hiro.Admin.States
{
    using JetBrains.Annotations;

    public class AdminAuthenticationState
    {
        /// <summary>
        /// Gets or sets the email address of the User who it trying to authenticate or register.
        /// </summary>
        [CanBeNull]
        public string Email { get; set; }
    }
}