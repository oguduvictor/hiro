﻿namespace Hiro.Admin.Constants
{
    public class KeyboardKeys
    {
        public const string Backspace = "Backspace";
        public const string Enter = "Enter";
    }
}