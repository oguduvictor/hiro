﻿namespace Hiro.Admin.Schemas
{
    using System;
    using System.Collections.Generic;
    using Hiro.Shared.Attributes;

    [Schema]
    public class MySchema
    {
        [SchemaProperty(
            Title = "My list",
            Description = "A simple list")]
        public List<string> MyList { get; set; } = new List<string>();

        [SchemaProperty(
            Title = "My table",
            Description = "A simple table",
            MaxTableColumns = 3)]
        public List<List<string>> MyTable { get; set; }

        [SchemaProperty(
            Title = "My image",
            Description = "A simple image")]
        public string MyImage { get; set; }

        [SchemaProperty(
            Title = "My number",
            Description = "A simple number")]
        public double MyNumber { get; set; }

        [SchemaProperty(
            Title = "My date",
            Description = "A simple date")]
        public DateTime MyDate { get; set; } = DateTime.Now;

        [SchemaProperty(
            Title = "My boolean",
            Description = "A simple boolean")]
        public bool MyBoolean { get; set; }
    }
}