﻿#pragma warning disable 1591
namespace Hiro.Admin.ComponentBases
{
    using System;
    using System.Threading.Tasks;
    using Hiro.Admin.States;
    using Hiro.Services;
    using Hiro.Shared.Dtos;
    using Microsoft.AspNetCore.Components;

    public class AdminPageComponentBase : ComponentBase
    {
        public AdminPageComponentBase()
        {
            // ----------------------------------------------------------
            // Reset AdminPageState properties at each pageload
            // ----------------------------------------------------------

            if (this.AdminSessionState != null)
            {
                this.AdminSessionState.IsLoading = false;
                this.AdminSessionState.ContainsUnsavedChanges = false;
                this.AdminSessionState.OperationResult = null;
            }
        }

        [Parameter]
        public string CountryUrl { get; set; }

        [Inject]
        protected AdminSessionState AdminSessionState { get; set; }

        [Inject]
        protected HiroHttpClient HiroHttpClient { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        protected void ShowOperationResultAlert(OperationResult operationResult)
        {
            this.AdminSessionState.OperationResult = operationResult;

            Task.Delay(TimeSpan.FromSeconds(10))
                .ContinueWith(t =>
                {
                    this.AdminSessionState.OperationResult = null;
                    this.StateHasChanged();
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
