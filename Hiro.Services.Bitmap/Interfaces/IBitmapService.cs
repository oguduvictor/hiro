﻿using System.IO;

namespace Hiro.Services.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IBitmapService
    {
        /// <summary>
        /// Resize Image method for Azure Storage.
        /// </summary>
        /// <param name="imageStream"></param>
        /// <param name="newWidth"></param>
        /// <param name="compression"></param>
        /// <returns></returns>
        MemoryStream ResizeImage(MemoryStream imageStream, int newWidth, int compression);
    }
}