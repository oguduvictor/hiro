﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hiro.Api.Shared.Constants;
using Hiro.Domain.Dtos;
using Hiro.Services;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Components.Authorization;
using Newtonsoft.Json;

namespace Hiro.Blazor.Services
{
    /// <summary>
    /// A client for the Hiro API.
    /// </summary>
    public class HiroApiClient
    {
        private readonly AuthenticationStateProvider authenticationStateProvider;
        private readonly HiroHttpClient hiroHttpClient;

        public HiroApiClient(
            AuthenticationStateProvider authenticationStateProvider,
            HiroHttpClient hiroHttpClient)
        {
            this.authenticationStateProvider = authenticationStateProvider;
            this.hiroHttpClient = hiroHttpClient;
        }

        ///// <summary>
        ///// Gets the Cart from the Hiro API.
        ///// </summary>
        ///// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        ///// <param name="localizationCountryUrl">The Localization Country Url.</param>
        public async Task<CartDto> GetCartAsync(string localizationCountryUrl)
        {
            var authenticationState = await this.authenticationStateProvider.GetAuthenticationStateAsync();

            if (authenticationState.User.Identity.IsAuthenticated)
            {
                return await this.hiroHttpClient.PostAsync<CartDto>(
                    HiroApiRoutes.ApiCartGetCart,
                    new { localizationCountryUrl },
                    null);
            }

            return null;
        }

        public async Task UpdateCartItemQuantityAsync(Guid cartItemId, int quantity)
        {
            await this.hiroHttpClient.PostAsync<dynamic>(
                HiroApiRoutes.ApiCartUpdateCartItemQuantity,
                new { cartItemId, quantity },
                null);
        }

        public async Task DeleteCartItemAsync(Guid cartItemId)
        {
            await this.hiroHttpClient.PostAsync<dynamic>(
                HiroApiRoutes.ApiCartDeleteCartItem,
                new { cartItemId },
                null);
        }

        public async Task<OperationResult> ApplyCouponAsync(string localizationCountryUrl, string couponCode)
        {
            return await this.hiroHttpClient.PostAsync<OperationResult>(
                HiroApiRoutes.ApiCartApplyCoupon,
                new { localizationCountryUrl, couponCode },
                null);
        }

        public async Task RemoveCouponAsync(string localizationCountryUrl)
        {
            await this.hiroHttpClient.PostAsync<dynamic>(
                HiroApiRoutes.ApiCartRemoveCoupon,
                new { localizationCountryUrl },
                null);
        }

        public async Task<double> GetUserCreditTotalAsync(string localizationCountryUrl)
        {
            return await this.hiroHttpClient.PostAsync<double>(
                HiroApiRoutes.ApiAuthenticationGetUserCreditTotal,
                new { localizationCountryUrl },
                null);
        }

        public async Task<OperationResult<double>> ApplyUserCreditAsync(string localizationCountryUrl, double amount)
        {
            return await this.hiroHttpClient.PostAsync<OperationResult<double>>(
                HiroApiRoutes.ApiCartApplyUserCredit,
                new { localizationCountryUrl, amount },
                null);
        }

        public async Task<OperationResult<double>> RemoveCreditAsync(string localizationCountryUrl)
        {
            return await this.hiroHttpClient.PostAsync<OperationResult<double>>(
                HiroApiRoutes.ApiCartRemoveCredit,
                new { localizationCountryUrl },
                null);
        }

        ///// <summary>
        ///// Gets the localization countries from the Hiro API.
        ///// </summary>
        ///// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<List<CountryDto>> GetLocalizationCountriesAsync()
        {
            return await this.hiroHttpClient.PostAsync<List<CountryDto>>(HiroApiRoutes.ApiCountryGetLocalizationCountries, null, null);
        }

        public async Task<List<CountryDto>> GetCountriesAsync()
        {
            return await this.hiroHttpClient.PostAsync<List<CountryDto>>(HiroApiRoutes.ApiCountryGetCountries, null, null);
        }

        public async Task<UserDto> GetAuthenticatedUser()
        {
            return await this.hiroHttpClient.PostAsync<UserDto>(HiroApiRoutes.ApiAuthenticationGetAuthenticatedUser, null, null);
        }

        public async Task<List<AddressDto>> GetUserAddressesAsync()
        {
            return await this.hiroHttpClient.PostAsync<List<AddressDto>>(HiroApiRoutes.ApiAuthenticationGetUserAddresses, null, null);
        }

        public async Task<OperationResult<string>> AuthenticateUserAsync(string email, string password)
        {
            return await this.hiroHttpClient.PostAsync<OperationResult<string>>(
                HiroApiRoutes.ApiAuthenticationAuthenticateUser,
                new { email, password },
                null);
        }

        public async Task<OperationResult<string>> PartiallyAuthenticateUserAsync(Guid userId)
        {
            return await this.hiroHttpClient.PostAsync<OperationResult<string>>(
                HiroApiRoutes.ApiAuthenticationPartiallyAuthenticateUser,
                new { userId },
                null);
        }

        public async Task<OperationResult<string>> RegisterUserAsync(string firstName, string lastName, string email, string password, Guid countryId)
        {
            return await this.hiroHttpClient.PostAsync<OperationResult<string>>(
                HiroApiRoutes.ApiAuthenticationRegisterUser,
                new { firstName, lastName, email, password },
                null);
        }

        public async Task<ContentSectionDto> GetContentSection(string localizationCountryUrl, string contentSectionName)
        {
            return await this.hiroHttpClient.PostAsync<ContentSectionDto>(
                HiroApiRoutes.ApiContentSectionGetContentSection,
                new { localizationCountryUrl, contentSectionName },
                null);
        }

        public async Task<T> GetContentSectionAs<T>(string localizationCountryUrl, string contentSectionName)
        {
            var contentSection = await this.hiroHttpClient.PostAsync<ContentSectionDto>(
                HiroApiRoutes.ApiContentSectionGetContentSection,
                new { localizationCountryUrl, contentSectionName },
                null);

            try
            {
                return JsonConvert.DeserializeObject<T>(contentSection.LocalizedContent);
            }
            catch (Exception)
            {
                return default;
            }
        }

        public async Task<List<ContentSectionDto>> GetContentSections(string localizationCountryUrl, List<string> contentSectionNames)
        {
            return await this.hiroHttpClient.PostAsync<List<ContentSectionDto>>(
                HiroApiRoutes.ApiContentSectionGetContentSections,
                new { localizationCountryUrl },
                contentSectionNames);
        }
    }
}
