﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Hiro.Shared.Constants;
using Hiro.Shared.MediatR.Notifications;
using MediatR;
using Microsoft.AspNetCore.Components.Authorization;

#pragma warning disable 1591
namespace Hiro.Blazor.Services
{
    public class JwtAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly HttpClient httpClient;
        private readonly ILocalStorageService localStorageService;
        private readonly IMediator mediator;

        public JwtAuthenticationStateProvider(
            ILocalStorageService localStorageService,
            HttpClient httpClient,
            IMediator mediator)
        {
            this.localStorageService = localStorageService;
            this.httpClient = httpClient;
            this.mediator = mediator;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var savedToken = await this.localStorageService.GetItemAsync<string>(LocalStorageKeys.AuthToken);

            if (string.IsNullOrWhiteSpace(savedToken))
            {
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            }

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", savedToken);

            return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity(ParseClaimsFromJwt(savedToken), "jwt")));
        }

        /// <summary>
        /// Helper method that's used to when a user logs in. Its sole purpose is to invoke the
        /// NotifyAuthenticationStateChanged method which fires an event called AuthenticationStateChanged.
        /// </summary>
        /// <param name="token">The Authentication JWT.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public virtual async Task SignInAsync(string token)
        {
            await this.localStorageService.SetItemAsync(LocalStorageKeys.AuthToken, token);

            var authState = await GetAuthenticationStateAsync();

            this.NotifyAuthenticationStateChanged(Task.FromResult(authState));

            await this.mediator.Publish(new SignInNotification());
        }

        /// <summary>
        /// Helper method that's used to when a user logs out. Its sole purpose is to invoke the
        /// NotifyAuthenticationStateChanged method which fires an event called AuthenticationStateChanged.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public virtual async Task SignOutAsync()
        {
            await this.localStorageService.ClearAsync();

            var authState = this.GetAuthenticationStateAsync();

            this.NotifyAuthenticationStateChanged(authState);

            await this.mediator.Publish(new SignOutNotification());
        }

        private static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var claims = new List<Claim>();
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);

            keyValuePairs.TryGetValue(ClaimTypes.Role, out object roles);

            if (roles != null)
            {
                if (roles.ToString().Trim().StartsWith("["))
                {
                    var parsedRoles = JsonSerializer.Deserialize<string[]>(roles.ToString());

                    foreach (var parsedRole in parsedRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, parsedRole));
                    }
                }
                else
                {
                    claims.Add(new Claim(ClaimTypes.Role, roles.ToString()));
                }

                keyValuePairs.Remove(ClaimTypes.Role);
            }

            claims.AddRange(keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString())));

            return claims;
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }

            return Convert.FromBase64String(base64);
        }
    }
}