﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Hiro.Services.Interfaces;
using Hiro.Services.Options;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Hiro.Services
{
    public class AzureMediaService : IMediaService
    {
        private readonly AzureMediaOptions azureMediaOptions;
        private readonly IBitmapService bitmapService;

        public AzureMediaService(
            IOptionsMonitor<AzureMediaOptions> optionsAccessor, 
            IBitmapService bitmapService)
        {
            this.bitmapService = bitmapService;
            this.azureMediaOptions = optionsAccessor.CurrentValue;

            // todo: validate options
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options?view=aspnetcore-3.1#options-validation
        }

        public MediaDirectory GetDirectory(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                localPath = $"/{this.azureMediaOptions.AzureStorageContainer}/";
            }

            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            var files = new List<MediaFile>();

            var directories = new List<MediaDirectory>();

            var blobs = this.GetCloudBlobDirectory(localPath).ListBlobs();

            foreach (var item in blobs.Where(x => !x.Uri.LocalPath.EndsWith(".info")))
            {
                if (item is CloudBlockBlob cloudBlockBlob)
                {
                    var file = this.CloudBlockBlobToMediaFile(cloudBlockBlob);

                    files.Add(file);
                }
                else if (item is CloudBlobDirectory cloudBlobDirectory)
                {
                    var directory = CloudBlobDirectoryToMediaDirectory(cloudBlobDirectory);

                    directories.Add(directory);
                }
            }

            return new MediaDirectory
            {
                Name = GetDirectoryName(localPath),
                LocalPath = localPath,
                Size = 0,
                CreationTime = DateTime.Now,
                Files = files,
                Directories = directories,
                Parent = new MediaDirectory
                {
                    LocalPath = GetDirectoryParentLocalPath(localPath)
                },
                IsRoot = localPath.TrimEnd('/').EndsWith(this.azureMediaOptions.AzureStorageContainer)
            };
        }

        public OperationResult CreateDirectory(string localPath, string newDirectoryName)
        {
            throw new NotImplementedException();
        }

        public OperationResult DeleteDirectory(string localPath)
        {
            throw new NotImplementedException();
        }

        public MediaFile GetFile(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            var blob = this.GetCloudBlockBlob(localPath);

            if (blob.Exists())
            {
                return this.CloudBlockBlobToMediaFile(blob);
            }

            return null;
        }

        public OperationResult DeleteFile(string localPath)
        {
            throw new NotImplementedException();
        }

        public OperationResult RenameFile(string localPath, string newFileName)
        {
            throw new NotImplementedException();
        }

        public OperationResult ResizeFile(string localPath, string destinationPath, int width, int compression, bool keepOriginal)
        {
            var operationResult = new OperationResult();

            try
            {
                var blob = this.GetCloudBlockBlob(localPath);

                // ------------------------------------------------
                // CREATE MEMORY STREAM
                // ------------------------------------------------

                var memoryStream = new MemoryStream();

                blob.DownloadToStream(memoryStream);

                memoryStream.Seek(0, SeekOrigin.Begin);

                // ------------------------------------------------
                // SET NEW IMAGE VALUES
                // ------------------------------------------------

                string newLocalPath;

                if (keepOriginal)
                {
                    newLocalPath = $"{destinationPath}{Path.GetFileNameWithoutExtension(blob.Uri.LocalPath)}_{width}{Path.GetExtension(blob.Uri.LocalPath)}";
                }
                else
                {
                    newLocalPath = destinationPath + Path.GetFileName(blob.Uri.LocalPath);
                }

                var newImage = this.bitmapService.ResizeImage(memoryStream, width, compression);

                if (!keepOriginal)
                {
                    this.DeleteFile(localPath);
                }

                // ------------------------------------------------
                // UPLOAD NEW IMAGE
                // ------------------------------------------------

                var newBlob = this.GetCloudBlockBlob(newLocalPath);

                newBlob.Properties.ContentType = GetFileContentType(Path.GetExtension(newLocalPath));

                newBlob.UploadFromStream(newImage);
            }
            catch (Exception e)
            {
                operationResult.AddError(e.Message);
            }

            return operationResult;
        }

        public OperationResult UploadFile(string directoryLocalPath, Stream fileStream, string fileName)
        {
            var operationResult = new OperationResult();

            try
            {
                var fileLocalPath = (directoryLocalPath + "/" + fileName)
                    .Replace("//", "/")
                    .Replace("//", "/");

                var existingBlob = this.GetCloudBlockBlob(fileLocalPath);

                existingBlob?.DeleteIfExists();

                var blob = this.GetCloudBlockBlob(fileLocalPath);

                blob.Properties.ContentType = GetFileContentType(Path.GetExtension(fileName));
                blob.Properties.CacheControl = "public, max-age=864000"; // 10 days

                blob.UploadFromStream(fileStream, AccessCondition.GenerateIfNotExistsCondition());

                fileStream.Seek(0, SeekOrigin.Begin);
            }
            catch (Exception e)
            {
                operationResult.AddError(e.Message);
            }

            return operationResult;
        }

        private CloudBlobContainer GetBlobContainer(string containerName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new NullReferenceException("The parameter 'containerName' cannot be null or empty.");
            }

            var storageAccount = CloudStorageAccount.Parse(this.azureMediaOptions.ConnectionString);

            var blobClient = storageAccount.CreateCloudBlobClient();

            return blobClient.GetContainerReference(containerName);
        }

        private CloudBlockBlob GetCloudBlockBlob(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            var containerName = GetContainerName(localPath);

            var container = this.GetBlobContainer(containerName);

            var blobName = GetBlobName(localPath);

            return container.GetBlockBlobReference(blobName);
        }

        private CloudBlobDirectory GetCloudBlobDirectory(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' cannot be null or empty.");
            }

            var containerName = GetContainerName(localPath);

            var container = this.GetBlobContainer(containerName);

            var blobName = GetBlobName(localPath);

            var cloudBlobDirectory = container.GetDirectoryReference(blobName);

            return cloudBlobDirectory;
        }

        private static MediaDirectory CloudBlobDirectoryToMediaDirectory(CloudBlobDirectory cloudBlobDirectory)
        {
            return new MediaDirectory
            {
                Name = GetDirectoryName(cloudBlobDirectory.Uri.LocalPath),
                LocalPath = cloudBlobDirectory.Uri.LocalPath,
                Size = 0,
                CreationTime = DateTime.Now,
                Files = null,
                Directories = null,
                Parent = null
            };
        }

        private MediaFile CloudBlockBlobToMediaFile(CloudBlob blob)
        {
            return new MediaFile
            {
                Name = Path.GetFileNameWithoutExtension(blob.Uri.LocalPath),
                Extension = Path.GetExtension(blob.Uri.LocalPath),
                LocalPath = blob.Uri.LocalPath,
                AbsolutePath = this.azureMediaOptions.MediaEndpoint + blob.Uri.LocalPath,
                Size = blob.Properties.Length,
                CreationTime = blob.Properties.LastModified.GetValueOrDefault().DateTime,
                UseAzureStorage = true,
                HeavyLoadingFileSize = this.azureMediaOptions.MediaHeavyLoadingKbSize
            };
        }

        private static string GetContainerName(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' in AzureStorageMethods cannot be null or empty.");
            }

            if (!localPath.StartsWith("/"))
            {
                throw new Exception("The parameter 'localPath' in AzureStorageMethods must start with a slash character, " +
                                    "followed by the name of the container. For example: '/my-container/my-folder/'");
            }

            var containerName = localPath.Trim('/').SubstringUpToFirst("/");

            if (string.IsNullOrEmpty(containerName))
            {
                throw new Exception("The parameter 'localPath' must include the name of the container.");
            }

            return containerName;
        }

        /// <summary>
        /// The Blob Name does not include the Container name.
        /// </summary>
        private static string GetBlobName(string localPath)
        {
            var containerName = GetContainerName(localPath);

            return localPath
                .ReplaceFirst(containerName, "")
                .Replace("//", "/")
                .Trim('/')
                .Replace("%20", " ");
        }

        private static string GetDirectoryName(string localPath)
        {
            return localPath.TrimEnd('/').Split('/').LastOrDefault();
        }

        private static string GetDirectoryParentLocalPath(string localPath)
        {
            var directoryName = GetDirectoryName(localPath);

            return localPath.Substring(0, localPath.LastIndexOf(directoryName, StringComparison.Ordinal));
        }

        private static string GetFileContentType(string extension)
        {
            var contentType = extension.ToLower() switch
            {
                ".jpg" => "image/jpeg",
                ".jpeg" => "image/jpeg",
                ".JPG" => "image/jpeg",
                ".JPEG" => "image/jpeg",
                ".png" => "image/png",
                ".gif" => "image/gif",
                ".svg" => "image/svg+xml",
                ".pdf" => "application/pdf",
                _ => "application/octet-stream"
            };

            return contentType;
        }
    }
}
