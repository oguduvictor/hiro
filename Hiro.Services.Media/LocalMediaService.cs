﻿using System;
using System.IO;
using Hiro.Services.Interfaces;
using Hiro.Shared.Dtos;

namespace Hiro.Services
{
    public class LocalMediaService : IMediaService
    {
        public MediaDirectory GetDirectory(string localPath)
        {
            throw new NotImplementedException();
        }

        public OperationResult CreateDirectory(string localPath, string newDirectoryName)
        {
            throw new NotImplementedException();
        }

        public OperationResult DeleteDirectory(string localPath)
        {
            throw new NotImplementedException();
        }

        public MediaFile GetFile(string localPath)
        {
            throw new NotImplementedException();
        }

        public OperationResult DeleteFile(string localPath)
        {
            throw new NotImplementedException();
        }

        public OperationResult RenameFile(string localPath, string newFileName)
        {
            throw new NotImplementedException();
        }

        public OperationResult ResizeFile(string localPath, string destinationPath, int width, int compression, bool keepOriginal)
        {
            throw new NotImplementedException();
        }

        public OperationResult UploadFile(string directoryLocalPath, Stream fileStream, string fileName)
        {
            throw new NotImplementedException();
        }
    }
}
