﻿namespace Hiro.Services.Options
{
    public class AzureMediaOptions
    {
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the name of the Azure Storage Container.
        /// </summary>
        public string AzureStorageContainer { get; set; }

        public string MediaEndpoint { get; set; }

        public int MediaHeavyLoadingKbSize { get; set; }
    }
}