﻿using System.IO;
using Hiro.Shared.Dtos;

namespace Hiro.Services.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IMediaService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="keywordFilter"></param>
        /// <returns></returns>
        MediaDirectory GetDirectory(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="newDirectoryName"></param>
        /// <returns></returns>
        OperationResult CreateDirectory(string localPath, string newDirectoryName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        OperationResult DeleteDirectory(string localPath);

        /// <summary>
        /// Maps a Blob to an object of Type FileDto.
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        MediaFile GetFile(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        OperationResult DeleteFile(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="newFileName"></param>
        /// <returns></returns>
        OperationResult RenameFile(string localPath, string newFileName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="destinationPath"></param>
        /// <param name="width"></param>
        /// <param name="compression"></param>
        /// <param name="keepOriginal"></param>
        /// <returns></returns>
        OperationResult ResizeFile(string localPath, string destinationPath, int width, int compression, bool keepOriginal);

        /// <summary>
        ///
        /// </summary>
        /// <param name="directoryLocalPath"></param>
        /// <param name="fileStream"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        OperationResult UploadFile(string directoryLocalPath, Stream fileStream, string fileName);
    }
}