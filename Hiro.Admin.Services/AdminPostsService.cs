using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminPostsService : IAdminPostsService
    {
        public virtual BlogPost AddOrUpdate(HiroDbContext dbContext, BlogPostDto blogPostDto)
        {
            var blogPost = dbContext.BlogPosts.Find(blogPostDto.Id);

            if (blogPost == null)
            {
                blogPost = new BlogPost
                {
                    Id = blogPostDto.Id,
                    Title = blogPostDto.Title,
                    Content = blogPostDto.Content,
                    IsPublished = blogPostDto.IsPublished,
                    PublicationDate = blogPostDto.PublicationDate,
                    Author = dbContext.Users.Find(blogPostDto.Author?.Id),
                    FeaturedImage = blogPostDto.FeaturedImage,
                    FeaturedImageHorizontalCrop = blogPostDto.FeaturedImageHorizontalCrop,
                    FeaturedImageVerticalCrop = blogPostDto.FeaturedImageVerticalCrop,
                    Url = blogPostDto.Url,
                    MetaTitle = blogPostDto.MetaTitle,
                    MetaDescription = blogPostDto.MetaDescription,
                    Excerpt = blogPostDto.Excerpt,
                    IsDisabled = blogPostDto.IsDisabled ?? false,
                    BlogCategories = blogPostDto.BlogCategories.Select(bc => dbContext.BlogCategories.Find(bc.Id)).ToList(),
                    BlogComments = blogPostDto.BlogComments.Select(bc => dbContext.BlogComments.Find(bc.Id)).ToList()
                };

                dbContext.BlogPosts.Add(blogPost);
            }
            else
            {
                blogPost.Title = blogPostDto.Title;
                blogPost.Content = blogPostDto.Content;
                blogPost.IsPublished = blogPostDto.IsPublished;
                blogPost.PublicationDate = blogPostDto.PublicationDate;
                blogPost.Author = dbContext.Users.Find(blogPostDto.Author?.Id);
                blogPost.FeaturedImage = blogPostDto.FeaturedImage;
                blogPost.FeaturedImageHorizontalCrop = blogPostDto.FeaturedImageHorizontalCrop;
                blogPost.FeaturedImageVerticalCrop = blogPostDto.FeaturedImageVerticalCrop;
                blogPost.Url = blogPostDto.Url;
                blogPost.MetaTitle = blogPostDto.MetaTitle;
                blogPost.MetaDescription = blogPostDto.MetaDescription;
                blogPost.Excerpt = blogPostDto.Excerpt;
                blogPost.IsDisabled = blogPostDto.IsDisabled ?? false;
                blogPost.BlogCategories.Clear();
                blogPost.BlogCategories = blogPostDto.BlogCategories.Select(bc => dbContext.BlogCategories.Find(bc.Id)).ToList();
                blogPost.BlogComments.Clear();
                blogPost.BlogComments = blogPostDto.BlogComments.Select(bc => dbContext.BlogComments.Find(bc.Id)).ToList();
            }

            dbContext.SaveChanges();

            return blogPost;
        }
    }
}
