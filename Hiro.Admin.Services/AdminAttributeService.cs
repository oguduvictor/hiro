using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminAttributeService : IAdminAttributeService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminAttributeService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual Attribute AddOrUpdate(HiroDbContext dbContext, AttributeDto attributeDto)
        {
            var attribute = dbContext.Attributes.Find(attributeDto.Id);

            if (attribute == null)
            {
                attribute = new Attribute
                {
                    Id = attributeDto.Id,
                    Name = attributeDto.Name,
                    Url = attributeDto.Url,
                    IsDisabled = attributeDto.IsDisabled ?? false,
                    SortOrder = attributeDto.SortOrder,
                    Type = attributeDto.Type,
                    AttributeLocalizedKits = attributeDto.AttributeLocalizedKits.Select(attributeLocalizedKitDto => new AttributeLocalizedKit
                    {
                        Id = attributeLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(attributeLocalizedKitDto.Country?.Id),
                        Title = attributeLocalizedKitDto.Title,
                        Description = attributeLocalizedKitDto.Description,
                        IsDisabled = attributeLocalizedKitDto.IsDisabled ?? false
                    }).ToList(),
                    AttributeOptions = attributeDto.AttributeOptions.Select(attributeOptionDto => new AttributeOption
                    {
                        Id = attributeOptionDto.Id,
                        Name = attributeOptionDto.Name,
                        Url = attributeOptionDto.Url,
                        Tags = attributeOptionDto.Tags,
                        IsDisabled = attributeOptionDto.IsDisabled ?? false,
                        AttributeOptionLocalizedKits = attributeOptionDto.AttributeOptionLocalizedKits.Select(attributeOptionLocalizedKitDto => new AttributeOptionLocalizedKit
                        {
                            Id = attributeOptionLocalizedKitDto.Id,
                            Country = dbContext.Countries.Find(attributeOptionLocalizedKitDto.Country?.Id),
                            Title = attributeOptionLocalizedKitDto.Title,
                            Description = attributeOptionLocalizedKitDto.Description,
                            IsDisabled = attributeOptionLocalizedKitDto.IsDisabled ?? false
                        }).ToList()
                    }).ToList()
                };

                dbContext.Attributes.Add(attribute);
            }
            else
            {
                attribute.Name = attributeDto.Name;
                attribute.Url = attributeDto.Url;
                attribute.IsDisabled = attributeDto.IsDisabled ?? false;
                attribute.SortOrder = attributeDto.SortOrder;
                attribute.Type = attributeDto.Type;

                foreach (var attributeLocalizedKitDto in attributeDto.AttributeLocalizedKits)
                {
                    var attributeLocalizedKit = attribute.AttributeLocalizedKits.FirstOrDefault(x => x.Id == attributeLocalizedKitDto.Id);

                    if (attributeLocalizedKit == null)
                    {
                        attributeLocalizedKit = new AttributeLocalizedKit
                        {
                            Id = attributeLocalizedKitDto.Id,
                            Description = attributeLocalizedKitDto.Description,
                            Title = attributeLocalizedKitDto.Title,
                            IsDisabled = attributeLocalizedKitDto.IsDisabled ?? false,
                            Attribute = attribute,
                            Country = dbContext.Countries.Find(attributeLocalizedKitDto.Country?.Id)
                        };

                        attribute.AttributeLocalizedKits.Add(attributeLocalizedKit);
                    }
                    else
                    {
                        attributeLocalizedKit.Title = attributeLocalizedKitDto.Title;
                        attributeLocalizedKit.Description = attributeLocalizedKitDto.Description;
                        attributeLocalizedKit.IsDisabled = attributeLocalizedKitDto.IsDisabled ?? false;
                    }
                }

                foreach (var attributeOptionDto in attributeDto.AttributeOptions)
                {
                    if (attributeOptionDto.IsDeleted ?? false)
                    {
                        dbContext.AttributeOptions.Delete(attributeOptionDto.Id, false);
                    }
                    else
                    {
                        var attributeOption = attribute.AttributeOptions.FirstOrDefault(x => x.Id == attributeOptionDto.Id);

                        if (attributeOption == null)
                        {
                            attributeOption = new AttributeOption
                            {
                                Id = attributeOptionDto.Id,
                                Attribute = attribute,
                                Name = attributeOptionDto.Name,
                                Url = attributeOptionDto.Url,
                                Tags = attributeOptionDto.Tags,
                                IsDisabled = attributeOptionDto.IsDisabled ?? false
                            };

                            foreach (var attributeOptionLocalizedKitDto in attributeOptionDto.AttributeOptionLocalizedKits)
                            {
                                var attributeOptionLocalizedKit = attribute.AttributeOptions.SelectMany(x => x.AttributeOptionLocalizedKits).FirstOrDefault(x => x.Id == attributeOptionLocalizedKitDto.Id);

                                if (attributeOptionLocalizedKit == null)
                                {
                                    attributeOptionLocalizedKit = new AttributeOptionLocalizedKit
                                    {
                                        Id = attributeOptionLocalizedKitDto.Id,
                                        AttributeOption = attributeOption,
                                        Title = attributeOptionLocalizedKitDto.Title,
                                        Description = attributeOptionLocalizedKitDto.Description,
                                        IsDisabled = attributeOptionLocalizedKitDto.IsDisabled ?? false,
                                        Country = dbContext.Countries.Find(attributeOptionLocalizedKitDto.Country?.Id)
                                    };

                                    attributeOption.AttributeOptionLocalizedKits.Add(attributeOptionLocalizedKit);
                                }
                                else
                                {
                                    attributeOptionLocalizedKit.Title = attributeOptionLocalizedKitDto.Title;
                                    attributeOptionLocalizedKit.Description = attributeOptionLocalizedKitDto.Description;
                                    attributeOptionLocalizedKit.IsDisabled = attributeOptionLocalizedKitDto.IsDisabled ?? false;
                                }
                            }

                            attribute.AttributeOptions.Add(attributeOption);
                        }
                        else
                        {
                            attributeOption.Name = attributeOptionDto.Name;
                            attributeOption.Url = attributeOptionDto.Url;
                            attributeOption.Tags = attributeOptionDto.Tags;
                            attributeOption.IsDisabled = attributeOptionDto.IsDisabled ?? false;

                            foreach (var attributeOptionLocalizedKitDto in attributeOptionDto.AttributeOptionLocalizedKits)
                            {
                                var attributeOptionLocalizedKit = attribute.AttributeOptions.SelectMany(x => x.AttributeOptionLocalizedKits).FirstOrDefault(x => x.Id == attributeOptionLocalizedKitDto.Id);

                                if (attributeOptionLocalizedKit == null)
                                {
                                    attributeOptionLocalizedKit = new AttributeOptionLocalizedKit
                                    {
                                        Id = attributeOptionLocalizedKitDto.Id,
                                        AttributeOption = attributeOption,
                                        Title = attributeOptionLocalizedKitDto.Title,
                                        Description = attributeOptionLocalizedKitDto.Description,
                                        IsDisabled = attributeOptionLocalizedKitDto.IsDisabled ?? false,
                                        Country = dbContext.Countries.Find(attributeOptionLocalizedKitDto.Country?.Id)
                                    };

                                    attributeOption.AttributeOptionLocalizedKits.Add(attributeOptionLocalizedKit);
                                }
                                else
                                {
                                    attributeOptionLocalizedKit.Title = attributeOptionLocalizedKitDto.Title;
                                    attributeOptionLocalizedKit.Description = attributeOptionLocalizedKitDto.Description;
                                    attributeOptionLocalizedKit.IsDisabled = attributeOptionLocalizedKitDto.IsDisabled ?? false;
                                }
                            }
                        }
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedAttributeLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(attribute.AttributeLocalizedKits);

            foreach (var localizedKit in duplicatedAttributeLocalizedKitsToDelete)
            {
                dbContext.AttributeLocalizedKits.Delete(localizedKit.Id, false);
            }

            foreach (var attributeOption in attribute.AttributeOptions)
            {
                var duplicatedAttributeOptionLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(attributeOption.AttributeOptionLocalizedKits);

                foreach (var localizedKit in duplicatedAttributeOptionLocalizedKitsToDelete)
                {
                    dbContext.AttributeOptionLocalizedKits.Delete(localizedKit.Id, false);
                }
            }

            dbContext.SaveChanges();

            return attribute;
        }

        public virtual void UpdateSortOrder(HiroDbContext dbContext, List<AttributeDto> attributeDtos)
        {
            foreach (var attributeDto in attributeDtos)
            {
                var attribute = dbContext.Attributes.FirstOrDefault(x => x.Id == attributeDto.Id);

                if (attribute == null)
                {
                    throw new System.NullReferenceException();
                }

                attribute.SortOrder = attributeDtos.FindIndex(x => x.Id == attribute.Id);
            }

            dbContext.SaveChanges();
        }
    }
}