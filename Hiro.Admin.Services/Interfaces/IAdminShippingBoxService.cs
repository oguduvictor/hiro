using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminShippingBoxService
    {
        ShippingBox AddOrUpdate(HiroDbContext dbContext, ShippingBoxDto shippingBoxDto);

        ShippingBox Duplicate(HiroDbContext dbContext, ShippingBox shippingBox);

        void UpdateSortOrder(HiroDbContext dbContext, List<ShippingBoxDto> shippingBoxs);
    }
}
