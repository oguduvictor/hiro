﻿using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminVariantService
    {
        Variant AddOrUpdate(HiroDbContext dbContext, VariantDto variantDto);

        VariantOption AddOrUpdateVariantOption(HiroDbContext dbContext, VariantOptionDto variantOptionDto);

        void UpdateSortOrder(HiroDbContext dbContext, List<VariantDto> variantDtos);
    }
}