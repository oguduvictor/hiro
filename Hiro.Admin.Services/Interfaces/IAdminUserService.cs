﻿using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminUserService
    {
        User AddOrUpdate(HiroDbContext dbContext, UserDto userDto);

        UserRole AddOrUpdateUserRole(HiroDbContext dbContext, UserRoleDto userRoleDto);

        UserCredit AddOrUpdateUserCredit(HiroDbContext dbContext, UserCreditDto userCreditDto);

        void AddOrUpdateUserCredits(HiroDbContext dbContext, List<UserCreditDto> userCreditDtos);

    }
}