using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminCouponService
    {
        Coupon AddOrUpdate(HiroDbContext dbContext, CouponDto couponDto, int quantity = 1);
    }
}
