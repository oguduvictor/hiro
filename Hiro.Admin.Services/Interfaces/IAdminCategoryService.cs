﻿using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminCategoryService
    {
        IEnumerable<Category> GetRootCategories(HiroDbContext dbContext);

        Category AddOrUpdate(HiroDbContext dbContext, CategoryDto categoryDto);

        void UpdateHierarchy(HiroDbContext dbContext, List<CategoryDto> rootCategories);
    }
}