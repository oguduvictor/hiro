﻿using System.Collections.Generic;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Interfaces;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminUtilityService
    {
        List<T> UpdateLocalizedKits<T>(List<T> localizedKits, T localizedKit) where T : BaseEntity, ILocalizedKit, new();

        List<T> GetDuplicatedLocalizedKitsToDelete<T>(List<T> localizedKits) where T : BaseEntity, ILocalizedKit;

        string CreateBackupFileName(string directoryLocalPath);

        UserDto GetAuthenticatedUserDto();

        List<TBaseEntity> GetEntitiesToDelete<TBaseEntity, TBaseEntityDto>(IEnumerable<TBaseEntity> originalCollection, IList<TBaseEntityDto> newCollection) where TBaseEntity : BaseEntity where TBaseEntityDto : BaseEntityDto;
    }
}