﻿using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminMailingListService
    {
        MailingList AddOrUpdateMailingList(HiroDbContext dbContext, MailingListDto mailingListDto);

        void UpdateSortOrder(HiroDbContext dbContext, List<MailingListDto> mailingListDtos);

        MailingList Duplicate(HiroDbContext dbContext, MailingList mailingList);
    }
}
