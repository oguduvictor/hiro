using System;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminCountryService
    {
        Country AddOrUpdate(HiroDbContext dbContext, CountryDto countryDto);

        OperationResult SetDefaultCountry(HiroDbContext dbContext, Guid countryId);
    }
}
