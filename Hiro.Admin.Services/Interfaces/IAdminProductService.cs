﻿using System;
using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminProductService
    {
        Product AddOrUpdate(HiroDbContext dbContext, ProductDto productDto);

        void AddOrUpdateProductVariants(HiroDbContext dbContext, Guid productId, List<ProductVariantDto> productVariantDtos);

        void AddOrUpdateProductAttributes(HiroDbContext dbContext, Guid productId, List<ProductAttributeDto> productAttributeDtos);

        void AddOrUpdateProductImageKits(HiroDbContext dbContext, Guid productId, List<ProductImageKitDto> productImageKitDtos);

        Product Duplicate(HiroDbContext dbContext, Product product);

        void AddOrUpdateProductStockUnits(HiroDbContext dbContext, Guid productId, List<ProductStockUnitDto> productStockUnitDtos);

        ContentSection AddContentSection(HiroDbContext dbContext, ContentSectionDto contentSectionDto);
    }
}