using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminBlogCategoryService
    {
        BlogCategory AddOrUpdate(HiroDbContext dbContext, BlogCategoryDto blogCategoryDto);
    }
}
