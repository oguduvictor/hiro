﻿using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminOrderService
    {
        Order Update(HiroDbContext dbContext, OrderDto orderDto);
    }
}