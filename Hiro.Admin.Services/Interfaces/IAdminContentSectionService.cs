using Hiro.DataAccess;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminContentSectionService
    {
        void AddOrUpdate(HiroDbContext dbContext, ContentSectionDto contentSectionDto);
    }
}
