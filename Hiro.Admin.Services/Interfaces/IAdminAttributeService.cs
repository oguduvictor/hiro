using System.Collections.Generic;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminAttributeService
    {
        Attribute AddOrUpdate(HiroDbContext dbContext, AttributeDto attributeDto);

        void UpdateSortOrder(HiroDbContext dbContext, List<AttributeDto> attributeDtos);
    }
}
