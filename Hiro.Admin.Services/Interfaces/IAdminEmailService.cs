using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;

namespace Hiro.Admin.Server.Services.Interfaces
{
    public interface IAdminEmailService
    {
        Email AddOrUpdate(HiroDbContext dbContext, EmailDto emailDto);

        ContentSection AddContentSection(HiroDbContext dbContext, ContentSectionDto contentSectionDto);
    }
}
