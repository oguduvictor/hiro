﻿using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Services.Interfaces;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminOrderService : IAdminOrderService
    {
        private readonly IAutoTagService autoTagService;

        public AdminOrderService(IAutoTagService autoTagService)
        {
            this.autoTagService = autoTagService;
        }

        public virtual Order Update(HiroDbContext dbContext, OrderDto orderDto)
        {
            var order = dbContext.Orders.Find(orderDto.Id);

            if (order == null)
            {
                return null;
            }

            order.Comments = orderDto.Comments;
            order.Status = orderDto.Status;

            foreach (var orderShippingBox in order.OrderShippingBoxes)
            {
                var orderShippingBoxDto = orderDto.OrderShippingBoxes.FirstOrDefault(x => x.Id == orderShippingBox.Id);

                orderShippingBox.TrackingCode = orderShippingBoxDto?.TrackingCode;
                orderShippingBox.Status = orderShippingBoxDto?.Status ?? OrderShippingBoxStatusEnum.Preparing;

                foreach (var orderItem in orderShippingBox.OrderItems)
                {
                    var orderItemDto = orderShippingBoxDto?.OrderItems.FirstOrDefault(x => x.Id == orderItem.Id);

                    orderItem.Status = orderItemDto?.Status ?? OrderItemStatusEnum.Default;
                }
            }

            order.AutoTags = this.autoTagService.GetAutoTags(order);

            dbContext.SaveChanges();

            return order;
        }
    }
}
