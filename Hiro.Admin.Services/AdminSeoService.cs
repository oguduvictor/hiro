using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminSeoService : IAdminSeoService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminSeoService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual SeoSection AddOrUpdate(HiroDbContext dbContext, SeoSectionDto seoSectionDto)
        {
            var seoSection = dbContext.SeoSections.Find(seoSectionDto.Id);

            if (seoSection == null)
            {
                seoSection = new SeoSection
                {
                    Id = seoSectionDto.Id,
                    Page = seoSectionDto.Page,
                    IsDisabled = seoSectionDto.IsDisabled ?? false,
                    SeoSectionLocalizedKits = seoSectionDto.SeoSectionLocalizedKits.Select(seoSectionLocalizedKitDto => new SeoSectionLocalizedKit
                    {
                        Id = seoSectionLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(seoSectionLocalizedKitDto.Country?.Id),
                        Title = seoSectionLocalizedKitDto.Title,
                        Description = seoSectionLocalizedKitDto.Description,
                        Image = seoSectionLocalizedKitDto.Image,
                        IsDisabled = seoSectionLocalizedKitDto.IsDisabled ?? false
                    }).ToList()
                };

                dbContext.SeoSections.Add(seoSection);
            }
            else
            {
                seoSection.Page = seoSectionDto.Page;
                seoSection.IsDisabled = seoSectionDto.IsDisabled ?? false;

                foreach (var seoSectionLocalizedKitDto in seoSectionDto.SeoSectionLocalizedKits)
                {
                    var seoSectionLocalizedKit = seoSection.SeoSectionLocalizedKits.FirstOrDefault(x => x.Id == seoSectionLocalizedKitDto.Id);

                    if (seoSectionLocalizedKit == null)
                    {
                        seoSectionLocalizedKit = new SeoSectionLocalizedKit
                        {
                            Id = seoSectionLocalizedKitDto.Id,
                            Title = seoSectionLocalizedKitDto.Title,
                            Description = seoSectionLocalizedKitDto.Description,
                            Image = seoSectionLocalizedKitDto.Image,
                            IsDisabled = seoSectionLocalizedKitDto.IsDisabled ?? false,
                            SeoSection = seoSection,
                            Country = dbContext.Countries.Find(seoSectionLocalizedKitDto.Country?.Id)
                        };

                        seoSection.SeoSectionLocalizedKits.Add(seoSectionLocalizedKit);
                    }
                    else
                    {
                        seoSectionLocalizedKit.Title = seoSectionLocalizedKitDto.Title;
                        seoSectionLocalizedKit.Description = seoSectionLocalizedKitDto.Description;
                        seoSectionLocalizedKit.Image = seoSectionLocalizedKitDto.Image;
                        seoSectionLocalizedKit.IsDisabled = seoSectionLocalizedKitDto.IsDisabled ?? false;
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(seoSection.SeoSectionLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.SeoSectionLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return seoSection;
        }

    }
}
