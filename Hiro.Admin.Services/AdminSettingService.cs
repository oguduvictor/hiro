using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Services.Interfaces;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminSettingService : IAdminSettingService
    {
        private readonly IEncryptionService encryptionService;

        public AdminSettingService(IEncryptionService encryptionService)
        {
            this.encryptionService = encryptionService;
        }

        public virtual CmsSettings AddOrUpdate(HiroDbContext dbContext, CmsSettingsDto cmsSettingsDto)
        {
            var cmsSettings = dbContext.CmsSettings.Find(cmsSettingsDto.Id);

            if (cmsSettings == null)
            {
                cmsSettings = new CmsSettings
                {
                    Id = cmsSettingsDto.Id,
                    IsSeeded = cmsSettingsDto.IsSeeded,
                    SmtpHost = cmsSettingsDto.SmtpHost,
                    SmtpPort = cmsSettingsDto.SmtpPort,
                    SmtpDisplayName = cmsSettingsDto.SmtpDisplayName,
                    SmtpEmail = cmsSettingsDto.SmtpEmail,
                    SmtpPassword = this.encryptionService.Encrypt(cmsSettingsDto.SmtpPassword),
                    UseAzureStorage = cmsSettingsDto.UseAzureStorage,
                    IsDisabled = cmsSettingsDto.IsDisabled ?? false
                };

                dbContext.CmsSettings.Add(cmsSettings);
            }
            else
            {
                cmsSettings.IsSeeded = cmsSettingsDto.IsSeeded;
                cmsSettings.SmtpHost = cmsSettingsDto.SmtpHost;
                cmsSettings.SmtpPort = cmsSettingsDto.SmtpPort;
                cmsSettings.SmtpDisplayName = cmsSettingsDto.SmtpDisplayName;
                cmsSettings.SmtpEmail = cmsSettingsDto.SmtpEmail;
                cmsSettings.SmtpPassword = this.encryptionService.Encrypt(cmsSettingsDto.SmtpPassword);
                cmsSettings.UseAzureStorage = cmsSettingsDto.UseAzureStorage;
                cmsSettings.IsDisabled = cmsSettingsDto.IsDisabled ?? false;
            }

            dbContext.SaveChanges();

            return cmsSettings;
        }
    }
}
