﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminVariantService : IAdminVariantService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminVariantService(
            IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual Variant AddOrUpdate(HiroDbContext dbContext, VariantDto variantDto)
        {
            var variant = dbContext.Variants.Find(variantDto.Id);

            if (variant == null)
            {
                variant = new Variant
                {
                    Id = variantDto.Id,
                    Name = variantDto.Name,
                    Url = variantDto.Url,
                    Type = variantDto.Type,
                    SortOrder = variantDto.SortOrder,
                    IsDisabled = variantDto.IsDisabled ?? false,
                    CreateProductImageKits = variantDto.CreateProductImageKits,
                    CreateProductStockUnits = variantDto.CreateProductStockUnits,
                    DefaultBooleanValue = variantDto.DefaultBooleanValue,
                    DefaultDoubleValue = variantDto.DefaultDoubleValue,
                    DefaultIntegerValue = variantDto.DefaultIntegerValue,
                    DefaultJsonValue = variantDto.DefaultJsonValue,
                    DefaultStringValue = variantDto.DefaultStringValue,
                    DefaultVariantOptionValue = dbContext.VariantOptions.Find(variantDto.DefaultVariantOptionValue?.Id),
                    VariantLocalizedKits = variantDto.VariantLocalizedKits.Select(variantLocalizedKitDto => new VariantLocalizedKit
                    {
                        Id = variantLocalizedKitDto.Id,
                        Variant = variant,
                        Country = dbContext.Countries.Find(variantLocalizedKitDto.Country?.Id),
                        Title = variantLocalizedKitDto.Title,
                        Description = variantLocalizedKitDto.Description,
                        IsDisabled = variantLocalizedKitDto.IsDisabled ?? false
                    }).ToList()
                };

                dbContext.Variants.Add(variant);
            }
            else
            {
                variant.Name = variantDto.Name;
                variant.Url = variantDto.Url;
                variant.Type = variantDto.Type;
                variant.SortOrder = variantDto.SortOrder;
                variant.IsDisabled = variantDto.IsDisabled ?? false;
                variant.CreateProductImageKits = variantDto.CreateProductImageKits;
                variant.CreateProductStockUnits = variantDto.CreateProductStockUnits;
                variant.DefaultBooleanValue = variantDto.DefaultBooleanValue;
                variant.DefaultDoubleValue = variantDto.DefaultDoubleValue;
                variant.DefaultIntegerValue = variantDto.DefaultIntegerValue;
                variant.DefaultJsonValue = variantDto.DefaultJsonValue;
                variant.DefaultStringValue = variantDto.DefaultStringValue;
                variant.DefaultVariantOptionValue = dbContext.VariantOptions.Find(variantDto.DefaultVariantOptionValue?.Id);

                foreach (var variantOptionDto in variantDto.VariantOptions)
                {
                    var variantOption = dbContext.VariantOptions.Find(variantOptionDto.Id);

                    if (variantOption == null)
                    {
                        throw new NullReferenceException();
                    }

                    variantOption.SortOrder = variantDto.VariantOptions.FindIndex(x => x.Id == variantOption.Id);
                }

                foreach (var variantLocalizedKitDto in variantDto.VariantLocalizedKits)
                {
                    var variantLocalizedKit = variant.VariantLocalizedKits.FirstOrDefault(x => x.Id == variantLocalizedKitDto.Id);

                    if (variantLocalizedKit == null)
                    {
                        variantLocalizedKit = new VariantLocalizedKit
                        {
                            Id = variantLocalizedKitDto.Id,
                            Variant = variant,
                            Country = dbContext.Countries.Find(variantLocalizedKitDto.Country?.Id),
                            Title = variantLocalizedKitDto.Title,
                            Description = variantLocalizedKitDto.Description,
                            IsDisabled = variantLocalizedKitDto.IsDisabled ?? false
                        };

                        variant.VariantLocalizedKits.Add(variantLocalizedKit);
                    }
                    else
                    {
                        variantLocalizedKit.Title = variantLocalizedKitDto.Title;
                        variantLocalizedKit.Description = variantLocalizedKitDto.Description;
                        variantLocalizedKit.IsDisabled = variantLocalizedKitDto.IsDisabled ?? false;
                    }
                }
            }

            dbContext.SaveChanges();

            return variant;
        }

        public virtual VariantOption AddOrUpdateVariantOption(HiroDbContext dbContext, VariantOptionDto variantOptionDto)
        {
            var variant = dbContext.Variants.Find(variantOptionDto.Variant?.Id);

            if (variant == null)
            {
                throw new NullReferenceException();
            }

            var variantOption = dbContext.VariantOptions.Find(variantOptionDto.Id);

            if (variantOption == null)
            {
                variantOption = new VariantOption
                {
                    Id = variantOptionDto.Id,
                    Code = variantOptionDto.Code,
                    Image = variantOptionDto.Image,
                    IsDisabled = variantOptionDto.IsDisabled ?? false,
                    Name = variantOptionDto.Name,
                    SortOrder = variantOptionDto.SortOrder,
                    Tags = variantOptionDto.Tags,
                    Url = variantOptionDto.Url,
                    Variant = variant,
                    VariantOptionLocalizedKits = variantOptionDto.VariantOptionLocalizedKits.Select(variantOptionLocalizedKitDto => new VariantOptionLocalizedKit
                    {
                        Id = variantOptionLocalizedKitDto.Id,
                        Title = variantOptionLocalizedKitDto.Title,
                        Description = variantOptionLocalizedKitDto.Description,
                        PriceModifier = variantOptionLocalizedKitDto.PriceModifier,
                        IsDisabled = variantOptionLocalizedKitDto.IsDisabled ?? false,
                        Country = dbContext.Countries.Find(variantOptionLocalizedKitDto.Country?.Id),
                        VariantOption = variantOption
                    }).ToList()
                };

                dbContext.VariantOptions.Add(variantOption);
            }
            else
            {
                variantOption.Code = variantOptionDto.Code;
                variantOption.Image = variantOptionDto.Image;
                variantOption.IsDisabled = variantOptionDto.IsDisabled ?? false;
                variantOption.Name = variantOptionDto.Name;
                variantOption.SortOrder = variantOptionDto.SortOrder;
                variantOption.Tags = variantOptionDto.Tags;
                variantOption.Url = variantOptionDto.Url;

                foreach (var variantOptionLocalizedKitDto in variantOptionDto.VariantOptionLocalizedKits)
                {
                    var variantOptionLocalizedKit = dbContext.VariantOptionLocalizedKits.Find(variantOptionLocalizedKitDto.Id);

                    if (variantOptionLocalizedKit == null)
                    {
                        variantOptionLocalizedKit = new VariantOptionLocalizedKit
                        {
                            Id = variantOptionLocalizedKitDto.Id,
                            Country = dbContext.Countries.Find(variantOptionLocalizedKitDto.Country?.Id),
                            Description = variantOptionLocalizedKitDto.Description,
                            IsDisabled = variantOptionLocalizedKitDto.IsDisabled ?? false,
                            PriceModifier = variantOptionLocalizedKitDto.PriceModifier,
                            Title = variantOptionLocalizedKitDto.Title,
                            VariantOption = variantOption
                        };

                        variantOption.VariantOptionLocalizedKits.Add(variantOptionLocalizedKit);
                    }
                    else
                    {
                        variantOptionLocalizedKit.IsDisabled = variantOptionLocalizedKitDto.IsDisabled ?? false;
                        variantOptionLocalizedKit.Description = variantOptionLocalizedKitDto.Description;
                        variantOptionLocalizedKit.PriceModifier = variantOptionLocalizedKitDto.PriceModifier;
                        variantOptionLocalizedKit.Title = variantOptionLocalizedKitDto.Title;
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(variantOption.VariantOptionLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.VariantOptionLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return variantOption;
        }

        public virtual void UpdateSortOrder(HiroDbContext dbContext, List<VariantDto> variantDtos)
        {
            foreach (var variantDto in variantDtos)
            {
                var variant = dbContext.Variants.FirstOrDefault(x => x.Id == variantDto.Id);

                if (variant == null)
                {
                    throw new NullReferenceException();
                }

                variant.SortOrder = variantDtos.FindIndex(x => x.Id == variant.Id);
            }

            dbContext.SaveChanges();
        }
    }
}
