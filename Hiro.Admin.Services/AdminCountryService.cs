using System;
using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminCountryService : IAdminCountryService
    {
        public virtual Country AddOrUpdate(HiroDbContext dbContext, CountryDto countryDto)
        {
            var country = dbContext.Countries.Find(countryDto.Id);

            if (country == null)
            {
                country = new Country
                {
                    Id = countryDto.Id,
                    IsDisabled = countryDto.IsDisabled ?? false,
                    FlagIcon = countryDto.FlagIcon,
                    Url = countryDto.Url,
                    Name = countryDto.Name,
                    LanguageCode = countryDto.LanguageCode,
                    IsDefault = countryDto.IsDefault,
                    Localize = countryDto.Localize,
                    PaymentAccountId = countryDto.PaymentAccountId
                };

                dbContext.Countries.Add(country);
            }
            else
            {
                country.IsDisabled = countryDto.IsDisabled ?? false;
                country.FlagIcon = countryDto.FlagIcon;
                country.Url = countryDto.Url;
                country.Name = countryDto.Name;
                country.LanguageCode = countryDto.LanguageCode;
                country.IsDefault = countryDto.IsDefault;
                country.Localize = countryDto.Localize;
                country.PaymentAccountId = countryDto.PaymentAccountId;

                foreach (var taxDto in countryDto.Taxes)
                {
                    if (taxDto.IsDeleted ?? false)
                    {
                        dbContext.Taxes.Delete(taxDto.Id, false);
                    }
                    else
                    {
                        var tax = country.Taxes.FirstOrDefault(x => x.Id == taxDto.Id);

                        if (tax == null)
                        {
                            tax = new Tax
                            {
                                Id = taxDto.Id,
                                Country = country,
                                Name = taxDto.Name,
                                Code = taxDto.Code,
                                Percentage = taxDto.Percentage,
                                Amount = taxDto.Amount,
                                ApplyToProductPrice = taxDto.ApplyToProductPrice,
                                ApplyToShippingPrice = taxDto.ApplyToShippingPrice
                            };

                            country.Taxes.Add(tax);
                        }
                        else
                        {
                            tax.Name = taxDto.Name;
                            tax.Code = taxDto.Code;
                            tax.Percentage = taxDto.Percentage;
                            tax.ApplyToProductPrice = taxDto.ApplyToProductPrice;
                            tax.ApplyToShippingPrice = taxDto.ApplyToShippingPrice;
                        }
                    }
                }
            }

            dbContext.SaveChanges();

            return country;
        }

        public virtual OperationResult SetDefaultCountry(HiroDbContext dbContext, Guid countryId)
        {
            var operationResult = new OperationResult();

            var country = dbContext.Countries.Find(countryId);

            if (country == null)
            {
                operationResult.Errors.Add(new OperationError("Country not found"));
                return operationResult;
            }

            if (country.IsDisabled)
            {
                operationResult.Errors.Add(new OperationError("A disabled Country cannot be set as Default"));
                return operationResult;
            }

            var oldDefaultCountries = dbContext.Countries.Where(x => x.IsDefault);
            foreach (var oldDefaultCountry in oldDefaultCountries)
            {
                oldDefaultCountry.IsDefault = false;
            }

            country.IsDefault = true;

            dbContext.SaveChanges();

            return operationResult;
        }

    }
}