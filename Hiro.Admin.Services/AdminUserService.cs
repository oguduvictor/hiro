﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Services.Interfaces;

#pragma warning disable 1591

namespace Hiro.Admin.Server.Services
{
    public class AdminUserService : IAdminUserService
    {
        private readonly IEncryptionService encryptionService;
        private readonly IAdminUtilityService adminUtilityService;

        public AdminUserService(
            IEncryptionService encryptionService,
            IAdminUtilityService adminUtilityService)
        {
            this.encryptionService = encryptionService;
            this.adminUtilityService = adminUtilityService;
        }

        public virtual User AddOrUpdate(HiroDbContext dbContext, UserDto userDto)
        {
            var user = dbContext.Users.Find(userDto.Id);

            if (user == null)
            {
                user = new User
                {
                    Id = userDto.Id,
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    Email = userDto.Email,
                    Gender = userDto.Gender,
                    Password = this.encryptionService.Encrypt(userDto.Password),
                    ShowMembershipSalePrice = userDto.ShowMembershipSalePrice,
                    IsDisabled = userDto.IsDisabled ?? false,
                    Country = dbContext.Countries.Find(userDto.Country?.Id),
                    Role = dbContext.UserRoles.Find(userDto.Role?.Id)
                };

                dbContext.Users.Add(user);
            }
            else
            {
                user.Id = userDto.Id;
                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Email = userDto.Email;
                user.Gender = userDto.Gender;
                user.Password = this.encryptionService.Encrypt(userDto.Password);
                user.ShowMembershipSalePrice = userDto.ShowMembershipSalePrice;
                user.IsDisabled = userDto.IsDisabled ?? false;
                user.Country = dbContext.Countries.Find(userDto.Country?.Id);
                user.Role = dbContext.UserRoles.Find(userDto.Role?.Id);

                if (!string.IsNullOrEmpty(userDto.Password))
                {
                    user.Password = this.encryptionService.Encrypt(userDto.Password);
                }

                foreach (var addressDto in userDto.Addresses)
                {
                    var address = user.Addresses.FirstOrDefault(x => x.Id == addressDto.Id);

                    if (address == null)
                    {
                        address = new Address
                        {
                            Id = addressDto.Id,
                            User = dbContext.Users.Find(addressDto.User?.Id),
                            Country = dbContext.Countries.Find(addressDto.Country?.Id),
                            FirstName = addressDto.FirstName,
                            LastName = addressDto.LastName,
                            AddressLine1 = addressDto.AddressLine1,
                            AddressLine2 = addressDto.AddressLine2,
                            City = addressDto.City,
                            StateCountyProvince = addressDto.StateCountyProvince,
                            Postcode = addressDto.Postcode,
                            PhoneNumber = addressDto.PhoneNumber,
                            IsDisabled = addressDto.IsDisabled ?? false
                        };

                        user.Addresses.Add(address);
                    }
                    else
                    {
                        address.Country = dbContext.Countries.Find(addressDto.Country?.Id);
                        address.FirstName = addressDto.FirstName;
                        address.LastName = addressDto.LastName;
                        address.AddressLine1 = addressDto.AddressLine1;
                        address.AddressLine2 = addressDto.AddressLine2;
                        address.City = addressDto.City;
                        address.StateCountyProvince = addressDto.StateCountyProvince;
                        address.Postcode = addressDto.Postcode;
                        address.PhoneNumber = addressDto.PhoneNumber;
                        address.IsDisabled = addressDto.IsDisabled ?? false;
                    }
                }
            }

            dbContext.SaveChanges();

            return user;
        }

        public virtual UserRole AddOrUpdateUserRole(HiroDbContext dbContext, UserRoleDto userRoleDto)
        {
            var userRole = dbContext.UserRoles.Find(userRoleDto.Id);

            if (userRole == null)
            {
                userRole = new UserRole
                {
                    Id = userRoleDto.Id,
                    AccessAdmin = userRoleDto.AccessAdmin,
                    AccessAdminBlog = userRoleDto.AccessAdminBlog,
                    AccessAdminContent = userRoleDto.AccessAdminContent,
                    AccessAdminCountries = userRoleDto.AccessAdminCountries,
                    AccessAdminEcommerce = userRoleDto.AccessAdminEcommerce,
                    AccessAdminEmails = userRoleDto.AccessAdminEmails,
                    AccessAdminMedia = userRoleDto.AccessAdminMedia,
                    AccessAdminOrders = userRoleDto.AccessAdminOrders,
                    AccessAdminSeo = userRoleDto.AccessAdminSeo,
                    AccessAdminSettings = userRoleDto.AccessAdminSettings,
                    AccessAdminUsers = userRoleDto.AccessAdminUsers,
                    CreateAttribute = userRoleDto.CreateAttribute,
                    CreateAttributeOption = userRoleDto.CreateAttributeOption,
                    CreateBlogPost = userRoleDto.CreateBlogPost,
                    CreateContentSection = userRoleDto.CreateContentSection,
                    CreateCountry = userRoleDto.CreateCountry,
                    CreateCoupon = userRoleDto.CreateCoupon,
                    CreateCategory = userRoleDto.CreateCategory,
                    CreateEmail = userRoleDto.CreateEmail,
                    CreateMailingList = userRoleDto.CreateMailingList,
                    CreateProduct = userRoleDto.CreateProduct,
                    CreateSeoSection = userRoleDto.CreateSeoSection,
                    CreateShippingBox = userRoleDto.CreateShippingBox,
                    CreateUser = userRoleDto.CreateUser,
                    CreateUserRole = userRoleDto.CreateUserRole,
                    CreateVariant = userRoleDto.CreateVariant,
                    CreateVariantOption = userRoleDto.CreateVariantOption,
                    DeleteAttribute = userRoleDto.DeleteAttribute,
                    DeleteAttributeOption = userRoleDto.DeleteAttributeOption,
                    DeleteBlogPost = userRoleDto.DeleteBlogPost,
                    DeleteContentSection = userRoleDto.DeleteContentSection,
                    DeleteCountry = userRoleDto.DeleteCountry,
                    DeleteCoupon = userRoleDto.DeleteCoupon,
                    DeleteCategory = userRoleDto.DeleteCategory,
                    DeleteEmail = userRoleDto.DeleteEmail,
                    DeleteMailingList = userRoleDto.DeleteMailingList,
                    DeleteProduct = userRoleDto.DeleteProduct,
                    DeleteSeoSection = userRoleDto.DeleteSeoSection,
                    DeleteShippingBox = userRoleDto.DeleteShippingBox,
                    DeleteUser = userRoleDto.DeleteUser,
                    DeleteUserRole = userRoleDto.DeleteUserRole,
                    DeleteVariant = userRoleDto.DeleteVariant,
                    DeleteVariantOption = userRoleDto.DeleteVariantOption,
                    EditAttribute = userRoleDto.EditAttribute,
                    EditAttributeAllLocalizedKits = userRoleDto.EditAttributeAllLocalizedKits,
                    EditAttributeName = userRoleDto.EditAttributeName,
                    EditAttributeOption = userRoleDto.EditAttributeOption,
                    EditAttributeOptionAllLocalizedKits = userRoleDto.EditAttributeOptionAllLocalizedKits,
                    EditAttributeOptionName = userRoleDto.EditAttributeOptionName,
                    EditBlogPost = userRoleDto.EditBlogPost,
                    EditContentSection = userRoleDto.EditContentSection,
                    EditContentSectionAllLocalizedKits = userRoleDto.EditContentSectionAllLocalizedKits,
                    EditContentSectionName = userRoleDto.EditContentSectionName,
                    EditCountry = userRoleDto.EditCountry,
                    EditCoupon = userRoleDto.EditCoupon,
                    EditCategory = userRoleDto.EditCategory,
                    EditCategoryAllLocalizedKits = userRoleDto.EditCategoryAllLocalizedKits,
                    EditCategoryName = userRoleDto.EditCategoryName,
                    EditCategoryUrl = userRoleDto.EditCategoryUrl,
                    EditEmail = userRoleDto.EditEmail,
                    EditMailingList = userRoleDto.EditMailingList,
                    EditMailingListName = userRoleDto.EditMailingListName,
                    EditEmailAllLocalizedKits = userRoleDto.EditEmailAllLocalizedKits,
                    EditMailingListAllLocalizedKits = userRoleDto.EditMailingListAllLocalizedKits,
                    EditEmailName = userRoleDto.EditEmailName,
                    EditOrder = userRoleDto.EditOrder,
                    EditProduct = userRoleDto.EditProduct,
                    EditProductAllLocalizedKits = userRoleDto.EditProductAllLocalizedKits,
                    EditSeoSection = userRoleDto.EditSeoSection,
                    EditSeoSectionAllLocalizedKits = userRoleDto.EditSeoSectionAllLocalizedKits,
                    EditSeoSectionPage = userRoleDto.EditSeoSectionPage,
                    EditShippingBox = userRoleDto.EditShippingBox,
                    EditShippingBoxAllLocalizedKits = userRoleDto.EditShippingBoxAllLocalizedKits,
                    EditShippingBoxName = userRoleDto.EditShippingBoxName,
                    EditUser = userRoleDto.EditUser,
                    EditUserRole = userRoleDto.EditUserRole,
                    EditVariant = userRoleDto.EditVariant,
                    EditVariantAllLocalizedKits = userRoleDto.EditVariantAllLocalizedKits,
                    EditVariantName = userRoleDto.EditVariantName,
                    EditVariantSelector = userRoleDto.EditVariantSelector,
                    EditVariantOption = userRoleDto.EditVariantOption,
                    EditVariantOptionAllLocalizedKits = userRoleDto.EditVariantOptionAllLocalizedKits,
                    EditVariantOptionName = userRoleDto.EditVariantOptionName,
                    IsDisabled = userRoleDto.IsDisabled ?? false,
                    Name = userRoleDto.Name,
                    ViewExceptionLogs = userRoleDto.ViewExceptionLogs,
                    ViewEcommerceLogs = userRoleDto.ViewEcommerceLogs,
                    ViewContentLogs = userRoleDto.ViewContentLogs
                };

                dbContext.UserRoles.Add(userRole);
            }
            else
            {
                userRole.AccessAdmin = userRoleDto.AccessAdmin;
                userRole.AccessAdminBlog = userRoleDto.AccessAdminBlog;
                userRole.AccessAdminContent = userRoleDto.AccessAdminContent;
                userRole.AccessAdminCountries = userRoleDto.AccessAdminCountries;
                userRole.AccessAdminEcommerce = userRoleDto.AccessAdminEcommerce;
                userRole.AccessAdminEmails = userRoleDto.AccessAdminEmails;
                userRole.AccessAdminMedia = userRoleDto.AccessAdminMedia;
                userRole.AccessAdminOrders = userRoleDto.AccessAdminOrders;
                userRole.AccessAdminSeo = userRoleDto.AccessAdminSeo;
                userRole.AccessAdminSettings = userRoleDto.AccessAdminSettings;
                userRole.AccessAdminUsers = userRoleDto.AccessAdminUsers;
                userRole.CreateAttribute = userRoleDto.CreateAttribute;
                userRole.CreateAttributeOption = userRoleDto.CreateAttributeOption;
                userRole.CreateBlogPost = userRoleDto.CreateBlogPost;
                userRole.CreateContentSection = userRoleDto.CreateContentSection;
                userRole.CreateCountry = userRoleDto.CreateCountry;
                userRole.CreateCoupon = userRoleDto.CreateCoupon;
                userRole.CreateCategory = userRoleDto.CreateCategory;
                userRole.CreateEmail = userRoleDto.CreateEmail;
                userRole.CreateMailingList = userRoleDto.CreateMailingList;
                userRole.CreateProduct = userRoleDto.CreateProduct;
                userRole.CreateSeoSection = userRoleDto.CreateSeoSection;
                userRole.CreateShippingBox = userRoleDto.CreateShippingBox;
                userRole.CreateUser = userRoleDto.CreateUser;
                userRole.CreateUserRole = userRoleDto.CreateUserRole;
                userRole.CreateVariant = userRoleDto.CreateVariant;
                userRole.CreateVariantOption = userRoleDto.CreateVariantOption;
                userRole.DeleteAttribute = userRoleDto.DeleteAttribute;
                userRole.DeleteAttributeOption = userRoleDto.DeleteAttributeOption;
                userRole.DeleteBlogPost = userRoleDto.DeleteBlogPost;
                userRole.DeleteContentSection = userRoleDto.DeleteContentSection;
                userRole.DeleteCountry = userRoleDto.DeleteCountry;
                userRole.DeleteCoupon = userRoleDto.DeleteCoupon;
                userRole.DeleteCategory = userRoleDto.DeleteCategory;
                userRole.DeleteEmail = userRoleDto.DeleteEmail;
                userRole.DeleteMailingList = userRoleDto.DeleteMailingList;
                userRole.DeleteProduct = userRoleDto.DeleteProduct;
                userRole.DeleteSeoSection = userRoleDto.DeleteSeoSection;
                userRole.DeleteShippingBox = userRoleDto.DeleteShippingBox;
                userRole.DeleteUser = userRoleDto.DeleteUser;
                userRole.DeleteUserRole = userRoleDto.DeleteUserRole;
                userRole.DeleteVariant = userRoleDto.DeleteVariant;
                userRole.DeleteVariantOption = userRoleDto.DeleteVariantOption;
                userRole.EditAttribute = userRoleDto.EditAttribute;
                userRole.EditAttributeAllLocalizedKits = userRoleDto.EditAttributeAllLocalizedKits;
                userRole.EditAttributeName = userRoleDto.EditAttributeName;
                userRole.EditAttributeOption = userRoleDto.EditAttributeOption;
                userRole.EditAttributeOptionAllLocalizedKits = userRoleDto.EditAttributeOptionAllLocalizedKits;
                userRole.EditAttributeOptionName = userRoleDto.EditAttributeOptionName;
                userRole.EditBlogPost = userRoleDto.EditBlogPost;
                userRole.EditContentSection = userRoleDto.EditContentSection;
                userRole.EditContentSectionAllLocalizedKits = userRoleDto.EditContentSectionAllLocalizedKits;
                userRole.EditContentSectionName = userRoleDto.EditContentSectionName;
                userRole.EditCountry = userRoleDto.EditCountry;
                userRole.EditCoupon = userRoleDto.EditCoupon;
                userRole.EditCategory = userRoleDto.EditCategory;
                userRole.EditCategoryAllLocalizedKits = userRoleDto.EditCategoryAllLocalizedKits;
                userRole.EditCategoryName = userRoleDto.EditCategoryName;
                userRole.EditCategoryUrl = userRoleDto.EditCategoryUrl;
                userRole.EditEmail = userRoleDto.EditEmail;
                userRole.EditMailingList = userRoleDto.EditMailingList;
                userRole.EditMailingListName = userRoleDto.EditMailingListName;
                userRole.EditEmailAllLocalizedKits = userRoleDto.EditEmailAllLocalizedKits;
                userRole.EditMailingListAllLocalizedKits = userRoleDto.EditMailingListAllLocalizedKits;
                userRole.EditEmailName = userRoleDto.EditEmailName;
                userRole.EditOrder = userRoleDto.EditOrder;
                userRole.EditProduct = userRoleDto.EditProduct;
                userRole.EditProductAllLocalizedKits = userRoleDto.EditProductAllLocalizedKits;
                userRole.EditSeoSection = userRoleDto.EditSeoSection;
                userRole.EditSeoSectionAllLocalizedKits = userRoleDto.EditSeoSectionAllLocalizedKits;
                userRole.EditSeoSectionPage = userRoleDto.EditSeoSectionPage;
                userRole.EditShippingBox = userRoleDto.EditShippingBox;
                userRole.EditShippingBoxAllLocalizedKits = userRoleDto.EditShippingBoxAllLocalizedKits;
                userRole.EditShippingBoxName = userRoleDto.EditShippingBoxName;
                userRole.EditUser = userRoleDto.EditUser;
                userRole.EditUserRole = userRoleDto.EditUserRole;
                userRole.EditVariant = userRoleDto.EditVariant;
                userRole.EditVariantAllLocalizedKits = userRoleDto.EditVariantAllLocalizedKits;
                userRole.EditVariantName = userRoleDto.EditVariantName;
                userRole.EditVariantSelector = userRoleDto.EditVariantSelector;
                userRole.EditVariantOption = userRoleDto.EditVariantOption;
                userRole.EditVariantOptionAllLocalizedKits = userRoleDto.EditVariantOptionAllLocalizedKits;
                userRole.EditVariantOptionName = userRoleDto.EditVariantOptionName;
                userRole.IsDisabled = userRoleDto.IsDisabled ?? false;
                userRole.Name = userRoleDto.Name;
                userRole.ViewExceptionLogs = userRoleDto.ViewExceptionLogs;
                userRole.ViewEcommerceLogs = userRoleDto.ViewEcommerceLogs;
                userRole.ViewContentLogs = userRoleDto.ViewContentLogs;
            }

            dbContext.SaveChanges();

            return userRole;
        }

        public virtual UserCredit AddOrUpdateUserCredit(HiroDbContext dbContext, UserCreditDto userCreditDto)
        {
            var userCredit = dbContext.UserCredits.Find(userCreditDto.Id);

            if (userCredit == null)
            {
                userCredit = new UserCredit
                {
                    Id = userCreditDto.Id,
                    Amount = userCreditDto.Amount,
                    IsDisabled = userCreditDto.IsDisabled ?? false,
                    Reference = userCreditDto.Reference,
                    UserLocalizedKit = dbContext.UserLocalizedKits.Find(userCreditDto.UserLocalizedKit.Id) ?? new UserLocalizedKit
                    {
                        Id = userCreditDto.UserLocalizedKit.Id,
                        Country = dbContext.Countries.Find(userCreditDto.UserLocalizedKit.Country?.Id),
                        IsDisabled = userCreditDto.UserLocalizedKit.IsDisabled ?? false,
                        User = dbContext.Users.Find(userCreditDto.UserLocalizedKit.User?.Id)
                    }
                };

                dbContext.UserCredits.Add(userCredit);
            }
            else
            {
                userCredit.Amount = userCreditDto.Amount;
                userCredit.IsDisabled = userCreditDto.IsDisabled ?? false;
                userCredit.Reference = userCreditDto.Reference;
            }

            //// -----------------------------------------------------------------------------
            //// Resolve multiple localizedKits associated to the same country
            //// -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(userCredit.UserLocalizedKit?.User?.UserLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.UserLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return userCredit;
        }

        public virtual void AddOrUpdateUserCredits(HiroDbContext dbContext, List<UserCreditDto> userCreditDtos)
        {
            foreach (var userCreditDto in userCreditDtos)
            {
                var userCredit = dbContext.UserCredits.Find(userCreditDto.Id);

                if (userCredit == null)
                {
                    userCredit = new UserCredit
                    {
                        Id = userCreditDto.Id,
                        Amount = userCreditDto.Amount,
                        IsDisabled = userCreditDto.IsDisabled ?? false,
                        Reference = userCreditDto.Reference,
                        UserLocalizedKit = dbContext.UserLocalizedKits.Find(userCreditDto.UserLocalizedKit.Id) ?? new UserLocalizedKit
                        {
                            Id = userCreditDto.UserLocalizedKit.Id,
                            Country = dbContext.Countries.Find(userCreditDto.UserLocalizedKit.Country?.Id),
                            IsDisabled = userCreditDto.UserLocalizedKit.IsDisabled ?? false,
                            User = dbContext.Users.Find(userCreditDto.UserLocalizedKit.User?.Id)
                        }
                    };

                    dbContext.UserCredits.Add(userCredit);
                }
                else
                {
                    userCredit.Amount = userCreditDto.Amount;
                    userCredit.Reference = userCreditDto.Reference;
                    userCredit.IsDisabled = userCreditDto.IsDisabled ?? false;
                }

                //// -----------------------------------------------------------------------------
                //// Resolve multiple localizedKits associated to the same country
                //// -----------------------------------------------------------------------------

                var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(userCredit.UserLocalizedKit?.User?.UserLocalizedKits);

                foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
                {
                    dbContext.UserLocalizedKits.Delete(localizedKit.Id, false);
                }
            }

            dbContext.SaveChanges();
        }
    }
}