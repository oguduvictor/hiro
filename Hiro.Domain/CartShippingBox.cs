﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class CartShippingBox : BaseEntity
    {
        public CartShippingBox()
        {
        }

        public CartShippingBox(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public virtual ShippingBox ShippingBox { get; set; }

        [CanBeNull]
        public virtual Cart Cart { get; set; }

        [NotNull]
        public virtual List<CartItem> CartItems { get; set; } = new List<CartItem>();
    }
}