﻿namespace Hiro.Domain
{
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class EmailLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Email Email { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string From { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string DisplayName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string ReplyTo { get; set; }
    }
}
