﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using Hiro.Domain.Interfaces;

    public class ProductStockUnitLocalizedKit : BaseEntity, ILocalizedKit
    {
        public ProductStockUnitLocalizedKit()
        {
        }

        public ProductStockUnitLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        public virtual ProductStockUnit ProductStockUnit { get; set; }

        public virtual Country Country { get; set; }

        public double BasePrice { get; set; }

        public double SalePrice { get; set; }

        public double MembershipSalePrice { get; set; }
    }
}
