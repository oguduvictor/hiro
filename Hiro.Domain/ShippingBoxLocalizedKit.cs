﻿namespace Hiro.Domain
{
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class ShippingBoxLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual ShippingBox ShippingBox { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string InternalDescription { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Carrier { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int MinDays { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int MaxDays { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool CountWeekends { get; set; }

        /// <summary>
        ///
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        ///
        /// </summary>
        public double FreeShippingMinimumPrice { get; set; }
    }
}