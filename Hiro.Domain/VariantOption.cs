﻿// ReSharper disable InheritdocConsiderUsage

#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class VariantOption : BaseEntity, ISortable, ITaggable
    {
        public VariantOption()
        {
        }

        public VariantOption(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Variant Variant { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        [CanBeNull]
        public string Image { get; set; }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        [CanBeNull]
        public string AutoTags { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<VariantOptionLocalizedKit> VariantOptionLocalizedKits { get; set; } = new List<VariantOptionLocalizedKit>();
    }
}