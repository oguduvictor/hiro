﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System.Collections.Generic;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public class MailingList : BaseEntity, ISortable
    {
        public string Name { get; set; }

        public int SortOrder { get; set; }

        [NotNull]
        public virtual List<MailingListSubscription> MailingListSubscriptions { get; set; } = new List<MailingListSubscription>();

        [NotNull]
        public virtual List<MailingListLocalizedKit> MailingListLocalizedKits { get; set; } = new List<MailingListLocalizedKit>();
    }
}
