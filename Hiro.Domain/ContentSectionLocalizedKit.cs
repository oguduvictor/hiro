﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class ContentSectionLocalizedKit : BaseEntity, ILocalizedKit
    {
        public ContentSectionLocalizedKit()
        {
        }

        public ContentSectionLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual ContentSection ContentSection { get; set; }

        /// <summary>
        /// The current content version.
        /// </summary>
        [CanBeNull]
        public string Content { get; set; }

        /// <summary>
        /// The previews content versions.
        /// </summary>
        [NotNull]
        public virtual List<ContentVersion> ContentVersions { get; set; } = new List<ContentVersion>();
    }
}