﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public class CategoryLocalizedKit : BaseEntity, ILocalizedKit
    {
        [CanBeNull]
        public virtual Country Country { get; set; }

        [CanBeNull]
        public virtual Category Category { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string Image { get; set; }

        [CanBeNull]
        public string FeaturedTitle { get; set; }

        [CanBeNull]
        public string FeaturedDescription { get; set; }

        [CanBeNull]
        public string FeaturedImage { get; set; }

        [CanBeNull]
        public string MetaTitle { get; set; }

        [CanBeNull]
        public string MetaDescription { get; set; }
    }
}