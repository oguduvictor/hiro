﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public class OrderItemVariant : BaseEntity
    {
        public virtual OrderItem OrderItem { get; set; }

        public VariantTypesEnum VariantType { get; set; }

        [CanBeNull]
        public string VariantName { get; set; }

        public string VariantUrl { get; set; }

        [CanBeNull]
        public string VariantLocalizedTitle { get; set; }

        public string VariantLocalizedDescription { get; set; }

        public bool BooleanValue { get; set; }

        public double DoubleValue { get; set; }

        public int IntegerValue { get; set; }

        [CanBeNull]
        public string StringValue { get; set; }

        [CanBeNull]
        public string JsonValue { get; set; }

        [CanBeNull]
        public string VariantOptionName { get; set; }

        [CanBeNull]
        public string VariantOptionUrl { get; set; }

        [CanBeNull]
        public string VariantOptionCode { get; set; }

        [CanBeNull]
        public string VariantOptionLocalizedTitle { get; set; }

        [CanBeNull]
        public string VariantOptionLocalizedDescription { get; set; }

        public double VariantOptionLocalizedPriceModifier { get; set; }
    }
}