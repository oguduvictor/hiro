﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class Coupon : BaseEntity
    {
        public Coupon()
        {
        }

        public Coupon(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Code { get; set; }

        public double Amount { get; set; }

        public double Percentage { get; set; }

        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        /// Marks when a coupon has been given away.
        /// </summary>
        public bool Published { get; set; }

        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// number of times the coupon can be used before being deleted.
        /// </summary>
        public int ValidityTimes { get; set; }

        /// <summary>
        /// Restrict the coupon to the current categories.
        /// </summary>
        [NotNull]
        public virtual List<Category> Categories { get; set; } = new List<Category>();

        /// <summary>
        /// The User who generated, published or shared the current Coupon.
        /// </summary>
        [CanBeNull]
        public virtual User Referee { get; set; }

        /// <summary>
        /// The amount of credits the Referee will receive when the coupon is used.
        /// </summary>
        public double RefereeReward { get; set; }

        /// <summary>
        /// Optional Identifier of the person who will receive the Coupon.
        /// </summary>
        [CanBeNull]
        public string Recipient { get; set; }

        [CanBeNull]
        public string Note { get; set; }

        /// <summary>
        /// Defines whether a coupon should be applicable to products on sale or not.
        /// </summary>
        public bool AllowOnSale { get; set; }
    }
}