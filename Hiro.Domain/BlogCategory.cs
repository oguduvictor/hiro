﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class BlogCategory : BaseEntity
    {
        /// <summary>
        /// Unique identifier.
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<BlogPost> BlogPosts { get; set; } = new List<BlogPost>();

        /// <summary>
        ///
        /// </summary>
        public virtual List<BlogCategoryLocalizedKit> BlogCategoryLocalizedKits { get; set; } = new List<BlogCategoryLocalizedKit>();
    }
}