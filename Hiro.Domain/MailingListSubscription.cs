﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    // todo: this should inherit from BaseEntity
    public class MailingListSubscription
    {
        [NotNull]
        public virtual MailingList MailingList { get; set; }

        public Guid MailingListId { get; set; }

        [CanBeNull]
        public virtual User User { get; set; }

        public Guid UserId { get; set; }

        public MailingListSubscriptionStatusEnum Status { get; set; }
    }
}
