﻿namespace Hiro.Domain
{
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class BlogCategoryLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        public virtual BlogCategory BlogCategory { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }
    }
}
