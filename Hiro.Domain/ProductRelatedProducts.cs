﻿namespace Hiro.Domain
{
    /// <summary>
    ///
    /// </summary>
    public class ProductRelatedProducts : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual Product ProductRelated { get; set; }
    }
}