﻿namespace Hiro.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

#pragma warning disable 1591

    public class UserVariantConfiguration : EntityTypeConfiguration<UserVariant>
    {
        public UserVariantConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.Property(c => c.StringValue).HasMaxLength(500);
            this.Property(c => c.JsonValue).HasMaxLength(4000);
        }
    }
}
