﻿#pragma warning disable 1591

namespace Hiro.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class OrderCreditConfiguration : EntityTypeConfiguration<OrderCredit>
    {
        public OrderCreditConfiguration()
        {
            this.HasKey(u => u.Id);
            this.Property(u => u.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
        }
    }
}