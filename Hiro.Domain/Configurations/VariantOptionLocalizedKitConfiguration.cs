﻿
#pragma warning disable 1591

namespace Hiro.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class VariantOptionLocalizedKitConfiguration : EntityTypeConfiguration<VariantOptionLocalizedKit>
    {
        public VariantOptionLocalizedKitConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
        }
    }
}