﻿#pragma warning disable 1591

namespace Hiro.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class ProductVariantConfiguration : EntityTypeConfiguration<ProductVariant>
    {
        public ProductVariantConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.HasMany(a => a.VariantOptions).WithMany();
            this.Property(c => c.DefaultStringValue).HasMaxLength(500);
            this.Property(c => c.DefaultJsonValue).HasMaxLength(4000);
        }
    }
}