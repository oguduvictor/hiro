﻿#pragma warning disable 1591

namespace Hiro.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class ShippingBoxLocalizedKitConfiguration : EntityTypeConfiguration<ShippingBoxLocalizedKit>
    {
        public ShippingBoxLocalizedKitConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
        }
    }
}