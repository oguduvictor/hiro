﻿namespace Hiro.Domain
{
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class AttributeLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Attribute Attribute { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }
    }
}