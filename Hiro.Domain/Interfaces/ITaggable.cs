﻿#pragma warning disable 1591
namespace Hiro.Domain.Interfaces
{
    /// <summary>
    /// An entity implementing this interface contains a SortOrder property, useful for Sorting collections in a predictable way.
    /// </summary>
    public interface ITaggable
    {
        /// <summary>
        /// Custom Tags.
        /// </summary>
        string Tags { get; set; }

        /// <summary>
        /// AutoTags are system generated and cannot be edited from the administration panel.
        /// </summary>
        string AutoTags { get; set; }
    }
}
