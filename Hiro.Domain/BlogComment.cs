﻿namespace Hiro.Domain
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class BlogComment : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual User Author { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Email { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Body { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual BlogPost Post { get; set; }
    }
}