﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public class ProductLocalizedKit : BaseEntity, ILocalizedKit
    {
        public ProductLocalizedKit()
        {
        }

        public ProductLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Product Product { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string MetaTitle { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string MetaDescription { get; set; }
    }
}