﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System.Collections.Generic;

    public class WishList : BaseEntity
    {
        public virtual User User { get; set; }

        public virtual List<CartItem> CartItems { get; set; } = new List<CartItem>();
    }
}