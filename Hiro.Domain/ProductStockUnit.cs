﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ProductStockUnit : BaseEntity
    {
        public ProductStockUnit()
        {
        }

        public ProductStockUnit(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public virtual Product Product { get; set; }

        [NotNull]
        public virtual List<ProductStockUnitLocalizedKit> ProductStockUnitLocalizedKits { get; set; } = new List<ProductStockUnitLocalizedKit>();

        [NotNull]
        public virtual List<VariantOption> VariantOptions { get; set; } = new List<VariantOption>();

        /// <summary>
        ///
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Code { get; set; }

        /// <summary>
        /// The date and time when the product will be released.
        /// </summary>
        public DateTime DispatchDate { get; set; } = DateTime.Now;

        /// <summary>
        /// If enabled, it allows the user to complete the purchase despite the ReleaseDate being in the future.
        /// </summary>
        public bool EnablePreorder { get; set; }

        /// <summary>
        /// The number of days between the order and the shipping.
        /// </summary>
        public int DispatchTime { get; set; }

        [NotNull]
        public virtual List<Category> Categories { get; set; } = new List<Category>();
    }
}