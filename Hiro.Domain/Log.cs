﻿namespace Hiro.Domain
{
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class Log : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        public LogTypesEnum Type { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Message { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual User User { get; set; }
    }
}