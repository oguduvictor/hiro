﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System.Collections.Generic;
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public class OrderShippingBox : BaseEntity
    {
        public virtual Order Order { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string InternalDescription { get; set; }

        public int MinDays { get; set; }

        public int MaxDays { get; set; }

        public bool CountWeekends { get; set; }

        public double ShippingPriceBeforeTax { get; set; }

        public double ShippingPriceAfterTax { get; set; }

        [CanBeNull]
        public string TrackingCode { get; set; }

        public OrderShippingBoxStatusEnum Status { get; set; }

        [NotNull]
        public virtual List<OrderItem> OrderItems { get; set; } = new List<OrderItem>();
    }
}