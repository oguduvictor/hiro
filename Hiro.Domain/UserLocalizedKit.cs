﻿namespace Hiro.Domain
{
    using System.Collections.Generic;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class UserLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual User User { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<UserCredit> UserCredits { get; set; } = new List<UserCredit>();
    }
}