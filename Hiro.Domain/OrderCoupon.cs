﻿
#pragma warning disable 1591

namespace Hiro.Domain
{
    using JetBrains.Annotations;

    public class OrderCoupon : BaseEntity
    {
        [CanBeNull]
        public string Code { get; set; }

        public double Amount { get; set; }
    }
}