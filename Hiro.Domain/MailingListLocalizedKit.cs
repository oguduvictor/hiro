﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using Hiro.Domain.Interfaces;

    public class MailingListLocalizedKit : BaseEntity, ILocalizedKit
    {
        public virtual MailingList MailingList { get; set; }

        public virtual Country Country { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
