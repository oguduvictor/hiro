﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using JetBrains.Annotations;

    public class ContentVersion : BaseEntity
    {
        [CanBeNull]
        public virtual ContentSectionLocalizedKit ContentSectionLocalizedKit { get; set; }

        [CanBeNull]
        public string Content { get; set; }
    }
}
