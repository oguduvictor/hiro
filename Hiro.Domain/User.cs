﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public class User : BaseEntity
    {
        public User()
        {
        }

        public User(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string FirstName { get; set; }

        [CanBeNull]
        public string LastName { get; set; }

        [NotNull]
        public string FullName => $"{this.FirstName} {this.LastName}";

        public bool IsRegistered => !string.IsNullOrEmpty(this.Email) && !string.IsNullOrEmpty(this.Password);

        [CanBeNull]
        public string Email { get; set; }

        [CanBeNull]
        public string Password { get; set; }

        [CanBeNull]
        public virtual UserRole Role { get; set; }

        public DateTime? LastLogin { get; set; }

        [NotNull]
        public virtual List<Address> Addresses { get; set; } = new List<Address>();

        public GenderEnum Gender { get; set; }

        [CanBeNull]
        public string LoginProvider { get; set; }

        public bool ShowMembershipSalePrice { get; set; }

        [CanBeNull]
        public virtual WishList WishList { get; set; }

        [CanBeNull]
        public virtual Cart Cart { get; set; }

        [CanBeNull]
        public virtual Country Country { get; set; }

        [NotNull]
        public virtual List<UserAttribute> UserAttributes { get; set; } = new List<UserAttribute>();

        [NotNull]
        public virtual List<UserLocalizedKit> UserLocalizedKits { get; set; } = new List<UserLocalizedKit>();

        [NotNull]
        public virtual List<UserVariant> UserVariants { get; set; } = new List<UserVariant>();

        [NotNull]
        public virtual List<MailingListSubscription> MailingListSubscriptions { get; set; } = new List<MailingListSubscription>();

    }
}