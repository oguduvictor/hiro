﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public class VariantOptionLocalizedKit : BaseEntity, ILocalizedKit
    {
        public VariantOptionLocalizedKit()
        {
        }

        public VariantOptionLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual VariantOption VariantOption { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        ///
        /// </summary>
        public double PriceModifier { get; set; }

    }
}