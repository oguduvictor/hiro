﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System;
    using System.Collections.Generic;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public class ShippingBox : BaseEntity, ISortable
    {
        public ShippingBox()
        {
        }

        public ShippingBox(Guid id)
        {
            this.Id = id;
        }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        public bool RequiresShippingAddress { get; set; }

        /// <summary>
        /// The list of countries where this ShippingBox is available.
        /// </summary>
        [NotNull]
        public virtual List<Country> Countries { get; set; } = new List<Country>();

        /// <summary>
        /// The collection of products that reference the current ShippingBox.
        /// </summary>
        [NotNull]
        public virtual List<Product> Products { get; set; } = new List<Product>();

        [NotNull]
        public virtual List<ShippingBoxLocalizedKit> ShippingBoxLocalizedKits { get; set; } = new List<ShippingBoxLocalizedKit>();
    }
}