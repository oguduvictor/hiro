﻿#pragma warning disable 1591

namespace Hiro.Domain
{
    using System.Collections.Generic;
    using Hiro.Domain.Enums;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public class Attribute : BaseEntity, ISortable
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        ///
        /// </summary>
        public AttributeTypesEnum Type { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<AttributeOption> AttributeOptions { get; set; } = new List<AttributeOption>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<AttributeLocalizedKit> AttributeLocalizedKits { get; set; } = new List<AttributeLocalizedKit>();

    }
}