﻿namespace Hiro.DataAccess
{
    using System;
    using System.Data.Entity.Infrastructure;
    using Microsoft.Extensions.Configuration;

    public class HiroDbContextFactory : IDbContextFactory<HiroDbContext>
    {
        public HiroDbContext Create()
        {
            var environmentName = Environment.GetEnvironmentVariable("Hosting:Environment");

            var basePath = AppContext.BaseDirectory;

            return this.Create(basePath, environmentName);
        }

        private HiroDbContext Create(string basePath, string environmentName)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environmentName}.json", true);

            var config = builder.Build();

            var connstr = config.GetConnectionString("DbConnectionString");

            return this.Create(connstr);
        }

        private HiroDbContext Create(string connectionString)
        {
            return new HiroDbContext(connectionString);
        }
    }
}
