﻿namespace Hiro.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Initial_Create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        StateCountyProvince = c.String(),
                        Postcode = c.String(),
                        PhoneNumber = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FlagIcon = c.String(),
                        Url = c.String(),
                        Name = c.String(maxLength: 250),
                        LanguageCode = c.String(maxLength: 250),
                        IsDefault = c.Boolean(nullable: false),
                        Localize = c.Boolean(nullable: false),
                        PaymentAccountId = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name)
                .Index(t => t.LanguageCode);
            
            CreateTable(
                "dbo.ShippingBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        SortOrder = c.Int(nullable: false),
                        Name = c.String(maxLength: 250),
                        RequiresShippingAddress = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(maxLength: 250),
                        Code = c.String(),
                        TaxCode = c.String(),
                        Comments = c.String(),
                        Tags = c.String(),
                        AutoTags = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Url);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Url = c.String(maxLength: 250),
                        SortOrder = c.Int(nullable: false),
                        Tags = c.String(),
                        AutoTags = c.String(),
                        ProductsSortOrder = c.String(),
                        ProductStockUnitsSortOrder = c.String(),
                        ProductImageKitsSortOrder = c.String(),
                        Type = c.Int(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Parent_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Parent_Id)
                .Index(t => t.Name)
                .Index(t => t.Url)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.CategoryLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        FeaturedTitle = c.String(),
                        FeaturedDescription = c.String(),
                        FeaturedImage = c.String(),
                        MetaTitle = c.String(),
                        MetaDescription = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Category_Id = c.Guid(),
                        Country_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.ProductImageKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Product_Id = c.Guid(),
                        ProductImageKit_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.ProductImageKits", t => t.ProductImageKit_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.ProductImageKit_Id);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        AltText = c.String(),
                        SortOrder = c.Int(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        ProductImageKit_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductImageKits", t => t.ProductImageKit_Id)
                .Index(t => t.ProductImageKit_Id);
            
            CreateTable(
                "dbo.VariantOptions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Url = c.String(maxLength: 250),
                        Code = c.String(),
                        Image = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Tags = c.String(),
                        AutoTags = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Variant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variants", t => t.Variant_Id)
                .Index(t => t.Name)
                .Index(t => t.Url)
                .Index(t => t.Variant_Id);
            
            CreateTable(
                "dbo.Variants",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Url = c.String(maxLength: 250),
                        SortOrder = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        DefaultBooleanValue = c.Boolean(nullable: false),
                        DefaultDoubleValue = c.Double(nullable: false),
                        DefaultIntegerValue = c.Int(nullable: false),
                        DefaultStringValue = c.String(maxLength: 500),
                        DefaultJsonValue = c.String(maxLength: 4000),
                        CreateProductStockUnits = c.Boolean(nullable: false),
                        CreateProductImageKits = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        DefaultVariantOptionValue_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VariantOptions", t => t.DefaultVariantOptionValue_Id)
                .Index(t => t.Name)
                .Index(t => t.Url)
                .Index(t => t.DefaultVariantOptionValue_Id);
            
            CreateTable(
                "dbo.VariantLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        Variant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Variants", t => t.Variant_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Variant_Id);
            
            CreateTable(
                "dbo.VariantOptionLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        PriceModifier = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        VariantOption_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.VariantOptions", t => t.VariantOption_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.VariantOption_Id);
            
            CreateTable(
                "dbo.ProductStockUnits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Stock = c.Int(nullable: false),
                        Code = c.String(),
                        DispatchDate = c.DateTime(nullable: false),
                        EnablePreorder = c.Boolean(nullable: false),
                        DispatchTime = c.Int(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Product_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.ProductStockUnitLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        BasePrice = c.Double(nullable: false),
                        SalePrice = c.Double(nullable: false),
                        MembershipSalePrice = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        ProductStockUnit_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.ProductStockUnits", t => t.ProductStockUnit_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ProductStockUnit_Id);
            
            CreateTable(
                "dbo.ProductAttributes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        StringValue = c.String(),
                        BooleanValue = c.Boolean(nullable: false),
                        DateTimeValue = c.DateTime(),
                        ImageValue = c.String(),
                        DoubleValue = c.Double(nullable: false),
                        JsonValue = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Attribute_Id = c.Guid(),
                        AttributeOptionValue_Id = c.Guid(),
                        ContentSectionValue_Id = c.Guid(),
                        Product_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.Attribute_Id)
                .ForeignKey("dbo.AttributeOptions", t => t.AttributeOptionValue_Id)
                .ForeignKey("dbo.ContentSections", t => t.ContentSectionValue_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Attribute_Id)
                .Index(t => t.AttributeOptionValue_Id)
                .Index(t => t.ContentSectionValue_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.Attributes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        SortOrder = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Url = c.String(maxLength: 250),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name)
                .Index(t => t.Url);
            
            CreateTable(
                "dbo.AttributeLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Attribute_Id = c.Guid(),
                        Country_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.Attribute_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Attribute_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.AttributeOptions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Url = c.String(maxLength: 250),
                        Tags = c.String(),
                        AutoTags = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Attribute_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.Attribute_Id)
                .Index(t => t.Name)
                .Index(t => t.Url)
                .Index(t => t.Attribute_Id);
            
            CreateTable(
                "dbo.AttributeOptionLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        AttributeOption_Id = c.Guid(),
                        Country_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AttributeOptions", t => t.AttributeOption_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.AttributeOption_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.ContentSections",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Schema = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContentSectionLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Content = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        ContentSection_Id = c.Guid(),
                        Country_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentSections", t => t.ContentSection_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.ContentSection_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.ContentVersions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Content = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        ContentSectionLocalizedKit_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentSectionLocalizedKits", t => t.ContentSectionLocalizedKit_Id)
                .Index(t => t.ContentSectionLocalizedKit_Id);
            
            CreateTable(
                "dbo.ProductLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        MetaTitle = c.String(),
                        MetaDescription = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        Product_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.ProductVariants",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        DefaultBooleanValue = c.Boolean(nullable: false),
                        DefaultDoubleValue = c.Double(nullable: false),
                        DefaultIntegerValue = c.Int(nullable: false),
                        DefaultStringValue = c.String(maxLength: 500),
                        DefaultJsonValue = c.String(maxLength: 4000),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        DefaultVariantOptionValue_Id = c.Guid(),
                        Product_Id = c.Guid(),
                        Variant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VariantOptions", t => t.DefaultVariantOptionValue_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.Variants", t => t.Variant_Id)
                .Index(t => t.DefaultVariantOptionValue_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.Variant_Id);
            
            CreateTable(
                "dbo.ShippingBoxLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        InternalDescription = c.String(),
                        Carrier = c.String(),
                        MinDays = c.Int(nullable: false),
                        MaxDays = c.Int(nullable: false),
                        CountWeekends = c.Boolean(nullable: false),
                        Price = c.Double(nullable: false),
                        FreeShippingMinimumPrice = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        ShippingBox_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.ShippingBoxes", t => t.ShippingBox_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ShippingBox_Id);
            
            CreateTable(
                "dbo.Taxes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Percentage = c.Double(nullable: false),
                        Amount = c.Double(),
                        ApplyToShippingPrice = c.Boolean(nullable: false),
                        ApplyToProductPrice = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        CartItem_Id = c.Guid(),
                        Country_Id = c.Guid(),
                        ShippingAddress_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CartItems", t => t.CartItem_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Addresses", t => t.ShippingAddress_Id)
                .Index(t => t.CartItem_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ShippingAddress_Id);
            
            CreateTable(
                "dbo.CartItems",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Image = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        WishList_Id = c.Guid(),
                        CartShippingBox_Id = c.Guid(),
                        Product_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WishLists", t => t.WishList_Id)
                .ForeignKey("dbo.CartShippingBoxes", t => t.CartShippingBox_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.WishList_Id)
                .Index(t => t.CartShippingBox_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.CartItemVariants",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        BooleanValue = c.Boolean(nullable: false),
                        DoubleValue = c.Double(nullable: false),
                        IntegerValue = c.Int(nullable: false),
                        StringValue = c.String(maxLength: 500),
                        JsonValue = c.String(maxLength: 4000),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        CartItem_Id = c.Guid(),
                        Variant_Id = c.Guid(),
                        VariantOptionValue_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CartItems", t => t.CartItem_Id)
                .ForeignKey("dbo.Variants", t => t.Variant_Id)
                .ForeignKey("dbo.VariantOptions", t => t.VariantOptionValue_Id)
                .Index(t => t.CartItem_Id)
                .Index(t => t.Variant_Id)
                .Index(t => t.VariantOptionValue_Id);
            
            CreateTable(
                "dbo.CartShippingBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Cart_Id = c.Guid(),
                        ShippingBox_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.Cart_Id)
                .ForeignKey("dbo.ShippingBoxes", t => t.ShippingBox_Id)
                .Index(t => t.Cart_Id)
                .Index(t => t.ShippingBox_Id);
            
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        BillingAddress_Id = c.Guid(),
                        User_Id = c.Guid(),
                        Coupon_Id = c.Guid(),
                        ShippingAddress_Id = c.Guid(),
                        UserCredit_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.BillingAddress_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.Coupons", t => t.Coupon_Id)
                .ForeignKey("dbo.Addresses", t => t.ShippingAddress_Id)
                .ForeignKey("dbo.UserCredits", t => t.UserCredit_Id)
                .Index(t => t.BillingAddress_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Coupon_Id)
                .Index(t => t.ShippingAddress_Id)
                .Index(t => t.UserCredit_Id);
            
            CreateTable(
                "dbo.Coupons",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Code = c.String(maxLength: 250),
                        Amount = c.Double(nullable: false),
                        Percentage = c.Double(nullable: false),
                        Published = c.Boolean(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        ValidityTimes = c.Int(nullable: false),
                        RefereeReward = c.Double(nullable: false),
                        Recipient = c.String(),
                        Note = c.String(),
                        AllowOnSale = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        Referee_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Users", t => t.Referee_Id)
                .Index(t => t.Code)
                .Index(t => t.Country_Id)
                .Index(t => t.Referee_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(maxLength: 255),
                        Password = c.String(),
                        LastLogin = c.DateTime(),
                        Gender = c.Int(nullable: false),
                        LoginProvider = c.String(),
                        ShowMembershipSalePrice = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        Role_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.UserRoles", t => t.Role_Id)
                .Index(t => t.Email, unique: true)
                .Index(t => t.Country_Id)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.MailingListSubscriptions",
                c => new
                    {
                        MailingList_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MailingList_Id, t.User_Id })
                .ForeignKey("dbo.MailingLists", t => t.MailingList_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.MailingList_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.MailingLists",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MailingListLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        MailingList_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.MailingLists", t => t.MailingList_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.MailingList_Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        AccessAdmin = c.Boolean(nullable: false),
                        AccessAdminEcommerce = c.Boolean(nullable: false),
                        AccessAdminOrders = c.Boolean(nullable: false),
                        AccessAdminBlog = c.Boolean(nullable: false),
                        AccessAdminMedia = c.Boolean(nullable: false),
                        AccessAdminContent = c.Boolean(nullable: false),
                        AccessAdminEmails = c.Boolean(nullable: false),
                        AccessAdminSeo = c.Boolean(nullable: false),
                        AccessAdminUsers = c.Boolean(nullable: false),
                        AccessAdminCountries = c.Boolean(nullable: false),
                        AccessAdminSettings = c.Boolean(nullable: false),
                        EditOrder = c.Boolean(nullable: false),
                        CreateProduct = c.Boolean(nullable: false),
                        EditProduct = c.Boolean(nullable: false),
                        EditProductAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteProduct = c.Boolean(nullable: false),
                        CreateAttribute = c.Boolean(nullable: false),
                        EditAttribute = c.Boolean(nullable: false),
                        EditAttributeName = c.Boolean(nullable: false),
                        EditAttributeAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteAttribute = c.Boolean(nullable: false),
                        CreateAttributeOption = c.Boolean(nullable: false),
                        EditAttributeOption = c.Boolean(nullable: false),
                        EditAttributeOptionName = c.Boolean(nullable: false),
                        EditAttributeOptionAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteAttributeOption = c.Boolean(nullable: false),
                        CreateVariant = c.Boolean(nullable: false),
                        EditVariant = c.Boolean(nullable: false),
                        EditVariantName = c.Boolean(nullable: false),
                        EditVariantSelector = c.Boolean(nullable: false),
                        EditVariantAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteVariant = c.Boolean(nullable: false),
                        CreateVariantOption = c.Boolean(nullable: false),
                        EditVariantOption = c.Boolean(nullable: false),
                        EditVariantOptionName = c.Boolean(nullable: false),
                        EditVariantOptionAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteVariantOption = c.Boolean(nullable: false),
                        CreateShippingBox = c.Boolean(nullable: false),
                        EditShippingBox = c.Boolean(nullable: false),
                        EditShippingBoxName = c.Boolean(nullable: false),
                        EditShippingBoxAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteShippingBox = c.Boolean(nullable: false),
                        CreateCategory = c.Boolean(nullable: false),
                        EditCategory = c.Boolean(nullable: false),
                        EditCategoryName = c.Boolean(nullable: false),
                        EditCategoryUrl = c.Boolean(nullable: false),
                        EditCategoryAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteCategory = c.Boolean(nullable: false),
                        CreateBlogPost = c.Boolean(nullable: false),
                        EditBlogPost = c.Boolean(nullable: false),
                        DeleteBlogPost = c.Boolean(nullable: false),
                        CreateContentSection = c.Boolean(nullable: false),
                        EditContentSection = c.Boolean(nullable: false),
                        EditContentSectionName = c.Boolean(nullable: false),
                        EditContentSectionAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteContentSection = c.Boolean(nullable: false),
                        CreateEmail = c.Boolean(nullable: false),
                        EditEmail = c.Boolean(nullable: false),
                        EditEmailName = c.Boolean(nullable: false),
                        EditEmailAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteEmail = c.Boolean(nullable: false),
                        CreateMailingList = c.Boolean(nullable: false),
                        EditMailingList = c.Boolean(nullable: false),
                        EditMailingListName = c.Boolean(nullable: false),
                        EditMailingListAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteMailingList = c.Boolean(nullable: false),
                        CreateSeoSection = c.Boolean(nullable: false),
                        EditSeoSection = c.Boolean(nullable: false),
                        EditSeoSectionPage = c.Boolean(nullable: false),
                        EditSeoSectionAllLocalizedKits = c.Boolean(nullable: false),
                        DeleteSeoSection = c.Boolean(nullable: false),
                        CreateUser = c.Boolean(nullable: false),
                        EditUser = c.Boolean(nullable: false),
                        DeleteUser = c.Boolean(nullable: false),
                        CreateUserRole = c.Boolean(nullable: false),
                        EditUserRole = c.Boolean(nullable: false),
                        DeleteUserRole = c.Boolean(nullable: false),
                        CreateCountry = c.Boolean(nullable: false),
                        EditCountry = c.Boolean(nullable: false),
                        DeleteCountry = c.Boolean(nullable: false),
                        CreateCoupon = c.Boolean(nullable: false),
                        EditCoupon = c.Boolean(nullable: false),
                        DeleteCoupon = c.Boolean(nullable: false),
                        ViewExceptionLogs = c.Boolean(nullable: false),
                        ViewEcommerceLogs = c.Boolean(nullable: false),
                        ViewContentLogs = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name);
            
            CreateTable(
                "dbo.UserAttributes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        StringValue = c.String(),
                        BooleanValue = c.Boolean(nullable: false),
                        DateTimeValue = c.DateTime(),
                        ImageValue = c.String(),
                        DoubleValue = c.Double(nullable: false),
                        JsonValue = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Attribute_Id = c.Guid(),
                        AttributeOptionValue_Id = c.Guid(),
                        ContentSectionValue_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.Attribute_Id)
                .ForeignKey("dbo.AttributeOptions", t => t.AttributeOptionValue_Id)
                .ForeignKey("dbo.ContentSections", t => t.ContentSectionValue_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Attribute_Id)
                .Index(t => t.AttributeOptionValue_Id)
                .Index(t => t.ContentSectionValue_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.UserLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.UserCredits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        Reference = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        UserLocalizedKit_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserLocalizedKits", t => t.UserLocalizedKit_Id)
                .Index(t => t.UserLocalizedKit_Id);
            
            CreateTable(
                "dbo.UserVariants",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        BooleanValue = c.Boolean(nullable: false),
                        DoubleValue = c.Double(nullable: false),
                        IntegerValue = c.Int(nullable: false),
                        StringValue = c.String(maxLength: 500),
                        JsonValue = c.String(maxLength: 4000),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        User_Id = c.Guid(),
                        Variant_Id = c.Guid(),
                        VariantOptionValue_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.Variants", t => t.Variant_Id)
                .ForeignKey("dbo.VariantOptions", t => t.VariantOptionValue_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Variant_Id)
                .Index(t => t.VariantOptionValue_Id);
            
            CreateTable(
                "dbo.WishLists",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.SentEmails",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        From = c.String(),
                        To = c.String(),
                        Reference = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Cart_Id = c.Guid(),
                        Coupon_Id = c.Guid(),
                        Email_Id = c.Guid(),
                        Order_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.Cart_Id)
                .ForeignKey("dbo.Coupons", t => t.Coupon_Id)
                .ForeignKey("dbo.Emails", t => t.Email_Id)
                .ForeignKey("dbo.Orders", t => t.Order_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Cart_Id)
                .Index(t => t.Coupon_Id)
                .Index(t => t.Email_Id)
                .Index(t => t.Order_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        ViewName = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        ContentSection_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentSections", t => t.ContentSection_Id)
                .Index(t => t.Name)
                .Index(t => t.ContentSection_Id);
            
            CreateTable(
                "dbo.EmailLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        From = c.String(),
                        DisplayName = c.String(),
                        ReplyTo = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        Email_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Emails", t => t.Email_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Email_Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        OrderNumber = c.Int(),
                        Status = c.Int(nullable: false),
                        Tags = c.String(),
                        AutoTags = c.String(),
                        CountryName = c.String(),
                        CountryLanguageCode = c.String(),
                        CurrencySymbol = c.String(),
                        ISO4217CurrencySymbol = c.String(),
                        UserId = c.Guid(nullable: false),
                        UserFirstName = c.String(),
                        UserLastName = c.String(),
                        UserEmail = c.String(),
                        TransactionId = c.String(),
                        Comments = c.String(),
                        TotalBeforeTax = c.Double(nullable: false),
                        TotalAfterTax = c.Double(nullable: false),
                        TotalToBePaid = c.Double(nullable: false),
                        TotalPaid = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        BillingAddress_Id = c.Guid(),
                        OrderCoupon_Id = c.Guid(),
                        OrderCredit_Id = c.Guid(),
                        ShippingAddress_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderAddresses", t => t.BillingAddress_Id)
                .ForeignKey("dbo.OrderCoupons", t => t.OrderCoupon_Id)
                .ForeignKey("dbo.OrderCredits", t => t.OrderCredit_Id)
                .ForeignKey("dbo.OrderAddresses", t => t.ShippingAddress_Id)
                .Index(t => t.BillingAddress_Id)
                .Index(t => t.OrderCoupon_Id)
                .Index(t => t.OrderCredit_Id)
                .Index(t => t.ShippingAddress_Id);
            
            CreateTable(
                "dbo.OrderAddresses",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        AddressFirstName = c.String(),
                        AddressLastName = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        CountryName = c.String(),
                        StateCountyProvince = c.String(),
                        City = c.String(),
                        Postcode = c.String(),
                        PhoneNumber = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderCoupons",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Code = c.String(),
                        Amount = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderCredits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        Reference = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderShippingBoxes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Title = c.String(),
                        Description = c.String(),
                        InternalDescription = c.String(),
                        MinDays = c.Int(nullable: false),
                        MaxDays = c.Int(nullable: false),
                        CountWeekends = c.Boolean(nullable: false),
                        ShippingPriceBeforeTax = c.Double(nullable: false),
                        ShippingPriceAfterTax = c.Double(nullable: false),
                        TrackingCode = c.String(),
                        Status = c.Int(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Order_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Order_Id)
                .Index(t => t.Name)
                .Index(t => t.Order_Id);
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ProductId = c.Guid(nullable: false),
                        ProductTitle = c.String(),
                        ProductName = c.String(),
                        ProductUrl = c.String(),
                        ProductCode = c.String(),
                        ProductQuantity = c.Int(nullable: false),
                        ProductStockUnitId = c.Guid(nullable: false),
                        ProductStockUnitReleaseDate = c.DateTime(nullable: false),
                        ProductStockUnitEnablePreorder = c.Boolean(nullable: false),
                        ProductStockUnitShipsIn = c.Int(nullable: false),
                        ProductStockUnitCode = c.String(),
                        ProductStockUnitBasePrice = c.Double(nullable: false),
                        ProductStockUnitSalePrice = c.Double(nullable: false),
                        ProductStockUnitMembershipSalePrice = c.Double(nullable: false),
                        Image = c.String(),
                        Status = c.Int(nullable: false),
                        StockStatus = c.Int(nullable: false),
                        MinEta = c.DateTime(nullable: false),
                        MaxEta = c.DateTime(nullable: false),
                        SubtotalBeforeTax = c.Double(nullable: false),
                        SubtotalAfterTax = c.Double(nullable: false),
                        TotalBeforeTax = c.Double(nullable: false),
                        TotalAfterTax = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        OrderShippingBox_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderShippingBoxes", t => t.OrderShippingBox_Id)
                .Index(t => t.OrderShippingBox_Id);
            
            CreateTable(
                "dbo.OrderItemVariants",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        VariantType = c.Int(nullable: false),
                        VariantName = c.String(),
                        VariantUrl = c.String(),
                        VariantLocalizedTitle = c.String(),
                        VariantLocalizedDescription = c.String(),
                        BooleanValue = c.Boolean(nullable: false),
                        DoubleValue = c.Double(nullable: false),
                        IntegerValue = c.Int(nullable: false),
                        StringValue = c.String(maxLength: 500),
                        JsonValue = c.String(maxLength: 4000),
                        VariantOptionName = c.String(),
                        VariantOptionUrl = c.String(),
                        VariantOptionCode = c.String(),
                        VariantOptionLocalizedTitle = c.String(),
                        VariantOptionLocalizedDescription = c.String(),
                        VariantOptionLocalizedPriceModifier = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        OrderItem_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderItems", t => t.OrderItem_Id)
                .Index(t => t.OrderItem_Id);
            
            CreateTable(
                "dbo.OrderItemTaxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Code = c.String(),
                        Amount = c.Double(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        OrderItem_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderItems", t => t.OrderItem_Id)
                .Index(t => t.OrderItem_Id);
            
            CreateTable(
                "dbo.BlogCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BlogCategoryLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        BlogCategory_Id = c.Guid(),
                        Country_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BlogCategories", t => t.BlogCategory_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.BlogCategory_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.BlogPosts",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        IsPublished = c.Boolean(nullable: false),
                        PublicationDate = c.DateTime(nullable: false),
                        FeaturedImage = c.String(),
                        FeaturedImageHorizontalCrop = c.String(),
                        FeaturedImageVerticalCrop = c.String(),
                        Url = c.String(maxLength: 250),
                        MetaTitle = c.String(),
                        MetaDescription = c.String(),
                        Excerpt = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Author_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Author_Id)
                .Index(t => t.Url)
                .Index(t => t.Author_Id);
            
            CreateTable(
                "dbo.BlogComments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Email = c.String(),
                        Url = c.String(),
                        Body = c.String(),
                        Published = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Author_Id = c.Guid(),
                        Post_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Author_Id)
                .ForeignKey("dbo.BlogPosts", t => t.Post_Id)
                .Index(t => t.Author_Id)
                .Index(t => t.Post_Id);
            
            CreateTable(
                "dbo.CmsSettings",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsSeeded = c.Boolean(nullable: false),
                        SmtpHost = c.String(),
                        SmtpPort = c.Int(nullable: false),
                        SmtpDisplayName = c.String(),
                        SmtpEmail = c.String(),
                        SmtpPassword = c.String(),
                        UseAzureStorage = c.Boolean(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Message = c.String(),
                        Details = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ProductRelatedProducts",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Product_Id = c.Guid(),
                        ProductRelated_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.Products", t => t.ProductRelated_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.ProductRelated_Id);
            
            CreateTable(
                "dbo.SeoSectionLocalizedKits",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                        Country_Id = c.Guid(),
                        SeoSection_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.SeoSections", t => t.SeoSection_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.SeoSection_Id);
            
            CreateTable(
                "dbo.SeoSections",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Page = c.String(),
                        IsDisabled = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ShippingBoxCountries",
                c => new
                    {
                        ShippingBox_Id = c.Guid(nullable: false),
                        Country_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ShippingBox_Id, t.Country_Id })
                .ForeignKey("dbo.ShippingBoxes", t => t.ShippingBox_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.ShippingBox_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.ProductImageKitVariantOptions",
                c => new
                    {
                        ProductImageKit_Id = c.Guid(nullable: false),
                        VariantOption_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductImageKit_Id, t.VariantOption_Id })
                .ForeignKey("dbo.ProductImageKits", t => t.ProductImageKit_Id)
                .ForeignKey("dbo.VariantOptions", t => t.VariantOption_Id)
                .Index(t => t.ProductImageKit_Id)
                .Index(t => t.VariantOption_Id);
            
            CreateTable(
                "dbo.CategoryProductImageKits",
                c => new
                    {
                        Category_Id = c.Guid(nullable: false),
                        ProductImageKit_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Category_Id, t.ProductImageKit_Id })
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .ForeignKey("dbo.ProductImageKits", t => t.ProductImageKit_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.ProductImageKit_Id);
            
            CreateTable(
                "dbo.CategoryProducts",
                c => new
                    {
                        Category_Id = c.Guid(nullable: false),
                        Product_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Category_Id, t.Product_Id })
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.ProductStockUnitVariantOptions",
                c => new
                    {
                        ProductStockUnit_Id = c.Guid(nullable: false),
                        VariantOption_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductStockUnit_Id, t.VariantOption_Id })
                .ForeignKey("dbo.ProductStockUnits", t => t.ProductStockUnit_Id)
                .ForeignKey("dbo.VariantOptions", t => t.VariantOption_Id)
                .Index(t => t.ProductStockUnit_Id)
                .Index(t => t.VariantOption_Id);
            
            CreateTable(
                "dbo.CategoryProductStockUnits",
                c => new
                    {
                        Category_Id = c.Guid(nullable: false),
                        ProductStockUnit_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Category_Id, t.ProductStockUnit_Id })
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .ForeignKey("dbo.ProductStockUnits", t => t.ProductStockUnit_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.ProductStockUnit_Id);
            
            CreateTable(
                "dbo.ProductVariantVariantOptions",
                c => new
                    {
                        ProductVariant_Id = c.Guid(nullable: false),
                        VariantOption_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductVariant_Id, t.VariantOption_Id })
                .ForeignKey("dbo.ProductVariants", t => t.ProductVariant_Id)
                .ForeignKey("dbo.VariantOptions", t => t.VariantOption_Id)
                .Index(t => t.ProductVariant_Id)
                .Index(t => t.VariantOption_Id);
            
            CreateTable(
                "dbo.RelatedProducts",
                c => new
                    {
                        ProductID = c.Guid(nullable: false),
                        RelatedProductID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductID, t.RelatedProductID })
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Products", t => t.RelatedProductID)
                .Index(t => t.ProductID)
                .Index(t => t.RelatedProductID);
            
            CreateTable(
                "dbo.ProductShippingBoxes",
                c => new
                    {
                        Product_Id = c.Guid(nullable: false),
                        ShippingBox_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Product_Id, t.ShippingBox_Id })
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.ShippingBoxes", t => t.ShippingBox_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.ShippingBox_Id);
            
            CreateTable(
                "dbo.CouponCategories",
                c => new
                    {
                        Coupon_Id = c.Guid(nullable: false),
                        Category_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Coupon_Id, t.Category_Id })
                .ForeignKey("dbo.Coupons", t => t.Coupon_Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .Index(t => t.Coupon_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.BlogPostBlogCategories",
                c => new
                    {
                        BlogPost_Id = c.Guid(nullable: false),
                        BlogCategory_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.BlogPost_Id, t.BlogCategory_Id })
                .ForeignKey("dbo.BlogPosts", t => t.BlogPost_Id)
                .ForeignKey("dbo.BlogCategories", t => t.BlogCategory_Id)
                .Index(t => t.BlogPost_Id)
                .Index(t => t.BlogCategory_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SeoSectionLocalizedKits", "SeoSection_Id", "dbo.SeoSections");
            DropForeignKey("dbo.SeoSectionLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.ProductRelatedProducts", "ProductRelated_Id", "dbo.Products");
            DropForeignKey("dbo.ProductRelatedProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Logs", "User_Id", "dbo.Users");
            DropForeignKey("dbo.BlogComments", "Post_Id", "dbo.BlogPosts");
            DropForeignKey("dbo.BlogComments", "Author_Id", "dbo.Users");
            DropForeignKey("dbo.BlogPostBlogCategories", "BlogCategory_Id", "dbo.BlogCategories");
            DropForeignKey("dbo.BlogPostBlogCategories", "BlogPost_Id", "dbo.BlogPosts");
            DropForeignKey("dbo.BlogPosts", "Author_Id", "dbo.Users");
            DropForeignKey("dbo.BlogCategoryLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.BlogCategoryLocalizedKits", "BlogCategory_Id", "dbo.BlogCategories");
            DropForeignKey("dbo.Addresses", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Taxes", "ShippingAddress_Id", "dbo.Addresses");
            DropForeignKey("dbo.Taxes", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Taxes", "CartItem_Id", "dbo.CartItems");
            DropForeignKey("dbo.CartItems", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.CartShippingBoxes", "ShippingBox_Id", "dbo.ShippingBoxes");
            DropForeignKey("dbo.CartItems", "CartShippingBox_Id", "dbo.CartShippingBoxes");
            DropForeignKey("dbo.Carts", "UserCredit_Id", "dbo.UserCredits");
            DropForeignKey("dbo.Carts", "ShippingAddress_Id", "dbo.Addresses");
            DropForeignKey("dbo.SentEmails", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Orders", "ShippingAddress_Id", "dbo.OrderAddresses");
            DropForeignKey("dbo.SentEmails", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.OrderItemTaxes", "OrderItem_Id", "dbo.OrderItems");
            DropForeignKey("dbo.OrderItems", "OrderShippingBox_Id", "dbo.OrderShippingBoxes");
            DropForeignKey("dbo.OrderItemVariants", "OrderItem_Id", "dbo.OrderItems");
            DropForeignKey("dbo.OrderShippingBoxes", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "OrderCredit_Id", "dbo.OrderCredits");
            DropForeignKey("dbo.Orders", "OrderCoupon_Id", "dbo.OrderCoupons");
            DropForeignKey("dbo.Orders", "BillingAddress_Id", "dbo.OrderAddresses");
            DropForeignKey("dbo.SentEmails", "Email_Id", "dbo.Emails");
            DropForeignKey("dbo.EmailLocalizedKits", "Email_Id", "dbo.Emails");
            DropForeignKey("dbo.EmailLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Emails", "ContentSection_Id", "dbo.ContentSections");
            DropForeignKey("dbo.SentEmails", "Coupon_Id", "dbo.Coupons");
            DropForeignKey("dbo.SentEmails", "Cart_Id", "dbo.Carts");
            DropForeignKey("dbo.Carts", "Coupon_Id", "dbo.Coupons");
            DropForeignKey("dbo.Coupons", "Referee_Id", "dbo.Users");
            DropForeignKey("dbo.WishLists", "User_Id", "dbo.Users");
            DropForeignKey("dbo.CartItems", "WishList_Id", "dbo.WishLists");
            DropForeignKey("dbo.UserVariants", "VariantOptionValue_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.UserVariants", "Variant_Id", "dbo.Variants");
            DropForeignKey("dbo.UserVariants", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserCredits", "UserLocalizedKit_Id", "dbo.UserLocalizedKits");
            DropForeignKey("dbo.UserLocalizedKits", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.UserAttributes", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserAttributes", "ContentSectionValue_Id", "dbo.ContentSections");
            DropForeignKey("dbo.UserAttributes", "AttributeOptionValue_Id", "dbo.AttributeOptions");
            DropForeignKey("dbo.UserAttributes", "Attribute_Id", "dbo.Attributes");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles");
            DropForeignKey("dbo.MailingListSubscriptions", "User_Id", "dbo.Users");
            DropForeignKey("dbo.MailingListSubscriptions", "MailingList_Id", "dbo.MailingLists");
            DropForeignKey("dbo.MailingListLocalizedKits", "MailingList_Id", "dbo.MailingLists");
            DropForeignKey("dbo.MailingListLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Users", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Carts", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Coupons", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.CouponCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.CouponCategories", "Coupon_Id", "dbo.Coupons");
            DropForeignKey("dbo.CartShippingBoxes", "Cart_Id", "dbo.Carts");
            DropForeignKey("dbo.Carts", "BillingAddress_Id", "dbo.Addresses");
            DropForeignKey("dbo.CartItemVariants", "VariantOptionValue_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.CartItemVariants", "Variant_Id", "dbo.Variants");
            DropForeignKey("dbo.CartItemVariants", "CartItem_Id", "dbo.CartItems");
            DropForeignKey("dbo.ShippingBoxLocalizedKits", "ShippingBox_Id", "dbo.ShippingBoxes");
            DropForeignKey("dbo.ShippingBoxLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.ProductShippingBoxes", "ShippingBox_Id", "dbo.ShippingBoxes");
            DropForeignKey("dbo.ProductShippingBoxes", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.RelatedProducts", "RelatedProductID", "dbo.Products");
            DropForeignKey("dbo.RelatedProducts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductVariantVariantOptions", "VariantOption_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.ProductVariantVariantOptions", "ProductVariant_Id", "dbo.ProductVariants");
            DropForeignKey("dbo.ProductVariants", "Variant_Id", "dbo.Variants");
            DropForeignKey("dbo.ProductVariants", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.ProductVariants", "DefaultVariantOptionValue_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.ProductLocalizedKits", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.ProductLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.ProductAttributes", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.ProductAttributes", "ContentSectionValue_Id", "dbo.ContentSections");
            DropForeignKey("dbo.ContentSectionLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.ContentVersions", "ContentSectionLocalizedKit_Id", "dbo.ContentSectionLocalizedKits");
            DropForeignKey("dbo.ContentSectionLocalizedKits", "ContentSection_Id", "dbo.ContentSections");
            DropForeignKey("dbo.ProductAttributes", "AttributeOptionValue_Id", "dbo.AttributeOptions");
            DropForeignKey("dbo.ProductAttributes", "Attribute_Id", "dbo.Attributes");
            DropForeignKey("dbo.AttributeOptionLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.AttributeOptionLocalizedKits", "AttributeOption_Id", "dbo.AttributeOptions");
            DropForeignKey("dbo.AttributeOptions", "Attribute_Id", "dbo.Attributes");
            DropForeignKey("dbo.AttributeLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.AttributeLocalizedKits", "Attribute_Id", "dbo.Attributes");
            DropForeignKey("dbo.CategoryProductStockUnits", "ProductStockUnit_Id", "dbo.ProductStockUnits");
            DropForeignKey("dbo.CategoryProductStockUnits", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.ProductStockUnitVariantOptions", "VariantOption_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.ProductStockUnitVariantOptions", "ProductStockUnit_Id", "dbo.ProductStockUnits");
            DropForeignKey("dbo.ProductStockUnitLocalizedKits", "ProductStockUnit_Id", "dbo.ProductStockUnits");
            DropForeignKey("dbo.ProductStockUnitLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.ProductStockUnits", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.CategoryProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.CategoryProducts", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.CategoryProductImageKits", "ProductImageKit_Id", "dbo.ProductImageKits");
            DropForeignKey("dbo.CategoryProductImageKits", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.ProductImageKitVariantOptions", "VariantOption_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.ProductImageKitVariantOptions", "ProductImageKit_Id", "dbo.ProductImageKits");
            DropForeignKey("dbo.VariantOptionLocalizedKits", "VariantOption_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.VariantOptionLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.VariantOptions", "Variant_Id", "dbo.Variants");
            DropForeignKey("dbo.VariantLocalizedKits", "Variant_Id", "dbo.Variants");
            DropForeignKey("dbo.VariantLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Variants", "DefaultVariantOptionValue_Id", "dbo.VariantOptions");
            DropForeignKey("dbo.ProductImageKits", "ProductImageKit_Id", "dbo.ProductImageKits");
            DropForeignKey("dbo.ProductImages", "ProductImageKit_Id", "dbo.ProductImageKits");
            DropForeignKey("dbo.ProductImageKits", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Categories", "Parent_Id", "dbo.Categories");
            DropForeignKey("dbo.CategoryLocalizedKits", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.CategoryLocalizedKits", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.ShippingBoxCountries", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.ShippingBoxCountries", "ShippingBox_Id", "dbo.ShippingBoxes");
            DropIndex("dbo.BlogPostBlogCategories", new[] { "BlogCategory_Id" });
            DropIndex("dbo.BlogPostBlogCategories", new[] { "BlogPost_Id" });
            DropIndex("dbo.CouponCategories", new[] { "Category_Id" });
            DropIndex("dbo.CouponCategories", new[] { "Coupon_Id" });
            DropIndex("dbo.ProductShippingBoxes", new[] { "ShippingBox_Id" });
            DropIndex("dbo.ProductShippingBoxes", new[] { "Product_Id" });
            DropIndex("dbo.RelatedProducts", new[] { "RelatedProductID" });
            DropIndex("dbo.RelatedProducts", new[] { "ProductID" });
            DropIndex("dbo.ProductVariantVariantOptions", new[] { "VariantOption_Id" });
            DropIndex("dbo.ProductVariantVariantOptions", new[] { "ProductVariant_Id" });
            DropIndex("dbo.CategoryProductStockUnits", new[] { "ProductStockUnit_Id" });
            DropIndex("dbo.CategoryProductStockUnits", new[] { "Category_Id" });
            DropIndex("dbo.ProductStockUnitVariantOptions", new[] { "VariantOption_Id" });
            DropIndex("dbo.ProductStockUnitVariantOptions", new[] { "ProductStockUnit_Id" });
            DropIndex("dbo.CategoryProducts", new[] { "Product_Id" });
            DropIndex("dbo.CategoryProducts", new[] { "Category_Id" });
            DropIndex("dbo.CategoryProductImageKits", new[] { "ProductImageKit_Id" });
            DropIndex("dbo.CategoryProductImageKits", new[] { "Category_Id" });
            DropIndex("dbo.ProductImageKitVariantOptions", new[] { "VariantOption_Id" });
            DropIndex("dbo.ProductImageKitVariantOptions", new[] { "ProductImageKit_Id" });
            DropIndex("dbo.ShippingBoxCountries", new[] { "Country_Id" });
            DropIndex("dbo.ShippingBoxCountries", new[] { "ShippingBox_Id" });
            DropIndex("dbo.SeoSectionLocalizedKits", new[] { "SeoSection_Id" });
            DropIndex("dbo.SeoSectionLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.ProductRelatedProducts", new[] { "ProductRelated_Id" });
            DropIndex("dbo.ProductRelatedProducts", new[] { "Product_Id" });
            DropIndex("dbo.Logs", new[] { "User_Id" });
            DropIndex("dbo.BlogComments", new[] { "Post_Id" });
            DropIndex("dbo.BlogComments", new[] { "Author_Id" });
            DropIndex("dbo.BlogPosts", new[] { "Author_Id" });
            DropIndex("dbo.BlogPosts", new[] { "Url" });
            DropIndex("dbo.BlogCategoryLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.BlogCategoryLocalizedKits", new[] { "BlogCategory_Id" });
            DropIndex("dbo.OrderItemTaxes", new[] { "OrderItem_Id" });
            DropIndex("dbo.OrderItemVariants", new[] { "OrderItem_Id" });
            DropIndex("dbo.OrderItems", new[] { "OrderShippingBox_Id" });
            DropIndex("dbo.OrderShippingBoxes", new[] { "Order_Id" });
            DropIndex("dbo.OrderShippingBoxes", new[] { "Name" });
            DropIndex("dbo.Orders", new[] { "ShippingAddress_Id" });
            DropIndex("dbo.Orders", new[] { "OrderCredit_Id" });
            DropIndex("dbo.Orders", new[] { "OrderCoupon_Id" });
            DropIndex("dbo.Orders", new[] { "BillingAddress_Id" });
            DropIndex("dbo.EmailLocalizedKits", new[] { "Email_Id" });
            DropIndex("dbo.EmailLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.Emails", new[] { "ContentSection_Id" });
            DropIndex("dbo.Emails", new[] { "Name" });
            DropIndex("dbo.SentEmails", new[] { "User_Id" });
            DropIndex("dbo.SentEmails", new[] { "Order_Id" });
            DropIndex("dbo.SentEmails", new[] { "Email_Id" });
            DropIndex("dbo.SentEmails", new[] { "Coupon_Id" });
            DropIndex("dbo.SentEmails", new[] { "Cart_Id" });
            DropIndex("dbo.WishLists", new[] { "User_Id" });
            DropIndex("dbo.UserVariants", new[] { "VariantOptionValue_Id" });
            DropIndex("dbo.UserVariants", new[] { "Variant_Id" });
            DropIndex("dbo.UserVariants", new[] { "User_Id" });
            DropIndex("dbo.UserCredits", new[] { "UserLocalizedKit_Id" });
            DropIndex("dbo.UserLocalizedKits", new[] { "User_Id" });
            DropIndex("dbo.UserLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.UserAttributes", new[] { "User_Id" });
            DropIndex("dbo.UserAttributes", new[] { "ContentSectionValue_Id" });
            DropIndex("dbo.UserAttributes", new[] { "AttributeOptionValue_Id" });
            DropIndex("dbo.UserAttributes", new[] { "Attribute_Id" });
            DropIndex("dbo.UserRoles", new[] { "Name" });
            DropIndex("dbo.MailingListLocalizedKits", new[] { "MailingList_Id" });
            DropIndex("dbo.MailingListLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.MailingListSubscriptions", new[] { "User_Id" });
            DropIndex("dbo.MailingListSubscriptions", new[] { "MailingList_Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "Country_Id" });
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.Coupons", new[] { "Referee_Id" });
            DropIndex("dbo.Coupons", new[] { "Country_Id" });
            DropIndex("dbo.Coupons", new[] { "Code" });
            DropIndex("dbo.Carts", new[] { "UserCredit_Id" });
            DropIndex("dbo.Carts", new[] { "ShippingAddress_Id" });
            DropIndex("dbo.Carts", new[] { "Coupon_Id" });
            DropIndex("dbo.Carts", new[] { "User_Id" });
            DropIndex("dbo.Carts", new[] { "BillingAddress_Id" });
            DropIndex("dbo.CartShippingBoxes", new[] { "ShippingBox_Id" });
            DropIndex("dbo.CartShippingBoxes", new[] { "Cart_Id" });
            DropIndex("dbo.CartItemVariants", new[] { "VariantOptionValue_Id" });
            DropIndex("dbo.CartItemVariants", new[] { "Variant_Id" });
            DropIndex("dbo.CartItemVariants", new[] { "CartItem_Id" });
            DropIndex("dbo.CartItems", new[] { "Product_Id" });
            DropIndex("dbo.CartItems", new[] { "CartShippingBox_Id" });
            DropIndex("dbo.CartItems", new[] { "WishList_Id" });
            DropIndex("dbo.Taxes", new[] { "ShippingAddress_Id" });
            DropIndex("dbo.Taxes", new[] { "Country_Id" });
            DropIndex("dbo.Taxes", new[] { "CartItem_Id" });
            DropIndex("dbo.ShippingBoxLocalizedKits", new[] { "ShippingBox_Id" });
            DropIndex("dbo.ShippingBoxLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.ProductVariants", new[] { "Variant_Id" });
            DropIndex("dbo.ProductVariants", new[] { "Product_Id" });
            DropIndex("dbo.ProductVariants", new[] { "DefaultVariantOptionValue_Id" });
            DropIndex("dbo.ProductLocalizedKits", new[] { "Product_Id" });
            DropIndex("dbo.ProductLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.ContentVersions", new[] { "ContentSectionLocalizedKit_Id" });
            DropIndex("dbo.ContentSectionLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.ContentSectionLocalizedKits", new[] { "ContentSection_Id" });
            DropIndex("dbo.AttributeOptionLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.AttributeOptionLocalizedKits", new[] { "AttributeOption_Id" });
            DropIndex("dbo.AttributeOptions", new[] { "Attribute_Id" });
            DropIndex("dbo.AttributeOptions", new[] { "Url" });
            DropIndex("dbo.AttributeOptions", new[] { "Name" });
            DropIndex("dbo.AttributeLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.AttributeLocalizedKits", new[] { "Attribute_Id" });
            DropIndex("dbo.Attributes", new[] { "Url" });
            DropIndex("dbo.Attributes", new[] { "Name" });
            DropIndex("dbo.ProductAttributes", new[] { "Product_Id" });
            DropIndex("dbo.ProductAttributes", new[] { "ContentSectionValue_Id" });
            DropIndex("dbo.ProductAttributes", new[] { "AttributeOptionValue_Id" });
            DropIndex("dbo.ProductAttributes", new[] { "Attribute_Id" });
            DropIndex("dbo.ProductStockUnitLocalizedKits", new[] { "ProductStockUnit_Id" });
            DropIndex("dbo.ProductStockUnitLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.ProductStockUnits", new[] { "Product_Id" });
            DropIndex("dbo.VariantOptionLocalizedKits", new[] { "VariantOption_Id" });
            DropIndex("dbo.VariantOptionLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.VariantLocalizedKits", new[] { "Variant_Id" });
            DropIndex("dbo.VariantLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.Variants", new[] { "DefaultVariantOptionValue_Id" });
            DropIndex("dbo.Variants", new[] { "Url" });
            DropIndex("dbo.Variants", new[] { "Name" });
            DropIndex("dbo.VariantOptions", new[] { "Variant_Id" });
            DropIndex("dbo.VariantOptions", new[] { "Url" });
            DropIndex("dbo.VariantOptions", new[] { "Name" });
            DropIndex("dbo.ProductImages", new[] { "ProductImageKit_Id" });
            DropIndex("dbo.ProductImageKits", new[] { "ProductImageKit_Id" });
            DropIndex("dbo.ProductImageKits", new[] { "Product_Id" });
            DropIndex("dbo.CategoryLocalizedKits", new[] { "Country_Id" });
            DropIndex("dbo.CategoryLocalizedKits", new[] { "Category_Id" });
            DropIndex("dbo.Categories", new[] { "Parent_Id" });
            DropIndex("dbo.Categories", new[] { "Url" });
            DropIndex("dbo.Categories", new[] { "Name" });
            DropIndex("dbo.Products", new[] { "Url" });
            DropIndex("dbo.ShippingBoxes", new[] { "Name" });
            DropIndex("dbo.Countries", new[] { "LanguageCode" });
            DropIndex("dbo.Countries", new[] { "Name" });
            DropIndex("dbo.Addresses", new[] { "User_Id" });
            DropIndex("dbo.Addresses", new[] { "Country_Id" });
            DropTable("dbo.BlogPostBlogCategories");
            DropTable("dbo.CouponCategories");
            DropTable("dbo.ProductShippingBoxes");
            DropTable("dbo.RelatedProducts");
            DropTable("dbo.ProductVariantVariantOptions");
            DropTable("dbo.CategoryProductStockUnits");
            DropTable("dbo.ProductStockUnitVariantOptions");
            DropTable("dbo.CategoryProducts");
            DropTable("dbo.CategoryProductImageKits");
            DropTable("dbo.ProductImageKitVariantOptions");
            DropTable("dbo.ShippingBoxCountries");
            DropTable("dbo.SeoSections");
            DropTable("dbo.SeoSectionLocalizedKits");
            DropTable("dbo.ProductRelatedProducts");
            DropTable("dbo.Logs");
            DropTable("dbo.CmsSettings");
            DropTable("dbo.BlogComments");
            DropTable("dbo.BlogPosts");
            DropTable("dbo.BlogCategoryLocalizedKits");
            DropTable("dbo.BlogCategories");
            DropTable("dbo.OrderItemTaxes");
            DropTable("dbo.OrderItemVariants");
            DropTable("dbo.OrderItems");
            DropTable("dbo.OrderShippingBoxes");
            DropTable("dbo.OrderCredits");
            DropTable("dbo.OrderCoupons");
            DropTable("dbo.OrderAddresses");
            DropTable("dbo.Orders");
            DropTable("dbo.EmailLocalizedKits");
            DropTable("dbo.Emails");
            DropTable("dbo.SentEmails");
            DropTable("dbo.WishLists");
            DropTable("dbo.UserVariants");
            DropTable("dbo.UserCredits");
            DropTable("dbo.UserLocalizedKits");
            DropTable("dbo.UserAttributes");
            DropTable("dbo.UserRoles");
            DropTable("dbo.MailingListLocalizedKits");
            DropTable("dbo.MailingLists");
            DropTable("dbo.MailingListSubscriptions");
            DropTable("dbo.Users");
            DropTable("dbo.Coupons");
            DropTable("dbo.Carts");
            DropTable("dbo.CartShippingBoxes");
            DropTable("dbo.CartItemVariants");
            DropTable("dbo.CartItems");
            DropTable("dbo.Taxes");
            DropTable("dbo.ShippingBoxLocalizedKits");
            DropTable("dbo.ProductVariants");
            DropTable("dbo.ProductLocalizedKits");
            DropTable("dbo.ContentVersions");
            DropTable("dbo.ContentSectionLocalizedKits");
            DropTable("dbo.ContentSections");
            DropTable("dbo.AttributeOptionLocalizedKits");
            DropTable("dbo.AttributeOptions");
            DropTable("dbo.AttributeLocalizedKits");
            DropTable("dbo.Attributes");
            DropTable("dbo.ProductAttributes");
            DropTable("dbo.ProductStockUnitLocalizedKits");
            DropTable("dbo.ProductStockUnits");
            DropTable("dbo.VariantOptionLocalizedKits");
            DropTable("dbo.VariantLocalizedKits");
            DropTable("dbo.Variants");
            DropTable("dbo.VariantOptions");
            DropTable("dbo.ProductImages");
            DropTable("dbo.ProductImageKits");
            DropTable("dbo.CategoryLocalizedKits");
            DropTable("dbo.Categories");
            DropTable("dbo.Products");
            DropTable("dbo.ShippingBoxes");
            DropTable("dbo.Countries");
            DropTable("dbo.Addresses");
        }
    }
}
