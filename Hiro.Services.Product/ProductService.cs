﻿namespace Hiro.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.Domain;
    using Hiro.Domain.Extensions;
    using Hiro.Services.Interfaces;
    using Hiro.Shared.Extensions;
    using JetBrains.Annotations;

    public class ProductService : IProductService
    {
        public List<ShippingBox> GetShippingBoxesForShippingCountry([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, [NotNull] Product product)
        {
            shippingCountry ??= localizationCountry;

            return product.ShippingBoxes
                .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled(localizationCountry) && x.Countries.ContainsById(shippingCountry))
                .OrderBySortOrder()
                .ToList();
        }

        public List<Tax> GetTaxes([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, [NotNull] Product product)
        {
            return (shippingCountry ?? localizationCountry).Taxes.Where(x => x.CartItem == null && x.ApplyToProductPrice).ToList();
        }

        public double GetTotalBeforeTax(Country localizationCountry, [CanBeNull] User user, Product product, List<VariantOption> variantOptions = null)
        {
            variantOptions ??= product.GetDefaultVariantOptions();

            var productStockUnit = product.GetProductStockUnit(variantOptions);

            if (productStockUnit == null)
            {
                return 0;
            }

            var basePrice = productStockUnit.GetLocalizedBasePrice(localizationCountry);
            var salePrice = GetApplicableSalePrice(localizationCountry, user, productStockUnit);
            var productPrice = salePrice > 0 ? salePrice : basePrice;
            var productPriceIncludingVariants = productPrice + variantOptions.Sum(x => x.GetLocalizedPriceModifier(localizationCountry));

            return productPriceIncludingVariants.ToMinimum(0);
        }

        public double GetTotalAfterTax(Country localizationCountry, [CanBeNull] Country shippingCountry, [CanBeNull] User user, Product product, List<VariantOption> variantOptions = null)
        {
            variantOptions ??= product.GetDefaultVariantOptions();

            var totalBeforeTax = this.GetTotalBeforeTax(localizationCountry, user, product, variantOptions);

            var productTaxes = this.GetTaxes(localizationCountry, shippingCountry, product);

            return totalBeforeTax.AddTaxes(productTaxes);
        }

        public double GetTotalBeforeTaxWithoutSalePrice([NotNull] Country localizationCountry, [CanBeNull] User user, [NotNull] Product product, [CanBeNull] List<VariantOption> variantOptions = null)
        {
            variantOptions ??= product.GetDefaultVariantOptions();

            var productStockUnit = product.GetProductStockUnit(variantOptions);

            if (productStockUnit == null)
            {
                return 0;
            }

            var basePrice = productStockUnit.GetLocalizedBasePrice(localizationCountry);
            var productPriceIncludingVariants = basePrice + variantOptions.Sum(x => x.GetLocalizedPriceModifier(localizationCountry));

            return productPriceIncludingVariants.ToMinimum(0);
        }

        public double GetTotalAfterTaxWithoutSalePrice([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, [CanBeNull] User user, [NotNull] Product product, [CanBeNull] List<VariantOption> variantOptions = null)
        {
            variantOptions ??= product.GetDefaultVariantOptions();

            var totalBeforeTaxWithoutSalePrice = this.GetTotalBeforeTaxWithoutSalePrice(localizationCountry, user, product, variantOptions);

            var productTaxes = this.GetTaxes(localizationCountry, shippingCountry, product);

            return totalBeforeTaxWithoutSalePrice.AddTaxes(productTaxes);
        }

        private static double GetApplicableSalePrice(Country localizationCountry, [CanBeNull] User user, ProductStockUnit productStockUnit)
        {
            if (user?.ShowMembershipSalePrice == true)
            {
                var membershipSalePrice = productStockUnit.GetLocalizedMembershipSalePrice(localizationCountry);

                return membershipSalePrice > 0 ? membershipSalePrice : productStockUnit.GetLocalizedSalePrice(localizationCountry);
            }

            return productStockUnit.GetLocalizedSalePrice(localizationCountry);
        }
    }
}