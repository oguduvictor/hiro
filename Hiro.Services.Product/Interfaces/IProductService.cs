﻿namespace Hiro.Services.Interfaces
{
    using System.Collections.Generic;
    using Hiro.Domain;
    using JetBrains.Annotations;

    public interface IProductService
    {
        List<ShippingBox> GetShippingBoxesForShippingCountry([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, Product product);

        List<Tax> GetTaxes([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, Product product);

        double GetTotalBeforeTax([NotNull] Country localizationCountry, User user, Product product, List<VariantOption> variantOptions = null);

        double GetTotalAfterTax([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, User user, Product product, List<VariantOption> variantOptions = null);

        double GetTotalBeforeTaxWithoutSalePrice([NotNull] Country localizationCountry, User user, Product product, List<VariantOption> variantOptions = null);

        double GetTotalAfterTaxWithoutSalePrice([NotNull] Country localizationCountry, [CanBeNull] Country shippingCountry, User user, Product product, List<VariantOption> variantOptions = null);
    }
}