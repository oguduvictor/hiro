﻿namespace Hiro.Shared.Attributes
{
    using System;

    public class SchemaPropertyAttribute : Attribute
    {
        public string Title { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a table's minimum number of columns.
        /// </summary>
        public int MinTableColumns { get; set; } = 1;

        /// <summary>
        /// Gets or sets a table's maximum number of columns.
        /// </summary>
        public int MaxTableColumns { get; set; } = int.MaxValue;
    }
}