﻿namespace Hiro.Shared.Attributes
{
    using System;

    public class SchemaAttribute : Attribute
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }
}