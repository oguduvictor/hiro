﻿#pragma warning disable 1591

namespace Hiro.Shared.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class MediaDirectory
    {
        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string LocalPath { get; set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; set; }

        public List<MediaFile> Files { get; set; } = new List<MediaFile>();

        public List<MediaDirectory> Directories { get; set; } = new List<MediaDirectory>();

        public MediaDirectory Parent { get; set; }

        public bool IsRoot { get; set; }
    }
}