﻿#pragma warning disable 1591

namespace Hiro.Shared.Dtos
{
    using System;
    using JetBrains.Annotations;

    // -----------------------------------------------------------------------
    // Properties example:
    //
    // Name = file
    // Extension = .jpeg
    // LocalPath = /content/img/file.jpeg
    // AbsolutePath = www.domain.com/content/img/file.jpeg
    // -----------------------------------------------------------------------

    public class MediaFile
    {
        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Extension { get; set; }

        [CanBeNull]
        public string LocalPath { get; set; }

        [CanBeNull]
        public string AbsolutePath { get; set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; set; } = DateTime.Now;

        public bool UseAzureStorage { get; set; }

        public long HeavyLoadingFileSize { get; set; }
    }
}