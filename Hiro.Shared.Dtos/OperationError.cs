﻿#pragma warning disable 1591

namespace Hiro.Shared.Dtos
{
    public class OperationError
    {
        public OperationError()
        {
        }

        public OperationError(string message)
        {
            this.Message = message;
        }

        public OperationError(string propertyName, string message)
        {
            this.PropertyName = propertyName;

            this.Message = message;
        }

        public string PropertyName { get; set; }

        public string Message { get; set; }
    }
}
