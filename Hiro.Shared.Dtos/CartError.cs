﻿#pragma warning disable 1591

namespace Hiro.Shared.Dtos
{
    using System;

    public class CartError
    {
        public CartErrorCode Code { get; set; }

        public string Message { get; set; }

        public Guid? TargetId { get; set; }
    }

    public enum CartErrorCode
    {
        CartItemWithDisabledProduct = 100,
        CartItemWithOutOfStockProduct = 150,
        CartItemWithInvalidShippingBox = 200,
        InvalidCoupon = 300,
        InvalidUserCredit = 400,
        InvalidTaxOverride = 500,
        LocalizedShippingAddressCountryNotMatchingCurrentCountry = 600,
        NonLocalizedShippingAddressCountryNotMatchingDefaultCountry = 700,
        ShippingAddressRequired = 800
    }

}