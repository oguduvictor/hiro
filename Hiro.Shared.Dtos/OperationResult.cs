﻿#pragma warning disable 1591

namespace Hiro.Shared.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public class OperationResult
    {
        public OperationResult()
        {
        }

        public OperationResult(List<OperationError> errors)
        {
            this.Errors = errors;
        }

        [CanBeNull]
        public string SuccessMessage { get; set; }

        [NotNull]
        public List<OperationError> Errors { get; set; } = new List<OperationError>();

        [CanBeNull]
        public string GetErrorMessageFor(string propertyName)
        {
            return this.Errors.FirstOrDefault(x => x.PropertyName?.Equals(propertyName, StringComparison.InvariantCultureIgnoreCase) ?? false)?.Message;
        }

        public void AddError(string message)
        {
            this.Errors.Add(new OperationError(message));
        }

        public void AddError(string propertyName, string message)
        {
            this.Errors.Add(new OperationError(propertyName, message));
        }
    }

    public class OperationResult<T> : OperationResult
    {
        public OperationResult()
        {
        }

        public OperationResult(List<OperationError> errors)
            : base(errors)
        {
        }

        public OperationResult(List<OperationError> errors, T data)
        {
            this.Errors = errors;
            this.Data = data;
        }

        [CanBeNull]
        public T Data { get; set; }
    }
}