﻿namespace Hiro.Shared.Extensions
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewEngines;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;

    public static class ControllerExtension
    {
        public static async Task<string> RenderViewAsync(this Controller controller, string viewName, string cultureName = null, object model = null, bool partial = false)
        {
            if (string.IsNullOrEmpty(cultureName) == false)
            {
                var cultureInfo = CultureInfo.CreateSpecificCulture(cultureName);

                Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
            }

            controller.ViewData.Model = model;

            using var writer = new StringWriter();

            var viewEngineResult = GetViewEngineResult(controller, viewName, partial);

            if (viewEngineResult.Success == false)
            {
                throw new Exception($"A view with the name {viewName} could not be found");
            }

            var viewContext = new ViewContext(
                controller.ControllerContext,
                viewEngineResult.View,
                controller.ViewData,
                controller.TempData,
                writer,
                new HtmlHelperOptions());

            await viewEngineResult.View.RenderAsync(viewContext);

            return writer.GetStringBuilder().ToString();
        }

        private static ViewEngineResult GetViewEngineResult(Controller controller, string viewName, bool isPartial)
        {
            var viewEngine = controller.HttpContext.RequestServices.GetService(typeof(ICompositeViewEngine)) as ICompositeViewEngine;

            if (viewName.StartsWith("~/"))
            {
                var hostingEnv = controller.HttpContext.RequestServices.GetService(typeof(IHostingEnvironment)) as IHostingEnvironment;

                return viewEngine.GetView(hostingEnv.WebRootPath, viewName, !isPartial);
            }
            else
            {
                return viewEngine.FindView(controller.ControllerContext, viewName, !isPartial);
            }
        }
    }
}
