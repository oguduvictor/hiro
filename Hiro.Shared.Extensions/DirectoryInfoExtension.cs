﻿namespace Hiro.Shared.Extensions
{
    using System.IO;
    using System.Linq;

    /// <summary>
    ///
    /// </summary>
    public static class DirectoryInfoExtension
    {
        /// <summary>
        ///
        /// </summary>
        public static long GetSize(this DirectoryInfo directory)
        {
            var files = directory.GetFiles();

            var totalSize = files.Sum(f => f.Length);

            var directories = directory.GetDirectories();

            totalSize += directories.Sum(d => GetSize(d));

            return totalSize;
        }

        /// <summary>
        ///
        /// </summary>
        public static string BytesToKiloBytes(long value)
        {
            var mag = value / 1024;
            return mag + " KB";
        }

        //private static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        ///// <summary>
        /////
        ///// </summary>
        //public static string SizeSuffix(Int64 value)
        //{
        //    if (value < 0) { return "-" + SizeSuffix(-value); }
        //    if (value == 0) { return "0.0 bytes"; }

        //    var mag = (int)Math.Log(value, 1024);
        //    var adjustedSize = (decimal)value / (1L << (mag * 10));

        //    return $"{adjustedSize:n1} {SizeSuffixes[mag]}";
        //}
    }
}