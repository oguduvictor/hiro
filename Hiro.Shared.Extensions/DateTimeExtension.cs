﻿namespace Hiro.Shared.Extensions
{
    using System;

    /// <summary>
    ///
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        ///
        /// </summary>
        public static DateTime AddWorkingDays(this DateTime date, int daysToAdd)
        {
            while (daysToAdd > 0)
            {
                date = date.AddDays(1);

                if (date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday)
                {
                    daysToAdd -= 1;
                }
            }

            return date;
        }

        /// <summary>
        /// Return the current date only if it's greater than the minimumDate parameter,
        /// otherwise return the minimumDate itself.
        /// </summary>
        public static DateTime ToMinimum(this DateTime date, DateTime? minimumDate)
        {
            if (minimumDate == null || minimumDate.Value < date)
            {
                return date;
            }

            return minimumDate.Value;
        }
    }
}
