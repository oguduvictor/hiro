﻿#pragma warning disable 1591

namespace Hiro.Shared.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class ListExtension
    {
        public static void AddRangeIfNotNull<T>(this List<T> list, IEnumerable<T> items)
        {
            if (items == null)
            {
                return;
            }

            list.AddRange(items.Where(x => x != null));
        }
    }
}
