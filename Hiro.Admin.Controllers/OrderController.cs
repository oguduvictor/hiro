﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Mediator.Requests;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class OrderController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminOrderService adminOrderService;
        private readonly ISendEmailConfirmationModalDtoValidator sendEmailConfirmationModalDtoValidator;
        private readonly IMediator mediator;
        private readonly IOrderDtoValidator orderDtoValidator;

        public OrderController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminOrderService adminOrderService,
            ISendEmailConfirmationModalDtoValidator sendEmailConfirmationModalDtoValidator,
            IOrderDtoValidator orderDtoValidator, 
            IMediator mediator)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.adminOrderService = adminOrderService;
            this.orderDtoValidator = orderDtoValidator;
            this.mediator = mediator;
            this.sendEmailConfirmationModalDtoValidator = sendEmailConfirmationModalDtoValidator;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminOrderIndex)]
        public virtual IActionResult IndexJson(int page = 1, string keywordFilter = null, int? orderStatusValueFilter = null, string countryNameFilter = null)
        {
            const int pageSize = 100;

            // -----------------------------------------------------
            // Filters
            // -----------------------------------------------------

            var filteredOrders = this.dbContext.Orders
                .Where(x => string.IsNullOrEmpty(keywordFilter) ||
                            x.OrderNumber.ToString().Contains(keywordFilter) ||
                            x.TransactionId.Contains(keywordFilter) ||
                            x.UserEmail.Contains(keywordFilter) ||
                            x.UserFirstName.Contains(keywordFilter) ||
                            x.UserLastName.Contains(keywordFilter) ||
                            x.Comments.Contains(keywordFilter) ||
                            x.Tags.Contains(keywordFilter) ||
                            x.AutoTags.Contains(keywordFilter))
                .Where(x => orderStatusValueFilter == null ||
                            x.Status == (OrderStatusEnum)orderStatusValueFilter.Value)
                .Where(x => string.IsNullOrEmpty(countryNameFilter) ||
                            x.CountryName == countryNameFilter)
                .OrderByDescending(x => x.CreatedDate);

            // -----------------------------------------------------
            // Dto
            // -----------------------------------------------------

            var dto = new OrderListDto
            {
                TotalItems = filteredOrders.Count(),
                PageSize = pageSize,
                Orders = filteredOrders.Skip((page - 1) * pageSize).Take(pageSize).AsEnumerable().Select(order => new OrderDto
                {
                    Id = order.Id,
                    CreatedDate = order.CreatedDate,
                    CountryName = order.CountryName,
                    CountryLanguageCode = order.CountryLanguageCode,
                    CurrencySymbol = order.CurrencySymbol,
                    OrderNumber = order.OrderNumber,
                    UserLastName = order.UserLastName,
                    UserFirstName = order.UserFirstName,
                    UserEmail = order.UserEmail,
                    ShippingAddress = new OrderAddressDto
                    {
                        Id = order.ShippingAddress?.Id ?? Guid.NewGuid(),
                        CountryName = order.ShippingAddress?.CountryName
                    },
                    Status = order.Status,
                    StatusDescriptions = order.Status.GetType().GetDescriptions(),
                    TotalAfterTax = order.TotalAfterTax
                }).ToList(),
                Countries = this.dbContext.Countries.OrderBy(x => x.Name).Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList()
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminOrderUpdate)]
        public virtual IActionResult Update(OrderDto dto)
        {
            var errors = this.orderDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            this.adminOrderService.Update(this.dbContext, dto);

            return this.Ok(new OperationResult(errors));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminOrderEdit)]
        public virtual IActionResult EditJson(Guid orderId)
        {
            var order = this.dbContext.Orders.Find(orderId);

            if (order == null)
            {
                throw new NullReferenceException();
            }

            var editOrderDto = new EditOrderDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Order = new OrderDto
                {
                    Id = order.Id,
                    CreatedDate = order.CreatedDate,
                    OrderNumber = order.OrderNumber,
                    Status = order.Status,
                    StatusDescriptions = typeof(OrderStatusEnum).GetDescriptions(), // simple implementation based on the [Description] attribute
                    CurrencySymbol = order.CurrencySymbol,
                    CountryLanguageCode = order.CountryLanguageCode,
                    UserId = order.UserId,
                    UserFirstName = order.UserFirstName,
                    UserLastName = order.UserLastName,
                    UserEmail = order.UserEmail,
                    TransactionId = order.TransactionId,
                    OrderShippingBoxes = order.OrderShippingBoxes.Select(shippingBox => new OrderShippingBoxDto
                    {
                        Id = shippingBox.Id,
                        CountWeekends = shippingBox.CountWeekends,
                        Description = shippingBox.Description,
                        InternalDescription = shippingBox.InternalDescription,
                        IsDisabled = shippingBox.IsDisabled,
                        MaxDays = shippingBox.MaxDays,
                        MinDays = shippingBox.MinDays,
                        Name = shippingBox.Name,
                        Order = shippingBox.Order == null ? null : new OrderDto(shippingBox.Order.Id),
                        ShippingPriceAfterTax = shippingBox.ShippingPriceAfterTax,
                        ShippingPriceBeforeTax = shippingBox.ShippingPriceBeforeTax,
                        Status = shippingBox.Status,
                        StatusDescriptions = shippingBox.Status.GetType().GetDescriptions(),
                        Title = shippingBox.Title,
                        TrackingCode = shippingBox.TrackingCode,
                        OrderItems = shippingBox.OrderItems.Select(orderItem => new OrderItemDto
                        {
                            Id = orderItem.Id,
                            MaxEta = orderItem.MaxEta,
                            MinEta = orderItem.MinEta,
                            StockStatusDescriptions = orderItem.StockStatus.GetType().GetDescriptions(),
                            ProductCode = orderItem.ProductCode,
                            ProductId = orderItem.ProductId,
                            ProductName = orderItem.ProductName,
                            ProductStockUnitCode = orderItem.ProductStockUnitCode,
                            ProductTitle = orderItem.ProductTitle,
                            ProductUrl = orderItem.ProductUrl,
                            ProductStockUnitEnablePreorder = orderItem.ProductStockUnitEnablePreorder,
                            ProductStockUnitId = orderItem.ProductStockUnitId,
                            ProductStockUnitReleaseDate = orderItem.ProductStockUnitReleaseDate,
                            ProductStockUnitSalePrice = orderItem.ProductStockUnitSalePrice,
                            ProductStockUnitBasePrice = orderItem.ProductStockUnitBasePrice,
                            ProductStockUnitMembershipSalePrice = orderItem.ProductStockUnitMembershipSalePrice,
                            ProductQuantity = orderItem.ProductQuantity,
                            ProductStockUnitShipsIn = orderItem.ProductStockUnitShipsIn,
                            Status = orderItem.Status,
                            StatusDescriptions = orderItem.Status.GetType().GetDescriptions(),
                            StockStatus = orderItem.StockStatus,
                            SubtotalAfterTax = orderItem.SubtotalAfterTax,
                            SubtotalBeforeTax = orderItem.SubtotalBeforeTax,
                            TotalAfterTax = orderItem.TotalAfterTax,
                            TotalBeforeTax = orderItem.TotalBeforeTax,
                            OrderShippingBox = orderItem.OrderShippingBox == null ? null : new OrderShippingBoxDto(orderItem.OrderShippingBox.Id),
                            OrderItemVariants = orderItem.OrderItemVariants.Select(x => new OrderItemVariantDto
                            {
                                Id = x.Id,
                                VariantName = x.VariantName,
                                VariantUrl = x.VariantUrl,
                                VariantType = x.VariantType,
                                VariantLocalizedTitle = x.VariantLocalizedTitle,
                                VariantLocalizedDescription = x.VariantLocalizedDescription,
                                VariantOptionLocalizedTitle = x.VariantOptionLocalizedTitle,
                                VariantOptionCode = x.VariantOptionCode,
                                VariantOptionLocalizedPriceModifier = x.VariantOptionLocalizedPriceModifier,
                                JsonValue = x.JsonValue,
                                DoubleValue = x.DoubleValue,
                                IntegerValue = x.IntegerValue,
                                BooleanValue = x.BooleanValue,
                                StringValue = x.StringValue
                            }).ToList(),
                            OrderTaxes = orderItem.OrderTaxes.Select(orderItemTax => new OrderItemTaxDto
                            {
                                Id = orderItemTax.Id,
                                Amount = orderItemTax.Amount,
                                Code = orderItemTax.Code,
                                IsDisabled = orderItemTax.IsDisabled,
                                Name = orderItemTax.Name
                            }).ToList()
                        }).ToList()
                    }).ToList(),
                    ShippingAddress = order.ShippingAddress == null ? null : new OrderAddressDto
                    {
                        Id = order.ShippingAddress.Id,
                        AddressFirstName = order.ShippingAddress.AddressFirstName,
                        AddressLastName = order.ShippingAddress.AddressLastName,
                        AddressLine1 = order.ShippingAddress.AddressLine1,
                        AddressLine2 = order.ShippingAddress.AddressLine2,
                        City = order.ShippingAddress.City,
                        CountryName = order.ShippingAddress.CountryName,
                        PhoneNumber = order.ShippingAddress.PhoneNumber,
                        Postcode = order.ShippingAddress.Postcode,
                        StateCountyProvince = order.ShippingAddress.StateCountyProvince
                    },
                    Comments = order.Comments,
                    SentEmails = order.SentEmails.Select(x => new SentEmailDto
                    {
                        Id = x.Id,
                        Email = x.Email == null ? null : new EmailDto
                        {
                            Id = x.Email.Id
                        }
                    }).ToList(),
                    OrderCoupon = order.OrderCoupon == null ? null : new OrderCouponDto
                    {
                        Id = order.OrderCoupon.Id,
                        Code = order.OrderCoupon.Code,
                        Amount = order.OrderCoupon.Amount
                    },
                    OrderCredit = order.OrderCredit == null ? null : new OrderCreditDto
                    {
                        Id = order.OrderCredit.Id,
                        Amount = order.OrderCredit.Amount
                    },
                    TotalBeforeTax = order.TotalBeforeTax,
                    TotalAfterTax = order.TotalAfterTax,
                    TotalToBePaid = order.TotalToBePaid
                },
                AllEmails = this.dbContext.Emails.Where(x => !x.IsDisabled).OrderBy(x => x.Name).Select(x => new EmailDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    ViewName = x.ViewName
                }).ToList()
            };

            return this.Ok(editOrderDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminOrderRestoreStock)]
        public virtual IActionResult RestoreStock(Guid orderItemId)
        {
            var orderItem = this.dbContext.OrderItems.Find(orderItemId);

            if (orderItem == null)
            {
                throw new NullReferenceException();
            }

            var productStockUnit = this.dbContext.ProductStockUnits.Find(orderItem.ProductStockUnitId);

            if (productStockUnit == null || orderItem.StockStatus == OrderItemStockStatusEnum.Restored)
            {
                //return this.utilityService.GetOperationResultJson(null, orderItem.OrderShippingBox?.Order.Id);
                var restoredOperationResult = new OperationResult<Guid?>
                {
                    Data = orderItem.OrderShippingBox?.Order.Id
                };

                return this.Ok(restoredOperationResult);
            }

            productStockUnit.Stock += 1;

            orderItem.StockStatus = OrderItemStockStatusEnum.Restored;

            this.dbContext.SaveChanges();

            //return this.utilityService.GetOperationResultJson(null, orderItem.OrderShippingBox?.Order.Id);
            var operationResult = new OperationResult<Guid?>
            {
                Data = orderItem.OrderShippingBox?.Order.Id
            };

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminOrderSendEmail)]
        public virtual async Task<IActionResult> SendEmailAsync(SendEmailConfirmationModalDto dto)
        {
            var errors = this.sendEmailConfirmationModalDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var result = await this.mediator.Send(new EmailRequest
            {
                EmailName = dto.EmailName,
                To = dto.SendTo.InList(),
                Data = this.dbContext.Orders.Find(dto.EntityId)
            });

            return this.Ok(result);
        }
    }
}