﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class AttributeController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAttributeDtoValidator attributeDtoValidator;
        private readonly IAdminAttributeService adminAttributeService;

        public AttributeController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAttributeDtoValidator attributeDtoValidator,
            IAdminAttributeService adminAttributeService)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.attributeDtoValidator = attributeDtoValidator;
            this.adminAttributeService = adminAttributeService;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminAttributeIndex)]
        public virtual IActionResult IndexJson()
        {
            var attributeListDto = new AttributeListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Attributes = this.dbContext.Attributes.OrderBy(x => x.SortOrder).AsEnumerable().Select(attribute => new AttributeDto
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    SortOrder = attribute.SortOrder,
                    Type = attribute.Type,
                    TypeDescriptions = attribute.Type.GetType().GetDescriptions(),
                    LocalizedTitle = attribute.GetLocalizedTitle(this.localizationCountry),
                })
                .ToList()
            };

            return this.Ok(attributeListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminAttributeEdit)]
        public virtual IActionResult EditJson(Guid? attributeId)
        {
            var isNew = attributeId == null;

            var attribute = isNew ? new Domain.Attribute() : this.dbContext.Attributes.Find(attributeId);

            if (attribute == null)
            {
                throw new NullReferenceException();
            }

            attribute.AttributeLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(attribute.AttributeLocalizedKits, new AttributeLocalizedKit
            {
                Attribute = attribute
            });

            foreach (var attributeOption in attribute.AttributeOptions)
            {
                attributeOption.AttributeOptionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(attributeOption.AttributeOptionLocalizedKits, new AttributeOptionLocalizedKit
                {
                    AttributeOption = attributeOption
                });
            }

            var editAttributeDto = new EditAttributeDto
            {
                Attribute = new AttributeDto
                {
                    Id = attribute.Id,
                    IsNew = isNew,
                    Name = attribute.Name,
                    Url = attribute.Url,
                    SortOrder = attribute.SortOrder,
                    Type = attribute.Type,
                    TypeDescriptions = attribute.Type.GetType().GetDescriptions(),
                    AttributeOptions = attribute.AttributeOptions.Select(option => new AttributeOptionDto
                    {
                        Id = option.Id,
                        Name = option.Name,
                        Url = option.Url,
                        Tags = option.Tags,
                        LocalizedTitle = option.GetLocalizedTitle(this.localizationCountry),
                        Attribute = new AttributeDto(option.Attribute.Id),
                        AttributeOptionLocalizedKits = option.AttributeOptionLocalizedKits.Select(localizedKit => new AttributeOptionLocalizedKitDto
                        {
                            Id = localizedKit.Id,
                            Title = localizedKit.Title,
                            Description = localizedKit.Description,
                            IsDisabled = localizedKit.IsDisabled,
                            AttributeOption = localizedKit.AttributeOption == null ? null : new AttributeOptionDto(localizedKit.AttributeOption.Id),
                            Country = new CountryDto
                            {
                                Id = localizedKit.Country?.Id ?? throw new NullReferenceException(),
                                Name = localizedKit.Country.Name,
                                IsDefault = localizedKit.Country.IsDefault
                            }
                        }).ToList()
                    }).ToList(),
                    AttributeLocalizedKits = attribute.AttributeLocalizedKits.Select(localizedKit => new AttributeLocalizedKitDto
                    {
                        Id = localizedKit.Id,
                        Title = localizedKit.Title,
                        Description = localizedKit.Description,
                        Attribute = localizedKit.Attribute == null ? null : new AttributeDto(localizedKit.Attribute.Id),
                        Country = localizedKit.Country == null ? null : new CountryDto
                        {
                            Id = localizedKit.Country.Id,
                            Name = localizedKit.Country.Name,
                            IsDefault = localizedKit.Country.IsDefault
                        }
                    }).ToList(),
                    LocalizedTitle = attribute.GetLocalizedTitle(this.localizationCountry)
                },
                NewAttributeOption = new AttributeOptionDto
                {
                    IsNew = true,
                    Attribute = new AttributeDto(attribute.Id),
                    AttributeOptionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(new List<AttributeOptionLocalizedKit>(), new AttributeOptionLocalizedKit
                    {
                        AttributeOption = new AttributeOption()
                    }).Select(localizedKit => new AttributeOptionLocalizedKitDto
                    {
                        Id = localizedKit.Id,
                        IsNew = true,
                        Title = localizedKit.Title,
                        Description = localizedKit.Description,
                        AttributeOption = new AttributeOptionDto(),
                        Country = new CountryDto
                        {
                            Id = localizedKit.Country?.Id ?? throw new NullReferenceException(),
                            Name = localizedKit.Country.Name,
                            IsDefault = localizedKit.Country.IsDefault
                        }
                    }).ToList()
                },
                AllTags = this.dbContext.AttributeOptions.AsEnumerable().SelectMany(x => x.GetTagList()).Distinct().ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto()
            };

            editAttributeDto.Attribute.IsNew = isNew;

            return this.Ok(editAttributeDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminAttributeAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(AttributeDto dto)
        {
            var errors = this.attributeDtoValidator.Validate(dto);

            if (!errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var attribute = this.adminAttributeService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, attribute.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminAttributeUpdateSortOrder)]
        public virtual IActionResult UpdateSortOrder(List<AttributeDto> attributeDtos)
        {
            var operationResult = new OperationResult<List<AttributeDto>>();

            this.adminAttributeService.UpdateSortOrder(this.dbContext, attributeDtos);

            operationResult.Data = this.dbContext.Attributes.OrderBy(x => x.SortOrder).AsEnumerable().Select(attribute => new AttributeDto
            {
                Id = attribute.Id,
                Name = attribute.Name,
                SortOrder = attribute.SortOrder,
                Type = attribute.Type,
                TypeDescriptions = attribute.Type.GetType().GetDescriptions(),
                LocalizedTitle = attribute.GetLocalizedTitle(this.localizationCountry),
            }).ToList();

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminAttributeDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var attribute = this.dbContext.Attributes.Find(id);

            if (attribute == null)
            {
                throw new NullReferenceException();
            }

            var operationResult = new OperationResult();

            try
            {
                this.dbContext.Attributes.Delete(id, true);
            }
            catch (Exception e)
            {
                operationResult.Errors.Add(new OperationError(e.Message));
            }

            return this.Ok(operationResult);
        }
    }
}