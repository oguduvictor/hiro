﻿using System;
using System.IO;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class PostsController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminPostsService adminPostsService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IBlogPostDtoValidator blogPostDtoValidator;
        private readonly IConfiguration configuration;

        public PostsController(
            HiroDbContext dbContext,
            IAdminPostsService adminPostsService,
            IAdminUtilityService adminUtilityService,
            IBlogPostDtoValidator blogPostDtoValidator,
            IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.adminPostsService = adminPostsService;
            this.adminUtilityService = adminUtilityService;
            this.blogPostDtoValidator = blogPostDtoValidator;
            this.configuration = configuration;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminPostsIndex)]
        public virtual IActionResult IndexJson()
        {
            var blogPostListDto = new BlogPostListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                BlogPosts = this.dbContext.BlogPosts.OrderByDescending(x => x.PublicationDate).AsEnumerable().Select(x => new BlogPostDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    IsPublished = x.IsPublished,
                    Author = new UserDto
                    {
                        FirstName = x.Author?.FirstName,
                        LastName = x.Author?.LastName
                    },
                    Title = x.Title
                }).ToList()
            };

            return this.Ok(blogPostListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminPostsEdit)]
        public virtual IActionResult EditJson(Guid? postId)
        {
            var isNew = postId == null;

            var blogPost = isNew ? new BlogPost() : this.dbContext.BlogPosts.Find(postId).AsNotNull();

            var dto = new EditBlogPostDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                BlogPost = new BlogPostDto
                {
                    IsNew = isNew,
                    Id = blogPost.Id,
                    Title = blogPost.Title,
                    Url = blogPost.Url,
                    FeaturedImage = blogPost.FeaturedImage,
                    FeaturedImageVerticalCrop = blogPost.FeaturedImageVerticalCrop,
                    FeaturedImageHorizontalCrop = blogPost.FeaturedImageHorizontalCrop,
                    Content = blogPost.Content,
                    IsPublished = blogPost.IsPublished,
                    PublicationDate = blogPost.PublicationDate,
                    Author = blogPost.Author == null ? null : new UserDto(blogPost.Author.Id),
                    Excerpt = blogPost.Excerpt,
                    MetaTitle = blogPost.MetaTitle,
                    MetaDescription = blogPost.MetaDescription,
                    BlogCategories = blogPost.BlogCategories.Select(blogCategory => new BlogCategoryDto(blogCategory.Id)).ToList()
                },
                Users = this.dbContext.Users.Where(u => u.Role.AccessAdmin).OrderByDescending(x => x.CreatedDate).AsEnumerable().Select(x => new UserDto
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList(),
                AllBlogCategories = this.dbContext.BlogCategories.OrderByDescending(x => x.CreatedDate).AsEnumerable().Select(x => new BlogCategoryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                }).ToList(),
                PostPreviewLink = Path.Combine(this.configuration["AdminPostPreviewLink"], !string.IsNullOrEmpty(blogPost.Url) ? blogPost.Url : "")
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminPostsAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(BlogPostDto dto)
        {
            var errors = this.blogPostDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            dto.Url = dto.Url?.ToLower();

            if (this.dbContext.BlogPosts.Any(x => x.Url == dto.Url && x.Id != dto.Id))
            {
                errors.Add(new OperationError("Already existing Url"));

                return this.Ok(new OperationResult(errors));
            }

            var blogPost = this.adminPostsService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, blogPost.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminPostsDelete)]
        public virtual IActionResult Delete(Guid postId)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.BlogPosts.Delete(postId, true);
            }
            catch (Exception e)
            {
                operationResult.Errors.Add(new OperationError(e.Message));
            }

            return this.Ok(operationResult);
        }
    }
}