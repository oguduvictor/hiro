﻿using System;
using System.Linq;
using Hiro.Admin.Shared.Constants;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Services.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class NotificationController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAuthenticationService authenticationService;

        public NotificationController(
            HiroDbContext dbContext,
            IAuthenticationService authenticationService)
        {
            this.dbContext = dbContext;
            this.authenticationService = authenticationService;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminNotificationsIndex)]
        public virtual IActionResult IndexJson()
        {
            var userRole = this.authenticationService.GetAuthenticatedUser()?.Role ??
                           throw new NullReferenceException();

            var logs = this.dbContext.Logs.AsQueryable();

            if (!userRole.ViewExceptionLogs)
            {
                logs = logs.Where(x => x.Type != LogTypesEnum.Exception);
            }

            if (!userRole.ViewEcommerceLogs)
            {
                logs = logs.Where(x => x.Type != LogTypesEnum.Ecommerce);
            }

            if (!userRole.ViewContentLogs)
            {
                logs = logs.Where(x => x.Type != LogTypesEnum.Content);
            }

            var dto = logs
                .OrderByDescending(x => x.CreatedDate)
                .Take(100)
                .AsEnumerable()
                .Select(x => new LogDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    Message = x.Message.Shorten(80, true)
                }).ToList();

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminNotificationsDetails)]
        public virtual IActionResult DetailsJson(Guid notificationId)
        {
            var log = this.dbContext.Logs.Find(notificationId);

            var logDto = log == null ? null : new LogDto
            {
                Id = log.Id,
                CreatedDate = log.CreatedDate,
                Type = log.Type,
                Message = log.Message,
                Details = log.Details,
                User = log.User == null ? null : new UserDto
                {
                    Id = log.User.Id,
                    FirstName = log.User.FirstName,
                    LastName = log.User.LastName,
                    Email = log.User.Email
                }
            };

            return this.Ok(logDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminNotificationsDelete)]
        [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.Logs.Delete(id, true);
            }
            catch (Exception e)
            {
                operationResult.AddError(e.Message);
            }

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminNotificationsDeleteAll)]
        public virtual IActionResult DeleteAll()
        {
            var logs = this.dbContext.Logs.ToList();

            foreach (var log in logs)
            {
                this.dbContext.Logs.Delete(log.Id, false);
            }

            this.dbContext.SaveChanges();

            return this.Ok();
        }
    }
}