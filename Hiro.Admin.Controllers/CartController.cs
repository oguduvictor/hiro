﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Services.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Mediator.Requests;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class CartController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly ICartService cartService;
        private readonly ISendEmailConfirmationModalDtoValidator sendEmailConfirmationModalDtoValidator;
        private readonly IMediator mediator;

        public CartController(
            HiroDbContext dbContext,
            ISendEmailConfirmationModalDtoValidator sendEmailConfirmationModalDtoValidator,
            ICartService cartService,
            IMediator mediator)
        {
            this.dbContext = dbContext;
            this.sendEmailConfirmationModalDtoValidator = sendEmailConfirmationModalDtoValidator;
            this.cartService = cartService;
            this.mediator = mediator;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCartIndex)]
        public virtual IActionResult IndexJson(int page = 1, string keywordFilter = null, string countryNameFilter = null, string shippingBoxNameFilter = null)
        {
            const int pageSize = 20;

            var filteredCarts = this.dbContext.Carts.Where(c => c.User != null && c.CartShippingBoxes.Any());

            if (!string.IsNullOrEmpty(keywordFilter))
            {
                filteredCarts = filteredCarts.Where(cart =>
                    cart.User.Email.Contains(keywordFilter) ||
                    cart.User.FirstName.Contains(keywordFilter) ||
                    cart.User.LastName.Contains(keywordFilter));
            }

            if (!string.IsNullOrEmpty(countryNameFilter))
            {
                filteredCarts = filteredCarts.Where(cart => cart.User.Country.Name.Contains(countryNameFilter));
            }

            if (!string.IsNullOrEmpty(shippingBoxNameFilter))
            {
                filteredCarts = filteredCarts.Where(cart => cart.CartShippingBoxes.Any(cartShippingBox => cartShippingBox.ShippingBox.Name.Contains(shippingBoxNameFilter)));
            }

            var dto = new CartListDto
            {
                TotalItems = filteredCarts.Count(),
                PageSize = pageSize,
                Carts = filteredCarts
                    //.Select(x => new
                    //{
                    //    Cart = x,
                    //    ModifiedDate = x.CartShippingBoxes
                    //        .SelectMany(csb => csb.CartItems)
                    //        .Select(ci => ci.ModifiedDate)
                    //        .OrderByDescending(md => md)
                    //        .FirstOrDefault()
                    //})
                    .OrderByDescending(c => c.ModifiedDate)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .AsEnumerable()
                    //.Select(x => new CartDto
                    //{
                    //    Id = x.Cart.Id,
                    //    ModifiedDate = x.ModifiedDate,
                    //    TotalAfterTax = this.cartService.GetTotalAfterTax(x.Cart.User?.Country ?? this.localizationCountry, x.Cart),
                    //    User = x.Cart.User == null ? null : new UserDto
                    //    {
                    //        Email = x.Cart.User.Email ?? "-",
                    //        Country = x.Cart.User.Country == null ? null : new CountryDto
                    //        {
                    //            Name = x.Cart.User.Country.Name,
                    //            CurrencySymbol = x.Cart.User.Country.GetCurrencySymbol()
                    //        }
                    //    }
                    //}).ToList(),
                    .Select(x => new CartDto
                    {
                        Id = x.Id,
                        ModifiedDate = x.ModifiedDate,
                        TotalAfterTax = this.cartService.GetTotalAfterTax(x.User?.Country ?? this.localizationCountry, x),
                        User = x.User == null ? null : new UserDto
                        {
                            Email = x.User.Email ?? "-",
                            Country = x.User.Country == null ? null : new CountryDto
                            {
                                Name = x.User.Country.Name,
                                CurrencySymbol = x.User.Country.GetCurrencySymbol()
                            }
                        }
                    }).ToList(),
                ShippingBoxes = this.dbContext.ShippingBoxes.Select(shippingBox => new ShippingBoxDto
                {
                    Id = shippingBox.Id,
                    Name = shippingBox.Name,
                    LocalizedInternalDescription = shippingBox.ShippingBoxLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).InternalDescription
                }).ToList(),
                Countries = this.dbContext.Countries.OrderBy(x => x.Name).Select(country => new CountryDto
                {
                    Id = country.Id,
                    Name = country.Name
                }).ToList()
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCartEdit)]
        public virtual IActionResult EditJson(Guid cartId)
        {
            var cart = this.dbContext.Carts.Find(cartId);

            if (cart == null)
            {
                throw new NullReferenceException();
            }

            var cartLocalizationCountry = cart.ShippingAddress?.Country ?? cart.User?.Country;

            var editCartDto = new EditCartDto
            {
                Cart = new CartDto
                {
                    Id = cart.Id,
                    CreatedDate = cart.CreatedDate,
                    User = cart.User == null ? new UserDto() : new UserDto
                    {
                        Email = cart.User.Email,
                        FirstName = cart.User.FirstName,
                        Id = cart.User.Id,
                        LastName = cart.User.LastName,
                        Country = cart.User.Country == null ? null : new CountryDto
                        {
                            Id = cart.User.Country.Id,
                            Name = cart.User.Country.Name,
                            Url = cart.User.Country.Url,
                            LanguageCode = cart.User.Country.LanguageCode,
                            CurrencySymbol = cart.User.Country.GetCurrencySymbol()
                        }
                    },
                    CartShippingBoxes = cart.CartShippingBoxes.Select(cartShippingBox => new CartShippingBoxDto
                    {
                        Cart = cartShippingBox.Cart == null ? null : new CartDto(cartShippingBox.Cart.Id),
                        CartItems = cartShippingBox.CartItems.Select(cartItem => new CartItemDto
                        {
                            Id = cartItem.Id,
                            TotalAfterTax = this.cartService.GetCartItemTotalAfterTax(cartLocalizationCountry, cartItem),
                            CartItemVariants = cartItem.CartItemVariants.Select(cartItemVariant => new CartItemVariantDto
                            {
                                Id = cartItemVariant.Id,
                                BooleanValue = cartItemVariant.BooleanValue,
                                DoubleValue = cartItemVariant.DoubleValue,
                                IsDisabled = cartItemVariant.IsDisabled,
                                IntegerValue = cartItemVariant.IntegerValue,
                                JsonValue = cartItemVariant.JsonValue,
                                PriceModifier = cartItemVariant.GetPriceModifier(cartLocalizationCountry),
                                StringValue = cartItemVariant.StringValue,
                                CartItem = cartItemVariant.CartItem == null ? null : new CartItemDto(cartItemVariant.CartItem.Id),
                                Variant = cartItemVariant.Variant == null ? null : new VariantDto
                                {
                                    Id = cartItemVariant.Variant.Id,
                                    Name = cartItemVariant.Variant.Name,
                                    Type = cartItemVariant.Variant.Type,
                                    LocalizedTitle = cartItemVariant.Variant.GetLocalizedTitle(cartLocalizationCountry)
                                },
                                VariantOptionValue = cartItemVariant.VariantOptionValue == null ? null : new VariantOptionDto
                                {
                                    Id = cartItemVariant.VariantOptionValue.Id,
                                    Name = cartItemVariant.VariantOptionValue.Name
                                }
                            }).ToList(),
                            Product = cartItem.Product == null ? null : new ProductDto
                            {
                                Id = cartItem.Product.Id,
                                LocalizedTitle = cartItem.Product.GetLocalizedTitle(cartLocalizationCountry)
                            }
                        }).ToList(),
                        TotalAfterTax = this.cartService.GetCartShippingBoxTotalAfterTax(cartLocalizationCountry, cartShippingBox),
                        ShippingPriceAfterTax = this.cartService.GetShippingPriceAfterTax(cartLocalizationCountry, cartShippingBox),
                        ShippingBox = cartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                        {
                            Id = cartShippingBox.ShippingBox.Id,
                            Name = cartShippingBox.ShippingBox.Name
                        }
                    }).ToList(),
                    ShippingAddress = cart.ShippingAddress == null ? null : new AddressDto
                    {
                        Id = cart.ShippingAddress.Id,
                        AddressLine1 = cart.ShippingAddress.AddressLine1,
                        AddressLine2 = cart.ShippingAddress.AddressLine2,
                        City = cart.ShippingAddress.City,
                        FirstName = cart.ShippingAddress.FirstName,
                        LastName = cart.ShippingAddress.LastName,
                        IsDisabled = cart.ShippingAddress.IsDisabled,
                        PhoneNumber = cart.ShippingAddress.PhoneNumber,
                        Postcode = cart.ShippingAddress.Postcode,
                        StateCountyProvince = cart.ShippingAddress.StateCountyProvince,
                        Country = cart.ShippingAddress.Country == null ? null : new CountryDto
                        {
                            Id = cart.ShippingAddress.Country.Id,
                            Name = cart.ShippingAddress.Country.Name
                        },
                        User = cart.ShippingAddress.User == null ? null : new UserDto(cart.ShippingAddress.User.Id)
                    },
                    Coupon = cart.Coupon == null ? null : new CouponDto(cart.Coupon.Id),
                    CouponValue = this.cartService.GetCouponValue(cartLocalizationCountry, cart),
                    UserCredit = cart.UserCredit == null ? null : new UserCreditDto
                    {
                        Amount = cart.UserCredit.Amount
                    },
                    SentEmails = cart.SentEmails.Select(x => new SentEmailDto
                    {
                        Id = x.Id,
                        Email = x.Email == null ? null : new EmailDto
                        {
                            Id = x.Email.Id
                        }
                    }).ToList(),
                    TotalAfterTax = this.cartService.GetTotalAfterTax(cartLocalizationCountry, cart),
                    TotalToBePaid = this.cartService.GetTotalToBePaid(cartLocalizationCountry, cart)
                },
                AllEmails = this.dbContext.Emails.Where(x => !x.IsDisabled).OrderBy(x => x.Name).Select(x => new EmailDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    ViewName = x.ViewName
                }).ToList()
            };

            if (editCartDto.Cart?.ShippingAddress?.Country != null)
            {
                editCartDto.Cart.ShippingAddress.Country.Taxes = new List<TaxDto>();
            }

            return this.Ok(editCartDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCartSendEmail)]
        public virtual IActionResult SendEmail(SendEmailConfirmationModalDto dto)
        {
            var errors = this.sendEmailConfirmationModalDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var result = this.mediator.Send(new EmailRequest
            {
                EmailName = dto.EmailName,
                To = dto.SendTo.InList(),
                Data = this.dbContext.Carts.Find(dto.EntityId)
            });

            return this.Ok(result);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCartCreateOrder)]
        public IActionResult CreateOrder(Guid cartId)
        {
            var operationResult = new OperationResult();

            var cart = this.dbContext.Carts.Find(cartId);

            if (cart == null)
            {
                operationResult.Errors.Add(new OperationError("Impossible to create the Order: the Cart is null."));

                return this.Ok(operationResult);
            }

            var cartLocalizationCountry = cart.ShippingAddress?.Country ?? cart.User?.Country;

            this.cartService.CreateOrder(cartLocalizationCountry, cart);

            this.cartService.DeleteCart(cart.Id);

            return this.Ok(operationResult);
        }
    }
}