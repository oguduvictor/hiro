﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Services.Interfaces;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class UserController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IEncryptionService encryptionService;
        private readonly IUserDtoValidator userDtoValidator;
        private readonly IUserRoleDtoValidator userRoleDtoValidator;
        private readonly IAdminUserService adminUserService;

        public UserController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminUserService adminUserService,
            IEncryptionService encryptionService,
            IUserDtoValidator userDtoValidator,
            IUserRoleDtoValidator userRoleDtoValidator)
        {
            this.dbContext = dbContext;
            this.userDtoValidator = userDtoValidator;
            this.userRoleDtoValidator = userRoleDtoValidator;
            this.adminUserService = adminUserService;
            this.encryptionService = encryptionService;
            this.adminUtilityService = adminUtilityService;
        }

        // ------------------------------------------------------------
        // USERS
        // ------------------------------------------------------------

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserIndex)]
        public virtual IActionResult IndexJson(int page = 1, string keywordFilter = null)
        {
            const int pageSize = 100;

            var filteredUsers = this.dbContext.Users
                .Where(x => !string.IsNullOrEmpty(x.Email) &&
                string.IsNullOrEmpty(keywordFilter) ||
                            x.Email.Contains(keywordFilter) ||
                            x.FirstName.Contains(keywordFilter) ||
                            x.LastName.Contains(keywordFilter) ||
                            (x.Role != null && x.Role.Name.Contains(keywordFilter)))
                .OrderBy(x => x.CreatedDate);

            var dto = new UserListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                TotalItems = filteredUsers.Count(),
                PageSize = pageSize,
                Users = filteredUsers.Skip((page - 1) * pageSize).Take(pageSize).Select(x => new UserDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    Password = x.Password,
                    Country = x.Country == null ? null : new CountryDto { Name = x.Country.Name },
                    Role = x.Role == null ? null : new UserRoleDto { Name = x.Role.Name }
                }).ToList()
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserExport)]
        public FileContentResult Export()
        {
            var users = this.dbContext.Users.Where(x => x.Email != null && x.Email != string.Empty);

            var csv =
                "Id,Email,First name,Last name, Country name, Country url" +
                Environment.NewLine +
                users.ToCsv(x => x.Id.ToString(), x => x.Email, x => x.FirstName, x => x.LastName, x => x.Country?.Name, x => x.Country?.GetUrlForStringConcatenation());

            return this.File(new UTF8Encoding().GetBytes(csv), "text/csv", "Users.csv");
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserEdit)]
        public virtual IActionResult EditJson(Guid? userId)
        {
            var isNew = userId == null;

            var user = isNew ? new User() : this.dbContext.Users.Find(userId);

            if (user == null)
            {
                throw new NullReferenceException();
            }

            // ----------------------------------------------------
            // Add and Map Missing Localized Kits
            // ----------------------------------------------------

            user.UserLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(user.UserLocalizedKits, new UserLocalizedKit
            {
                User = user
            });

            var userRoles = this.dbContext.UserRoles.OrderByDescending(x => x.Name == UserRoleNames.Default).ToList();
            var countries = this.dbContext.Countries.OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name).ToList();
            var orders = this.dbContext.Orders.Where(x => x.UserId == user.Id).ToList();

            var userCountry = user.Country ?? countries.First();
            var userRole = user.Role ?? userRoles.First();

            var authenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto();

            var editUserDto = new EditUserDto
            {
                UserRoles = userRoles.Select(x => new UserRoleDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                Countries = countries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                Orders = orders.Select(x => new OrderDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    CurrencySymbol = x.CurrencySymbol,
                    CountryName = x.CountryName,
                    CountryLanguageCode = x.CountryLanguageCode,
                    OrderNumber = x.OrderNumber,
                    Status = x.Status,
                    StatusDescriptions = x.Status.GetType().GetDescriptions(),
                    TotalAfterTax = x.TotalAfterTax
                }).ToList(),
                User = new UserDto
                {
                    IsNew = isNew,
                    Id = user.Id,
                    CreatedDate = user.CreatedDate,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    ShowMembershipSalePrice = user.ShowMembershipSalePrice,
                    IsDisabled = user.IsDisabled,
                    Gender = user.Gender,
                    GenderDescriptions = user.Gender.GetType().GetDescriptions(),
                    Role = new UserRoleDto
                    {
                        Name = userRole.Name,
                        Id = userRole.Id
                    },
                    Country = new CountryDto
                    {
                        Name = userCountry.Name,
                        Id = userCountry.Id
                    },
                    Addresses = user.Addresses.Select(x => new AddressDto
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        AddressLine1 = x.AddressLine1,
                        AddressLine2 = x.AddressLine2,
                        City = x.City,
                        Country = x.Country == null ? new CountryDto() : new CountryDto
                        {
                            Id = x.Country.Id,
                            Name = x.Country.Name
                        },
                        StateCountyProvince = x.StateCountyProvince,
                        Postcode = x.Postcode,
                        PhoneNumber = x.PhoneNumber
                    }).ToList(),
                    UserLocalizedKits = user.UserLocalizedKits.Select(userLocalizedKit => new UserLocalizedKitDto
                    {
                        Id = userLocalizedKit.Id,
                        Country = userLocalizedKit.Country == null ? new CountryDto() : new CountryDto
                        {
                            Id = userLocalizedKit.Country.Id,
                            Name = userLocalizedKit.Country.Name,
                            IsDefault = userLocalizedKit.Country.IsDefault,
                            CurrencySymbol = userLocalizedKit.Country.GetCurrencySymbol()
                        },
                        User = userLocalizedKit.User == null ? new UserDto() : new UserDto(userLocalizedKit.User.Id),
                        UserCredits = userLocalizedKit.UserCredits.Select(userCredit => new UserCreditDto
                        {
                            Id = userCredit.Id,
                            Amount = userCredit.Amount,
                            Reference = userCredit.Reference,
                            UserLocalizedKit = userCredit.UserLocalizedKit == null ? new UserLocalizedKitDto() : new UserLocalizedKitDto(userCredit.UserLocalizedKit.Id)
                        }).ToList()
                    }).ToList()
                },
                AuthenticatedUser = authenticatedUser
            };

            return this.Ok(editUserDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(UserDto dto)
        {
            dto.Password = string.IsNullOrEmpty(dto.Password)
                ? this.encryptionService.Decrypt(this.dbContext.Users.Find(dto.Id)?.Password)
                : dto.Password;

            var errors = this.userDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var user = this.adminUserService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, user.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.Users.Delete(id, true);
            }
            catch (Exception ex)
            {
                operationResult.Errors.Add(new OperationError(ex.Message));
            }

            return this.Ok(operationResult);
        }

        // ------------------------------------------------------------
        // USER ROLES
        // ------------------------------------------------------------

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserRoles)]
        public virtual IActionResult RolesJson()
        {
            var dto = new UserRoleListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                UserRoles = this.dbContext.UserRoles.Select(userRole => new UserRoleDto
                {
                    Id = userRole.Id,
                    Name = userRole.Name,
                    IsDisabled = userRole.IsDisabled
                }).ToList()
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserRolesEdit)]
        public virtual IActionResult EditUserRoleJson(Guid? userRoleId)
        {
            var userRole = userRoleId == null ? new UserRole() : this.dbContext.UserRoles.Find(userRoleId);

            if (userRole == null)
            {
                throw new NullReferenceException();
            }

            var dto = new EditUserRoleDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                UserRole = new UserRoleDto
                {
                    Id = userRole.Id,
                    AccessAdmin = userRole.AccessAdmin,
                    AccessAdminBlog = userRole.AccessAdminBlog,
                    AccessAdminContent = userRole.AccessAdminContent,
                    AccessAdminCountries = userRole.AccessAdminCountries,
                    AccessAdminEcommerce = userRole.AccessAdminEcommerce,
                    AccessAdminEmails = userRole.AccessAdminEmails,
                    AccessAdminMedia = userRole.AccessAdminMedia,
                    AccessAdminOrders = userRole.AccessAdminOrders,
                    AccessAdminSeo = userRole.AccessAdminSeo,
                    AccessAdminSettings = userRole.AccessAdminSettings,
                    AccessAdminUsers = userRole.AccessAdminUsers,
                    CreateAttribute = userRole.CreateAttribute,
                    CreateAttributeOption = userRole.CreateAttributeOption,
                    CreateBlogPost = userRole.CreateBlogPost,
                    CreateContentSection = userRole.CreateContentSection,
                    CreateCountry = userRole.CreateCountry,
                    CreateCoupon = userRole.CreateCoupon,
                    CreateCategory = userRole.CreateCategory,
                    CreateEmail = userRole.CreateEmail,
                    CreateMailingList = userRole.CreateMailingList,
                    CreateProduct = userRole.CreateProduct,
                    CreateSeoSection = userRole.CreateSeoSection,
                    CreateShippingBox = userRole.CreateShippingBox,
                    CreateUser = userRole.CreateUser,
                    CreateUserRole = userRole.CreateUserRole,
                    CreateVariant = userRole.CreateVariant,
                    CreateVariantOption = userRole.CreateVariantOption,
                    DeleteAttribute = userRole.DeleteAttribute,
                    DeleteAttributeOption = userRole.DeleteAttributeOption,
                    DeleteBlogPost = userRole.DeleteBlogPost,
                    DeleteContentSection = userRole.DeleteContentSection,
                    DeleteCountry = userRole.DeleteCountry,
                    DeleteCoupon = userRole.DeleteCoupon,
                    DeleteCategory = userRole.DeleteCategory,
                    DeleteEmail = userRole.DeleteEmail,
                    DeleteMailingList = userRole.DeleteMailingList,
                    DeleteProduct = userRole.DeleteProduct,
                    DeleteSeoSection = userRole.DeleteSeoSection,
                    DeleteShippingBox = userRole.DeleteShippingBox,
                    DeleteUser = userRole.DeleteUser,
                    DeleteUserRole = userRole.DeleteUserRole,
                    DeleteVariant = userRole.DeleteVariant,
                    DeleteVariantOption = userRole.DeleteVariantOption,
                    EditAttribute = userRole.EditAttribute,
                    EditAttributeAllLocalizedKits = userRole.EditAttributeAllLocalizedKits,
                    EditAttributeName = userRole.EditAttributeName,
                    EditAttributeOption = userRole.EditAttributeOption,
                    EditAttributeOptionAllLocalizedKits = userRole.EditAttributeOptionAllLocalizedKits,
                    EditAttributeOptionName = userRole.EditAttributeOptionName,
                    EditBlogPost = userRole.EditBlogPost,
                    EditContentSection = userRole.EditContentSection,
                    EditContentSectionAllLocalizedKits = userRole.EditContentSectionAllLocalizedKits,
                    EditContentSectionName = userRole.EditContentSectionName,
                    EditCountry = userRole.EditCountry,
                    EditCoupon = userRole.EditCoupon,
                    EditCategory = userRole.EditCategory,
                    EditCategoryAllLocalizedKits = userRole.EditCategoryAllLocalizedKits,
                    EditCategoryName = userRole.EditCategoryName,
                    EditCategoryUrl = userRole.EditCategoryUrl,
                    EditEmail = userRole.EditEmail,
                    EditMailingList = userRole.EditMailingList,
                    EditMailingListName = userRole.EditMailingListName,
                    EditEmailAllLocalizedKits = userRole.EditEmailAllLocalizedKits,
                    EditMailingListAllLocalizedKits = userRole.EditMailingListAllLocalizedKits,
                    EditEmailName = userRole.EditEmailName,
                    EditOrder = userRole.EditOrder,
                    EditProduct = userRole.EditProduct,
                    EditProductAllLocalizedKits = userRole.EditProductAllLocalizedKits,
                    EditSeoSection = userRole.EditSeoSection,
                    EditSeoSectionAllLocalizedKits = userRole.EditSeoSectionAllLocalizedKits,
                    EditSeoSectionPage = userRole.EditSeoSectionPage,
                    EditShippingBox = userRole.EditShippingBox,
                    EditShippingBoxAllLocalizedKits = userRole.EditShippingBoxAllLocalizedKits,
                    EditShippingBoxName = userRole.EditShippingBoxName,
                    EditUser = userRole.EditUser,
                    EditUserRole = userRole.EditUserRole,
                    EditVariant = userRole.EditVariant,
                    EditVariantAllLocalizedKits = userRole.EditVariantAllLocalizedKits,
                    EditVariantName = userRole.EditVariantName,
                    EditVariantSelector = userRole.EditVariantSelector,
                    EditVariantOption = userRole.EditVariantOption,
                    EditVariantOptionAllLocalizedKits = userRole.EditVariantOptionAllLocalizedKits,
                    EditVariantOptionName = userRole.EditVariantOptionName,
                    IsDisabled = userRole.IsDisabled,
                    Name = userRole.Name,
                    ViewExceptionLogs = userRole.ViewExceptionLogs,
                    ViewEcommerceLogs = userRole.ViewEcommerceLogs,
                    ViewContentLogs = userRole.ViewContentLogs
                }
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserAddOrUpdateUserRole)]
        public virtual IActionResult AddOrUpdateUserRole(UserRoleDto dto)
        {
            var errors = this.userRoleDtoValidator.Validate(dto);

            var dbUserRole = this.dbContext.UserRoles.Find(dto.Id);

            if (dbUserRole == null)
            {
                var userRoleByName = this.dbContext.UserRoles.FirstOrDefault(x => x.Name == dto.Name);

                if (userRoleByName != null)
                {
                    errors.Add(new OperationError("Already existing user role name"));
                }
            }

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var userRole = this.adminUserService.AddOrUpdateUserRole(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, userRole.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserRolesDelete)]
        public virtual IActionResult DeleteUserRole(Guid userRoleId)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.UserRoles.Delete(userRoleId, true);
            }
            catch (Exception e)
            {
                operationResult.Errors.Add(new OperationError(e.Message));
            }

            return this.Ok(operationResult);
        }

        // -------------------------------------------------------------
        // User Credit
        // -------------------------------------------------------------

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserAddOrUpdateUserCredit)]
        public virtual IActionResult AddOrUpdateUserCredit(UserCreditDto dto)
        {
            var operationResult = new OperationResult();

            if (dto.Amount.ApproximatelyEqualsTo(0))
            {
                operationResult.Errors.Add(new OperationError("Amount", "Amount cannot be 0"));

                return this.Ok(operationResult);
            }

            this.adminUserService.AddOrUpdateUserCredit(this.dbContext, dto);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserAddOrUpdateUserCredits)]
        public virtual IActionResult AddOrUpdateUserCredits(List<UserCreditDto> userCredits)
        {
            var operationResult = new OperationResult();

            if (userCredits.Any(x => x.Amount.ApproximatelyEqualsTo(0)))
            {
                operationResult.Errors.Add(new OperationError("Amount", "Amount cannot be 0"));

                return this.Ok(operationResult);
            }

            this.adminUserService.AddOrUpdateUserCredits(this.dbContext, userCredits);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminUserDeleteUserCredit)]
        public virtual IActionResult DeleteUserCredit(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.UserCredits.Delete(id, true);
            }
            catch (Exception e)
            {
                operationResult.Errors.Add(new OperationError(e.Message));
            }

            return this.Ok(operationResult);
        }
    }
}