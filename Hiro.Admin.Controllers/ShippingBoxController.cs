﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class ShippingBoxController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminShippingBoxService adminShippingBoxService;
        private readonly IShippingBoxDtoValidator shippingBoxDtoValidator;

        public ShippingBoxController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminShippingBoxService adminShippingBoxService,
            IShippingBoxDtoValidator shippingBoxDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.adminShippingBoxService = adminShippingBoxService;
            this.shippingBoxDtoValidator = shippingBoxDtoValidator;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxIndex)]
        public virtual IActionResult IndexJson()
        {
            var shippingBoxes = this.dbContext.ShippingBoxes.OrderBy(x => x.SortOrder).AsEnumerable().Select(shippingBox => new ShippingBoxDto()
            {
                Name = shippingBox.Name,
                Id = shippingBox.Id,
                SortOrder = shippingBox.SortOrder,
                LocalizedDescription = shippingBox.GetLocalizedDescription(this.localizationCountry),
                LocalizedTitle = shippingBox.GetLocalizedTitle(this.localizationCountry),
            }).OrderBy(a => a.SortOrder).ToList();

            var shippingBoxListDto = new ShippingBoxListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                ShippingBoxes = shippingBoxes
            };

            return this.Ok(shippingBoxListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxEdit)]
        public virtual IActionResult EditJson(Guid? shippingBoxId)
        {
            var isNew = shippingBoxId == null;

            var shippingBox = isNew ? new ShippingBox() : this.dbContext.ShippingBoxes.Find(shippingBoxId);

            if (shippingBox == null)
            {
                throw new NullReferenceException();
            }

            // ---------------------------------------------------------------------
            // Add missing ShippingBoxLocalizedKits
            // ---------------------------------------------------------------------

            shippingBox.ShippingBoxLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(shippingBox.ShippingBoxLocalizedKits, new ShippingBoxLocalizedKit
            {
                ShippingBox = shippingBox
            });

            // ---------------------------------------------------------------------
            // Model
            // ---------------------------------------------------------------------

            var shippingBoxDto = new ShippingBoxDto
            {
                IsNew = isNew,
                Id = shippingBox.Id,
                Name = shippingBox.Name,
                RequiresShippingAddress = shippingBox.RequiresShippingAddress,
                SortOrder = shippingBox.SortOrder,
                IsDisabled = shippingBox.IsDisabled,
                LocalizedPrice = shippingBox.GetLocalizedPrice(this.localizationCountry),
                LocalizedTitle = shippingBox.GetLocalizedTitle(this.localizationCountry),
                LocalizedDescription = shippingBox.GetLocalizedDescription(this.localizationCountry),
                LocalizedInternalDescription = shippingBox.GetLocalizedInternalDescription(this.localizationCountry),
                LocalizedMinDays = shippingBox.GetLocalizedMinDays(this.localizationCountry),
                LocalizedMaxDays = shippingBox.GetLocalizedMaxDays(this.localizationCountry),
                LocalizedCountWeekends = shippingBox.GetLocalizedCountWeekends(this.localizationCountry),
                LocalizedFreeShippingMinimumPrice = shippingBox.GetLocalizedFreeShippingMinimumPrice(this.localizationCountry),
                Countries = shippingBox.Countries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderBy(x => x.Name).ToList(),
                Products = shippingBox.Products.OrderBy(x => x.Name).Select(x => new ProductDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                }).ToList(),
                ShippingBoxLocalizedKits = shippingBox.ShippingBoxLocalizedKits.OrderByDefaultCountry().Select(localizedKit => new ShippingBoxLocalizedKitDto
                {
                    Id = localizedKit.Id,
                    Title = localizedKit.Title,
                    Description = localizedKit.Description,
                    InternalDescription = localizedKit.InternalDescription,
                    Carrier = localizedKit.Carrier,
                    Price = localizedKit.Price,
                    FreeShippingMinimumPrice = localizedKit.FreeShippingMinimumPrice,
                    MinDays = localizedKit.MinDays,
                    MaxDays = localizedKit.MaxDays,
                    CountWeekends = localizedKit.CountWeekends,
                    Country = localizedKit.Country == null ? null : new CountryDto
                    {
                        Id = localizedKit.Country.Id,
                        Name = localizedKit.Country.Name,
                        CurrencySymbol = localizedKit.Country.GetCurrencySymbol(),
                        IsDefault = localizedKit.Country.IsDefault
                    },
                    ShippingBox = localizedKit.ShippingBox == null ? null : new ShippingBoxDto(localizedKit.ShippingBox.Id)
                }).ToList()
            };

            var editShippingBoxDto = new EditShippingBoxDto
            {
                ShippingBox = shippingBoxDto,
                AllCountries = this.dbContext.Countries.OrderBy(x => x.Name).Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto()
            };

            return this.Ok(editShippingBoxDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxDuplicate)]
        public virtual IActionResult Duplicate(Guid id)
        {
            var shippingBox = this.dbContext.ShippingBoxes.Find(id);

            if (shippingBox == null)
            {
                throw new NullReferenceException();
            }

            var duplicate = this.adminShippingBoxService.Duplicate(this.dbContext, shippingBox);

            return this.Ok(new OperationResult<Guid>(new List<OperationError>(), duplicate.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxGetAllProducts)]
        public virtual IActionResult GetAllProductsJson()
        {
            var productsDto = this.dbContext.Products.AsEnumerable().Select(x => new ProductDto
            {
                Id = x.Id,
                LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
            }).OrderBy(x => x.LocalizedTitle).ToList();

            return this.Ok(productsDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(ShippingBoxDto dto)
        {
            var errors = this.shippingBoxDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var shippingBox = this.adminShippingBoxService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, shippingBox.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxDelete)]
        public virtual IActionResult Delete(Guid shippingBoxId)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.ShippingBoxes.Delete(shippingBoxId, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminShippingBoxUpdateSortOrder)]
        public virtual IActionResult UpdateSortOrder(List<ShippingBoxDto> shippingBoxDtos)
        {
            var operationResult = new OperationResult<List<ShippingBoxDto>>();

            this.adminShippingBoxService.UpdateSortOrder(this.dbContext, shippingBoxDtos);

            operationResult.Data = this.dbContext.ShippingBoxes.OrderBy(x => x.SortOrder).AsEnumerable().Select(shippingBox => new ShippingBoxDto
            {
                Id = shippingBox.Id,
                Name = shippingBox.Name,
                SortOrder = shippingBox.SortOrder,
                LocalizedDescription = shippingBox.GetLocalizedDescription(this.localizationCountry),
                LocalizedTitle = shippingBox.GetLocalizedTitle(this.localizationCountry),
            }).ToList();

            return this.Ok(operationResult);
        }
    }
}