﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Domain.Extensions;
using Hiro.Services.Email.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Mediator.Requests;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class EmailController : Controller
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IContentSectionDtoValidator contentSectionDtoValidator;
        private readonly IEmailDtoValidator emailDtoValidator;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminEmailService adminEmailService;
        private readonly IHostingEnvironment webHostEnvironment;
        private readonly IMediator mediator;
        private readonly IEmailService emailService;

        public EmailController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminEmailService adminEmailService,
            IContentSectionDtoValidator contentSectionDtoValidator,
            IEmailDtoValidator emailDtoValidator,
            IHostingEnvironment webHostEnvironment,
            IMediator mediator,
            IEmailService emailService)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.adminEmailService = adminEmailService;
            this.contentSectionDtoValidator = contentSectionDtoValidator;
            this.emailDtoValidator = emailDtoValidator;
            this.webHostEnvironment = webHostEnvironment;
            this.mediator = mediator;
            this.emailService = emailService;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailIndex)]
        public virtual IActionResult IndexJson()
        {
            var emailListDto = new EmailListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Emails = this.dbContext.Emails.OrderBy(x => x.Name).Select(email => new EmailDto
                {
                    Id = email.Id,
                    Name = email.Name,
                    ViewName = email.ViewName
                }).ToList()
            };

            return this.Ok(emailListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailEdit)]
        public virtual IActionResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var email = isNew ? new Email() : this.dbContext.Emails.Find(id);

            if (email == null)
            {
                throw new NullReferenceException();
            }

            email.EmailLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(email.EmailLocalizedKits, new EmailLocalizedKit
            {
                Email = email
            });

            var editEmailDto = new EditEmailDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Email = new EmailDto
                {
                    IsNew = isNew,
                    Id = email.Id,
                    Name = email.Name,
                    ViewName = email.ViewName,
                    ContentSection = email.ContentSection == null ? null : new ContentSectionDto
                    {
                        Id = email.ContentSection.Id,
                        Name = email.ContentSection.Name
                    },
                    LocalizedFrom = email.GetLocalizedFrom(this.localizationCountry),
                    LocalizedReplyTo = email.GetLocalizedReplyTo(this.localizationCountry),
                    LocalizedDisplayName = email.GetLocalizedDisplayName(this.localizationCountry),
                    EmailLocalizedKits = email.EmailLocalizedKits.Select(emailLocalizedKit => new EmailLocalizedKitDto
                    {
                        Id = emailLocalizedKit.Id,
                        From = emailLocalizedKit.From,
                        ReplyTo = emailLocalizedKit.ReplyTo,
                        DisplayName = emailLocalizedKit.DisplayName,
                        IsDisabled = emailLocalizedKit.IsDisabled,
                        Country = emailLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = emailLocalizedKit.Country.Id,
                            Name = emailLocalizedKit.Country.Name,
                            IsDefault = emailLocalizedKit.Country.IsDefault,
                            LanguageCode = emailLocalizedKit.Country.LanguageCode,
                            Url = emailLocalizedKit.Country.Url,
                            Localize = emailLocalizedKit.Country.Localize
                        }
                    }).ToList()
                },
                AllContentSections = this.dbContext.ContentSections.Select(x => new ContentSectionDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderBy(x => x.Name).ToList()
            };

            var emailViewsDirectory = new DirectoryInfo(this.webHostEnvironment.WebRootPath + "/Views/Emails");

            if (emailViewsDirectory.Exists)
            {
                editEmailDto.ViewNames = emailViewsDirectory
                    .GetFiles()
                    .Where(x => !x.Name.StartsWith("_"))
                    .Select(x => x.Name?.Substring(0, x.Name.IndexOf(x.Extension, StringComparison.Ordinal)))
                    .ToList();
            }

            return this.Ok(editEmailDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailGetAllMailingLists)]
        public virtual IActionResult GetAllMailingListsJson()
        {
            var mailingListsDto = this.dbContext.MailingLists.Select(mailingList => new MailingListDto
            {
                Id = mailingList.Id,
                LocalizedTitle = mailingList.MailingListLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).Title,
                Name = mailingList.Name
            }).OrderBy(x => x.Name).ToList();

            return this.Ok(mailingListsDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailGetAllUsers)]
        public virtual IActionResult GetAllUsersJson()
        {
            var usersDto = this.dbContext.Users.Where(x => !x.IsDisabled && x.Email != null).Select(user => new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            }).ToList();

            return this.Ok(usersDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailGetAllCountries)]
        public virtual IActionResult GetAllCountriesJson()
        {
            var countriesDto = this.dbContext.Countries.Where(x => !x.IsDisabled && x.Localize).Select(x => new CountryDto
            {
                Id = x.Id,
                Name = x.Name,
                IsDefault = x.IsDefault
            }).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name).ToList();

            return this.Ok(countriesDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(EmailDto dto)
        {
            var errors = this.emailDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var email = this.adminEmailService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, email.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.Emails.Delete(id, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }


        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailViewInBrowser)]
        public virtual ActionResult ViewInBrowser(Guid id)
        {
            var email = this.dbContext.Emails.Find(id);

            if (email == null)
            {
                throw new NullReferenceException();
            }

            object content = email.ContentSection?.GetLocalizedContent(this.localizationCountry);

            return this.View("/Views/Emails/" + email.ViewName + ".cshtml", content);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailRenderEmailView)]
        public virtual async Task<string> RenderEmailViewAsync(Guid id)
        {
            var email = this.dbContext.Emails.Find(id);

            if (email == null)
            {
                throw new NullReferenceException();
            }

            object content = email.ContentSection?.GetLocalizedContent(this.localizationCountry);

            return await this.RenderViewAsync(email.ViewName, this.localizationCountry.LanguageCode, content);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailSendEmailToUsers)]
        public virtual IActionResult SendEmailToUsers(Guid emailId, List<string> to)
        {
            var operationResult = new OperationResult();

            var email = this.dbContext.Emails.Find(emailId);

            if (email == null)
            {
                operationResult.Errors.Add(new OperationError("An error occurred: email not found"));

                return this.Ok(operationResult);
            }

            var result = this.mediator.Send(new EmailRequest
            {
                EmailName = email.Name,
                To = to
            });

            return this.Ok(result);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailSendEmailToMailingList)]
        public virtual async Task<IActionResult> SendEmailToMailingListAsync(Guid emailId, Guid mailingListId, Guid? countryId)
        {
            var operationResult = new OperationResult();

            var email = this.dbContext.Emails.Find(emailId);

            if (email == null)
            {
                operationResult.Errors.Add(new OperationError("An error occurred: email not found"));

                return this.Ok(operationResult);
            }

            var subscriptionsQuery = this.dbContext.MailingListSubscriptions.Where(x =>
                x.MailingListId == mailingListId &&
                x.Status == MailingListSubscriptionStatusEnum.Subscribed);

            if (countryId.HasValue)
            {
                subscriptionsQuery = subscriptionsQuery.Where(x => x.User.Country.Id == countryId);
            }

            var result = await this.mediator.Send(new EmailRequest
            {
                EmailName = email.Name,
                To = subscriptionsQuery.Select(x => x.User.Email)
            });

            return this.Ok(result);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminEmailAddContentSection)]
        public virtual IActionResult AddContentSection(ContentSectionDto contentSectionDto, bool checkForSimilarContent)
        {
            var defaultCountry = this.dbContext.Countries.First(X => X.IsDefault);

            contentSectionDto.ContentSectionLocalizedKits[0].Country = new CountryDto(defaultCountry.Id);

            var errors = this.contentSectionDtoValidator.Validate(contentSectionDto);

            var createContentSectionDto = new CreateContentSectionDto();

            if (errors.Any())
            {
                createContentSectionDto.OperationResult = new OperationResult(errors);

                return this.Ok(createContentSectionDto);
            }

            var similarContentSectionDto = checkForSimilarContent ? this.SimilarityMatch(contentSectionDto) : null;

            if (similarContentSectionDto != null)
            {
                createContentSectionDto.SimilarContentSection = similarContentSectionDto;

                return this.Ok(createContentSectionDto);
            }

            var newContentSection = this.adminEmailService.AddContentSection(this.dbContext, contentSectionDto);

            createContentSectionDto.SavedContentSection = new ContentSectionDto
            {
                Id = newContentSection.Id,
                Name = newContentSection.Name
            };

            createContentSectionDto.OperationResult = new OperationResult(errors);

            return this.Ok(createContentSectionDto);
        }

        // -------------------------------------------------------------------------------------
        // PRIVATE METHODS
        // -------------------------------------------------------------------------------------

        private ContentSectionDto SimilarityMatch(ContentSectionDto contentSectionDto)
        {
            var defaultLocalizedContent = contentSectionDto.ContentSectionLocalizedKits.First();

            var allDefaultLocalizedContents = this.dbContext.ContentSectionLocalizedKits.Where(x => x.Country.IsDefault).OrderBy(x => x.Country.LanguageCode == Thread.CurrentThread.CurrentCulture.Name).ToList();

            var defaultLocalizedKit = allDefaultLocalizedContents.FirstOrDefault(x => x.Content != null && x.Content.AproximatelyEqualsTo(defaultLocalizedContent.Content, 3));

            var similarContentSection = defaultLocalizedKit?.ContentSection;

            similarContentSection?.ContentSectionLocalizedKits.Clear();
            similarContentSection?.ContentSectionLocalizedKits.Add(defaultLocalizedKit);

            return similarContentSection == null ? null : new ContentSectionDto
            {
                Id = similarContentSection.Id,
                Name = similarContentSection.Name,
                IsDisabled = similarContentSection.IsDisabled,
                ContentSectionLocalizedKits = similarContentSection.ContentSectionLocalizedKits.Select(contentSectionLocalizedKit => new ContentSectionLocalizedKitDto
                {
                    Id = contentSectionLocalizedKit.Id,
                    Country = contentSectionLocalizedKit.Country == null ? null : new CountryDto(contentSectionLocalizedKit.Country.Id),
                    Content = contentSectionLocalizedKit.Content
                }).ToList()
            };
        }
    }
}