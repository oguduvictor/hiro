﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.DataAccess.Extensions.Enums;
using Hiro.Domain.Dtos;
using Hiro.Services.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 4014

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class MediaController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IMediaService mediaService;
        private readonly IDirectoryDtoValidator directoryDtoValidator;
        private readonly IBatchRenameFilesDtoValidator batchRenameFilesDtoValidator;
        private readonly IRenameFileDtoValidator renameFileDtoValidator;
        private readonly IResizeFileDtoValidator resizeFileDtoValidator;
        private readonly IBitmapService bitmapService;

        public MediaController(
            HiroDbContext dbContext,
            IMediaService mediaService,
            IDirectoryDtoValidator directoryDtoValidator,
            IBatchRenameFilesDtoValidator batchRenameFilesDtoValidator,
            IRenameFileDtoValidator renameFileDtoValidator,
            IResizeFileDtoValidator resizeFileDtoValidator,
            IBitmapService bitmapService)
        {
            this.dbContext = dbContext;
            this.mediaService = mediaService;
            this.directoryDtoValidator = directoryDtoValidator;
            this.batchRenameFilesDtoValidator = batchRenameFilesDtoValidator;
            this.renameFileDtoValidator = renameFileDtoValidator;
            this.resizeFileDtoValidator = resizeFileDtoValidator;
            this.bitmapService = bitmapService;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMediaIndexJson)]
        public virtual IActionResult IndexJson(string localPath)
        {
            var mediaDirectory = this.mediaService.GetDirectory(localPath);

            return this.Ok(mediaDirectory);
        }

        /// <summary>
        /// This is "simplified" version of what the Imagik Controller can do.
        /// However, since Imagik is an optional component, and the admin panel
        /// only needs a subset of its functionalities, it makes sense to keep this method
        /// separate instead of using Imagik.
        /// </summary>
        /// <param name="sourceUrl"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        [Route(HiroAdminApiRoutes.AdminMediaIndexJson)]
        public async Task<IActionResult> GetCompressedJpg(string sourceUrl, int width)
        {
            if (string.IsNullOrEmpty(sourceUrl))
            {
                return this.BadRequest();
            }

            if (width < 10 || width > 200)
            {
                return this.BadRequest();
            }

            MemoryStream sourceFileMemoryStream;

            try
            {
                var bytes = await new WebClient().DownloadDataTaskAsync(sourceUrl);

                sourceFileMemoryStream = new MemoryStream(bytes);
            }
            catch (Exception)
            {
                return this.NotFound();
            }

            var compressedFileMemoryStream = this.bitmapService.ResizeImage(sourceFileMemoryStream, width, 80);

            return this.File(compressedFileMemoryStream, "image/jpeg");
        }

        // TODO
        //[HttpPost]
        //public virtual IActionResult UploadFileForm()
        //{
        //    // ---------------------------------------------------------------
        //    // Formal validation
        //    // ---------------------------------------------------------------

        //    var formResponse = new OperationResult();

        //    var uploadedFiles = this.Request.Files;

        //    if (uploadedFiles.Count > 100)
        //    {
        //        formResponse.AddError("The maximum number of files that is possible to upload at the same time is 100");

        //        return this.Ok(formResponse);
        //    }

        //    // ---------------------------------------------------------------
        //    // Upload files and collect exceptions as validation errors
        //    // ---------------------------------------------------------------

        //    var directoryFullName = this.Request.Form[0];

        //    for (var i = 0; i < uploadedFiles.Count; i++)
        //    {
        //        var file = uploadedFiles[i];

        //        if (file == null || file.ContentLength <= 0)
        //        {
        //            continue;
        //        }

        //        var stream = file.InputStream;
        //        var fileName = file.FileName.ToPrettyImageUrl();

        //        if (!SharedConstants.ValidMediaExtensions.Any(item => fileName.EndsWith(item.ToString(), StringComparison.OrdinalIgnoreCase)))
        //        {
        //            formResponse.AddError($"The file '{fileName}' could not be uploaded. This file type is not supported.");

        //            continue;
        //        }

        //        var errors = this.mediaService.UploadFile(directoryFullName, stream, fileName).Errors;

        //        formResponse.Errors.AddRange(errors);
        //    }

        //    return this.Ok(formResponse);
        //}

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMediaNewDirectoryForm)]
        public virtual IActionResult NewDirectoryForm(MediaDirectory model)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var validationErrors = this.directoryDtoValidator.Validate(model);

            if (validationErrors.Any())
            {
                return this.Ok(new OperationResult(validationErrors));
            }

            // ---------------------------------------------------------------
            // Create new directory and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var operationResult = new OperationResult();

            model.Name = model.Name.ToPrettyUrl();

            List<OperationError> errors;

            errors = this.mediaService.CreateDirectory(model.LocalPath, model.Name).Errors;

            operationResult.Errors.AddRange(errors);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMediaDeleteFilesForm)]
        public virtual IActionResult DeleteFilesForm(List<string> localPaths)
        {
            // ---------------------------------------------------------------
            // Delete files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var formResponse = new OperationResult();

            foreach (var url in localPaths)
            {
                var errors = this.mediaService.DeleteFile(url).Errors;

                formResponse.Errors.AddRange(errors);
            }

            return this.Ok(formResponse);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMediaDeleteDirectoryForm)]
        public virtual IActionResult DeleteDirectoryForm(string localPath)
        {
            var formResponse = this.mediaService.DeleteDirectory(localPath);

            return this.Ok(formResponse);
        }

        // TODO
        //[HttpPost]
        //public virtual IActionResult ResizeFilesForm(ResizeFileDto dto) // don't use this dto, just list flat parameters.
        //{
        //    // ---------------------------------------------------------------
        //    // Formal validation
        //    // ---------------------------------------------------------------

        //    var validationResult = this.resizeFileDtoValidator.Validate(dto);

        //    if (!validationResult.IsValid)
        //    {
        //        return this.utilityService.GetOperationResultJson(validationResult);
        //    }

        //    // ---------------------------------------------------------------
        //    // Resize files and collect exceptions as validation errors
        //    // ---------------------------------------------------------------

        //    var formResponse = new OperationResult();

        //    foreach (var url in dto.LocalPaths)
        //    {
        //        var errors = this.mediaService.ResizeFile(url, dto).Errors;

        //        formResponse.Errors.AddRange(errors);
        //    }

        //    return this.Ok(formResponse);
        //}

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMediaRenameFileForm)]
        public virtual IActionResult RenameFileForm(RenameFileDto dto)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var errors = this.renameFileDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            // ---------------------------------------------------------------
            // Rename file and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var operationResult = this.RenameFile(dto.LocalPath, dto.NewName);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMediaBatchRenameFiles)]
        public virtual IActionResult BatchRenameFiles(BatchRenameFilesDto dto)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var validationErrors = this.batchRenameFilesDtoValidator.Validate(dto);

            if (validationErrors.Any())
            {
                return this.Ok(new OperationResult(validationErrors));
            }

            // ---------------------------------------------------------------
            // Batch Rename files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var operationResult = new OperationResult();

            var files = dto.LocalPaths.Select(localPath => this.mediaService.GetFile(localPath)).ToList();

            foreach (var fileDto in files)
            {
                var newName = this.mediaService.GetFile(fileDto.LocalPath)?.Name;

                if (newName == null)
                {
                    operationResult.Errors.Add(new OperationError($"Name is null for file: '{fileDto.LocalPath}'"));

                    continue;
                }

                if (!string.IsNullOrEmpty(dto.ReplaceText))
                {
                    newName = newName.Replace(dto.ReplaceText, dto.ReplaceTextTarget);
                }

                if (!string.IsNullOrEmpty(dto.PrefixText))
                {
                    newName = dto.PrefixText + newName;
                }

                if (!string.IsNullOrEmpty(dto.SuffixText))
                {
                    newName = newName + dto.SuffixText;
                }

                var errors = this.RenameFile(fileDto.LocalPath, newName).Errors;

                operationResult.Errors.AddRange(errors);
            }

            return this.Ok(operationResult);
        }

        private OperationResult RenameFile(string localPath, string newName)
        {
            var formResponse = new OperationResult();

            var oldname = localPath.Split('/').Last();

            newName = newName.ToPrettyImageUrl();

            var extension = Path.GetExtension(localPath);

            if (newName + extension == oldname)
            {
                formResponse.Errors.Add(new OperationError($"The new file name cannot be the same with it's previous name: '{localPath}'"));

                return formResponse;
            }

            string newLocalPath;
            string newAbsolutePath;

            newLocalPath = this.mediaService.GetDirectory(localPath) + newName + extension; // TODO: check this

            formResponse = this.mediaService.RenameFile(localPath, newName);

            newAbsolutePath = this.mediaService.GetFile(newLocalPath)?.AbsolutePath;

            // ---------------------------------------------------------------------------------
            // Update the entities that were referencing the image
            // ---------------------------------------------------------------------------------

            this.dbContext.ProductImages.BulkUpdate("Url", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.dbContext.ProductAttributes.BulkUpdate("ImageValue", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.dbContext.VariantOptions.BulkUpdate("Image", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.dbContext.CategoryLocalizedKits.BulkUpdate("Image", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.dbContext.BlogPosts.BulkUpdate("FeaturedImage", localPath, newAbsolutePath, PreviousValueMatch.Contains);

            return formResponse;
        }
    }
}