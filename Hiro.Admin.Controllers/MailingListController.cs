﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class MailingListController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminMailingListService adminMailingListService;
        private readonly IMailingListDtoValidator mailingListDtoValidator;

        public MailingListController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminMailingListService adminMailingListService,
            IMailingListDtoValidator mailingListDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.adminMailingListService = adminMailingListService;
            this.mailingListDtoValidator = mailingListDtoValidator;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListIndex)]
        public virtual IActionResult IndexJson()
        {
            var mailingListDto = this.dbContext.MailingLists.Select(x => new MailingListDto
            {
                Id = x.Id,
                Name = x.Name,
                SortOrder = x.SortOrder,
                TotalUsers = x.MailingListSubscriptions.Count(),
                TotalSubscribers = x.MailingListSubscriptions.Count(y => y.Status == MailingListSubscriptionStatusEnum.Subscribed)
            }).OrderBy(x => x.SortOrder).ToList();

            return this.Ok(mailingListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListEdit)]
        public virtual IActionResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var mailingList = isNew ? new MailingList() : this.dbContext.MailingLists.Find(id);

            if (mailingList == null)
            {
                throw new NullReferenceException();
            }

            mailingList.MailingListLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(mailingList.MailingListLocalizedKits, new MailingListLocalizedKit
            {
                MailingList = mailingList
            });

            var editMailingListDto = new EditMailingListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                MailingList = new MailingListDto
                {
                    IsNew = isNew,
                    Id = mailingList.Id,
                    IsDisabled = mailingList.IsDisabled,
                    LocalizedDescription = mailingList.GetLocalizedDescription(this.localizationCountry),
                    LocalizedTitle = mailingList.GetLocalizedTitle(this.localizationCountry),
                    Name = mailingList.Name,
                    SortOrder = mailingList.SortOrder,
                    MailingListLocalizedKits = mailingList.MailingListLocalizedKits.Select(mailingListLocalizedKit => new MailingListLocalizedKitDto
                    {
                        Id = mailingListLocalizedKit.Id,
                        Title = mailingListLocalizedKit.Title,
                        Description = mailingListLocalizedKit.Description,
                        IsDisabled = mailingListLocalizedKit.IsDisabled,
                        MailingList = new MailingListDto
                        {
                            Id = mailingList.Id
                        },
                        Country = mailingListLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = mailingListLocalizedKit.Country.Id,
                            Name = mailingListLocalizedKit.Country.Name,
                            IsDefault = mailingListLocalizedKit.Country.IsDefault,
                            LanguageCode = mailingListLocalizedKit.Country.LanguageCode,
                            Url = mailingListLocalizedKit.Country.Url,
                            Localize = mailingListLocalizedKit.Country.Localize
                        }
                    }).ToList(),
                    MailingListSubscriptions = mailingList.MailingListSubscriptions.Select(subscription => new MailingListSubscriptionDto
                    {
                        MailingList = new MailingListDto(subscription.MailingList.Id),
                        User = new UserDto
                        {
                            Id = subscription.User.Id,
                            FirstName = subscription.User.FirstName,
                            LastName = subscription.User.LastName,
                            Email = subscription.User.Email
                        },
                        Status = subscription.Status,
                        StatusDescriptions = subscription.Status.GetType().GetDescriptions()
                    }).ToList()
                }
            };

            return this.Ok(editMailingListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(MailingListDto dto)
        {
            var errors = this.mailingListDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var mailingList = this.adminMailingListService.AddOrUpdateMailingList(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, mailingList.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListDuplicate)]
        public virtual IActionResult Duplicate(Guid id)
        {
            var mailingList = this.dbContext.MailingLists.Find(id);

            if (mailingList == null)
            {
                throw new NullReferenceException();
            }

            var newMailingList = this.adminMailingListService.Duplicate(this.dbContext, mailingList);

            var operationResult = new OperationResult<Guid>
            {
                Data = newMailingList.Id
            };

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListUpdateSortOrder)]
        public virtual IActionResult UpdateSortOrder(List<MailingListDto> mailingListDtos)
        {
            var operationResult = new OperationResult<List<MailingListDto>>();

            this.adminMailingListService.UpdateSortOrder(this.dbContext, mailingListDtos);

            operationResult.Data = this.dbContext.MailingLists.OrderBy(x => x.SortOrder).Select(x => new MailingListDto
            {
                Id = x.Id,
                Name = x.Name,
                SortOrder = x.SortOrder,
                TotalUsers = x.MailingListSubscriptions.Count(),
                TotalSubscribers = x.MailingListSubscriptions.Count(y => y.Status == MailingListSubscriptionStatusEnum.Subscribed)
            }).ToList();

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.MailingLists.Delete(id, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminMailingListExport)]
        public FileContentResult Export(Guid id)
        {
            var mailingList = this.dbContext.MailingLists.Find(id);

            if (mailingList == null)
            {
                return null;
            }

            var csv =
                "Id,Email,First name,Last name, Country name, Country url, Subscription status" +
                Environment.NewLine +
                mailingList.MailingListSubscriptions
                .ToCsv(
                    x => x.User?.Id.ToString(),
                    x => x.User?.Email,
                    x => x.User?.FirstName,
                    x => x.User?.LastName,
                    x => x.User?.Country?.Name,
                    x => x.User?.Country?.GetUrlForStringConcatenation(),
                    x => x.Status.GetDescription());

            return this.File(new UTF8Encoding().GetBytes(csv), "text/csv", $"{mailingList.Name}.csv");
        }

        //[HttpPost]
        //public virtual IActionResult ImportEmails()
        //{
        //    // ---------------------------------------------------------------
        //    // Formal validation
        //    // ---------------------------------------------------------------

        //    var operationResult = new FormResponse();

        //    var uploadedFiles = this.Request.Files;

        //    if (uploadedFiles.Count > 1)
        //    {
        //        operationResult.AddError("The maximum number of files that is possible to upload at the same time is 1");

        //        return this.Ok(operationResult);
        //    }

        //    var mailingListId = Guid.Parse(this.Request.Form[0]);

        //    var mailingList = this.dbContext.MailingLists.Find(mailingListId);

        //    if (mailingList.MailingListSubscriptions.Count > 0)
        //    {
        //        operationResult.AddError("Unable to upload users as this mailing list already have users");

        //        return this.Ok(operationResult);
        //    }

        //    // ---------------------------------------------------------------
        //    // Upload files and collect exceptions as validation errors
        //    // ---------------------------------------------------------------

        //    for (var i = 0; i < uploadedFiles.Count; i++)
        //    {
        //        var file = uploadedFiles[i];

        //        if (!file.FileName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
        //        {
        //            operationResult.AddError($"The file '{file.FileName}' could not be uploaded. Only CSV format is supported.");

        //            continue;
        //        }

        //        var emailsInCsv = this.utilityService.GetDataFromCsv(file.InputStream).Select(row => row[0]).ToList();

        //        foreach (var email in emailsInCsv)
        //        {
        //            var user = this.dbContext.Users.FirstOrDefault(x => x.Email == email);

        //            if (user == null)
        //            {
        //                operationResult.AddError($"'{email}' does not exist");

        //                return this.Ok(operationResult);
        //            }

        //            mailingList.MailingListSubscriptions.Add(new MailingListSubscription
        //            {
        //                Status = MailingListSubscriptionStatusEnum.Subscribed,
        //                User = user
        //            });
        //        }
        //    }

        //    this.dbContext.SaveChanges();

        //    return this.Ok(operationResult);
        //}
    }
}