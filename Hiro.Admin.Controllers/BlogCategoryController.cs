﻿using System;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class BlogCategoryController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminBlogCategoryService adminBlogCategoryService;
        private readonly IBlogCategoryDtoValidator blogCategoryDtoValidator;

        public BlogCategoryController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminBlogCategoryService adminBlogCategoryService,
            IBlogCategoryDtoValidator blogCategoryDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.adminBlogCategoryService = adminBlogCategoryService;
            this.blogCategoryDtoValidator = blogCategoryDtoValidator;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminBlogCategoryIndex)]
        public IActionResult IndexJson()
        {
            var blogCategories = new BlogCategoryListDto
            {
                BlogCategories = this.dbContext.BlogCategories
                    .OrderByDescending(x => x.CreatedDate)
                    .AsEnumerable()
                    .Select(x => new BlogCategoryDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry),
                        BlogPosts = x.BlogPosts.Select(post => new BlogPostDto(post.Id)).ToList()
                    }).ToList()
            };

            return this.Ok(blogCategories);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminBlogCategoryEdit)]
        public IActionResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var blogCategory = isNew ? new BlogCategory() : this.dbContext.BlogCategories.Find(id);

            if (blogCategory == null)
            {
                throw new NullReferenceException();
            }

            // ---------------------------------------------------------------------
            // Add missing ShippingBoxLocalizedKits
            // ---------------------------------------------------------------------

            blogCategory.BlogCategoryLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(blogCategory.BlogCategoryLocalizedKits, new BlogCategoryLocalizedKit
            {
                BlogCategory = blogCategory
            });

            // ---------------------------------------------------------------------
            // Model
            // ---------------------------------------------------------------------

            var blogCategoryDto = new BlogCategoryDto
            {
                Id = blogCategory.Id,
                IsNew = isNew,
                IsDisabled = blogCategory.IsDisabled,
                Name = blogCategory.Name,
                Url = blogCategory.Url,
                LocalizedTitle = blogCategory.GetLocalizedTitle(this.localizationCountry),
                LocalizedDescription = blogCategory.GetLocalizedDescription(this.localizationCountry),
                BlogCategoryLocalizedKits = blogCategory.BlogCategoryLocalizedKits.OrderByDefaultCountry().Select(localizedKit => new BlogCategoryLocalizedKitDto
                {
                    Id = localizedKit.Id,
                    Title = localizedKit.Title,
                    Description = localizedKit.Description,
                    Country = localizedKit.Country == null ? null : new CountryDto
                    {
                        Id = localizedKit.Country.Id,
                        Name = localizedKit.Country.Name,
                        IsDefault = localizedKit.Country.IsDefault
                    },
                    BlogCategory = localizedKit.BlogCategory == null ? null : new BlogCategoryDto(localizedKit.BlogCategory.Id)
                }).ToList()
            };

            return this.Ok(blogCategoryDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminBlogCategoryAddOrUpdate)]
        public IActionResult AddOrUpdate(BlogCategoryDto dto)
        {
            var errors = this.blogCategoryDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var blogCategory = this.adminBlogCategoryService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, blogCategory.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminBlogCategoryDelete)]
        public IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.BlogCategories.Delete(id, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }
    }
}