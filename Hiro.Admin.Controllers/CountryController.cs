﻿using System;
using System.Globalization;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class CountryController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAdminCountryService adminCountryService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly ICountryDtoValidator countryDtoValidator;

        public CountryController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminCountryService adminCountryService,
            ICountryDtoValidator countryDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminCountryService = adminCountryService;
            this.countryDtoValidator = countryDtoValidator;
            this.adminUtilityService = adminUtilityService;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCountryIndex)]
        public IActionResult IndexJson()
        {
            var dto = new CountryListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Countries = this.dbContext.Countries
                    .AsEnumerable()
                    .OrderByDescending(x => x.IsDefault)
                    .ThenByDescending(x => x.Localize)
                    .ThenBy(x => x.IsDisabled)
                    .ThenBy(x => x.Name).Select(x => new CountryDto
                    {
                        Id = x.Id,
                        IsDefault = x.IsDefault,
                        FlagIcon = x.FlagIcon,
                        Name = x.Name,
                        Localize = x.Localize,
                        Url = x.Url,
                        LanguageCode = x.LanguageCode,
                        IsDisabled = x.IsDisabled,
                        PaymentAccountId = x.PaymentAccountId.IfNullOrEmptyConvertTo("-"),
                        CurrencySymbol = x.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = x.GetIso4217CurrencySymbol()
                    }).ToList()
            };

            return this.Ok(dto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCountryEdit)]
        public virtual IActionResult EditJson(Guid? countryId)
        {
            var isNew = countryId == null;

            var country = isNew ? new Country() : this.dbContext.Countries.Find(countryId);

            if (country == null)
            {
                throw new NullReferenceException();
            }

            var countryDto = new CountryDto
            {
                Id = country.Id,
                CurrencySymbol = country.GetCurrencySymbol(),
                FlagIcon = country.FlagIcon,
                IsDefault = country.IsDefault,
                IsDisabled = country.IsDisabled,
                IsNew = isNew,
                Iso4217CurrencySymbol = country.GetIso4217CurrencySymbol(),
                LanguageCode = country.LanguageCode,
                Localize = country.Localize,
                Name = country.Name,
                PaymentAccountId = country.PaymentAccountId,
                Url = country.Url,
                Taxes = country.Taxes.Select(tax => new TaxDto
                {
                    Id = tax.Id,
                    Name = tax.Name,
                    IsDisabled = tax.IsDisabled,
                    Amount = tax.Amount,
                    ApplyToProductPrice = tax.ApplyToProductPrice,
                    ApplyToShippingPrice = tax.ApplyToShippingPrice,
                    Code = tax.Code,
                    Percentage = tax.Percentage,
                    CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                    ShippingAddress = tax.ShippingAddress == null ? null : new AddressDto(tax.ShippingAddress.Id),
                    Country = tax.Country == null ? null : new CountryDto(tax.Country.Id)
                }).ToList()
            };

            var allLocalizedCountries = this.dbContext.Countries.Where(x => x.Localize).OrderBy(x => x.Name).ToList();
            var allCategories = this.dbContext.Categories.OrderBy(x => x.Name).ToList();

            var editCountryDto = new EditCountryDto
            {
                AllLocalizedCountries = allLocalizedCountries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsDefault = x.IsDefault
                }).ToList(),
                AllCategories = allCategories.Select(x => new CategoryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Country = countryDto,
                //FlagIcons = this.localStorageService.GetDirectoryDto(SharedConstants.FlagIconsPath).Files.Select(x => x.Name),
                Cultures = CultureInfo.GetCultures(CultureTypes.AllCultures).OrderBy(c => c.Name).Select(x => x.Name)
            };

            return this.Ok(editCountryDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCountryAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(CountryDto dto)
        {
            var error = this.countryDtoValidator.Validate(dto);

            if (error.Any())
            {
                return this.Ok(new OperationResult(error));
            }

            var country = this.adminCountryService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(error, country.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCountrySetDefaultCountry)]
        public virtual IActionResult SetDefaultCountry(Guid countryId)
        {
            var operationResult = this.adminCountryService.SetDefaultCountry(this.dbContext, countryId);

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCountryDelete)]
        public virtual IActionResult Delete(Guid countryId)
        {
            var operationResult = new OperationResult();

            var dbCountry = this.dbContext.Countries.Find(countryId);

            if (dbCountry == null)
            {
                throw new NullReferenceException("Impossible to delete the Country. Entity not found using the ID: " + countryId);
            }

            if (dbCountry.IsDefault)
            {
                throw new NullReferenceException("It's not possible to delete the Default Country. Switch Default Country first.");
            }

            try
            {
                this.dbContext.Countries.Delete(countryId, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }
    }
}