﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class VariantController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IVariantOptionDtoValidator variantOptionDtoValidator;
        private readonly IVariantDtoValidator variantDtoValidator;
        private readonly IAdminVariantService adminVariantService;

        public VariantController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminVariantService adminVariantService,
            IVariantOptionDtoValidator variantOptionDtoValidator,
            IVariantDtoValidator variantDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.variantOptionDtoValidator = variantOptionDtoValidator;
            this.variantDtoValidator = variantDtoValidator;
            this.adminVariantService = adminVariantService;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantIndex)]
        public virtual IActionResult IndexJson()
        {
            var variants = this.dbContext.Variants.AsEnumerable().Select(variant => new VariantDto
            {
                Name = variant.Name,
                SortOrder = variant.SortOrder,
                Id = variant.Id,
                LocalizedTitle = variant.GetLocalizedTitle(this.localizationCountry),
                LocalizedDescription = variant.GetLocalizedDescription(this.localizationCountry),
                Type = variant.Type,
                TypeDescriptions = typeof(VariantTypesEnum).GetDescriptions()
            }).OrderBy(x => x.SortOrder).ToList();

            var variantList = new VariantListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Variants = variants.ToList()
            };

            return this.Ok(variantList);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantEdit)]
        public virtual IActionResult EditJson(Guid? variantId)
        {
            var isNew = variantId == null;

            var variant = isNew ? new Variant() : this.dbContext.Variants.Find(variantId);

            if (variant == null)
            {
                throw new NullReferenceException();
            }

            variant.VariantLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(variant.VariantLocalizedKits, new VariantLocalizedKit
            {
                Variant = variant
            });

            var variantDto = new VariantDto
            {
                Id = variant.Id,
                Name = variant.Name,
                CreateProductImageKits = variant.CreateProductImageKits,
                CreateProductStockUnits = variant.CreateProductStockUnits,
                DefaultBooleanValue = variant.DefaultBooleanValue,
                DefaultDoubleValue = variant.DefaultDoubleValue,
                DefaultIntegerValue = variant.DefaultIntegerValue,
                DefaultJsonValue = variant.DefaultJsonValue,
                DefaultStringValue = variant.DefaultStringValue,
                DefaultVariantOptionValue = variant.DefaultVariantOptionValue == null ? null : new VariantOptionDto
                {
                    Id = variant.DefaultVariantOptionValue.Id,
                    Name = variant.DefaultVariantOptionValue.Name,
                    LocalizedTitle = variant.DefaultVariantOptionValue.GetLocalizedTitle(this.localizationCountry),
                    LocalizedDescription = variant.DefaultVariantOptionValue.GetLocalizedDescription(this.localizationCountry),
                    LocalizedIsDisabled = variant.DefaultVariantOptionValue.GetLocalizedIsDisabled(this.localizationCountry),
                    LocalizedPriceModifier = variant.DefaultVariantOptionValue.GetLocalizedPriceModifier(this.localizationCountry),
                    IsDisabled = variant.DefaultVariantOptionValue.IsDisabled,
                    SortOrder = variant.DefaultVariantOptionValue.SortOrder,
                    Code = variant.DefaultVariantOptionValue.Code
                },
                IsNew = isNew,
                IsDisabled = variant.IsDisabled,
                LocalizedDescription = variant.GetLocalizedDescription(this.localizationCountry),
                LocalizedTitle = variant.GetLocalizedTitle(this.localizationCountry),
                SortOrder = variant.SortOrder,
                Type = variant.Type,
                TypeDescriptions = typeof(VariantTypesEnum).GetDescriptions(),
                Url = variant.Url,
                VariantLocalizedKits = variant.VariantLocalizedKits.Select(variantLocalizedKit => new VariantLocalizedKitDto
                {
                    Id = variantLocalizedKit.Id,
                    Country = variantLocalizedKit.Country == null ? null : new CountryDto
                    {
                        Id = variantLocalizedKit.Country.Id,
                        Name = variantLocalizedKit.Country.Name,
                        IsDefault = variantLocalizedKit.Country.IsDefault,
                        IsDisabled = variantLocalizedKit.Country.IsDisabled,
                        Localize = variantLocalizedKit.Country.Localize,
                        Url = variantLocalizedKit.Country.Url
                    },
                    Description = variantLocalizedKit.Description,
                    Title = variantLocalizedKit.Title,
                    IsDisabled = variantLocalizedKit.IsDisabled
                }).ToList(),
                VariantOptions = variant.VariantOptions.Select(variantOptionDto => new VariantOptionDto
                {
                    Id = variantOptionDto.Id,
                    SortOrder = variantOptionDto.SortOrder,
                    Name = variantOptionDto.Name,
                    Url = variantOptionDto.Url,
                    Code = variantOptionDto.Code,
                    Tags = variantOptionDto.Tags,
                    LocalizedTitle = variantOptionDto.GetLocalizedTitle(this.localizationCountry),
                    LocalizedDescription = variantOptionDto.GetLocalizedDescription(this.localizationCountry),
                    Variant = variantOptionDto.Variant == null ? null : new VariantDto(variantOptionDto.Variant.Id)
                }).OrderBy(x => x.SortOrder).ThenBy(x => x.Name).ToList()
            };

            var editVariantDto = new EditVariantDto
            {
                Variant = variantDto,
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto()
            };

            return this.Ok(editVariantDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(VariantDto variantDto)
        {
            var errors = this.variantDtoValidator.Validate(variantDto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var variant = this.adminVariantService.AddOrUpdate(this.dbContext, variantDto);

            return this.Ok(new OperationResult<Guid>(errors, variant.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantUpdateSortOrder)]
        public virtual IActionResult UpdateSortOrder(List<VariantDto> variantDtos)
        {
            var operationResult = new OperationResult<List<VariantDto>>();

            this.adminVariantService.UpdateSortOrder(this.dbContext, variantDtos);

            operationResult.Data = this.dbContext.Variants.OrderBy(x => x.SortOrder).AsEnumerable().Select(variant => new VariantDto
            {
                Id = variant.Id,
                Name = variant.Name,
                SortOrder = variant.SortOrder,
                Type = variant.Type,
                TypeDescriptions = variant.Type.GetType().GetDescriptions(),
                LocalizedTitle = variant.GetLocalizedTitle(this.localizationCountry),
                LocalizedDescription = variant.GetLocalizedDescription(this.localizationCountry)
            }).ToList();

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            this.dbContext.Variants.Delete(id, true);

            return this.Ok(operationResult);
        }

        // -----------------------------------------------------------------------------------
        // VariantOptions
        // -----------------------------------------------------------------------------------

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantEditVariantOption)]
        public virtual IActionResult EditVariantOptionJson(Guid variantId, Guid? variantOptionId)
        {
            var variantOption = this.dbContext.VariantOptions.Find(variantOptionId);

            if (variantOption == null)
            {
                var variant = this.dbContext.Variants.Find(variantId);

                if (variant == null)
                {
                    throw new NullReferenceException();
                }

                variantOption = new VariantOption
                {
                    Variant = variant
                };
            }

            if (variantOption.Variant?.Id != variantId)
            {
                throw new Exception("The 'variantId' parameter does not match the Id of the VariantOption.Variant");
            }

            variantOption.VariantOptionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(variantOption.VariantOptionLocalizedKits, new VariantOptionLocalizedKit
            {
                VariantOption = variantOption
            });

            var variantOptionDto = new VariantOptionDto
            {
                Id = variantOption.Id,
                Code = variantOption.Code,
                Image = variantOption.Image,
                IsDisabled = variantOption.IsDisabled,
                IsNew = variantOptionId == null,
                Name = variantOption.Name,
                Variant = variantOption.Variant == null ? null : new VariantDto(variantOption.Variant.Id),
                Tags = variantOption.Tags,
                Url = variantOption.Url,
                SortOrder = variantOption.SortOrder,
                LocalizedTitle = variantOption.GetLocalizedTitle(this.localizationCountry),
                LocalizedDescription = variantOption.GetLocalizedDescription(this.localizationCountry),
                LocalizedPriceModifier = variantOption.GetLocalizedPriceModifier(this.localizationCountry),
                LocalizedIsDisabled = variantOption.GetLocalizedIsDisabled(this.localizationCountry),
                VariantOptionLocalizedKits = variantOption.VariantOptionLocalizedKits.Select(variantOptionLocalizedKit => new VariantOptionLocalizedKitDto
                {
                    Id = variantOptionLocalizedKit.Id,
                    Title = variantOptionLocalizedKit.Title,
                    Description = variantOptionLocalizedKit.Description,
                    PriceModifier = variantOptionLocalizedKit.PriceModifier,
                    IsDisabled = variantOptionLocalizedKit.IsDisabled,
                    Country = variantOptionLocalizedKit.Country == null ? null : new CountryDto
                    {
                        Id = variantOptionLocalizedKit.Country.Id,
                        Name = variantOptionLocalizedKit.Country.Name,
                        IsDefault = variantOptionLocalizedKit.Country.IsDefault,
                        IsDisabled = variantOptionLocalizedKit.Country.IsDisabled,
                        Localize = variantOptionLocalizedKit.Country.Localize,
                        Url = variantOptionLocalizedKit.Country.Url,
                        CurrencySymbol = variantOptionLocalizedKit.Country.GetCurrencySymbol()
                    },
                    ModifiedDate = variantOptionLocalizedKit.ModifiedDate,
                    CreatedDate = variantOptionLocalizedKit.CreatedDate
                }).ToList()
            };

            variantOptionDto.Variant.AsNotNull().Name = variantOption.Variant?.Name;

            var editVariantOptionDto = new EditVariantOptionDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                AllTags = this.dbContext.VariantOptions.Where(x => x.Variant.Id == variantId).OrderBy(x => x.SortOrder).AsEnumerable().SelectMany(x => x.GetTagList()).Distinct().ToList(),
                VariantOption = variantOptionDto
            };

            return this.Ok(editVariantOptionDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantAddOrUpdateVariantOption)]
        [HttpPost]
        public virtual IActionResult AddOrUpdateVariantOption(VariantOptionDto variantOptionDto)
        {
            var errors = this.variantOptionDtoValidator.Validate(variantOptionDto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var variantOption = this.adminVariantService.AddOrUpdateVariantOption(this.dbContext, variantOptionDto);

            return this.Ok(new OperationResult<Guid?>(errors, variantOption.Variant?.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminVariantDeleteVariantOption)]
        public virtual IActionResult DeleteVariantOption(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.VariantOptions.Delete(id, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }
    }
}