﻿using System;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class CouponController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminCouponService adminCouponService;
        private readonly IEditCouponDtoValidator editCouponDtoValidator;

        public CouponController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminCouponService adminCouponService,
            IEditCouponDtoValidator editCouponDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminCouponService = adminCouponService;
            this.adminUtilityService = adminUtilityService;
            this.editCouponDtoValidator = editCouponDtoValidator;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCouponIndex)]
        public virtual IActionResult IndexJson(int page = 1, string keywordFilter = null)
        {
            const int pageSize = 100;

            // -----------------------------------------------------
            // FILTERS
            // -----------------------------------------------------

            var coupons = this.dbContext.Coupons
                .Where(x => string.IsNullOrEmpty(keywordFilter)
                            || x.Code.Contains(keywordFilter)
                            || x.Note.Contains(keywordFilter)
                            || x.Country.Name.Contains(keywordFilter))
                .OrderByDescending(x => x.CreatedDate)
                .ToList();

            // -----------------------------------------------------
            // DTO
            // -----------------------------------------------------

            var couponListDto = new CouponListDto
            {
                TotalItems = coupons.Count(),
                PageSize = pageSize,
                Coupons = coupons.Skip((page - 1) * pageSize).Take(pageSize).Select(coupon => new CouponDto
                {
                    Id = coupon.Id,
                    Code = coupon.Code,
                    Note = coupon.Note.Shorten(20),
                    Published = coupon.Published,
                    ExpirationDate = coupon.ExpirationDate,
                    ValidityTimes = coupon.ValidityTimes,
                    Amount = coupon.Amount,
                    Percentage = coupon.Percentage,
                    Country = coupon.Country == null ? null : new CountryDto
                    {
                        Id = coupon.Country.Id,
                        Name = coupon.Country.Name
                    }
                }).ToList()
            };

            return this.Ok(couponListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCouponEdit)]
        public virtual IActionResult EditJson(Guid? couponId)
        {
            var isNew = couponId == null;

            var coupon = isNew ? new Coupon
            {
                Amount = 0,
                Percentage = 0,
                Country = this.dbContext.Countries.First(x => x.IsDefault),
                ExpirationDate = DateTime.Now.AddMonths(1),
                ValidityTimes = 1,
                Published = true,
                Referee = new User()
            } : this.dbContext.Coupons.Find(couponId);

            if (coupon == null)
            {
                throw new NullReferenceException();
            }

            var editCouponDto = new EditCouponDto
            {
                Quantity = 1,
                Coupon = new CouponDto
                {
                    IsNew = isNew,
                    Id = coupon.Id,
                    Code = coupon.Code,
                    Amount = coupon.Amount,
                    Percentage = coupon.Percentage,
                    Country = coupon.Country != null ? new CountryDto
                    {
                        Id = coupon.Country.Id,
                        Name = coupon.Country.Name
                    } : null,
                    Published = coupon.Published,
                    AllowOnSale = coupon.AllowOnSale,
                    ExpirationDate = coupon.ExpirationDate,
                    ValidityTimes = coupon.ValidityTimes,
                    Categories = coupon.Categories.Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                    }).ToList(),
                    Referee = coupon.Referee != null ? new UserDto
                    {
                        Id = coupon.Referee.Id,
                        FirstName = coupon.Referee.FirstName,
                        LastName = coupon.Referee.FirstName,
                        Email = coupon.Referee.Email,
                    } : null,
                    RefereeReward = coupon.RefereeReward,
                    Recipient = coupon.Recipient,
                    Note = coupon.Note
                },
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                AllCategories = this.dbContext.Categories.OrderBy(x => x.SortOrder).AsEnumerable().Select(x => new CategoryDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                }).ToList(),
                AllCountries = this.dbContext.Countries.OrderBy(x => x.Name).Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                AllUsers = this.dbContext.Users.AsEnumerable().Where(x => !x.IsDisabled && x.IsRegistered).Select(x => new UserDto
                {
                    Id = x.Id,
                    Email = x.Email
                }).OrderBy(x => x.Email).ToList()
            };

            return this.Ok(editCouponDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCouponAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(EditCouponDto editCouponDto)
        {
            var errors = this.editCouponDtoValidator.Validate(editCouponDto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var coupon = this.adminCouponService.AddOrUpdate(this.dbContext, editCouponDto.Coupon, editCouponDto.Quantity);

            return this.Ok(new OperationResult<Guid>(errors, coupon.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCouponDelete)]
        public virtual IActionResult DeleteJson(Guid couponId)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.Coupons.Delete(couponId, true);
            }
            catch (Exception e)
            {
                operationResult.Errors.Add(new OperationError(e.Message));
            }

            return this.Ok(operationResult);
        }
    }
}