﻿using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Services.Interfaces;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class SettingController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAdminSettingService adminSettingService;
        private readonly ICmsSettingsDtoValidator cmsSettingsDtoValidator;
        private readonly IEncryptionService encryptionService;

        public SettingController(
            HiroDbContext dbContext,
            IAdminSettingService adminSettingService,
            ICmsSettingsDtoValidator cmsSettingsDtoValidator,
            IEncryptionService encryptionService)
        {
            this.dbContext = dbContext;
            this.adminSettingService = adminSettingService;
            this.cmsSettingsDtoValidator = cmsSettingsDtoValidator;
            this.encryptionService = encryptionService;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminSettingIndex)]
        public virtual IActionResult IndexJson()
        {
            var setting = this.dbContext.CmsSettings.First();

            if (!string.IsNullOrEmpty(setting.SmtpPassword))
            {
                setting.SmtpPassword = this.encryptionService.Decrypt(setting.SmtpPassword);
            }

            var model = new CmsSettingsDto
            {
                Id = setting.Id,
                IsSeeded = setting.IsSeeded,
                SmtpDisplayName = setting.SmtpDisplayName,
                SmtpEmail = setting.SmtpEmail,
                SmtpHost = setting.SmtpHost,
                SmtpPassword = setting.SmtpPassword,
                SmtpPort = setting.SmtpPort,
                UseAzureStorage = setting.UseAzureStorage,
                ModifiedDate = setting.ModifiedDate,
                CreatedDate = setting.CreatedDate
            };

            return this.Ok(model); // todo
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminSettingAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(CmsSettingsDto dto)
        {
            var errors = this.cmsSettingsDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            this.adminSettingService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult(errors));
        }
    }
}