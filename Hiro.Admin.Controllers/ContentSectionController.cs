﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class ContentSectionController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminContentSectionService adminContentSectionService;
        private readonly IContentSectionDtoValidator contentSectionDtoValidator;

        public ContentSectionController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IAdminContentSectionService adminContentSectionService,
            IContentSectionDtoValidator contentSectionDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.adminContentSectionService = adminContentSectionService;
            this.contentSectionDtoValidator = contentSectionDtoValidator;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminContentSectionIndexJson)]
        public virtual IActionResult IndexJson(int page = 1, string filter = null)
        {
            const int pageSize = 50;

            var countries = this.dbContext.Countries.Where(x => x.Localize).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name);

            var contentSectionQuery = this.dbContext.ContentSections.AsQueryable();

            if (!string.IsNullOrEmpty(filter))
            {
                contentSectionQuery = contentSectionQuery.Where(x => x.Name.Contains(filter) || x.ContentSectionLocalizedKits.Any(y => y.Content.Contains(filter)));
            }

            var contentSectionsCount = contentSectionQuery.Count();

            var contentSections = contentSectionQuery
                .OrderBy(x => x.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            foreach (var contentSection in contentSections)
            {
                contentSection.ContentSectionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(contentSection.ContentSectionLocalizedKits, new ContentSectionLocalizedKit
                {
                    ContentSection = contentSection
                });
            }

            // -----------------------------------------------------------------------------
            // Create dto
            // -----------------------------------------------------------------------------

            var dto = new EditContentSectionsDto
            {
                TotalItems = contentSectionsCount,
                PageSize = pageSize,
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Countries = countries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LanguageCode = x.LanguageCode,
                    IsDefault = x.IsDefault,
                    FlagIcon = x.FlagIcon
                }).ToList(),
                ContentSections = contentSections.Select(x => new ContentSectionDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Schema = x.Schema,
                    ContentSectionLocalizedKits = x.ContentSectionLocalizedKits.Select(localizedKit => new ContentSectionLocalizedKitDto
                    {
                        Id = localizedKit.Id,
                        ContentSection = localizedKit.ContentSection == null ? null : new ContentSectionDto
                        {
                            Id = localizedKit.ContentSection.Id,
                            Name = localizedKit.ContentSection.Name
                        },
                        Country = localizedKit.Country == null ? null : new CountryDto
                        {
                            Id = localizedKit.Country.Id,
                            Name = localizedKit.Country.Name,
                            LanguageCode = localizedKit.Country.LanguageCode,
                            IsDefault = localizedKit.Country.IsDefault
                        },
                        Content = localizedKit.Content
                    }).OrderByDescending(y => y.Country?.IsDefault).ThenBy(y => y.Country?.Name).ToList()
                }).ToList()
            };

            // -----------------------------------------------------------------------------
            // Get schema names from schema directory
            // -----------------------------------------------------------------------------

            //var contentSectionSchemaDirectory = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("~/Scripts/contentSectionSchemas"));

            //if (contentSectionSchemaDirectory.Exists)
            //{
            //    dto.ContentSectionsSchemaNames = contentSectionSchemaDirectory.GetFiles().Where(x => x.Extension == ".ts").Select(x => x.Name.Substring(1, x.Name.IndexOf(x.Extension, StringComparison.Ordinal) - 1)).ToList();
            //}

            return this.Ok(dto);
        }

        /// <summary>
        /// This endpoint is only used in Hiro.
        /// </summary>
        /// <param name="contentSectionId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminContentSectionEdit)]
        public virtual IActionResult EditJson(Guid? contentSectionId)
        {
            var contentSection = contentSectionId == null ?
                new ContentSection() :
                this.dbContext.ContentSections.Find(contentSectionId) ?? throw new NullReferenceException();

            contentSection.ContentSectionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(contentSection.ContentSectionLocalizedKits, new ContentSectionLocalizedKit
            {
                ContentSection = contentSection
            });

            var contentSectionDto = new ContentSectionDto
            {
                Id = contentSection.Id,
                Name = contentSection.Name,
                Schema = contentSection.Schema,
                ContentSectionLocalizedKits = contentSection.ContentSectionLocalizedKits.Select(localizedKit => new ContentSectionLocalizedKitDto
                {
                    Id = localizedKit.Id,
                    ContentSection = localizedKit.ContentSection == null ? null : new ContentSectionDto
                    {
                        Id = localizedKit.ContentSection.Id,
                        Name = localizedKit.ContentSection.Name
                    },
                    Country = localizedKit.Country == null ? null : new CountryDto
                    {
                        Id = localizedKit.Country.Id,
                        Name = localizedKit.Country.Name,
                        LanguageCode = localizedKit.Country.LanguageCode,
                        IsDefault = localizedKit.Country.IsDefault
                    },
                    Content = localizedKit.Content
                }).OrderByDescending(y => y.Country?.IsDefault).ThenBy(y => y.Country?.Name).ToList()
            };

            return this.Ok(contentSectionDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminContentSectionGetSchemaProperties)]
        public virtual IActionResult GetSchemaPropertiesJson(string schema)
        {
            if (string.IsNullOrEmpty(schema))
            {
                return null;
            }

            var schemaType = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .FirstOrDefault(t => t.FullName != null && t.FullName.EndsWith("ContentSectionSchemas." + schema));

            if (schemaType == null)
            {
                throw new NullReferenceException("Schema not found");
            }

            var schemaDescriptionAttributes = (DescriptionAttribute[])schemaType.GetCustomAttributes(typeof(DescriptionAttribute), false);

            var schemaDescription = schemaDescriptionAttributes.ElementAtOrDefault(0)?.Description;

            var schemaProperties = schemaType.GetProperties().Select(propertyInfo => new SchemaPropertyDto
            {
                Name = propertyInfo.Name.ToLowerFirstLetter(),
                DisplayName = propertyInfo.GetDisplayAttribute()?.Name ?? propertyInfo.Name,
                DisplayDescription = propertyInfo.GetDisplayAttribute()?.Description,
                Type = propertyInfo.PropertyType == typeof(List<string>)
                    ? "string[]"
                    : propertyInfo.PropertyType == typeof(List<List<string>>)
                        ? "string[][]"
                        : "string"
            });

            var model = new
            {
                Description = schemaDescription,
                Properties = schemaProperties
            };

            return this.Ok(model);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminContentSectionGetContentVersions)]
        public virtual IActionResult GetContentVersions(Guid contentSectionLocalizedKitId)
        {
            var localizedKit = this.dbContext.ContentSectionLocalizedKits.Find(contentSectionLocalizedKitId).AsNotNull();

            var model = localizedKit.ContentVersions.Select(version => new ContentVersionDto
            {
                Id = version.Id,
                Content = version.Content,
                CreatedDate = version.CreatedDate,
                ContentSectionLocalizedKit = version.ContentSectionLocalizedKit == null ? null : new ContentSectionLocalizedKitDto
                {
                    Id = version.ContentSectionLocalizedKit.Id,
                    ContentSection = localizedKit.ContentSection == null ? null : new ContentSectionDto(localizedKit.ContentSection.Id),
                }
            }).OrderByDescending(y => y.CreatedDate).ToList();

            return this.Ok(model);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminContentSectionSave)]
        public virtual IActionResult Save(ContentSectionDto dto)
        {
            var errors = this.contentSectionDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            this.adminContentSectionService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult(errors));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminContentSectionDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.ContentSections.Delete(id, true);
            }
            catch (Exception e)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the content section: " + e.Message));
            }

            return this.Ok(operationResult);
        }
    }
}