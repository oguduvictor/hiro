﻿using System.Linq;
using Hiro.Admin.Shared.Constants;
using Hiro.DataAccess;
using Hiro.Services.Enums;
using Hiro.Services.Interfaces;
using Hiro.Services.Validators.Interfaces;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly HiroDbContext dbContext;
        private readonly IAuthenticationService authenticationService;
        private readonly IEmailIsRegisteredValidator emailIsRegisteredValidator;

        public AccountController(
            HiroDbContext dbContext,
            IAuthenticationService authenticationService,
            IEmailIsRegisteredValidator emailIsRegisteredValidator)
        {
            this.dbContext = dbContext;
            this.authenticationService = authenticationService;
            this.emailIsRegisteredValidator = emailIsRegisteredValidator;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AccountAuthenticateUser)]
        public virtual IActionResult AuthenticateUser(string email, string password)
        {
            var authenticateUserOperationResult = this.authenticationService.AuthenticateUser(email, password);

            if (authenticateUserOperationResult.Errors.Any())
            {
                return this.Ok(authenticateUserOperationResult);
            }

            var createAuthenticationTokenOperationResult = new OperationResult<string>
            {
                Data = this.authenticationService.CreateAuthenticationToken(authenticateUserOperationResult.Data, AuthenticationType.Full)
            };

            return this.Ok(createAuthenticationTokenOperationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AccountRegisterUser)]
        public virtual IActionResult RegisterUser(string firstName, string lastName, string email, string password)
        {
            var registerUserOperationResult = this.authenticationService.RegisterUser(
                firstName,
                lastName,
                email,
                password,
                this.dbContext.UserRoles.FirstOrDefault(x => x.Name == UserRoleNames.Default),
                this.dbContext.Countries.First(x => x.IsDefault));

            if (registerUserOperationResult.Errors.Any())
            {
                return this.Ok(registerUserOperationResult);
            }

            var createAuthenticationTokenOperationResult = new OperationResult<string>
            {
                Data = this.authenticationService.CreateAuthenticationToken(registerUserOperationResult.Data, AuthenticationType.Full)
            };

            return this.Ok(createAuthenticationTokenOperationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AccountEmailIsRegistered)]
        public virtual IActionResult EmailIsRegistered(string email)
        {
            var validationErrors = this.emailIsRegisteredValidator.Validate(email);

            if (validationErrors.Any())
            {
                return this.Ok(new OperationResult(validationErrors));
            }
            else
            {
                var user = this.dbContext.Users.FirstOrDefault(x => x.Email.Equals(email));

                var operationResult = new OperationResult<bool>
                {
                    Data = user?.IsRegistered ?? false
                };

                return this.Ok(operationResult);
            }
        }
    }
}