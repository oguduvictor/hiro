﻿using System;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class SeoController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminSeoService adminSeoService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly ISeoSectionDtoValidator seoSectionDtoValidator;

        public SeoController(
            HiroDbContext dbContext,
            IAdminSeoService adminSeoService,
            IAdminUtilityService adminUtilityService,
            ISeoSectionDtoValidator seoSectionDtoValidator)
        {
            this.dbContext = dbContext;
            this.adminSeoService = adminSeoService;
            this.adminUtilityService = adminUtilityService;
            this.seoSectionDtoValidator = seoSectionDtoValidator;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminSeoIndex)]
        public virtual IActionResult IndexJson()
        {
            var seoSectionsDto = new SeoSectionListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                SeoSections = this.dbContext.SeoSections.OrderBy(x => x.Page).AsEnumerable().Select(seoSection => new SeoSectionDto
                {
                    Id = seoSection.Id,
                    Page = seoSection.Page,
                    LocalizedTitle = seoSection.GetLocalizedTitle(this.localizationCountry),
                    LocalizedDescription = seoSection.GetLocalizedDescription(this.localizationCountry),
                    LocalizedImage = seoSection.GetLocalizedImage(this.localizationCountry)
                }).ToList()
            };

            return this.Ok(seoSectionsDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminSeoEdit)]
        public virtual IActionResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var seoSection = isNew ? new SeoSection() : this.dbContext.SeoSections.Find(id);

            if (seoSection == null)
            {
                throw new NullReferenceException();
            }

            seoSection.SeoSectionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(seoSection.SeoSectionLocalizedKits, new SeoSectionLocalizedKit
            {
                SeoSection = seoSection
            });

            var editSeoSectionDto = new EditSeoSectionDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                SeoSection = new SeoSectionDto
                {
                    Id = seoSection.Id,
                    IsNew = isNew,
                    Page = seoSection.Page,
                    LocalizedDescription = seoSection.GetLocalizedDescription(this.localizationCountry),
                    LocalizedTitle = seoSection.GetLocalizedTitle(this.localizationCountry),
                    LocalizedImage = seoSection.GetLocalizedImage(this.localizationCountry),
                    SeoSectionLocalizedKits = seoSection.SeoSectionLocalizedKits.Select(x => new SeoSectionLocalizedKitDto
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Description = x.Description,
                        Image = x.Image,
                        IsDisabled = x.IsDisabled,
                        ModifiedDate = x.ModifiedDate,
                        SeoSection = x.SeoSection == null ? null : new SeoSectionDto(x.SeoSection.Id),
                        Country = x.Country == null ? null : new CountryDto
                        {
                            Id = x.Country.Id,
                            Name = x.Country.Name,
                            CurrencySymbol = x.Country.GetCurrencySymbol(),
                            IsDefault = x.Country.IsDefault
                        }
                    }).ToList()
                }
            };

            return this.Ok(editSeoSectionDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminSeoAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(SeoSectionDto dto)
        {
            var errors = this.seoSectionDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var seo = this.adminSeoService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, seo.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminSeoDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var operationResult = new OperationResult();

            try
            {
                this.dbContext.SeoSections.Delete(id, true);
            }
            catch (Exception)
            {
                operationResult.Errors.Add(new OperationError("An error occurred while deleting the entity"));
            }

            return this.Ok(operationResult);
        }
    }
}