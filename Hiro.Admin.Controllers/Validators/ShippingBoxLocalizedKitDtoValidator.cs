﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ShippingBoxLocalizedKitDtoValidator : HiroValidator, IShippingBoxLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(ShippingBoxLocalizedKitDto shippingBoxLocalizedKit)
        {
            this.Must(shippingBoxLocalizedKit.Country != null, "Country must not be null.");
            this.Must(shippingBoxLocalizedKit.ShippingBox != null, "ShippingBox must not be null.");

            if (shippingBoxLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(shippingBoxLocalizedKit.Title, "Title must be set for default localized country.");
                this.MustNotBeEmpty(shippingBoxLocalizedKit.Description, "Description must be set for default localized country.");
                this.MustNotBeEmpty(shippingBoxLocalizedKit.InternalDescription, "InternalDescription must be set for default localized country.");
            }

            return this.Errors;
        }
    }
}
