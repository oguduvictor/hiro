﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class BlogCategoryLocalizedKitDtoValidator : HiroValidator, IBlogCategoryLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(BlogCategoryLocalizedKitDto blogCategoryLocalizedKit)
        {
            this.Must(blogCategoryLocalizedKit.Country != null, "Country must not be null.");
            this.Must(blogCategoryLocalizedKit.BlogCategory != null, "BlogCategory must not be null.");

            if (blogCategoryLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(blogCategoryLocalizedKit.Title, "Title must be set for default localized country.");
            }

            return this.Errors;
        }
    }
}
