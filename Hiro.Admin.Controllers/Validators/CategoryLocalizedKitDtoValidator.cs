﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class CategoryLocalizedKitDtoValidator : HiroValidator, ICategoryLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(CategoryLocalizedKitDto categoryLocalizedKit)
        {
            this.Must(categoryLocalizedKit.Country != null, "Country cannot be null.");

            if (categoryLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(categoryLocalizedKit.Title, "Title must be set for default localized kit country.");
            }

            return this.Errors;
        }
    }
}
