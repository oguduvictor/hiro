﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EmailLocalizedKitDtoValidator : HiroValidator, IEmailLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(EmailLocalizedKitDto emailLocalizedKit)
        {
            this.Must(emailLocalizedKit.Country != null, "Country must not be null.");

            if (!string.IsNullOrEmpty(emailLocalizedKit.From))
            {
                this.MustBeValidEmailAddress(emailLocalizedKit.From, $"{emailLocalizedKit.From} is not a valid email address.");
            }

            if (!string.IsNullOrEmpty(emailLocalizedKit.ReplyTo))
            {
                this.MustBeValidEmailAddress(emailLocalizedKit.ReplyTo, $"{emailLocalizedKit.ReplyTo} is not a valid email address.");
            }

            return this.Errors;
        }
    }
}
