﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class UserRoleDtoValidator : HiroValidator, IUserRoleDtoValidator
    {
        public List<OperationError> Validate(UserRoleDto userRole)
        {
            this.MustNotBeEmpty(userRole.Name, "Name must not be empty");

            return this.Errors;
        }
    }
}
