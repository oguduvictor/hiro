﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class BatchRenameFilesDtoValidator : HiroValidator, IBatchRenameFilesDtoValidator
    {
        public List<OperationError> Validate(BatchRenameFilesDto batchRenameFiles)
        {
            char[] disAllowedCharacters = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };
            const string disAllowedCharactersMessage = "cannot contain any of the following characters \\ / : * ? \" < > | ";

            this.MustNotBeEmpty(batchRenameFiles.DirectoryLocalPath, "DirectoryLocalPath must not be empty.");
            this.Must(batchRenameFiles.LocalPaths != null, "LocalPaths must not be null.");

            this.Must(batchRenameFiles.LocalPaths?.Count >= 1, "LocalPaths must have at least one item.");

            this.Must(!string.IsNullOrEmpty(batchRenameFiles.ReplaceText)
                || !string.IsNullOrEmpty(batchRenameFiles.ReplaceTextTarget)
                || !string.IsNullOrEmpty(batchRenameFiles.PrefixText)
                || !string.IsNullOrEmpty(batchRenameFiles.SuffixText),
                "At least one field must contain a value.");

            if (!string.IsNullOrEmpty(batchRenameFiles.ReplaceTextTarget))
            {
                this.MustNotBeEmpty(batchRenameFiles.ReplaceText, "'Text To Replace' should not be empty.");
                this.Must(!batchRenameFiles.ReplaceTextTarget.Equals(batchRenameFiles.ReplaceText), "'Replace Text With' should not be equal to 'Text To Replace'.");
                this.Must(!batchRenameFiles.ReplaceTextTarget.ContainsAny(disAllowedCharacters), $"'Replace Text With' {disAllowedCharactersMessage}");
            }

            if (!string.IsNullOrEmpty(batchRenameFiles.PrefixText))
            {
                this.Must(!batchRenameFiles.PrefixText.ContainsAny(disAllowedCharacters), $"'Add Prefix' {disAllowedCharactersMessage}");
            }

            if (!string.IsNullOrEmpty(batchRenameFiles.SuffixText))
            {
                this.Must(!batchRenameFiles.SuffixText.ContainsAny(disAllowedCharacters), $"'Add Suffix' {disAllowedCharactersMessage}");
            }

            return this.Errors;
        }
    }
}
