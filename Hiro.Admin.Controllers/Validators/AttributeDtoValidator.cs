﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers.Validators
{
    public class AttributeDtoValidator : HiroValidator, IAttributeDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly IAttributeLocalizedKitDtoValidator attributeLocalizedKitDtoValidator;
        private readonly IAttributeOptionDtoValidator attributeOptionDtoValidator;
        private readonly INameValidator nameValidator;
        private readonly IUrlValidator urlValidator;

        public AttributeDtoValidator(
            HiroDbContext hiroDbContext,
            IAttributeLocalizedKitDtoValidator attributeLocalizedKitDtoValidator,
            IAttributeOptionDtoValidator attributeOptionDtoValidator,
            INameValidator nameValidator,
            IUrlValidator urlValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.attributeLocalizedKitDtoValidator = attributeLocalizedKitDtoValidator;
            this.attributeOptionDtoValidator = attributeOptionDtoValidator;
            this.nameValidator = nameValidator;
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(AttributeDto attribute)
        {
            this.Errors.AddRange(this.nameValidator.Validate(attribute.Name));

            this.Errors.AddRange(this.urlValidator.Validate(
                attribute.Url,
                this.hiroDbContext.Attributes.Where(y => y.Url == attribute.Url && y.Id != attribute.Id).Select(z => z.Url).ToList()));

            foreach (var attributeOption in attribute.AttributeOptions)
            {
                this.Errors.AddRange(this.attributeOptionDtoValidator.Validate(attributeOption, attribute));
            }

            this.Must(!attribute.AttributeLocalizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue); // todo: get from contentsection using localized country

            foreach (var attributeLocalizedKit in attribute.AttributeLocalizedKits)
            {
                this.Errors.AddRange(this.attributeLocalizedKitDtoValidator.Validate(attributeLocalizedKit));
            }

            return this.Errors;
        }
    }
}
