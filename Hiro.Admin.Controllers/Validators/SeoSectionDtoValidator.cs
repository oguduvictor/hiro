﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class SeoSectionDtoValidator : HiroValidator, ISeoSectionDtoValidator
    {
        private readonly ISeoSectionLocalizedKitDtoValidator seoSectionLocalizedKitDtoValidator;

        public SeoSectionDtoValidator(ISeoSectionLocalizedKitDtoValidator seoSectionLocalizedKitDtoValidator)
        {
            this.seoSectionLocalizedKitDtoValidator = seoSectionLocalizedKitDtoValidator;
        }

        public List<OperationError> Validate(SeoSectionDto seoSection)
        {
            this.MustNotBeEmpty(seoSection.Page, "Page must not be empty.");

            this.Must(seoSection.Page?.StartsWith("/") == true, "Page must starts with '/'");

            this.Must(
                !seoSection.SeoSectionLocalizedKits.Select(x => x.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue
                );

            foreach (var seoSectionLocalizedKit in seoSection.SeoSectionLocalizedKits)
            {
                this.Errors.AddRange(this.seoSectionLocalizedKitDtoValidator.Validate(seoSectionLocalizedKit));
            }

            return this.Errors;
        }
    }
}
