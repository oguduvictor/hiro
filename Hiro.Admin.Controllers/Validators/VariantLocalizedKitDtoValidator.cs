﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class VariantLocalizedKitDtoValidator : HiroValidator, IVariantLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(VariantLocalizedKitDto variantLocalizedKit)
        {
            if (variantLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(variantLocalizedKit.Title, "Title must be set for default country.");
            }

            return this.Errors;
        }
    }
}
