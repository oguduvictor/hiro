﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class CategoryDtoValidator : HiroValidator, ICategoryDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly ICategoryLocalizedKitDtoValidator categoryLocalizedKitDtoValidator;
        private readonly INameValidator nameValidator;
        private readonly IUrlValidator urlValidator;

        public CategoryDtoValidator(
            HiroDbContext hiroDbContext,
            ICategoryLocalizedKitDtoValidator categoryLocalizedKitDtoValidator,
            INameValidator nameValidator,
            IUrlValidator urlValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.categoryLocalizedKitDtoValidator = categoryLocalizedKitDtoValidator;
            this.nameValidator = nameValidator;
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(CategoryDto category)
        {
            this.Errors.AddRange(this.nameValidator.Validate(
                category.Name,
                this.hiroDbContext.Categories.Where(x => x.Name == category.Name && x.Id != category.Id).Select(x => x.Name).ToList()));

            this.Errors.AddRange(this.urlValidator.Validate(
                category.Url,
                this.hiroDbContext.Categories.Where(x => x.Url == category.Url && x.Id != category.Id).Select(x => x.Url).ToList()));

            foreach (var child in category.Children)
            {
                this.Must(child.Id != child.Parent.Id, "It's not possible to simultaneusly set another category as both parent and child.");
            }

            this.Must(
                !category.CategoryLocalizedKits.Select(lk => lk.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue);

            foreach (var categoryLocalizedKit in category.CategoryLocalizedKits)
            {
                this.Errors.AddRange(this.categoryLocalizedKitDtoValidator.Validate(categoryLocalizedKit));
            }

            return this.Errors;
        }
    }
}
