﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class AttributeOptionDtoValidator : HiroValidator, IAttributeOptionDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly IAttributeOptionLocalizedKitDtoValidator attributeOptionLocalizedKitDtoValidator;
        private readonly INameValidator nameValidator;
        private readonly IUrlValidator urlValidator;

        public AttributeOptionDtoValidator(
            HiroDbContext hiroDbContext,
            IAttributeOptionLocalizedKitDtoValidator attributeOptionLocalizedKitDtoValidator,
            INameValidator nameValidator,
            IUrlValidator urlValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.attributeOptionLocalizedKitDtoValidator = attributeOptionLocalizedKitDtoValidator;
            this.nameValidator = nameValidator;
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(AttributeOptionDto attributeOption, AttributeDto attribute)
        {
            this.Must(
                attributeOption.Attribute.Id.Equals(attribute.Id),
                "AttributeOption's attribute id does not match supplied attribute's id.");

            this.Must(
                !attributeOption.AttributeOptionLocalizedKits.Select(x => x.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue); // todo: get from contentsection using localized country
                                                                                           // (Actually let's just hard-code messages for now, we'll think of localization later)

            // Note: Using this.Errors.AddRange() is fine when the aim is to use an external validator
            this.Errors.AddRange(this.nameValidator.Validate(
                attributeOption.Name,
                attribute.AttributeOptions.Select(attributeOption => attributeOption.Name)));

            this.Errors.AddRange(this.urlValidator.Validate(
                attributeOption.Url,
                this.hiroDbContext.AttributeOptions.Where(y => y.Url == attributeOption.Url && y.Id != attributeOption.Id).Select(z => z.Url).ToList()));

            foreach (var attributeOptionLocalizedKit in attributeOption.AttributeOptionLocalizedKits)
            {
                this.Errors.AddRange(this.attributeOptionLocalizedKitDtoValidator.Validate(attributeOptionLocalizedKit));
            }

            return this.Errors;
        }
    }
}
