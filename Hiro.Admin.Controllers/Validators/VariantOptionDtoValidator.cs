﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class VariantOptionDtoValidator : HiroValidator, IVariantOptionDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly INameValidator nameValidator;
        private readonly IUrlValidator urlValidator;
        private readonly IVariantOptionLocalizedKitDtoValidator variantOptionLocalizedKitDtoValidator;

        public VariantOptionDtoValidator(
            HiroDbContext hiroDbContext,
            INameValidator nameValidator,
            IUrlValidator urlValidator,
            IVariantOptionLocalizedKitDtoValidator variantOptionLocalizedKitDtoValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.nameValidator = nameValidator;
            this.urlValidator = urlValidator;
            this.variantOptionLocalizedKitDtoValidator = variantOptionLocalizedKitDtoValidator;
        }

        public List<OperationError> Validate(VariantOptionDto variantOption)
        {
            this.Errors.AddRange(this.nameValidator.Validate(
                variantOption.Name,
                this.hiroDbContext.VariantOptions
                .Where(x =>
                (x.Variant.Id == variantOption.Id
                || (x.Name == variantOption.Name && x.Variant.Id == variantOption.Variant.Id))
                && x.Name == variantOption.Name && x.Id != variantOption.Id)
                .Select(c => c.Name).ToList()));

            this.Errors.AddRange(this.urlValidator.Validate(
                variantOption.Url,
                this.hiroDbContext.VariantOptions
                .Where(x =>
                (x.Variant.Id == variantOption.Id
                || (x.Url == variantOption.Url && x.Variant.Id == variantOption.Variant.Id))
                && x.Url == variantOption.Url && x.Id != variantOption.Id)
                .Select(c => c.Url).ToList()));

            this.Must(
                !variantOption.VariantOptionLocalizedKits.Select(x => x.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue);

            foreach (var variantOptionLocalizedKit in variantOption.VariantOptionLocalizedKits)
            {
                this.Errors.AddRange(this.variantOptionLocalizedKitDtoValidator.Validate(variantOptionLocalizedKit));
            }

            return this.Errors;
        }
    }
}
