﻿namespace Hiro.Admin.Controllers.Validators
{
    using System.Collections.Generic;
    using Hiro.Admin.Controllers.Validators.Interfaces;
    using Hiro.Domain.Dtos;
    using Hiro.Shared.Dtos;
    using Hiro.Shared.Validation;

    public class MailingListDtoValidator : HiroValidator, IMailingListDtoValidator
    {
        private readonly INameValidator nameValidator;
        private readonly IMailingListLocalizedKitDtoValidator mailingListLocalizedKitDtoValidator;

        public MailingListDtoValidator(
            INameValidator nameValidator,
            IMailingListLocalizedKitDtoValidator mailingListLocalizedKitDtoValidator
            )
        {
            this.nameValidator = nameValidator;
            this.mailingListLocalizedKitDtoValidator = mailingListLocalizedKitDtoValidator;
        }

        public List<OperationError> Validate(MailingListDto mailingList)
        {
            this.Errors.AddRange(this.nameValidator.Validate(mailingList.Name));

            foreach (var mailingListLocalizedKit in mailingList.MailingListLocalizedKits)
            {
                this.Errors.AddRange(this.mailingListLocalizedKitDtoValidator.Validate(mailingListLocalizedKit));
            }

            return this.Errors;
        }
    }
}
