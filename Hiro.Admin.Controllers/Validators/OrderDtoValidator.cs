﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class OrderDtoValidator : HiroValidator, IOrderDtoValidator
    {
        private readonly IOrderShippingBoxDtoValidator orderShippingBoxDtoValidator;

        public OrderDtoValidator(IOrderShippingBoxDtoValidator orderShippingBoxDtoValidator)
        {
            this.orderShippingBoxDtoValidator = orderShippingBoxDtoValidator;
        }

        public List<OperationError> Validate(OrderDto order)
        {
            foreach (var orderShippingBox in order.OrderShippingBoxes)
            {
                this.Errors.AddRange(this.orderShippingBoxDtoValidator.Validate(orderShippingBox));
            }

            return this.Errors;
        }
    }
}
