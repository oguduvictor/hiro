﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EmailDtoValidator : HiroValidator, IEmailDtoValidator
    {
        private readonly IEmailLocalizedKitDtoValidator emailLocalizedKitDtoValidator;
        private readonly INameValidator nameValidator;

        public EmailDtoValidator(
            IEmailLocalizedKitDtoValidator emailLocalizedKitDtoValidator,
            INameValidator nameValidator)
        {
            this.emailLocalizedKitDtoValidator = emailLocalizedKitDtoValidator;
            this.nameValidator = nameValidator;
        }

        public List<OperationError> Validate(EmailDto email)
        {
            this.Errors.AddRange(this.nameValidator.Validate(email.Name));

            this.MustNotBeEmpty(email.ViewName, "ViewName must not be empty.");

            this.Must(email.ContentSection != null, $"Please select a content section for '{email.Name}'");

            foreach (var emailLocalizedKit in email.EmailLocalizedKits)
            {
                this.Errors.AddRange(this.emailLocalizedKitDtoValidator.Validate(emailLocalizedKit));
            }

            return this.Errors;
        }
    }
}
