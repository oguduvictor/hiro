﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class VariantOptionLocalizedKitDtoValidator : HiroValidator, IVariantOptionLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(VariantOptionLocalizedKitDto variantOptionLocalizedKit)
        {
            if (variantOptionLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(variantOptionLocalizedKit.Title, "Title must be set for default country.");
            }

            return this.Errors;
        }
    }
}
