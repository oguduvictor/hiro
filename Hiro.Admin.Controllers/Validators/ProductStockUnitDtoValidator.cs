﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductStockUnitDtoValidator : HiroValidator, IProductStockUnitDtoValidator
    {
        private readonly IProductStockUnitLocalizedKitDtoValidator productStockUnitLocalizedKitDtoValidator;

        public ProductStockUnitDtoValidator(IProductStockUnitLocalizedKitDtoValidator productStockUnitLocalizedKitDtoValidator)
        {
            this.productStockUnitLocalizedKitDtoValidator = productStockUnitLocalizedKitDtoValidator;
        }

        public List<OperationError> Validate(ProductStockUnitDto productStockUnit)
        {
            this.Must(productStockUnit.DispatchDate != null, "DispatchDate must not be null");

            if (productStockUnit.IsDisabled.IsNullOrFalse())
            {
                foreach (var productStockUnitLocalizedKit in productStockUnit.ProductStockUnitLocalizedKits)
                {
                    this.Errors.AddRange(this.productStockUnitLocalizedKitDtoValidator.Validate(productStockUnitLocalizedKit));
                }
            }

            return this.Errors;
        }
    }
}
