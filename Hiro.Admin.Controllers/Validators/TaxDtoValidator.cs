﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class TaxDtoValidator : HiroValidator, ITaxDtoValidator
    {
        private readonly INameValidator nameValidator;

        public TaxDtoValidator(INameValidator nameValidator)
        {
            this.nameValidator = nameValidator;
        }

        public List<OperationError> Validate(TaxDto tax)
        {
            this.Must(tax.Country != null, "Country can not be null.");
            this.Must(tax.Percentage >= 0 && tax.Percentage <= 100, "Percentage must be between 0 and 100.");

            this.Errors.AddRange(this.nameValidator.Validate(tax.Name));

            return this.Errors;
        }
    }
}
