﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class MailingListLocalizedKitDtoValidator : HiroValidator, IMailingListLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(MailingListLocalizedKitDto mailingListLocalizedKit)
        {
            this.Must(mailingListLocalizedKit.Country != null, "Country cannot be null.");

            if (mailingListLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(mailingListLocalizedKit.Title, "Title must be set for default localized kit country.");
            }

            return this.Errors;
        }
    }
}
