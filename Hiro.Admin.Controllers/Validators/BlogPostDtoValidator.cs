﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class BlogPostDtoValidator : HiroValidator, IBlogPostDtoValidator
    {
        private readonly IUrlValidator urlValidator;

        public BlogPostDtoValidator(IUrlValidator urlValidator)
        {
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(BlogPostDto blogPost)
        {
            this.MustNotBeEmpty(blogPost.Title, "Title must not be empty.");
            this.MustNotBeEmpty(blogPost.Content, "Content must not be empty.");

            this.Errors.AddRange(this.urlValidator.Validate(blogPost.Url));

            return this.Errors;
        }
    }
}
