﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class NameValidator : HiroValidator, INameValidator
    {
        public List<OperationError> Validate(string name, IEnumerable<string> conflictingNames = null)
        {
            this.CascadeMode = true;

            this.MustNotBeEmpty(name, "Please insert a valid Name.");
            this.Must(name.Length >= 1 && name.Length <= 100, "The length of the Name must be between 1 and 100 characters.");
            this.Must(!name.Contains(" "), "The Name cannot contain empty spaces. Use the underscore character to separate words.");
            this.Must(Regex.IsMatch(name, "^[a-zA-Z0-9_]*$"), "The Name can only contain letters, numbers and underscores.");
            this.Must(char.IsUpper(name, 0), "The Name must start with a capital letter.");
            this.Must(!conflictingNames?.Contains(name) ?? true, $"The Name '{name}' is already in use.");

            var splitStrings = name.Split('_');

            foreach (var str in splitStrings)
            {
                this.MustNotBeEmpty(str, "Two consecutive underscore characters are not allowed.");
                this.Must(!char.IsLower(str, 0), "Each underscore characted must be followed by a capital letter");
            }

            return this.Errors;
        }
    }
}
