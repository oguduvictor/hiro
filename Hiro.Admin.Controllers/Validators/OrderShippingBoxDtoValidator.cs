﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class OrderShippingBoxDtoValidator : HiroValidator, IOrderShippingBoxDtoValidator
    {
        private readonly IOrderItemDtoValidator orderItemDtoValidator;

        public OrderShippingBoxDtoValidator(IOrderItemDtoValidator orderItemDtoValidator)
        {
            this.orderItemDtoValidator = orderItemDtoValidator;
        }

        public List<OperationError> Validate(OrderShippingBoxDto orderShippingBox)
        {
            this.Must(orderShippingBox.Order != null, "Order must not be null");

            foreach (var orderItem in orderShippingBox.OrderItems)
            {
                this.Errors.AddRange(this.orderItemDtoValidator.Validate(orderItem));
            }

            return this.Errors;
        }
    }
}
