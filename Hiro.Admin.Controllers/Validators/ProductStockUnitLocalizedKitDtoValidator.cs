﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductStockUnitLocalizedKitDtoValidator : HiroValidator, IProductStockUnitLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(ProductStockUnitLocalizedKitDto productStockUnitLocalizedKit)
        {
            this.Must(productStockUnitLocalizedKit.ProductStockUnit != null, "ProductStockUnit must not be null.");
            this.Must(productStockUnitLocalizedKit.Country != null, "Country must not be null.");

            return this.Errors;
        }
    }
}
