﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ResizeFileDtoValidator : HiroValidator, IResizeFileDtoValidator
    {
        public List<OperationError> Validate(ResizeFileDto resizeFileDto)
        {
            this.Must(resizeFileDto.LocalPaths?.Any() ?? false, "LocalPath must contain at least one item.");

            return this.Errors;
        }
    }
}
