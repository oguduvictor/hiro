﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class CmsSettingsDtoValidator : HiroValidator, ICmsSettingsDtoValidator
    {
        public List<OperationError> Validate(CmsSettingsDto cmsSettings)
        {
            this.MustNotBeEmpty(cmsSettings.SmtpHost, "Smtp Host must not be empty");
            this.MustNotBeEmpty(cmsSettings.SmtpPassword, "Smtp Password must not be empty");
            this.MustNotBeEmpty(cmsSettings.SmtpEmail, "Smtp Email must not be empty");
            this.MustBeValidEmailAddress(cmsSettings.SmtpEmail, "Provide valid email for SmtpEmail");

            return this.Errors;
        }
    }
}
