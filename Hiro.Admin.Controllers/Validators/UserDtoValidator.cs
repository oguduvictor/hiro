﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class UserDtoValidator : HiroValidator, IUserDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly IAddressDtoValidator addressDtoValidator;

        public UserDtoValidator(
            HiroDbContext hiroDbContext,
            IAddressDtoValidator addressDtoValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.addressDtoValidator = addressDtoValidator;
        }

        public List<OperationError> Validate(UserDto user)
        {
            this.Must(user.Role != null, "Role must not be null.");
            this.MustNotBeEmpty(user.Email, "Email must not be empty.");
            this.MustBeValidEmailAddress(user.Email, "Provide valid email address.");

            foreach (var address in user.Addresses)
            {
                this.Errors.AddRange(this.addressDtoValidator.Validate(address));
            }

            this.Must(
                !this.hiroDbContext.Users.Any(x => x.Email == user.Email && x.Id != user.Id),
                "Another user with the same email already exists.");

            var role = this.hiroDbContext.UserRoles.Find(user.Role?.Id);

            this.Must(role != null, "Invalid user role."); // ToDo: Check password strength of admin roles

            return this.Errors;
        }
    }
}
