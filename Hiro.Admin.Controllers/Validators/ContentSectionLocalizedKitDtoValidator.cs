﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ContentSectionLocalizedKitDtoValidator : HiroValidator, IContentSectionLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(ContentSectionLocalizedKitDto contentSectionLocalizedKit)
        {
            this.Must(contentSectionLocalizedKit.Country != null, "Country must not be null.");
            this.Must(contentSectionLocalizedKit.ContentSection != null, "Content Section must not be null.");

            return this.Errors;
        }
    }
}
