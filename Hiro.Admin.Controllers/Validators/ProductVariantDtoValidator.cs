﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductVariantDtoValidator : HiroValidator, IProductVariantDtoValidator
    {
        public List<OperationError> Validate(ProductVariantDto productVariant)
        {
            this.Must(productVariant.Product != null, "Product must not be null");
            this.Must(productVariant.Variant != null, "Variant must not be null");

            if (productVariant.Variant?.Type == VariantTypesEnum.Options)
            {
                this.Must(productVariant.VariantOptions.Count > 0, "You must select at least one option");
                this.Must(productVariant.DefaultVariantOptionValue != null, "Default Variant Option Value must not be null");
            }

            if (productVariant.Variant?.Type == VariantTypesEnum.Double)
            {
                this.Must(productVariant.DefaultDoubleValue > 0, "Default Double Value must be greater than 0");
            }

            if (productVariant.Variant?.Type == VariantTypesEnum.Integer)
            {
                this.Must(productVariant.DefaultIntegerValue > 0, "Default Integer Value must be greater than 0");
            }

            return this.Errors;
        }
    }
}
