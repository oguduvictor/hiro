﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductAttributeDtoValidator : HiroValidator, IProductAttributeDtoValidator
    {
        public List<OperationError> Validate(ProductAttributeDto productAttribute)
        {
            this.Must(productAttribute.Product != null, "Product must not be null");
            this.Must(productAttribute.Attribute != null, "Attribute must not be null");

            if (productAttribute.Attribute?.Type == AttributeTypesEnum.Options)
            {
                this.Must(
                    productAttribute.AttributeOptionValue != null,
                    $"Please select a an Attribute Option for '{productAttribute.Attribute.LocalizedTitle}'");
            }

            if (productAttribute.Attribute?.Type == AttributeTypesEnum.String)
            {
                this.MustNotBeEmpty(
                    productAttribute.StringValue,
                    "StringValue must not be empty when attribute type is set to string");
            }

            if (productAttribute.Attribute?.Type == AttributeTypesEnum.DateTime)
            {
                this.Must(
                    productAttribute.DateTimeValue != null,
                    "DateTimeValue must not be empty when attribute type is set to date");
            }

            if (productAttribute.Attribute?.Type == AttributeTypesEnum.Image)
            {
                this.MustNotBeEmpty(
                    productAttribute.ImageValue,
                    "ImageValue must not be empty when attribute type is set to image");
            }

            if (productAttribute.Attribute?.Type == AttributeTypesEnum.Double)
            {
                this.Must(
                    productAttribute.DoubleValue >= 0.0 && productAttribute.DoubleValue <= double.MaxValue,
                    $"DoubleValue must be between 0.0 and {double.MaxValue}");
            }

            if (productAttribute.Attribute?.Type == AttributeTypesEnum.ContentSection)
            {
                this.Must(
                    productAttribute.ContentSectionValue != null,
                    $"Please select a an Content Section for '{productAttribute.Attribute.LocalizedTitle}'");
            }

            return this.Errors;
        }
    }
}
