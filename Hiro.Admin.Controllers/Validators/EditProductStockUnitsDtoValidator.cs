﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EditProductStockUnitsDtoValidator : HiroValidator, IEditProductStockUnitsDtoValidator
    {
        private readonly IProductStockUnitDtoValidator productStockUnitDtoValidator;

        public EditProductStockUnitsDtoValidator(IProductStockUnitDtoValidator productStockUnitDtoValidator)
        {
            this.productStockUnitDtoValidator = productStockUnitDtoValidator;
        }

        public List<OperationError> Validate(EditProductStockUnitsDto editProductStockUnitsDto)
        {
            this.Must(
                !editProductStockUnitsDto.ProductStockUnits.Where(x => !string.IsNullOrEmpty(x.Code) && !x.IsDeleted.IsTrue()).Select(x => x.Code).ContainsDuplicates(),
                "Barcodes must be unique.");

            foreach (var productStockUnit in editProductStockUnitsDto.ProductStockUnits)
            {
                this.Errors.AddRange(this.productStockUnitDtoValidator.Validate(productStockUnit));
            }

            return this.Errors;
        }
    }
}
