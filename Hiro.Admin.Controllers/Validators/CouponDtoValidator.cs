﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class CouponDtoValidator : HiroValidator, ICouponDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;

        public CouponDtoValidator(HiroDbContext hiroDbContext)
        {
            this.hiroDbContext = hiroDbContext;
        }

        public List<OperationError> Validate(CouponDto coupon)
        {
            if (coupon.Percentage.ApproximatelyEqualsTo(0))
            {
                this.Must(coupon.Amount > 0, "Please set the value of the Coupon using the Amount property or the Percentage property");
            }

            if (coupon.Amount > 0)
            {
                this.Must(coupon.Percentage.ApproximatelyEqualsTo(0), "It's not possible to set both the Amount and Percentage properties");
            }

            this.Must(coupon.ExpirationDate != null, "Expiration date must be set");

            this.Must(
                !this.hiroDbContext.Coupons.Any(x => x.Code == coupon.Code && x.Id != coupon.Id),
                $"The Code '{coupon.Code}' is already in use.");

            return this.Errors;
        }
    }
}
