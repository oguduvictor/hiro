﻿using System;
using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class SendEmailConfirmationModalDtoValidator : HiroValidator, ISendEmailConfirmationModalDtoValidator
    {
        public List<OperationError> Validate(SendEmailConfirmationModalDto sendEmailConfirmationModalDto)
        {
            this.Must(sendEmailConfirmationModalDto.EntityId != Guid.Empty, "Entity Id must be set.");
            this.MustNotBeEmpty(sendEmailConfirmationModalDto.EmailName, "Email Name must be set.");

            return this.Errors;
        }
    }
}
