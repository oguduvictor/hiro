﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductImageDtoValidator : HiroValidator, IProductImageDtoValidator
    {
        private readonly INameValidator nameValidator;

        public ProductImageDtoValidator(INameValidator nameValidator)
        {
            this.nameValidator = nameValidator;
        }

        public List<OperationError> Validate(ProductImageDto productImage)
        {
            if (!string.IsNullOrEmpty(productImage.Name))
            {
                this.Errors.AddRange(this.nameValidator.Validate(productImage.Name));
            }

            this.MustNotBeEmpty(productImage.Url, "Url must not be empty");

            return this.Errors;
        }
    }
}
