﻿using System;
using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EditProductAttributesDtoValidator : HiroValidator, IEditProductAttributesDtoValidator
    {
        private readonly IProductAttributeDtoValidator productAttributeDtoValidator;

        public EditProductAttributesDtoValidator(IProductAttributeDtoValidator productAttributeDtoValidator)
        {
            this.productAttributeDtoValidator = productAttributeDtoValidator;
        }

        public List<OperationError> Validate(EditProductAttributesDto editProductAttributes)
        {
            this.Must(editProductAttributes.ProductId != Guid.Empty, "ProductId must not be empty");

            foreach (var productAttribute in editProductAttributes.ProductAttributes)
            {
                this.Errors.AddRange(this.productAttributeDtoValidator.Validate(productAttribute));
            }

            return this.Errors;
        }
    }
}
