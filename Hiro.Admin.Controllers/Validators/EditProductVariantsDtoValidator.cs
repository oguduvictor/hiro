﻿using System;
using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EditProductVariantsDtoValidator : HiroValidator, IEditProductVariantsDtoValidator
    {
        private readonly IProductVariantDtoValidator productVariantDtoValidator;

        public EditProductVariantsDtoValidator(IProductVariantDtoValidator productVariantDtoValidator)
        {
            this.productVariantDtoValidator = productVariantDtoValidator;
        }

        public List<OperationError> Validate(EditProductVariantsDto editProductVariants)
        {
            this.Must(editProductVariants.ProductId != Guid.Empty, "ProductId must not be empty");

            foreach (var productVariant in editProductVariants.ProductVariants)
            {
                this.Errors.AddRange(this.productVariantDtoValidator.Validate(productVariant));
            }

            return this.Errors;
        }
    }
}
