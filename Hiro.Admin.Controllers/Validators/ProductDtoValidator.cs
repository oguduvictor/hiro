﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductDtoValidator : HiroValidator, IProductDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly INameValidator nameValidator;
        private readonly IProductLocalizedKitDtoValidator productLocalizedKitDtoValidator;
        private readonly IUrlValidator urlValidator;

        public ProductDtoValidator(
            HiroDbContext hiroDbContext,
            INameValidator nameValidator,
            IProductLocalizedKitDtoValidator productLocalizedKitDtoValidator,
            IUrlValidator urlValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.nameValidator = nameValidator;
            this.productLocalizedKitDtoValidator = productLocalizedKitDtoValidator;
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(ProductDto product)
        {
            this.Errors.AddRange(this.nameValidator.Validate(product.Name));

            this.Errors.AddRange(this.urlValidator.Validate(
                product.Url,
                this.hiroDbContext.Products.Where(x => x.Url == product.Url && x.Id != product.Id).Select(x => x.Url).ToList()));

            this.Must(product.ShippingBoxes.Count > 0, "You must select at least one shipping option");

            this.Must(
                !product.ProductLocalizedKits.Select(x => x.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue);

            foreach (var productLocalizedKit in product.ProductLocalizedKits)
            {
                this.Errors.AddRange(this.productLocalizedKitDtoValidator.Validate(productLocalizedKit));
            }

            return this.Errors;
        }
    }
}
