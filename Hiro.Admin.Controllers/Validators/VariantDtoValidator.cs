﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Domain.Enums;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class VariantDtoValidator : HiroValidator, IVariantDtoValidator
    {
        private readonly IVariantLocalizedKitDtoValidator variantLocalizedKitDtoValidator;
        private readonly INameValidator nameValidator;
        private readonly IUrlValidator urlValidator;

        public VariantDtoValidator(
            IVariantLocalizedKitDtoValidator variantLocalizedKitDtoValidator,
            INameValidator nameValidator,
            IUrlValidator urlValidator)
        {
            this.variantLocalizedKitDtoValidator = variantLocalizedKitDtoValidator;
            this.nameValidator = nameValidator;
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(VariantDto variant)
        {
            this.Errors.AddRange(this.nameValidator.Validate(variant.Name));
            this.Errors.AddRange(this.urlValidator.Validate(variant.Url));
            this.Must(
                !variant.VariantLocalizedKits.Select(x => x.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue);

            foreach (var variantLocalizedKit in variant.VariantLocalizedKits)
            {
                this.Errors.AddRange(this.variantLocalizedKitDtoValidator.Validate(variantLocalizedKit));
            }

            if (variant.Type == VariantTypesEnum.Options && variant.VariantOptions.Any())
            {
                this.Must(variant.DefaultVariantOptionValue != null, "Default variant option value can not be null when variant type is set to options.");
            }

            return this.Errors;
        }
    }
}
