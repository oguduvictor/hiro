﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class DirectoryDtoValidator : HiroValidator, IDirectoryDtoValidator
    {
        public List<OperationError> Validate(MediaDirectory mediaDirectory)
        {
            this.MustNotBeEmpty(mediaDirectory.LocalPath, "LocalPath must not be empty");
            this.MustNotBeEmpty(mediaDirectory.Name, "Name must not be empty");
            this.Must(mediaDirectory.Name.Equals("_backups"), "'_backups' is a reserved word and cannot be used as the name of a directory");

            return this.Errors;
        }
    }
}
