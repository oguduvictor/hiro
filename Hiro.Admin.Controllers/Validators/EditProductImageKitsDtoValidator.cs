﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EditProductImageKitsDtoValidator : HiroValidator, IEditProductImageKitsDtoValidator
    {
        private readonly IProductImageKitDtoValidator productImageKitDtoValidator;

        public EditProductImageKitsDtoValidator(IProductImageKitDtoValidator productImageKitDtoValidator)
        {
            this.productImageKitDtoValidator = productImageKitDtoValidator;
        }

        public List<OperationError> Validate(EditProductImageKitsDto editProductImageKitsDto)
        {
            foreach (var productImageKit in editProductImageKitsDto.ProductImageKits)
            {
                this.Errors.AddRange(this.productImageKitDtoValidator.Validate(productImageKit));
            }

            return this.Errors;
        }
    }
}
