﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class EditCouponDtoValidator : HiroValidator, IEditCouponDtoValidator
    {
        private readonly ICouponDtoValidator couponDtoValidator;

        public EditCouponDtoValidator(ICouponDtoValidator couponDtoValidator)
        {
            this.couponDtoValidator = couponDtoValidator;
        }

        public List<OperationError> Validate(EditCouponDto editCoupon)
        {
            this.Must(editCoupon.Quantity >= 1, "Quantity must be greater than or equal to 1.");

            this.Errors.AddRange(this.couponDtoValidator.Validate(editCoupon.Coupon));

            return this.Errors;
        }
    }
}
