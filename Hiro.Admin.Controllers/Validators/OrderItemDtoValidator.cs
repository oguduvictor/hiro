﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class OrderItemDtoValidator : HiroValidator, IOrderItemDtoValidator
    {
        public List<OperationError> Validate(OrderItemDto orderItem)
        {
            this.Must(orderItem.OrderShippingBox != null, "OrderShippingBox must not be null");

            return this.Errors;
        }
    }
}
