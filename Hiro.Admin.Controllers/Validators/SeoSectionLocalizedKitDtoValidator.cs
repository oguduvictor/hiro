﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class SeoSectionLocalizedKitDtoValidator : HiroValidator, ISeoSectionLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(SeoSectionLocalizedKitDto seoSectionLocalizedKit)
        {
            this.Must(seoSectionLocalizedKit.Country != null, "Country cannot be null.");
            this.Must(seoSectionLocalizedKit.SeoSection != null, "SeoSection cannot be null.");

            if (seoSectionLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(seoSectionLocalizedKit.Title, "Title for Default Country must not be empty.");
            }

            return this.Errors;
        }
    }
}
