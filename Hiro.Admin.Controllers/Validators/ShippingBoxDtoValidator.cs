﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ShippingBoxDtoValidator : HiroValidator, IShippingBoxDtoValidator
    {
        private readonly INameValidator nameValidator;
        private readonly IShippingBoxLocalizedKitDtoValidator shippingBoxLocalizedKitDtoValidator;

        public ShippingBoxDtoValidator(
            INameValidator nameValidator,
            IShippingBoxLocalizedKitDtoValidator shippingBoxLocalizedKitDtoValidator)
        {
            this.nameValidator = nameValidator;
            this.shippingBoxLocalizedKitDtoValidator = shippingBoxLocalizedKitDtoValidator;
        }

        public List<OperationError> Validate(ShippingBoxDto shippingBox)
        {
            this.Errors.AddRange(this.nameValidator.Validate(shippingBox.Name));

            this.Must(
                !shippingBox.ShippingBoxLocalizedKits.Select(x => x.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue);

            foreach (var shippingBoxLocalizedKit in shippingBox.ShippingBoxLocalizedKits)
            {
                this.Errors.AddRange(this.shippingBoxLocalizedKitDtoValidator.Validate(shippingBoxLocalizedKit));
            }

            return this.Errors;
        }
    }
}
