﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class AttributeLocalizedKitDtoValidator : HiroValidator, IAttributeLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(AttributeLocalizedKitDto attributeLocalizedKit)
        {
            this.Must(attributeLocalizedKit.Country != null, "Country cannot be null.");

            if (attributeLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(attributeLocalizedKit.Title, "Title must be set for default attribute localized kit country.");
            }

            return this.Errors;
        }
    }
}