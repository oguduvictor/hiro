﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductImageKitDtoValidator : HiroValidator, IProductImageKitDtoValidator
    {
        private readonly IProductImageDtoValidator productImageDtoValidator;

        public ProductImageKitDtoValidator(IProductImageDtoValidator productImageDtoValidator)
        {
            this.productImageDtoValidator = productImageDtoValidator;
        }

        public List<OperationError> Validate(ProductImageKitDto productImageKit)
        {
            foreach (var productImage in productImageKit.ProductImages)
            {
                this.Errors.AddRange(this.productImageDtoValidator.Validate(productImage));
            }

            return this.Errors;
        }
    }
}
