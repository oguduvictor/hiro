﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IAttributeDtoValidator
    {
        List<OperationError> Validate(AttributeDto attribute);
    }
}
