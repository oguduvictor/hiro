﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface ISeoSectionLocalizedKitDtoValidator
    {
        List<OperationError> Validate(SeoSectionLocalizedKitDto seoSectionLocalizedKit);
    }
}
