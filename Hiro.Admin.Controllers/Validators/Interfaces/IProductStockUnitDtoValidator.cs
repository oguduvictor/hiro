﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IProductStockUnitDtoValidator
    {
        List<OperationError> Validate(ProductStockUnitDto productStockUnit);
    }
}
