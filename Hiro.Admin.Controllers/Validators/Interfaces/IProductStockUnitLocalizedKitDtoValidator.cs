﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IProductStockUnitLocalizedKitDtoValidator
    {
        List<OperationError> Validate(ProductStockUnitLocalizedKitDto productStockUnitLocalizedKit);
    }
}
