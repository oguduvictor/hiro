﻿using System.Collections.Generic;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface INameValidator
    {
        List<OperationError> Validate(string name, IEnumerable<string> conflictingNames = null);
    }
}
