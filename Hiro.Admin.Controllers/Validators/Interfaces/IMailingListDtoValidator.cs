﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IMailingListDtoValidator
    {
        List<OperationError> Validate(MailingListDto mailingList);
    }
}
