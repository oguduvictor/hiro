﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface ITaxDtoValidator
    {
        List<OperationError> Validate(TaxDto tax);
    }
}
