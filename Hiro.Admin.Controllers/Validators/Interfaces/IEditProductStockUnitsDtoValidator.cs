﻿using System.Collections.Generic;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IEditProductStockUnitsDtoValidator
    {
        List<OperationError> Validate(EditProductStockUnitsDto editProductStockUnitsDto);
    }
}
