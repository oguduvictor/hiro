﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IAttributeOptionDtoValidator
    {
        List<OperationError> Validate(AttributeOptionDto attributeOption, AttributeDto attribute);
    }
}
