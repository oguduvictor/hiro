﻿using System.Collections.Generic;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IUrlValidator
    {
        List<OperationError> Validate(string url, IEnumerable<string> conflictingUrls = null);
    }
}
