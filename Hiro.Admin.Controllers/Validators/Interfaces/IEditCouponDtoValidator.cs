﻿using System.Collections.Generic;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IEditCouponDtoValidator
    {
        List<OperationError> Validate(EditCouponDto editCoupon);
    }
}
