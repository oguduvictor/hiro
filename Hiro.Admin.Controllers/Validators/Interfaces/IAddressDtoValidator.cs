﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IAddressDtoValidator
    {
        List<OperationError> Validate(AddressDto address);
    }
}
