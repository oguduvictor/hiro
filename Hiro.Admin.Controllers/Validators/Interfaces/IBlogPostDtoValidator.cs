﻿using System.Collections.Generic;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IBlogPostDtoValidator
    {
        List<OperationError> Validate(BlogPostDto blogPost);
    }
}
