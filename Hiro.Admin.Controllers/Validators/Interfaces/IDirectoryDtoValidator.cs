﻿using System.Collections.Generic;
using Hiro.Shared.Dtos;

namespace Hiro.Admin.Controllers.Validators.Interfaces
{
    public interface IDirectoryDtoValidator
    {
        List<OperationError> Validate(MediaDirectory mediaDirectory);
    }
}
