﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class AddressDtoValidator : HiroValidator, IAddressDtoValidator
    {
        public List<OperationError> Validate(AddressDto address)
        {
            this.Must(address.FirstName?.Length >= 1 && address.FirstName?.Length <= 100, "The length of first name should be between 1 and 100.");
            this.Must(address.LastName?.Length >= 1 && address.LastName?.Length <= 100, "The length of last name should be between 1 and 100.");
            this.Must(address.AddressLine1?.Length >= 1 && address.AddressLine1?.Length <= 100, "The length of address line1 should be between 1 and 100.");
            this.Must(address.City?.Length >= 1 && address.City?.Length <= 100, "The length of city should be between 1 and 100.");
            this.Must(address.StateCountyProvince?.Length >= 1 && address.StateCountyProvince?.Length <= 100, "The length of state county province should be between 1 and 100.");
            this.Must(address.Postcode?.Length >= 1 && address.Postcode?.Length <= 100, "The length of post code should be between 1 and 100.");
            this.Must(address.PhoneNumber?.Length >= 1 && address.PhoneNumber?.Length <= 100, "The length of phone Number should be between 1 and 100.");
            this.Must(address.Country != null, "Country must not be null");

            return this.Errors;
        }
    }
}
