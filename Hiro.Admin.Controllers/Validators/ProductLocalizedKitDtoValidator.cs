﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ProductLocalizedKitDtoValidator : HiroValidator, IProductLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(ProductLocalizedKitDto productLocalizedKitDto)
        {
            this.Must(productLocalizedKitDto.Country != null, "Country must not be null.");
            this.Must(productLocalizedKitDto.Product != null, "Product must not be null.");

            if (productLocalizedKitDto.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(productLocalizedKitDto.Title, "Title must be set for default localized kit country.");
            }

            return this.Errors;
        }
    }
}
