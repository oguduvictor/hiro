﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class UrlValidator : HiroValidator, IUrlValidator
    {
        public List<OperationError> Validate(string url, IEnumerable<string> conflictingUrls = null)
        {
            this.CascadeMode = true;

            this.MustNotBeEmpty(url, "Please insert a valid Url.");
            this.Must(url.Length >= 1 && url.Length <= 100, "The length of the Url must be between 1 and 100 characters.");
            this.Must(!url.Contains(" "), "The Url cannot contain empty spaces. Please use dashed instead.");
            this.Must(Regex.IsMatch(url, "^[a-z0-9-]*$"), "The Url can only contain lowercase letters, numbers and dashes.");
            this.Must(!conflictingUrls?.Contains(url) ?? true, $"The Url '{url}' is already in use.");

            return this.Errors;
        }
    }
}
