﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class AttributeOptionLocalizedKitDtoValidator : HiroValidator, IAttributeOptionLocalizedKitDtoValidator
    {
        public List<OperationError> Validate(AttributeOptionLocalizedKitDto attributeOptionLocalizedKit)
        {
            this.Must(attributeOptionLocalizedKit.Country != null, "Country cannot be null.");

            if (attributeOptionLocalizedKit.Country?.IsDefault == true)
            {
                this.MustNotBeEmpty(attributeOptionLocalizedKit.Title,
                    "Title must be set for default attribute option localized kit country.");
            }

            return this.Errors;
        }
    }
}
