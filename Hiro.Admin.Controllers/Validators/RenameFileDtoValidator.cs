﻿using System.Collections.Generic;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Shared.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class RenameFileDtoValidator : HiroValidator, IRenameFileDtoValidator
    {
        public List<OperationError> Validate(RenameFileDto renameFileDto)
        {
            char[] disAllowedCharacters = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };

            this.MustNotBeEmpty(renameFileDto.LocalPath, "LocalPath can not be empty");
            this.MustNotBeEmpty(renameFileDto.NewName, "New name can not be empty");

            if (!string.IsNullOrEmpty(renameFileDto.NewName))
            {
                this.Must(!renameFileDto.NewName.ContainsAny(disAllowedCharacters), "'New Name' cannot contain any of the following characters \\ / : * ? \" < > | ");
            }

            return this.Errors;
        }
    }
}
