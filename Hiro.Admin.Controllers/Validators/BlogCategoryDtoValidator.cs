﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Hiro.Shared.Extensions;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class BlogCategoryDtoValidator : HiroValidator, IBlogCategoryDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly IBlogCategoryLocalizedKitDtoValidator blogCategoryLocalizedKitDtoValidator;
        private readonly INameValidator nameValidator;
        private readonly IUrlValidator urlValidator;

        public BlogCategoryDtoValidator(
            HiroDbContext hiroDbContext,
            IBlogCategoryLocalizedKitDtoValidator blogCategoryLocalizedKitDtoValidator,
            INameValidator nameValidator,
            IUrlValidator urlValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.blogCategoryLocalizedKitDtoValidator = blogCategoryLocalizedKitDtoValidator;
            this.nameValidator = nameValidator;
            this.urlValidator = urlValidator;
        }

        public List<OperationError> Validate(BlogCategoryDto blogCategory)
        {
            this.Errors.AddRange(this.nameValidator.Validate(
                blogCategory.Name,
                this.hiroDbContext.BlogCategories.Where(x => x.Name == blogCategory.Name && x.Id != blogCategory.Id).Select(y => y.Name).ToList()));

            this.Errors.AddRange(this.urlValidator.Validate(blogCategory.Url));

            this.Must(
                !blogCategory.BlogCategoryLocalizedKits.Select(lk => lk.Country?.Id).ContainsDuplicates(),
                ContentSectionNames.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue);

            foreach (var blogCategoryLocalizedKit in blogCategory.BlogCategoryLocalizedKits)
            {
                this.Errors.AddRange(this.blogCategoryLocalizedKitDtoValidator.Validate(blogCategoryLocalizedKit));
            }

            return this.Errors;
        }
    }
}
