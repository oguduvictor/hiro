﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class ContentSectionDtoValidator : HiroValidator, IContentSectionDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly IContentSectionLocalizedKitDtoValidator contentSectionLocalizedKitDtoValidator;
        private readonly INameValidator nameValidator;

        public ContentSectionDtoValidator(
            HiroDbContext hiroDbContext,
            IContentSectionLocalizedKitDtoValidator contentSectionLocalizedKitDtoValidator,
            INameValidator nameValidator)
        {
            this.contentSectionLocalizedKitDtoValidator = contentSectionLocalizedKitDtoValidator;
            this.nameValidator = nameValidator;
            this.hiroDbContext = hiroDbContext;
        }

        public List<OperationError> Validate(ContentSectionDto contentSection)
        {
            this.Errors.AddRange(this.nameValidator.Validate(
                contentSection.Name,
                this.hiroDbContext.ContentSections.Where(x => x.Name == contentSection.Name && x.Id != contentSection.Id).Select(x => x.Name).ToList()));

            foreach (var contentSectionLocalizedKit in contentSection.ContentSectionLocalizedKits)
            {
                this.Errors.AddRange(this.contentSectionLocalizedKitDtoValidator.Validate(contentSectionLocalizedKit));
            }

            return this.Errors;
        }
    }
}
