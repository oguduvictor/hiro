﻿using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.DataAccess;
using Hiro.Domain.Dtos;
using Hiro.Shared.Dtos;
using Hiro.Shared.Validation;

namespace Hiro.Admin.Controllers.Validators
{
    public class CountryDtoValidator : HiroValidator, ICountryDtoValidator
    {
        private readonly HiroDbContext hiroDbContext;
        private readonly INameValidator nameValidator;
        private readonly ITaxDtoValidator taxDtoValidator;

        public CountryDtoValidator(
            HiroDbContext hiroDbContext,
            INameValidator nameValidator,
            ITaxDtoValidator taxDtoValidator)
        {
            this.hiroDbContext = hiroDbContext;
            this.nameValidator = nameValidator;
            this.taxDtoValidator = taxDtoValidator;
        }

        public List<OperationError> Validate(CountryDto country)
        {
            this.Errors.AddRange(this.nameValidator.Validate(country.Name));

            this.MustNotBeEmpty(country.LanguageCode, "Language code must not be empty.");

            if (country.IsDefault)
            {
                this.Must(country.IsDisabled == false, "The default country cannot be disabled.");
            }

            this.Must(
                !this.hiroDbContext.Countries.Any(x => x.LanguageCode == country.LanguageCode && x.Id != country.Id),
                "The LanguageCode must be unique.");

            foreach (var tax in country.Taxes)
            {
                this.Errors.AddRange(this.taxDtoValidator.Validate(tax));
            }

            if (country.Localize && country.IsDisabled == false)
            {
                this.MustNotBeEmpty(country.PaymentAccountId, "PaymentAccount Id must be set for enabled localized countries");
            }

            return this.Errors;
        }
    }
}
