﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hiro.Admin.Controllers.Validators.Interfaces;
using Hiro.Admin.Server.Services.Interfaces;
using Hiro.Admin.Shared.Constants;
using Hiro.Admin.Shared.Dtos;
using Hiro.DataAccess;
using Hiro.DataAccess.Extensions;
using Hiro.Domain;
using Hiro.Domain.Dtos;
using Hiro.Domain.Extensions;
using Hiro.Services.Interfaces;
using Hiro.Shared.Constants;
using Hiro.Shared.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

#pragma warning disable 1591

namespace Hiro.Admin.Controllers
{
    [ApiController]
    [Authorize(Policy = nameof(UserRoleDto.AccessAdmin))]
    public class CategoryController : ControllerBase
    {
        private readonly Country localizationCountry;
        private readonly HiroDbContext dbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly ICategoryDtoValidator categoryDtoValidator;
        private readonly IAdminCategoryService adminCategoryService;
        private readonly IContentSectionService contentSectionService;

        public CategoryController(
            HiroDbContext dbContext,
            IAdminUtilityService adminUtilityService,
            IContentSectionService contentSectionService,
            ICategoryDtoValidator categoryDtoValidator,
            IAdminCategoryService adminCategoryService)
        {
            this.dbContext = dbContext;
            this.adminUtilityService = adminUtilityService;
            this.categoryDtoValidator = categoryDtoValidator;
            this.adminCategoryService = adminCategoryService;
            this.localizationCountry = dbContext.Countries.First(x => x.IsDefault);
            this.contentSectionService = contentSectionService;
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryIndex)]
        public virtual IActionResult IndexJson()
        {
            var categoryListDto = new CategoryListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Categories = this.adminCategoryService.GetRootCategories(this.dbContext).Select(this.GetRootCategoryDto).ToList()
            };

            return this.Ok(categoryListDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryUpdateHierarchy)]
        public virtual IActionResult UpdateHierarchy(List<CategoryDto> categories)
        {
            var operationResult = new OperationResult<List<CategoryDto>>();

            this.adminCategoryService.UpdateHierarchy(this.dbContext, categories);

            operationResult.Data = this.adminCategoryService.GetRootCategories(this.dbContext).Select(this.GetRootCategoryDto).ToList();

            return this.Ok(operationResult);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryEdit)]
        public virtual IActionResult EditJson(Guid? categoryId)
        {
            var isNew = categoryId == null;

            var category = isNew ? new Category() : this.dbContext.Categories.Find(categoryId);

            if (category == null)
            {
                throw new NullReferenceException();
            }

            category.CategoryLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(category.CategoryLocalizedKits, new CategoryLocalizedKit
            {
                Category = category
            });

            var products = (string.IsNullOrEmpty(category.ProductsSortOrder)
                ? category.Products.OrderBy(x => x.Url)
                : category.GetSortedProducts(this.localizationCountry, true)).Select(x => new ProductDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                }).ToList();

            var productImageKits = category.ProductImageKits.OrderBy(x => x.Product?.Url).Select(productImageKit => new ProductImageKitDto
            {
                Id = productImageKit.Id,
                VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    Variant = new VariantDto { SortOrder = variantOption.Variant?.SortOrder ?? 0 },
                    SortOrder = variantOption.SortOrder,
                    LocalizedTitle = variantOption.GetLocalizedTitle(this.localizationCountry)
                }).OrderBy(x => x.Variant?.SortOrder).ThenBy(x => x.SortOrder).ToList(),
                Product = productImageKit.Product == null ? null : new ProductDto
                {
                    LocalizedTitle = productImageKit.Product.GetLocalizedTitle(this.localizationCountry)
                }
            }).ToList();

            var productStockUnits = category.ProductStockUnits.OrderBy(x => x.Product?.Url).Select(stockUnit => new ProductStockUnitDto
            {
                Id = stockUnit.Id,
                VariantOptions = stockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    LocalizedTitle = variantOption.GetLocalizedTitle(this.localizationCountry)
                }).ToList(),
                Product = stockUnit.Product == null ? null : new ProductDto
                {
                    LocalizedTitle = stockUnit.Product.GetLocalizedTitle(this.localizationCountry)
                }
            }).ToList();

            var editCategoryDto = new EditCategoryDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(),
                Category = new CategoryDto
                {
                    IsNew = isNew,
                    Id = category.Id,
                    Url = category.Url,
                    SortOrder = category.SortOrder,
                    Name = category.Name,
                    TypeDescriptions = new List<string>
                    {
                        this.contentSectionService.GetOrSetLocalizedContent(
                            this.localizationCountry,
                            ContentSectionNames.Cms_CategoryTypesEnum_Products,
                            ContentSectionNames.Cms_CategoryTypesEnum_Products_FallbackValue),
                        this.contentSectionService.GetOrSetLocalizedContent(
                            this.localizationCountry,
                            ContentSectionNames.Cms_CategoryTypesEnum_ProductImageKits,
                            ContentSectionNames.Cms_CategoryTypesEnum_ProductImageKits_FallbackValue),
                        this.contentSectionService.GetOrSetLocalizedContent(
                            this.localizationCountry,
                            ContentSectionNames.Cms_CategoryTypesEnum_ProductStockUnits,
                            ContentSectionNames.Cms_CategoryTypesEnum_ProductStockUnits_FallbackValue)
                    },
                    Type = category.Type,
                    IsDisabled = category.IsDisabled,
                    LocalizedTitle = category.GetLocalizedTitle(this.localizationCountry),
                    Parent = category.Parent == null ? null : new CategoryDto
                    {
                        Id = category.Parent.Id,
                        LocalizedTitle = category.Parent.GetLocalizedTitle(this.localizationCountry)
                    },
                    Children = category.Children.Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                    }).OrderBy(x => x.Name).ToList(),
                    Tags = category.Tags,
                    CategoryLocalizedKits = category.CategoryLocalizedKits.Select(categoryLocalizedKit => new CategoryLocalizedKitDto
                    {
                        Id = categoryLocalizedKit.Id,
                        Category = categoryLocalizedKit.Category == null ? null : new CategoryDto(categoryLocalizedKit.Category.Id),
                        Country = categoryLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = categoryLocalizedKit.Country.Id,
                            Name = categoryLocalizedKit.Country.Name,
                            IsDefault = categoryLocalizedKit.Country.IsDefault
                        },
                        Title = categoryLocalizedKit.Title,
                        Description = categoryLocalizedKit.Description,
                        Image = categoryLocalizedKit.Image,
                        FeaturedTitle = categoryLocalizedKit.FeaturedTitle,
                        FeaturedDescription = categoryLocalizedKit.FeaturedDescription,
                        FeaturedImage = categoryLocalizedKit.FeaturedImage,
                        IsDisabled = categoryLocalizedKit.IsDisabled,
                        MetaDescription = categoryLocalizedKit.MetaDescription,
                        MetaTitle = categoryLocalizedKit.MetaTitle,
                    }).ToList(),
                    Products = products,
                    ProductImageKits = category.GetSortedProductImageKits(this.localizationCountry, true).Select(x => new ProductImageKitDto(x.Id)).ToList(),
                    ProductStockUnits = category.GetSortedProductStockUnits(this.localizationCountry, true).Select(x => new ProductStockUnitDto(x.Id)).ToList(),
                    ProductsSortOrder = category.ProductsSortOrder,
                    ProductImageKitsSortOrder = category.ProductImageKitsSortOrder,
                    ProductStockUnitsSortOrder = category.ProductStockUnitsSortOrder
                },
                AllTags = this.dbContext.Categories.OrderBy(x => x.SortOrder).AsEnumerable().SelectMany(x => x.GetTagList()).Distinct().ToList(),
                AllCategories = this.dbContext.Categories.Where(x => x.Id != category.Id).OrderBy(x => x.Name).AsEnumerable().Select(x => new CategoryDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
                }).ToList(),
                // --------------------------------------------------------------------------------------------------------------
                // initially load only the current items. The rest will be Lazy Loaded using Ajax from the React component:
                // --------------------------------------------------------------------------------------------------------------
                AllProducts = products,
                AllProductImageKits = productImageKits,
                AllProductStockUnits = productStockUnits
            };

            return this.Ok(editCategoryDto);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryGetAllProducts)]
        public virtual IActionResult GetAllProductsJson()
        {
            var productDtos = this.dbContext.Products.AsEnumerable().Select(x => new ProductDto
            {
                Id = x.Id,
                LocalizedTitle = x.GetLocalizedTitle(this.localizationCountry)
            }).OrderBy(x => x.LocalizedTitle).ToList();

            return this.Ok(productDtos);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryGetAllProductImageKits)]
        public virtual IActionResult GetAllProductImageKitsJson()
        {
            var productImageKitDtos = this.dbContext.ProductImageKits.AsEnumerable().Select(productImageKit => new ProductImageKitDto
            {
                Id = productImageKit.Id,
                VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    Variant = new VariantDto { SortOrder = variantOption.Variant?.SortOrder ?? 0 },
                    SortOrder = variantOption.SortOrder,
                    LocalizedTitle = variantOption.GetLocalizedTitle(this.localizationCountry)
                }).OrderBy(x => x.Variant?.SortOrder).ThenBy(x => x.SortOrder).ToList(),
                Product = productImageKit.Product == null ? null : new ProductDto
                {
                    LocalizedTitle = productImageKit.Product.GetLocalizedTitle(this.localizationCountry)
                }
            })
            .OrderBy(x => x.Product?.LocalizedTitle).ToList();

            return this.Ok(productImageKitDtos);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryGetAllProductStockUnits)]
        public virtual IActionResult GetAllProductStockUnitsJson()
        {
            var productStockUnitDtos = this.dbContext.ProductStockUnits.AsEnumerable().Select(stockUnit => new ProductStockUnitDto
            {
                Id = stockUnit.Id,
                VariantOptions = stockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    LocalizedTitle = variantOption.GetLocalizedTitle(this.localizationCountry)
                }).ToList(),
                Product = stockUnit.Product == null ? null : new ProductDto
                {
                    LocalizedTitle = stockUnit.Product.GetLocalizedTitle(this.localizationCountry)
                }
            }).OrderBy(x => x.Product?.LocalizedTitle).ToList();

            return this.Ok(productStockUnitDtos);
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryAddOrUpdate)]
        public virtual IActionResult AddOrUpdate(CategoryDto dto)
        {
            var errors = this.categoryDtoValidator.Validate(dto);

            if (errors.Any())
            {
                return this.Ok(new OperationResult(errors));
            }

            var category = this.adminCategoryService.AddOrUpdate(this.dbContext, dto);

            return this.Ok(new OperationResult<Guid>(errors, category.Id));
        }

        [HttpPost]
        [Route(HiroAdminApiRoutes.AdminCategoryDelete)]
        public virtual IActionResult Delete(Guid id)
        {
            var category = this.dbContext.Categories.Find(id);

            if (category == null)
            {
                throw new NullReferenceException();
            }

            this.dbContext.Categories.Delete(category.Id, true);

            return this.Ok(new OperationResult());
        }

        private CategoryDto GetRootCategoryDto(Category category)
        {
            return new CategoryDto
            {
                Id = category.Id,
                Name = category.Name,
                SortOrder = category.SortOrder,
                LocalizedTitle = category.GetLocalizedTitle(this.localizationCountry),
                Children = category.Children.Any() ? category.Children.Select(this.GetRootCategoryDto).ToList() : new List<CategoryDto>()
            };
        }
    }
}