﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Hiro.Shared.Dtos;

namespace Hiro.Shared.Validation
{
    public class HiroValidator
    {
        public bool CascadeMode { get; set; }

        public List<OperationError> Errors { get; set; } = new List<OperationError>();

        public void Must(bool condition, string error, string propertyName = null)
        {
            if (CascadeMode && this.Errors.Any())
            {
                return;
            }

            if (!condition)
            {
                this.Errors.Add(new OperationError(propertyName, error));
            }
        }

        public void MustBeValidEmailAddress(string value, string error, string propertyName = null)
        {
            try
            {
                MailAddress unused = new MailAddress(value);

                this.Must(true, error, propertyName);
            }
            catch
            {
                this.Must(false, error, propertyName);
            }
        }

        public void MustNotBeEmpty(string value, string error, string propertyName = null)
        {
            this.Must(!string.IsNullOrEmpty(value), error, propertyName);
        }
    }
}