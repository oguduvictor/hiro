﻿namespace Hiro.Services
{
    using System.Collections.Generic;
    using Hiro.Services.Interfaces;
    using JetBrains.Annotations;

    public class CacheStore : ICacheStore<CacheEntry>
    {
        [NotNull]
        public List<CacheEntry> Entries { get; set; } = new List<CacheEntry>();
    }
}