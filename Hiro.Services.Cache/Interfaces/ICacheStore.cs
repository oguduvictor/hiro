﻿namespace Hiro.Services.Interfaces
{
    using System.Collections.Generic;

    public interface ICacheStore<TEntry>
    {
        List<TEntry> Entries { get; set; }
    }
}