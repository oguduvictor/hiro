﻿namespace Hiro.Services.Interfaces
{
    using System;
    using System.Collections.Generic;

    public interface ICacheEntry
    {
        DateTime? ExpirationDate { get; set; }

        string Key { get; set; }

        /// <summary>
        /// Gets or sets the keys of dependent objects whose change should invelidate the entry.
        /// </summary>
        List<string> DependencyKeys { get; set; }

        object Data { get; set; }
    }
}