﻿namespace Hiro.Services
{
    using System;
    using System.Collections.Generic;
    using Hiro.Services.Interfaces;

    public class CacheEntry : ICacheEntry
    {
        public DateTime? ExpirationDate { get; set; }

        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the keys of dependent objects whose change should invelidate the entry.
        /// </summary>
        public List<string> DependencyKeys { get; set; } = new List<string>();

        public object Data { get; set; }
    }
}
