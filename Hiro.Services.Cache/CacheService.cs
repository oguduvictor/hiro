﻿namespace Hiro.Services
{
    using System;
    using System.Linq;
    using Hiro.Services.Interfaces;
    using Hiro.Shared.Extensions;
    using JetBrains.Annotations;

    public class CacheService : ICacheService
    {
        private readonly ICacheStore<CacheEntry> store;

        public CacheService(
            ICacheStore<CacheEntry> store)
        {
            this.store = store;
        }

        [CanBeNull]
        public T Get<T>(string key)
        {
            var entry = this.store.Entries.FirstOrDefault(x => x.Key.Equals(key));

            if (entry == null)
            {
                return default;
            }

            if (entry.ExpirationDate < DateTime.Now)
            {
                this.store.Entries.Remove(entry);

                return default;
            }

            return entry.Data.TryCast<T>();
        }
    }
}