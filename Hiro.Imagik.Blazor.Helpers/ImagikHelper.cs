﻿namespace Hiro.Imagik.Blazor.Helpers
{
    public static class ImagikHelper
    {
        public static string GetResizedJpeg(string url, int width, bool save = false)
        {
            return $"/imagik?url={url}&width={width}&save={save}";
        }
    }
}