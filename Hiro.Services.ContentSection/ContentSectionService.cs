﻿namespace Hiro.Services
{
    using System;
    using System.Linq;
    using Hiro.DataAccess;
    using Hiro.Domain;
    using Hiro.Services.Interfaces;
    using JetBrains.Annotations;
    using Newtonsoft.Json;

    public class ContentSectionService : IContentSectionService
    {
        private readonly HiroDbContext dbContext;

        public ContentSectionService(
            HiroDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [CanBeNull]
        public string GetLocalizedContent(Country localizationCountry, ProductAttribute productAttribute)
        {
            return productAttribute == null
                    ? string.Empty
                    : this.GetLocalizedContent(localizationCountry, productAttribute.ContentSectionValue?.Name);
        }

        [CanBeNull]
        public T GetLocalizedContentAs<T>(Country localizationCountry, ProductAttribute productAttribute)
        {
            var contentSectionJson = this.GetLocalizedContent(localizationCountry, productAttribute);

            if (string.IsNullOrEmpty(contentSectionJson))
            {
                return default;
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(contentSectionJson);
            }
            catch (Exception)
            {
                return default;
            }
        }

        [CanBeNull]
        public string GetLocalizedContent(Country localizationCountry, string contentSectionName, string fallbackValue = null)
        {
            var content = this.dbContext.ContentSectionLocalizedKits
                   .Where(x => !x.IsDisabled && x.ContentSection.Name == contentSectionName && x.Country.Id == localizationCountry.Id)
                   .Select(x => x.Content)
                   .FirstOrDefault();

            if (string.IsNullOrEmpty(content))
            {
                content = this.dbContext.ContentSectionLocalizedKits
                    .Where(x => !x.IsDisabled && x.ContentSection.Name == contentSectionName && x.Country.IsDefault)
                    .Select(x => x.Content)
                    .FirstOrDefault();
            }

            return string.IsNullOrEmpty(content) ? fallbackValue : content;
        }

        [CanBeNull]
        public string GetOrSetLocalizedContent(Country localizationCountry, string contentSectionName, string fallbackValue)
        {
            fallbackValue ??= string.Empty;

            var content = this.GetLocalizedContent(localizationCountry, contentSectionName);

            if (string.IsNullOrEmpty(content))
            {
                var contentSectionExists = this.dbContext.ContentSections.Any(x => x.Name == contentSectionName);

                if (contentSectionExists)
                {
                    return fallbackValue;
                }

                var contentSection = new ContentSection();
                var contentSectionLocalizedKit = new ContentSectionLocalizedKit();

                contentSection.Name = contentSectionName;
                contentSection.Schema = fallbackValue.StartsWith("{") && fallbackValue.EndsWith("}") ? contentSectionName : null;
                contentSection.ContentSectionLocalizedKits.Add(contentSectionLocalizedKit);

                contentSectionLocalizedKit.ContentSection = contentSection;
                contentSectionLocalizedKit.Content = fallbackValue;
                contentSectionLocalizedKit.Country = this.dbContext.Countries.FirstOrDefault(x => x.IsDefault);

                this.dbContext.ContentSections.Add(contentSection);

                this.dbContext.SaveChanges();

                return fallbackValue;
            }

            return content;
        }

        [CanBeNull]
        public T GetLocalizedContentAs<T>(Country localizationCountry, string contentSectionName, string fallbackValue = null)
        {
            var contentSectionJson = this.GetLocalizedContent(localizationCountry, contentSectionName, fallbackValue);

            if (string.IsNullOrEmpty(contentSectionJson))
            {
                return default;
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(contentSectionJson);
            }
            catch (Exception)
            {
                return default;
            }
        }

        [CanBeNull]
        public T GetOrSetLocalizedContentAs<T>(Country localizationCountry, string contentSectionName, string fallbackValue)
        {
            var contentSectionJson = this.GetOrSetLocalizedContent(localizationCountry, contentSectionName, fallbackValue);

            if (string.IsNullOrEmpty(contentSectionJson))
            {
                return default;
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(contentSectionJson);
            }
            catch (Exception)
            {
                return default;
            }
        }
    }
}
