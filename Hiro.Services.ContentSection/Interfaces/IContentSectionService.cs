﻿namespace Hiro.Services.Interfaces
{
    using Hiro.Domain;
    using JetBrains.Annotations;

    public interface IContentSectionService
    {
        [CanBeNull]
        string GetLocalizedContent(Country localizationCountry, ProductAttribute productAttribute);

        [CanBeNull]
        T GetLocalizedContentAs<T>(Country localizationCountry, ProductAttribute productAttribute);

        [CanBeNull]
        string GetLocalizedContent(Country localizationCountry, string contentSectionName, string fallbackValue = null);

        [CanBeNull]
        T GetLocalizedContentAs<T>(Country localizationCountry, string contentSectionName, string fallbackValue = null);

        [NotNull]
        string GetOrSetLocalizedContent(Country localizationCountry, string contentSectionName, string fallbackValue);

        [NotNull]
        T GetOrSetLocalizedContentAs<T>(Country localizationCountry, string contentSectionName, string fallbackValue);
    }
}