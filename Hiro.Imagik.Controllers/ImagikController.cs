﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Hiro.Services.Interfaces;
using Hiro.Services.Options;
using Hiro.Shared.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Hiro.Imagik.Controllers
{
    [ApiController]
    public class ImagikController : ControllerBase
    {
        private readonly AzureMediaOptions azureMediaOptions;
        private readonly IBitmapService bitmapService;
        private readonly IMediaService mediaService;

        public ImagikController(
            IOptionsMonitor<AzureMediaOptions> optionsAccessor,
            IBitmapService bitmapService,
            IMediaService mediaService)
        {
            this.azureMediaOptions = optionsAccessor.CurrentValue;
            this.bitmapService = bitmapService;
            this.mediaService = mediaService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url">The absolute url of the original image that needs to be compressed.</param>
        /// <param name="width"></param>
        /// <param name="save"></param>
        /// <returns></returns>
        [Route("/imagik")]
        public async Task<IActionResult> Index(string url, int width, bool save = false)
        {
            if (string.IsNullOrEmpty(url))
            {
                return this.BadRequest();
            }

            if (width < 1 || width > 3000)
            {
                return this.BadRequest();
            }

            var uri = new Uri(url);
            var localPath = uri.LocalPath;
            var fileNameAndExtension = localPath.Split('/').Last();
            var fileName = fileNameAndExtension.SubstringUpToFirst(".");
            var compressedFileQuery = uri.Query
                .Replace("?", "-")
                .Replace("&", "-")
                .Replace("=", "-");
            var compressedFileNameAndExtension = $"{fileName}{compressedFileQuery}-{width}px.jpg";
            var compressedFileUrl = url
                .Replace($"/{this.azureMediaOptions.AzureStorageContainer}", "/imagik")
                .Replace(fileNameAndExtension, compressedFileNameAndExtension)
                .SubstringUpToFirst("?");

            // -------------------------------------------------------------
            // Check first if a compressed image already exists.
            // If a compressed images doesn't exist yet, create it.
            // -------------------------------------------------------------

            try
            {
                var request = WebRequest.Create(new Uri(compressedFileUrl));

                request.Method = "HEAD";

                request.GetResponse().Close();

                return this.Redirect(compressedFileUrl);
            }
            catch (Exception)
            {
                // -----------------------------------------------
                // Handle 404 file nor found exception
                // -----------------------------------------------

                MemoryStream sourceFileMemoryStream;

                try
                {
                    var bytes = await new WebClient().DownloadDataTaskAsync(url);

                    sourceFileMemoryStream = new MemoryStream(bytes);
                }
                catch (Exception)
                {
                    return this.NotFound();
                }

                // -----------------------------------------------
                // Generate compressed file
                // -----------------------------------------------

                var compressedFileMemoryStream = this.bitmapService.ResizeImage(sourceFileMemoryStream, width, 80);

                if (save)
                {
                    var imagikDirectoryLocalPath = compressedFileUrl
                        .Replace(this.azureMediaOptions.MediaEndpoint, "")
                        .Replace(compressedFileNameAndExtension, "")
                        .Replace($"/{this.azureMediaOptions.AzureStorageContainer}", "/imagik");

                    this.mediaService.UploadFile(imagikDirectoryLocalPath, compressedFileMemoryStream, compressedFileNameAndExtension);
                }

                return this.File(compressedFileMemoryStream, "image/jpeg");
            }
        }
    }
}
