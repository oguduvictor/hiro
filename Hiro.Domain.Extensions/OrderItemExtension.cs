﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Linq;
    using JetBrains.Annotations;

    public static class OrderItemExtension
    {
        [CanBeNull]
        public static OrderItemVariant GetOrderItemVariantByName(this OrderItem orderItem, string variantName)
        {
            return orderItem.OrderItemVariants.FirstOrDefault(x => x.VariantName == variantName);
        }
    }
}