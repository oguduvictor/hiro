﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.Domain.Interfaces;
    using Hiro.Shared.Extensions;

    public static class IEnumerableExtension
    {
        /// <summary>
        /// Check if a List of Entities contains a certain item, based on its Id.
        /// </summary>
        public static bool ContainsById<T>(this IEnumerable<T> items, T item) where T : BaseEntity
        {
            return items.Select(x => x.Id).Contains(item.Id);
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IEnumerable<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Any();
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IEnumerable<T> items, List<T> args) where T : BaseEntity
        {
            return ContainsAnyById(items, args.ToArray());
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IEnumerable<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Count() == args.Length;
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IEnumerable<T> items, List<T> args) where T : BaseEntity
        {
            return ContainsAllById(items, args.ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<T> OrderBySortOrder<T>(this IEnumerable<T> items) where T : ISortable
        {
            return items.OrderBy(x => x.SortOrder);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="products"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<Product> OrderByCategoryNames(this IEnumerable<Product> products, List<string> categoryNames)
        {
            var result = products.OrderByDescending(x => x.HasCategories(categoryNames.ElementAtOrDefault(0)));

            for (var i = 1; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                result = result.ThenByDescending(x => x.HasCategories(categoryName));
            }

            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKits"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<ProductImageKit> OrderByProductCategoryNames(this IEnumerable<ProductImageKit> productImageKits, List<string> categoryNames)
        {
            var result = productImageKits.OrderByDescending(x => x.Product.AsNotNull().HasCategories(categoryNames.ElementAtOrDefault(0)));

            for (var i = 1; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                result = result.ThenByDescending(x => x.Product.AsNotNull().HasCategories(categoryName));
            }

            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="idList"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<T> OrderByIds<T>(this IEnumerable<T> list, IReadOnlyList<Guid> idList) where T : BaseEntity
        {
            var result = list.OrderByDescending(x => x.Id == idList.ElementAtOrDefault(0));

            for (var i = 1; i < idList.Count; i++)
            {
                var id = idList.ElementAtOrDefault(i);

                result = result.ThenByDescending(x => x.Id == id);
            }

            return result;
        }
    }
}
