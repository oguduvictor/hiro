﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.Domain.Enums;
    using JetBrains.Annotations;

    public static class ProductExtension
    {
        [NotNull]
        public static string GetLocalizedTitle(this Product product, Country localizationCountry)
        {
            return
                product.ProductLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        [NotNull]
        public static string GetLocalizedDescription(this Product product, Country localizationCountry)
        {
            return
                product.ProductLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        [NotNull]
        public static string GetLocalizedMetaTitle(this Product product, Country localizationCountry)
        {
            return
                product.ProductLocalizedKits.GetByCountry(localizationCountry)?.MetaTitle ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.MetaTitle ??
                string.Empty;
        }

        [NotNull]
        public static string GetLocalizedMetaDescription(this Product product, Country localizationCountry)
        {
            return
                product.ProductLocalizedKits.GetByCountry(localizationCountry)?.MetaDescription ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.MetaDescription ??
                string.Empty;
        }

        public static bool GetLocalizedIsDisabled(this Product product, Country localizationCountry)
        {
            return product.ProductLocalizedKits.GetByCountry(localizationCountry)?.IsDisabled ?? false;
        }

        [CanBeNull]
        public static ProductAttribute GetProductAttributeByName(this Product product, string attributeName)
        {
            return product.ProductAttributes.FirstOrDefault(x => x.Attribute?.Name == attributeName);
        }

        [CanBeNull]
        public static ProductVariant GetProductVariantByName(this Product product, string variantName)
        {
            return product.ProductVariants.FirstOrDefault(x => x.Variant?.Name == variantName);
        }

        [NotNull]
        public static List<VariantOption> GetDefaultVariantOptions(this Product product)
        {
            return product.ProductVariants
                .Where(x => x.Variant?.Type == VariantTypesEnum.Options)
                .Select(x => x.DefaultVariantOptionValue ?? x.Variant.DefaultVariantOptionValue)
                .Where(x => x != null)
                .ToList();
        }

        [CanBeNull]
        public static ProductImageKit GetProductImageKit(this Product product, List<VariantOption> variantOptions = null)
        {
            variantOptions ??= GetDefaultVariantOptions(product);

            return product.ProductImageKits.FirstOrDefault(x => x.Matches(variantOptions));
        }

        [CanBeNull]
        public static ProductStockUnit GetProductStockUnit(this Product product, List<VariantOption> variantOptions = null)
        {
            variantOptions ??= GetDefaultVariantOptions(product);

            return product.ProductStockUnits.FirstOrDefault(x => x.Matches(variantOptions));
        }

        public static int GetStock(this Product product, List<VariantOption> selectedVariantOptions)
        {
            return GetProductStockUnit(product, selectedVariantOptions)?.Stock ?? 0;
        }

        public static bool HasCategories(this Product product, params string[] categoryNames)
        {
            return categoryNames.Intersect(product.Categories.Select(x => x.Name)).Count() == categoryNames.Length;
        }

        public static bool HasCategories(this Product product, params Category[] categories)
        {
            return HasCategories(product, categories.Select(x => x.Name).ToArray());
        }
    }
}