﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    public static class MailingListExtension
    {
        /// <summary>
        ///  Return the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this MailingList mailingList, Country localizationCountry)
        {
            return mailingList.MailingListLocalizedKits.GetByCountry(localizationCountry)?.IsDisabled ?? false;
        }

        /// <summary>
        ///  Return the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this MailingList mailingList, Country localizationCountry)
        {
            return
                mailingList.MailingListLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                mailingList.MailingListLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <summary>
        ///  Return the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this MailingList mailingList, Country localizationCountry)
        {
            return
                mailingList.MailingListLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                mailingList.MailingListLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }
    }
}
