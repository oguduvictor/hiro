﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class EmailExtension
    {
        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public static string GetLocalizedFrom(this Email email, Country localizationCountry)
        {
            return
                email.EmailLocalizedKits.GetByCountry(localizationCountry)?.From ??
                email.EmailLocalizedKits.GetByDefaultCountry()?.From ??
                string.Empty;
        }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public static string GetLocalizedDisplayName(this Email email, Country localizationCountry)
        {
            return
                email.EmailLocalizedKits.GetByCountry(localizationCountry)?.DisplayName ??
                email.EmailLocalizedKits.GetByDefaultCountry()?.DisplayName ??
                string.Empty;
        }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public static string GetLocalizedReplyTo(this Email email, Country localizationCountry)
        {
            return
                email.EmailLocalizedKits.GetByCountry(localizationCountry)?.ReplyTo ??
                email.EmailLocalizedKits.GetByDefaultCountry()?.ReplyTo ??
                string.Empty;
        }
    }
}
