﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Hiro.Domain.Interfaces;
    using JetBrains.Annotations;

    public static class IListExtension
    {
        /// <summary>
        /// Returns the LocalizedKit associated to a specific Country.
        /// </summary>
        [CanBeNull]
        public static T GetByCountry<T>(this IList<T> localizedKits, Country localizationCountry) where T : BaseEntity, ILocalizedKit
        {
            return localizationCountry == null ?
                GetByDefaultCountry(localizedKits) :
                localizedKits.FirstOrDefault(x => x.Country?.Id == localizationCountry.Id);
        }

        [CanBeNull]
        public static T GetByDefaultCountry<T>(this IList<T> localizedKits) where T : BaseEntity, ILocalizedKit
        {
            return localizedKits.FirstOrDefault(x => x.Country != null && x.Country.IsDefault);
        }

        public static List<T> OrderByDefaultCountry<T>(this IList<T> localizedKits) where T : ILocalizedKit
        {
            return localizedKits.OrderByDescending(x => x.Country?.IsDefault).ThenBy(x => x.Country?.Name).ToList();
        }

        public static List<T> OrderByCurrentCountry<T>(this IList<T> localizedKits) where T : ILocalizedKit
        {
            return localizedKits.OrderByDescending(x => x.Country?.LanguageCode == Thread.CurrentThread.CurrentCulture.Name).ThenBy(x => x.Country.Name).ToList();
        }

        public static IOrderedEnumerable<T> OrderBySortOrder<T>(this IList<T> list) where T : ISortable
        {
            return list.OrderBy(x => x.SortOrder);
        }

        /// <summary>
        /// Check if a List of Entities contains a certain item, based on its Id.
        /// </summary>
        public static bool ContainsById<T>(this IList<T> items, T item) where T : BaseEntity
        {
            return items.Select(x => x.Id).Contains(item.Id);
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IList<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Count() == args.Length;
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IList<T> items, List<T> args) where T : BaseEntity
        {
            return ContainsAllById(items, args.ToArray());
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IList<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Any();
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IList<T> items, List<T> args) where T : BaseEntity
        {
            return ContainsAnyById(items, args.ToArray());
        }
    }
}