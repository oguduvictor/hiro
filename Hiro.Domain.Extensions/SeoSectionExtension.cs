﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    public static class SeoSectionExtension
    {
        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this SeoSection seoSection, Country localizationCountry)
        {
            return
                seoSection.SeoSectionLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                seoSection.SeoSectionLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this SeoSection seoSection, Country localizationCountry)
        {
            return
                seoSection.SeoSectionLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                seoSection.SeoSectionLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Image or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedImage(this SeoSection seoSection, Country localizationCountry)
        {
            return
                seoSection.SeoSectionLocalizedKits.GetByCountry(localizationCountry)?.Image ??
                seoSection.SeoSectionLocalizedKits.GetByDefaultCountry()?.Image ??
                string.Empty;
        }
    }
}
