﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    public static class BlogCategoryExtension
    {
        public static string GetLocalizedTitle(this BlogCategory blogCategory, Country localizationCountry)
        {
            return
                blogCategory.BlogCategoryLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                blogCategory.BlogCategoryLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        public static string GetLocalizedDescription(this BlogCategory blogCategory, Country localizationCountry)
        {
            return
                blogCategory.BlogCategoryLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                blogCategory.BlogCategoryLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }
    }
}