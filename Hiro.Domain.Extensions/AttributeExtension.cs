﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    public static class AttributeExtension
    {
        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Attribute attribute, Country localizationCountry)
        {
            return
                attribute.AttributeLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                attribute.AttributeLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }
    }
}