﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public static class ProductImageKitExtension
    {
        public static bool HasCategories(this ProductImageKit productImageKit, params string[] categoryNames)
        {
            return categoryNames.Intersect(productImageKit.Categories.Select(x => x.Name)).Count() == categoryNames.Length;
        }

        public static bool HasCategories(this ProductImageKit productImageKit, params Category[] categories)
        {
            return HasCategories(productImageKit, categories.Select(x => x.Name).ToArray());
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this ProductImageKit productImageKit, string variantOptionName)
        {
            return productImageKit.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }

        /// <summary>
        /// Returns whether all the VariantOptions of the current productImageKit are
        /// included  in a superset of VariantOptions passed as parameter. Use this method
        /// to find which productImageKit matches the selected options of a CartItem.
        /// </summary>
        public static bool Matches(this ProductImageKit productImageKit, List<VariantOption> selectedVariantOptions)
        {
            return productImageKit.VariantOptions.All(x => selectedVariantOptions.Any(s => s.Id == x.Id));
        }

        [CanBeNull]
        public static ProductImage GetProductImageByName(this ProductImageKit productImageKit, string imageName)
        {
            return productImageKit.ProductImages.FirstOrDefault(x => x.Name == imageName);
        }
    }
}
