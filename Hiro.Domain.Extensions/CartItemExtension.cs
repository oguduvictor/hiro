﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.Shared.Extensions;
    using JetBrains.Annotations;

    public static class CartItemExtension
    {
        /// <summary>
        /// Gets the CartItemProductVariant by its Variant Name.
        /// </summary>
        [CanBeNull]
        public static CartItemVariant GetCartItemVariantByName(this CartItem cartItem, string variantName)
        {
            return cartItem.CartItemVariants.FirstOrDefault(x => x.Variant?.Name == variantName);
        }

        [CanBeNull]
        public static ProductAttribute GetProductAttributeByName(this CartItem cartItem, string attributeName)
        {
            return cartItem.Product?.GetProductAttributeByName(attributeName);
        }

        [CanBeNull]
        public static ProductVariant GetProductVariantByName(this CartItem cartItem, string variantName)
        {
            return cartItem.Product?.GetProductVariantByName(variantName);
        }

        public static List<VariantOption> GetSelectedVariantOptions(this CartItem cartItem)
        {
            return cartItem.CartItemVariants.Where(x => x.VariantOptionValue != null).Select(y => y.VariantOptionValue).ToList();
        }

        [CanBeNull]
        public static ProductImageKit GetProductImageKit(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            selectedVariantOptions ??= GetSelectedVariantOptions(cartItem);

            return cartItem.Product?.GetProductImageKit(selectedVariantOptions);
        }

        [CanBeNull]
        public static ProductStockUnit GetProductStockUnit(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            selectedVariantOptions ??= GetSelectedVariantOptions(cartItem);

            return cartItem.Product?.GetProductStockUnit(selectedVariantOptions);
        }

        public static int GetStock(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            selectedVariantOptions ??= GetSelectedVariantOptions(cartItem);

            return GetProductStockUnit(cartItem, selectedVariantOptions)?.Stock ?? 0;
        }

        public static DateTime GetMinEta(this CartItem cartItem, Country localizationCountry)
        {
            var productStockUnit = GetProductStockUnit(cartItem) ?? throw new Exception($"{cartItem.Product?.GetLocalizedTitle(localizationCountry)} ProductStockUnit cannot be null");

            var shippingBox = cartItem.CartShippingBox?.ShippingBox ?? throw new Exception("ShippingBox cannot be null");

            var startDate = productStockUnit.DispatchDate > DateTime.Now
                ? productStockUnit.DispatchDate
                : DateTime.Now;

            return shippingBox.GetLocalizedCountWeekends(localizationCountry)
                ? startDate.AddWorkingDays(productStockUnit.DispatchTime).AddDays(shippingBox.GetLocalizedMinDays(localizationCountry))
                : startDate.AddWorkingDays(productStockUnit.DispatchTime).AddWorkingDays(shippingBox.GetLocalizedMinDays(localizationCountry));
        }

        public static DateTime GetMaxEta(this CartItem cartItem, Country localizationCountry)
        {
            var productStockUnit = GetProductStockUnit(cartItem) ?? throw new Exception($"{cartItem.Product?.GetLocalizedTitle(localizationCountry)} ProductStockUnit cannot be null");

            var shippingBox = cartItem.CartShippingBox?.ShippingBox ?? throw new Exception("ShippingBox cannot be null");

            var startDate = productStockUnit.DispatchDate > DateTime.Now
                ? productStockUnit.DispatchDate
                : DateTime.Now;

            return shippingBox.GetLocalizedCountWeekends(localizationCountry)
                ? startDate.AddWorkingDays(productStockUnit.DispatchTime).AddDays(shippingBox.GetLocalizedMaxDays(localizationCountry))
                : startDate.AddWorkingDays(productStockUnit.DispatchTime).AddWorkingDays(shippingBox.GetLocalizedMaxDays(localizationCountry));
        }
    }
}