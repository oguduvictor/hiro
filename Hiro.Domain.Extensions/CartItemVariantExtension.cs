﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    public static class CartItemVariantExtension
    {
        /// <summary>
        /// Return the price modifier of the VariantOption that is currently selected.
        /// </summary>
        public static double GetPriceModifier(this CartItemVariant cartItemVariant, Country localizationCountry)
        {
            return cartItemVariant.VariantOptionValue?.GetLocalizedPriceModifier(localizationCountry) ?? 0;
        }
    }
}
