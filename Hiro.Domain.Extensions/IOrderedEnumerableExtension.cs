﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.Shared.Extensions;

    public static class IOrderedEnumerable
    {
        public static IOrderedEnumerable<Product> ThenByCategoryNames(this IOrderedEnumerable<Product> products, List<string> categoryNames)
        {
            for (var i = 0; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                products = products.ThenByDescending(x => x.HasCategories(categoryName));
            }

            return products;
        }

        public static IOrderedEnumerable<ProductImageKit> ThenByProductCategoryNames(this IOrderedEnumerable<ProductImageKit> productImageKits, List<string> categoryNames)
        {
            for (var i = 0; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                productImageKits = productImageKits.ThenByDescending(x => x.Product.AsNotNull().HasCategories(categoryName));
            }

            return productImageKits;
        }

        public static IOrderedEnumerable<T> ThenByIds<T>(this IOrderedEnumerable<T> list, string ids) where T : BaseEntity
        {
            if (string.IsNullOrEmpty(ids))
            {
                return list;
            }

            var idList = ids.Split(',').Select(x => new Guid(x.Trim())).ToList();

            for (var i = 0; i < idList.Count; i++)
            {
                var id = idList.ElementAtOrDefault(i);

                list = list.ThenByDescending(x => x.Id == id);
            }

            return list;
        }
    }
}
