﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public static class ProductStockUnitExtension
    {
        public static bool HasCategories(this ProductStockUnit productStockUnit, params string[] categoryNames)
        {
            return categoryNames.Intersect(productStockUnit.Categories.Select(x => x.Name)).Count() == categoryNames.Length;
        }

        public static bool HasCategories(this ProductStockUnit productStockUnit, params Category[] categories)
        {
            return HasCategories(productStockUnit, categories.Select(x => x.Name).ToArray());
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this ProductStockUnit productStockUnit, string variantOptionName)
        {
            return productStockUnit.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }

        /// <summary>
        /// Returns whether all the VariantOptions of the current ProductStockUnit are
        /// included  in a superset of VariantOptions passed as parameter. Use this method
        /// to find which ProductStockUnit matches the selected options of a CartItem.
        /// </summary>
        public static bool Matches(this ProductStockUnit productStockUnit, List<VariantOption> selectedVariantOptions)
        {
            return productStockUnit.VariantOptions.All(x => selectedVariantOptions.Any(s => s.Id == x.Id));
        }

        /// <summary>
        ///  Returns the localized BasePrice.
        /// </summary>
        public static double GetLocalizedBasePrice(this ProductStockUnit productStockUnit, Country localizationCountry)
        {
            return productStockUnit.ProductStockUnitLocalizedKits.GetByCountry(localizationCountry)?.BasePrice ?? 0;
        }

        /// <summary>
        /// Returns the localized SalePrice.
        /// </summary>
        public static double GetLocalizedSalePrice(this ProductStockUnit productStockUnit, Country localizationCountry)
        {
            return productStockUnit.ProductStockUnitLocalizedKits.GetByCountry(localizationCountry)?.SalePrice ?? 0;
        }

        /// <summary>
        /// Returns the localized MembershipSalePrice.
        /// </summary>
        public static double GetLocalizedMembershipSalePrice(this ProductStockUnit productStockUnit, Country localizationCountry)
        {
            return productStockUnit.ProductStockUnitLocalizedKits.GetByCountry(localizationCountry)?.MembershipSalePrice ?? 0;
        }
    }
}
