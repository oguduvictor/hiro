﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using Hiro.Domain.Interfaces;
    using Hiro.Shared.Extensions;

    public static class ITaggableExtension
    {
        public static List<string> GetTagList(this ITaggable iTaggable)
        {
            return string.IsNullOrEmpty(iTaggable.Tags)
                ? new List<string>()
                : $"{iTaggable.Tags},{iTaggable.AutoTags}".Split(',').ToList();
        }

        public static bool HasTags(this ITaggable iTaggable, params string[] tags)
        {
            return GetTagList(iTaggable).ContainsAll(tags);
        }
    }
}
