﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public static class CartExtension
    {
        [NotNull]
        public static List<CartItem> GetCartItems(this Cart cart)
        {
            return cart.CartShippingBoxes.SelectMany(x => x.CartItems).ToList();
        }

        [CanBeNull]
        public static CartShippingBox GetCartShippingBoxByName(this Cart cart, string shippingBoxName)
        {
            return cart.CartShippingBoxes.FirstOrDefault(x => x.ShippingBox?.Name == shippingBoxName);
        }
    }
}