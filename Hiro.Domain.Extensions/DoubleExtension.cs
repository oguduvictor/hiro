﻿namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;

    /// <summary>
    ///
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="netValue"></param>
        /// <param name="taxes"></param>
        /// <returns></returns>
        public static double AddTaxes(this double netValue, IEnumerable<Tax> taxes)
        {
            double totalTax = 0;

            foreach (var tax in taxes)
            {
                if (tax.Amount > 0)
                {
                    totalTax += tax.Amount.Value;
                }
                else
                {
                    totalTax += netValue * tax.Percentage / 100;
                }
            }

            return netValue + totalTax;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="grossValue"></param>
        /// <param name="taxes"></param>
        /// <returns></returns>
        public static double RemoveTaxes(this double grossValue, IEnumerable<Tax> taxes)
        {
            double totalTax = 0;

            foreach (var tax in taxes)
            {
                if (tax.Amount > 0)
                {
                    totalTax += tax.Amount.Value;
                }
                else
                {
                    totalTax += grossValue - grossValue * (100 / (100 + tax.Percentage));
                }
            }

            return grossValue - totalTax;
        }
    }
}
