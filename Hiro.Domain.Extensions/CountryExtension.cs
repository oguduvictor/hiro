﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System;
    using System.Globalization;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class CountryExtension
    {
        [CanBeNull]
        public static CultureInfo GetCultureInfo(this Country country)
        {
            try
            {
                return CultureInfo.CreateSpecificCulture(country.LanguageCode ?? string.Empty);
            }
            catch (Exception)
            {
                return null;
            }
        }

        [CanBeNull]
        public static RegionInfo GetRegionInfo(this Country country)
        {
            var cultureInfo = GetCultureInfo(country);

            if (cultureInfo == null)
            {
                return null;
            }

            try
            {
                return new RegionInfo(cultureInfo.LCID);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Return the currency symbol ($, £...)
        /// </summary>
        [CanBeNull]
        public static string GetCurrencySymbol(this Country country)
        {
            return GetCultureInfo(country)?.NumberFormat.CurrencySymbol;
        }

        /// <summary>
        /// Return the ISO4217 Currency Symbol (USD, GBP...)
        /// </summary>
        [CanBeNull]
        public static string GetIso4217CurrencySymbol(this Country country)
        {
            return GetRegionInfo(country)?.ISOCurrencySymbol;
        }

        /// <summary>
        /// Returns the Country Url preceded by the slash [/] character if the string is not empty.
        /// Useful for string concatenations like: Country.GetUrlForStringConcatenation() + "/controller/action".
        /// </summary>
        public static string GetUrlForStringConcatenation(this Country country)
        {
            return string.IsNullOrEmpty(country.Url) ? string.Empty : "/" + country.Url;
        }
    }
}