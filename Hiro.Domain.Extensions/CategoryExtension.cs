﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System;
    using System.Linq;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class CategoryExtension
    {
        /// <summary>
        ///  Return the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.IsDisabled ??
                false;
        }

        /// <summary>
        ///  Return the localized Name or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <summary>
        ///  Return the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <summary>
        ///  Return the localized Image or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedImage(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.Image ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.Image ??
                string.Empty;
        }

        /// <summary>
        /// Get the Category localized title when featured in a collection banner.
        /// </summary>
        [NotNull]
        public static string GetLocalizedFeaturedTitle(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.FeaturedTitle ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.FeaturedTitle ??
                string.Empty;
        }

        /// <summary>
        /// Get the Category localized description when featured in a collection banner.
        /// </summary>
        [NotNull]
        public static string GetLocalizedFeaturedDescription(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.FeaturedDescription ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.FeaturedDescription ??
                string.Empty;
        }

        /// <summary>
        /// Get the Category localized image when featured in a collection banner.
        /// </summary>
        public static string GetLocalizedFeaturedImage(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.FeaturedImage ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.FeaturedImage ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized MetaTitle or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedMetaTitle(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.MetaTitle ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.MetaTitle ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized MetaDescription or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedMetaDescription(this Category category, Country localizationCountry)
        {
            return
                category.CategoryLocalizedKits.GetByCountry(localizationCountry)?.MetaDescription ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.MetaDescription ??
                string.Empty;
        }

        /// <summary>
        /// Return the concatenation of the category url with the url of all its parents.
        /// </summary>
        public static string GetUrl(this Category category)
        {
            var url = category.Url;

            var parent = category.Parent;

            while (parent != null)
            {
                url = string.Join("/", parent.Url, url);

                parent = parent.Parent;
            }

            return url;
        }

        public static IOrderedEnumerable<Product> GetSortedProducts(this Category category, Country localizationCountry, bool includeDisabled)
        {
            var products = includeDisabled
                ? category.Products
                : category.Products.Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled(localizationCountry));

            if (string.IsNullOrEmpty(category.ProductsSortOrder))
            {
                return products.OrderBy(x => x.Url);
            }

            var idList = category.ProductsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

            return products.OrderByIds(idList);
        }

        public static IOrderedEnumerable<ProductImageKit> GetSortedProductImageKits(this Category category, Country localizationCountry, bool includeDisabled)
        {
            var productImageKits = includeDisabled
                ? category.ProductImageKits
                : category.ProductImageKits.Where(x => !x.IsDisabled && x.Product != null && !x.Product.IsDisabled && !x.Product.GetLocalizedIsDisabled(localizationCountry));

            if (string.IsNullOrEmpty(category.ProductImageKitsSortOrder))
            {
                return productImageKits.OrderBy(x => x.Product?.Url);
            }

            var idList = category.ProductImageKitsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

            return productImageKits.OrderByIds(idList);
        }

        public static IOrderedEnumerable<ProductStockUnit> GetSortedProductStockUnits(this Category category, Country localizationCountry, bool includeDisabled)
        {
            var productStockUnits = includeDisabled
                ? category.ProductStockUnits
                : category.ProductStockUnits.Where(x => !x.IsDisabled && x.Product != null && !x.Product.IsDisabled && !x.Product.GetLocalizedIsDisabled(localizationCountry));

            if (string.IsNullOrEmpty(category.ProductStockUnitsSortOrder))
            {
                return productStockUnits.OrderBy(x => x.Product?.Url);
            }

            var idList = category.ProductStockUnitsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

            return productStockUnits.OrderByIds(idList);
        }
    }
}