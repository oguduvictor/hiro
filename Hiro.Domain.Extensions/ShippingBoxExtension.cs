﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    public static class ShippingBoxExtension
    {
        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this ShippingBox shippingBox, Country localizationCountry)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this ShippingBox shippingBox, Country localizationCountry)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized InternalDescription or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedInternalDescription(this ShippingBox shippingBox, Country localizationCountry)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.InternalDescription ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.InternalDescription ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Price or the default country version.
        /// </summary>
        public static double GetLocalizedPrice(this ShippingBox shippingBox, Country localizationCountry)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.Price ?? 0;
        }

        public static int GetLocalizedMinDays(this ShippingBox shippingBox, Country localizationCountry)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.MinDays ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.MinDays ??
                0;
        }

        public static int GetLocalizedMaxDays(this ShippingBox shippingBox, Country localizationCountry)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.MaxDays ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.MaxDays ??
                0;
        }

        public static bool GetLocalizedCountWeekends(this ShippingBox shippingBox, Country localizationCountry)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.CountWeekends ?? false;
        }

        public static double GetLocalizedFreeShippingMinimumPrice(this ShippingBox shippingBox, Country localizationCountry)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.FreeShippingMinimumPrice ?? 0;
        }

        public static bool GetLocalizedIsDisabled(this ShippingBox shippingBox, Country localizationCountry)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByCountry(localizationCountry)?.IsDisabled ?? false;
        }
    }
}
