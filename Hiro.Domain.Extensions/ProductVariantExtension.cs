﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Linq;
    using JetBrains.Annotations;

    public static class ProductVariantExtension
    {
        /// <summary>
        /// This method tries to return the Product.DefaultVariantOptionValue. If null, it returns the Variant.DefaultVariantOptionValue.
        /// If even this last value is null, it returns Variant?.VariantOptions.OrderBy(x => x.SortOrder).FirstOrDefault().
        /// </summary>
        [CanBeNull]
        public static VariantOption GetDefaultVariantOptionValue(this ProductVariant productVariant)
        {
            return
                productVariant.DefaultVariantOptionValue ??
                productVariant.Variant?.DefaultVariantOptionValue ??
                productVariant.Variant?.VariantOptions.OrderBy(x => x.SortOrder).FirstOrDefault();
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this ProductVariant productVariant, string variantOptionName)
        {
            return productVariant.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }
    }
}