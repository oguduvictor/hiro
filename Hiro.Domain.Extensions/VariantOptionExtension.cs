﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class VariantOptionExtension
    {
        /// <summary>
        ///  Returns the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this VariantOption variantOption, Country localizationCountry)
        {
            return
                variantOption.VariantOptionLocalizedKits.GetByCountry(localizationCountry)?.IsDisabled ??
                false;
        }

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this VariantOption variantOption, Country localizationCountry)
        {
            return
                variantOption.VariantOptionLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                variantOption.VariantOptionLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this VariantOption variantOption, Country localizationCountry)
        {
            return
                variantOption.VariantOptionLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                variantOption.VariantOptionLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Title.
        /// </summary>
        public static double GetLocalizedPriceModifier(this VariantOption variantOption, Country localizationCountry)
        {
            return variantOption.VariantOptionLocalizedKits.GetByCountry(localizationCountry)?.PriceModifier ?? 0;
        }
    }
}