﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Linq;
    using JetBrains.Annotations;

    public static class VariantExtension
    {
        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Variant variant, Country localizationCountry)
        {
            return
                variant.VariantLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                variant.VariantLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this Variant variant, Country localizationCountry)
        {
            return
                variant.VariantLocalizedKits.GetByCountry(localizationCountry)?.Description ??
                variant.VariantLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this Variant variant, string variantOptionName)
        {
            return variant.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByUrl(this Variant variant, string variantOptionUrl)
        {
            return variant.VariantOptions.FirstOrDefault(x => x.Url == variantOptionUrl);
        }
    }
}
