﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public static class UserExtension
    {
        [NotNull]
        public static ICollection<UserCredit> GetLocalizedUserCredits(this User user, Country localizationCountry)
        {
            return
                user.UserLocalizedKits.GetByCountry(localizationCountry)?.UserCredits.OrderByDescending(x => x.CreatedDate).ToList() ??
                new List<UserCredit>();
        }

        /// <summary>
        /// Gets a UserAttribute by its Name.
        /// </summary>
        [CanBeNull]
        public static UserAttribute GetUserAttributeByName(this User user, string attributeName)
        {
            return user.UserAttributes.FirstOrDefault(x => x.Attribute?.Name == attributeName);
        }
    }
}