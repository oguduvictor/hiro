﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    public static class AttributeOptionExtension
    {
        /// <summary>
        ///  Returns the localized Text or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this AttributeOption attributeOption, Country localizationCountry)
        {
            return
                attributeOption.AttributeOptionLocalizedKits.GetByCountry(localizationCountry)?.Title ??
                attributeOption.AttributeOptionLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }
    }
}
