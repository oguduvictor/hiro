﻿#pragma warning disable 1591

namespace Hiro.Domain.Extensions
{
    using JetBrains.Annotations;

    public static class ContentSectionExtension
    {
        /// <summary>
        ///  Returns the localized Content or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedContent(this ContentSection contentSection, Country localizationCountry)
        {
            return
                contentSection.ContentSectionLocalizedKits.GetByCountry(localizationCountry)?.Content ??
                contentSection.ContentSectionLocalizedKits.GetByDefaultCountry()?.Content ??
                string.Empty;
        }
    }
}