﻿namespace Hiro.Api.Shared.Constants
{
    public static class HiroApiRoutes
    {
        public const string ApiAuthenticationGetAuthenticatedUser = "api/authentication/get-authenticated-user";
        public const string ApiAuthenticationGetUserAddresses = "api/authentication/get-user-addresses";
        public const string ApiAuthenticationGetUserCreditTotal = "api/authentication/get-user-credit-total";
        public const string ApiAuthenticationAuthenticateUser = "api/authentication/authenticate-user";
        public const string ApiAuthenticationPartiallyAuthenticateUser = "api/authentication/partially-authenticate-user";
        public const string ApiAuthenticationRegisterUser = "api/authentication/register-user";
        public const string ApiAuthenticationPartiallyRegisterUser = "api/authentication/partially-register-user";

        public const string ApiCartAddOrUpdateAddress = "api/cart/add-or-update-address";
        public const string ApiCartDeleteAddress = "api/cart/delete-address";
        public const string ApiCartGetCart = "api/cart/get-cart";
        public const string ApiCartGetCartItemFromProduct = "api/cart/get-cartitem-from-product";
        public const string ApiCartAddCartItemToCart = "api/cart/add-cartitem-to-cart";
        public const string ApiCartUpdateCartItem = "api/cart/update-cartitem";
        public const string ApiCartUpdateCartItemQuantity = "api/cart/update-cartitem-quantity";
        public const string ApiCartDeleteCartItem = "api/cart/delete-cartitem";
        public const string ApiCartApplyCoupon = "api/cart/apply-coupon";
        public const string ApiCartRemoveCoupon = "api/cart/remove-coupon";
        public const string ApiCartApplyUserCredit = "api/cart/apply-user-credit";
        public const string ApiCartRemoveCredit = "api/cart/remove-credit";
        public const string ApiCartSetCartShippingAddress = "api/cart/set-cart-shipping-address";
        public const string ApiCartSetCartBillingAddress = "api/cart/set-cart-billing-address";
        public const string ApiCartSetShippingBox = "api/cart/set-shipping-box";

        public const string ApiLogCreateLog = "api/log/create-log";

        public const string ApiContentSectionGetContentSections = "api/contentsection/get-contentsections";
        public const string ApiContentSectionGetContentSection = "api/contentsection/get-contentsection";

        public const string ApiCountryGetLocalizationCountries = "api/country/get-localization-countries";
        public const string ApiCountryGetCountries = "api/country/get-countries";
    }
}