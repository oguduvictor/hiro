﻿#pragma warning disable 1591

namespace Hiro.Shared.Constants
{
    public class UserRoleNames
    {
        public const string SystemAdmin = "System Administrator";
        public const string Admin = "Administrator";
        public const string Default = "User";
    }
}
