﻿namespace Hiro.Shared.Constants
{
    public static class HiroJwtRegisteredClaimNames
    {
        public const string UserId = "user_id";
        public const string AuthenticationType = "auth_type";
    }
}
