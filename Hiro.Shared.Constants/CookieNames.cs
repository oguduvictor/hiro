﻿#pragma warning disable 1591

namespace Hiro.Shared.Constants
{
    public class CookieNames
    {
        public const string AspAuth = ".ASPXAUTH";
        public const string PartiallyAuthenticatedUserId = "PartiallyAuthenticatedUserId";
        public const string LocalStorageCookieKey = "LocalStorageCookieKey";
    }
}
